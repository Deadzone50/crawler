#include "WFC.h"

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <set>

#include "Console.h"
#include "Debug.h"
#include "Globals.h"
#include "Random.h"
#include "TextureManager.h"
#include "XML.h"

std::string TextLog;

static std::vector<std::string> G_ModuleNames;

static void PrintPossibilities(const std::vector<std::vector<std::vector<WFCNode>>> &Nodes, int x, int y, int z)
{
	size_t Max = 0;
	for(uint32_t YY = 0; YY < Nodes[0].size(); ++YY)
	{
		for(uint32_t ZZ = 0; ZZ < Nodes.size(); ++ZZ)
		{
			for(uint32_t XX = 0; XX < Nodes[0][0].size(); ++XX)
			{
				if(Nodes[ZZ][YY][XX].ModulePossibilities.size() > Max)
				{
					Max = Nodes[ZZ][YY][XX].ModulePossibilities.size();
				}
			}
		}
	}
	std::set<int> ModulesUsed;
	std::string Text, Line1, Line2, Line3;
	Text.append("#############################\n");
	for(int YY = (int)Nodes[0].size() - 1; YY >= 0; --YY)
	{
		Text.append("Y:" + std::to_string(YY) + "\n");
		for(int i = 0; i < 3 * Nodes[0][0].size(); ++i)
		{
			Text.append("---------------");
		}
		Text.append("\n");
		for(uint32_t ZZ = 0; ZZ < Nodes.size(); ++ZZ)
		{
			Line1.clear();
			Line2.clear();
			Line3.clear();
			for(uint32_t XX = 0; XX < Nodes[0][0].size(); ++XX)
			{
				uint8_t CurrentCharNum = 0;
				if(Nodes[ZZ][YY][XX].ModulePossibilities.empty())
				{
					Line1.append("\33[35m#\33[0m");
				}
				else
				{
					for(int P : Nodes[ZZ][YY][XX].ModulePossibilities)
					{
						if(x == XX && y == YY && z == ZZ)
						{
							Line1.append("\33[31m");
							if(!Line2.empty()) Line2.append("\33[31m");
							if(!Line3.empty()) Line3.append("\33[31m");
						}
						auto P_str = std::to_string(P);
						auto new_str = std::string(2 - std::min((size_t)2, P_str.length()), '0') + P_str + ",";
						if(CurrentCharNum < 15) Line1.append(new_str);
						else if(CurrentCharNum < 30) Line2.append(new_str);
						else Line3.append(new_str);
						++CurrentCharNum;
						if(x == XX && y == YY && z == ZZ)
						{
							Line1.append("\33[0m");
							if(!Line2.empty()) Line2.append("\33[0m");
							if(!Line3.empty()) Line3.append("\33[0m");
						}
						ModulesUsed.insert(P);
					}
				}
				size_t Padding = Max - Nodes[ZZ][YY][XX].ModulePossibilities.size();
				for(int i = 0; i < Padding; ++i)
				{
					if(CurrentCharNum < 15) Line1.append("  ,");
					else if(CurrentCharNum < 30) Line2.append("  ,");
					else Line3.append("  ,");
					++CurrentCharNum;
				}
				if(!Line2.empty() && !Line3.empty())
				{
					while(Line3.size() < Line2.size())
					{
						Line3.append("  ,");
					}
				}
				Line1.append("|");
				if(!Line2.empty()) Line2.append("|");
				if(!Line3.empty()) Line3.append("|");
			}
			if(!Line1.empty())
			{
				Text.append(Line1);
				Text.append("\n");
			}
			if(!Line2.empty())
			{
				Text.append(Line2);
				Text.append("\n");
			}
			if(!Line3.empty())
			{
				Text.append(Line3);
				Text.append("\n");
			}
			Text.append("\n");
		}
	}
	std::string ModuleList;
	for(auto i : ModulesUsed)
	{
		auto P_str = std::to_string(i);
		auto new_str = std::string(2 - std::min((size_t)2, P_str.length()), '0') + P_str;
		ModuleList.append(new_str + ": " + G_ModuleNames[i] + "\n");
	}

	ModuleList.append(Text);
	std::cout << ModuleList;
}

void WFCMap::Setup(Vec3i Size)
{
	m_Print = false;

	//Size = {4,2,4};

	WFCNode Node;
	int i = 0;
	//All nodes are possible
	std::vector<int> TopPossibilities = {0};
	std::vector<int> BottomPossibilities = {0};
	for(const auto &M : m_Modules)
	{
		if(M.Name.find("Start", 0) != std::string::npos)
		{
			m_EntryExitRooms.push_back(i);	  //Save index of start rooms
		}
		for(const auto &S : m_topPossibilities)
		{
			if(M.Name.find(S, 0) != std::string::npos) TopPossibilities.push_back(i);
		}
		for(const auto &S : m_bottomPossibilities)
		{
			if(M.Name.find(S, 0) != std::string::npos) BottomPossibilities.push_back(i);
		}
		Node.ModulePossibilities.push_back(i);
		G_ModuleNames.push_back(M.Name);
		++i;
	}
	//Set grid size, add empty on all sides
	m_SizeX = Size.x + 2;
	m_SizeY = Size.y + 2;
	m_SizeZ = Size.z + 2;
	m_Nodes = std::vector<std::vector<std::vector<WFCNode>>>(
		m_SizeZ, std::vector<std::vector<WFCNode>>(m_SizeY, std::vector<WFCNode>(m_SizeX, Node)));
	auto t0 = std::chrono::high_resolution_clock::now();
	//Top and bottom only empy node possible
	for(uint32_t ZZ = 0; ZZ < m_SizeZ; ++ZZ)
	{
		for(uint32_t XX = 0; XX < m_SizeX; ++XX)
		{
			m_Nodes[ZZ][0][XX].ModulePossibilities = BottomPossibilities;
			m_Nodes[ZZ][m_SizeY - 1][XX].ModulePossibilities = TopPossibilities;
		}
	}
	for(uint32_t ZZ = 0; ZZ < m_SizeZ; ++ZZ)
	{
		for(uint32_t XX = 0; XX < m_SizeX; ++XX)
		{
			UpdatePossibilities(XX, 0, ZZ);
			UpdatePossibilities(XX, m_SizeY - 1, ZZ);
		}
	}
	auto t1 = std::chrono::high_resolution_clock::now();

	//Edges only empy node possible
	for(uint32_t YY = 0; YY < m_SizeY - 1; ++YY)
	{
		for(uint32_t XX = 0; XX < m_SizeX; ++XX)
		{
			m_Nodes[0][YY][XX].ModulePossibilities = {0};
			m_Nodes[m_SizeZ - 1][YY][XX].ModulePossibilities = {0};
		}
		for(uint32_t ZZ = 0; ZZ < m_SizeZ; ++ZZ)
		{
			m_Nodes[ZZ][YY][0].ModulePossibilities = {0};
			m_Nodes[ZZ][YY][m_SizeX - 1].ModulePossibilities = {0};
		}
	}
	for(uint32_t YY = 0; YY < m_SizeY - 1; ++YY)
	{
		for(uint32_t XX = 0; XX < m_SizeX; ++XX)
		{
			UpdatePossibilities(XX, YY, 0);
			UpdatePossibilities(XX, YY, m_SizeZ - 1);
		}
		for(uint32_t ZZ = 0; ZZ < m_SizeZ; ++ZZ)
		{
			UpdatePossibilities(0, YY, ZZ);
			UpdatePossibilities(m_SizeX - 1, YY, ZZ);
		}
	}
	m_InitialNodes = m_Nodes;
	auto t2 = std::chrono::high_resolution_clock::now();
	Log("Setup1: " + std::to_string(std::chrono::duration<float>(t1 - t0).count()));
	Log("Setup2: " + std::to_string(std::chrono::duration<float>(t2 - t1).count()));
}

void WFCMap::SetupDebug()
{
	const std::vector<std::string> RoomList = {"Rig_Floor", "Rig_Walkway1", "Rig_Corner1", "Rig_FloorWall", "Rig_FloorEnd"};
	std::vector<int> Poss;
	int FloorI = 0;
	int i = 0;
	for(const auto &M : m_Modules)
	{
		for(const auto &StartsWith : RoomList)
		{
			if(M.Name.find(StartsWith, 0) != std::string::npos)
			{
				Poss.push_back(i);
				break;
			}
		}
		if(M.Name.find("Rig_Floor", 0) != std::string::npos)
		{
			FloorI = i;
			break;
		}
		++i;
	}
	m_EntryExitRooms = {FloorI};

	for(uint32_t ZZ = 0; ZZ < m_SizeZ; ++ZZ)
	{
		for(uint32_t XX = 0; XX < m_SizeX; ++XX)
		{
			m_Nodes[ZZ][1][XX].ModulePossibilities = {FloorI};
		}
	}
	//Clear everything over floor level
	for(uint32_t YY = 2; YY < m_SizeY; ++YY)
	{
		for(uint32_t ZZ = 0; ZZ < m_SizeZ; ++ZZ)
		{
			for(uint32_t XX = 0; XX < m_SizeX; ++XX)
			{
				m_Nodes[ZZ][YY][XX].ModulePossibilities = {0};
			}
		}
	}
	for(uint32_t YY = 2; YY < m_SizeY; ++YY)
	{
		for(uint32_t ZZ = 0; ZZ < m_SizeZ; ++ZZ)
		{
			for(uint32_t XX = 0; XX < m_SizeX; ++XX)
			{
				UpdatePossibilities(XX, YY, ZZ);
			}
		}
	}
	m_InitialNodes = m_Nodes;
}

bool WFCMap::Generate(Vec3i Start)
{
	auto t0 = std::chrono::high_resolution_clock::now();
	//Start = {1,0,0};
	// offset the empty rows
	m_StartX = Start.x + 1;
	m_StartY = Start.y + 1;
	m_StartZ = Start.z + 1;

	uint32_t StartRoomI = GetRandomUI(0, (uint32_t)m_EntryExitRooms.size() - 1);
	int StartRoom = m_EntryExitRooms[StartRoomI];

	//StartRoom = std::find(G_ModuleNames.begin(), G_ModuleNames.end(), "Rig_Start_r0") - G_ModuleNames.begin();
	//m_Print = true;

	m_Nodes = m_InitialNodes;
	m_Nodes[m_StartZ][m_StartY][m_StartX].ModulePossibilities = {StartRoom};

	//int EndRoomI = GetRandomUI(0, m_StartRooms.size()-1);
	//int EndRoom = m_StartRooms[EndRoomI];
	//uint32_t EndX = GetRandomUI(1, m_SizeX-2);
	//uint32_t EndY = GetRandomUI(1, m_SizeY-2);
	//uint32_t EndZ = GetRandomUI(1, m_SizeZ-2);
	//m_Nodes[EndZ][EndY][EndX].ModulePossibilities = {EndRoom};

	auto t1 = std::chrono::high_resolution_clock::now();
	Log("Generate1: " + std::to_string(std::chrono::duration<float>(t1 - t0).count()));

	if(!UpdatePossibilities(m_StartX, m_StartY, m_StartZ))
	{
		return false;
	}
	auto t2 = std::chrono::high_resolution_clock::now();
	Log("Generate2: " + std::to_string(std::chrono::duration<float>(t2 - t1).count()));
	while(SelectNext())
	{
		if(!StepGeneration())
		{
			auto t3 = std::chrono::high_resolution_clock::now();
			Log("Generate3: " + std::to_string(std::chrono::duration<float>(t3 - t2).count()));
			return false;
		}
	}
	auto t3 = std::chrono::high_resolution_clock::now();
	Log("Generate3: " + std::to_string(std::chrono::duration<float>(t3 - t2).count()));
	return true;
}

bool WFCMap::SelectNext()
{
	uint32_t MinPos[3] = {0, 0, 0};
	uint64_t Min = _UI64_MAX;
	for(uint32_t Z = 1; Z < m_SizeZ - 1; ++Z)
	{
		for(uint32_t Y = 1; Y < m_SizeY - 1; ++Y)
		{
			for(uint32_t X = 1; X < m_SizeX - 1; ++X)
			{
				size_t s = m_Nodes[Z][Y][X].ModulePossibilities.size();
				if(1 < s && s < Min)
				{
					MinPos[0] = X;
					MinPos[1] = Y;
					MinPos[2] = Z;
					Min = s;
				}
				else if(s == Min && GetRandomUI(0, 1))	  //50% chance to pick a new one
				{
					MinPos[0] = X;
					MinPos[1] = Y;
					MinPos[2] = Z;
				}
			}
		}
	}
	m_CurrentX = MinPos[0];
	m_CurrentY = MinPos[1];
	m_CurrentZ = MinPos[2];

	Vec3f Col = (Min != _UI64_MAX) ? Vec3f{0, 100, 0} : Vec3f{0, 0, 100};
	DrawDebugCube(
		{m_CurrentX * ROOM_SIZE - ROOM_SIZE / 2 - 0.01f, m_CurrentY * ROOM_SIZE, m_CurrentZ * ROOM_SIZE - ROOM_SIZE / 2 - 0.01f},
		{m_CurrentX * ROOM_SIZE + ROOM_SIZE / 2 + 0.01f, m_CurrentY * ROOM_SIZE + ROOM_SIZE + 0.01f,
		 m_CurrentZ * ROOM_SIZE + ROOM_SIZE / 2 + 0.01f},
		Col);

	return Min != _UI64_MAX;
}

bool WFCMap::StepGeneration()
{
	WFCNode &CurrentNode = m_Nodes[m_CurrentZ][m_CurrentY][m_CurrentX];

	//remove start rooms if possible
	auto MP = CurrentNode.ModulePossibilities;
	if(GetRandomF(0, 1) > 0.3f)
	{
		for(auto P : CurrentNode.ModulePossibilities)
		{
			if(std::find(m_EntryExitRooms.begin(), m_EntryExitRooms.end(), P) == m_EntryExitRooms.end())
			{
				for(auto i : m_EntryExitRooms)
				{
					MP.erase(std::remove(MP.begin(), MP.end(), i), MP.end());
				}
				break;
			}
		}
	}

	uint32_t r = GetRandomUI(0, (uint32_t)MP.size() - 1);
	CurrentNode.ModulePossibilities = {MP[r]};
	return UpdatePossibilities(m_CurrentX, m_CurrentY, m_CurrentZ);
}

bool NeighbourCmp(const std::string &N1, const std::string &N2)
{
	if(N1.size() > 1 && (N1.back() == 'A' || N1.back() == 'B') && *(N1.end() - 2) == '_')	 //if non symmetric
	{
		return ((N1.size() == N2.size()) &&										   //same len
				(N2.substr(0, N2.size() - 2) == N1.substr(0, N1.size() - 2)) &&	   //same prefix
				(N2.back() != N1.back())										   //different suffix
		);
	}
	else
	{
		return N1 == N2;
	}
}

//Checks if the list contains atleast one match
bool NeighbourMatch(const std::set<std::string> &CP1, const std::string &CP2)
{
	for(const auto &P1 : CP1)
	{
		if(NeighbourCmp(P1, CP2))
		{
			return true;
		}
	}
	return false;
}

bool PrunePossibilities(const std::vector<WFCModule> &Modules, const WFCNode &CurrentNode,
						std::vector<int> &NeighbourModulePossibilities, uint8_t NIndexOfCurrent)
{
	std::set<std::string> NeighbourConnectionPosibilities;
	for(const auto &CMP : CurrentNode.ModulePossibilities)
	{
		NeighbourConnectionPosibilities.insert(Modules[CMP].NeightbourConnections[NIndexOfCurrent]);
	}
	bool Removed = false;
	auto it = NeighbourModulePossibilities.begin();
	while(it != NeighbourModulePossibilities.end())
	{
		if(NeighbourMatch(NeighbourConnectionPosibilities, Modules[*it].NeightbourConnections[(NIndexOfCurrent + 2) % 4]))
		{
			++it;
		}
		else
		{
			it = NeighbourModulePossibilities.erase(it);
			Removed = true;
		}
	}
	return Removed;
}

bool PrunePossibilitiesUpDown(const std::vector<WFCModule> &Modules, const WFCNode &CurrentNode,
							  std::vector<int> &ModulePossibilities, uint8_t I)
{
	std::set<std::string> CurrentNeighbourPos;
	for(const auto &CMP : CurrentNode.ModulePossibilities)
	{
		CurrentNeighbourPos.insert(Modules[CMP].UpDownConnections[I]);
	}
	bool Removed = false;
	auto it = ModulePossibilities.begin();
	while(it != ModulePossibilities.end())
	{
		if(NeighbourMatch(CurrentNeighbourPos, Modules[*it].UpDownConnections[(I + 1) % 2]))
		{
			++it;
		}
		else
		{
			it = ModulePossibilities.erase(it);
			Removed = true;
		}
	}
	return Removed;
}

bool WFCMap::UpdatePossibilities(int X, int Y, int Z)
{
	WFCNode &CurrentNode = m_Nodes[Z][Y][X];
	if(m_Print)
	{
		PrintPossibilities(m_Nodes, X, Y, Z);
	}
	if(CurrentNode.ModulePossibilities.empty())
	{
		Log("Level generation failed: No possibilities left");
		return false;
	}
	//DrawDebugCube({X*ROOM_SIZE-ROOM_SIZE/2, Y*ROOM_SIZE, Z*ROOM_SIZE-ROOM_SIZE/2}, {X*ROOM_SIZE+ROOM_SIZE/2,
	//Y*ROOM_SIZE+ROOM_SIZE, Z*ROOM_SIZE+ROOM_SIZE/2}, {100,100,100});

	if(Z + 1 < (int)m_SizeZ)	//back
	{
		auto &MP = m_Nodes[Z + 1][Y][X].ModulePossibilities;
		if(PrunePossibilities(m_Modules, CurrentNode, MP, 0))
			if(!UpdatePossibilities(X, Y, Z + 1)) return false;
	}
	if(X - 1 >= 0)	  //left
	{
		auto &MP = m_Nodes[Z][Y][X - 1].ModulePossibilities;
		if(PrunePossibilities(m_Modules, CurrentNode, MP, 1))
			if(!UpdatePossibilities(X - 1, Y, Z)) return false;
	}
	if(Z - 1 >= 0)	  //forward
	{
		auto &MP = m_Nodes[Z - 1][Y][X].ModulePossibilities;
		if(PrunePossibilities(m_Modules, CurrentNode, MP, 2))
			if(!UpdatePossibilities(X, Y, Z - 1)) return false;
	}
	if(X + 1 < (int)m_SizeX)	//right
	{
		auto &MP = m_Nodes[Z][Y][X + 1].ModulePossibilities;
		if(PrunePossibilities(m_Modules, CurrentNode, MP, 3))
			if(!UpdatePossibilities(X + 1, Y, Z)) return false;
	}

	if(Y + 1 < (int)m_SizeY)	//up
	{
		auto &MP = m_Nodes[Z][Y + 1][X].ModulePossibilities;
		if(PrunePossibilitiesUpDown(m_Modules, CurrentNode, MP, 0))
			if(!UpdatePossibilities(X, Y + 1, Z)) return false;
	}
	if(Y - 1 >= 0)	  //down
	{
		auto &MP = m_Nodes[Z][Y - 1][X].ModulePossibilities;
		if(PrunePossibilitiesUpDown(m_Modules, CurrentNode, MP, 1))
			if(!UpdatePossibilities(X, Y - 1, Z)) return false;
	}
	return true;
}

void WFCMap::DrawMap(Shader &S)
{
	Mat4f Model;
	for(uint32_t Z = 0; Z < m_SizeZ; ++Z)
	{
		for(uint32_t Y = 0; Y < m_SizeY; ++Y)
		{
			for(uint32_t X = 0; X < m_SizeX; ++X)
			{
				if(m_Nodes[Z][Y][X].ModulePossibilities.empty()) DebugBreak();
				if(m_Nodes[Z][Y][X].ModulePossibilities.size() == 1)
				{
					int ModuleIndex = m_Nodes[Z][Y][X].ModulePossibilities[0];
					if(ModuleIndex)
					{
						Model.SetPosition({X * ROOM_SIZE, Y * ROOM_SIZE, Z * ROOM_SIZE});
						S.SetMat4("Model", Model);
						m_Modules[ModuleIndex].Mesh.Draw(S);
					}
				}
			}
		}
	}
}

struct ConnectionCheck
{
	int From;
	Vec3i To;
	int R1OI, R2OI;
};

static bool IsSame(const Vec3f &V1, const Vec3f &V2)
{
	const float e = 0.2f;
	if(V1.x - e < V2.x && V2.x < V1.x + e)
		if(V1.y - e < V2.y && V2.y < V1.y + e)
			if(V1.z - e < V2.z && V2.z < V1.z + e) return true;
	return false;
}

//Connect openings of already exsisting rooms
static void ConnectOpening(size_t NewRoomIndex, MapObject &Map, const ConnectionCheck &CCout, bool Reverse)
{
	LevelRoom &Room1 = Map.Rooms[CCout.From];
	LevelRoom &Room2 = Map.Rooms[NewRoomIndex];
	if(Room1.IsDecoration() || Room2.IsDecoration())	//dont do anything for decorations
		return;
	Room1.UpdateNavZone(Room2, (int)NewRoomIndex);
	Room2.UpdateNavZone(Room1, CCout.From);
}

static int AddRoom(MapObject &Map, const ConnectionCheck &CC, const WFCModule &Module, Vec3f Pos)
{
	Map.Rooms.emplace_back(Module.Mesh, MeshMan.GetCollisionMeshesVector(Module.Name), Module.Type, Module.Decoration);
	LevelRoom &NewRoom = Map.Rooms.back();
	NewRoom.MovePos(Pos);
	if(CC.From != -1)
	{
		size_t NewRoomIndex = Map.Rooms.size() - 1;
		ConnectOpening(NewRoomIndex, Map, CC, false);
	}
	return (int)Map.Rooms.size() - 1;
}

MapObject WFCMap::CreateMap(uint32_t &ExitIndex)
{
	MapObject NewMap;
	std::vector<ConnectionCheck> ToCheck(1);
	ConnectionCheck &CC = ToCheck.back();
	CC.From = {-1};
	CC.To = {(int)m_StartX, (int)m_StartY, (int)m_StartZ};
	CC.R1OI = -1;
	CC.R2OI = -1;

	//Keeps track of next room to process for every node
	std::vector<std::vector<std::vector<int>>> NodeToRoomMap;
	NodeToRoomMap.resize(m_SizeZ, std::vector<std::vector<int>>(m_SizeY, std::vector<int>(m_SizeX, -1)));

	while(!ToCheck.empty())	   //Add connected rooms
	{
		ConnectionCheck CCin = ToCheck.back();
		ToCheck.pop_back();
		int x = CCin.To.x;
		int y = CCin.To.y;
		int z = CCin.To.z;

		if(!m_Nodes[z][y][x].ModulePossibilities.empty())
		{
			int ModuleIndex = m_Nodes[z][y][x].ModulePossibilities[0];
			if(ModuleIndex)
			{
				ConnectionCheck CCout = {};	   //Next connection
				Vec3f Pos = CCin.To * ROOM_SIZE;
				CCout.From = AddRoom(NewMap, CCin, m_Modules[ModuleIndex], Pos);
				NodeToRoomMap[z][y][x] = CCout.From;
				//Check all directions from current room
				if(z + 1 < (int)m_SizeZ)
				{
					if(m_Modules[ModuleIndex].NeightbourConnections[0] != "0")
					{
						CCout.To = {x, y, z + 1};
						CCout.R1OI = 0;
						CCout.R2OI = 2;
						int NextRoomIndex = NodeToRoomMap[z + 1][y][x];
						if(NextRoomIndex == -1) ToCheck.push_back(CCout);
						else	//room exsists
							ConnectOpening(NextRoomIndex, NewMap, CCout, true);
					}
				}
				if(z - 1 >= 0)
				{
					if(m_Modules[ModuleIndex].NeightbourConnections[2] != "0")
					{
						CCout.To = {x, y, z - 1};
						CCout.R1OI = 2;
						CCout.R2OI = 0;
						int NextRoomIndex = NodeToRoomMap[z - 1][y][x];
						if(NextRoomIndex == -1) ToCheck.push_back(CCout);
						else	//room exsists
							ConnectOpening(NextRoomIndex, NewMap, CCout, true);
					}
				}
				if(x + 1 < (int)m_SizeX)
				{
					if(m_Modules[ModuleIndex].NeightbourConnections[3] != "0")
					{
						CCout.To = {x + 1, y, z};
						CCout.R1OI = 3;
						CCout.R2OI = 1;
						int NextRoomIndex = NodeToRoomMap[z][y][x + 1];
						if(NextRoomIndex == -1) ToCheck.push_back(CCout);
						else	//room exsists
							ConnectOpening(NextRoomIndex, NewMap, CCout, true);
					}
				}
				if(x - 1 >= 0)
				{
					if(m_Modules[ModuleIndex].NeightbourConnections[1] != "0")
					{
						CCout.To = {x - 1, y, z};
						CCout.R1OI = 1;
						CCout.R2OI = 3;
						int NextRoomIndex = NodeToRoomMap[z][y][x - 1];
						if(NextRoomIndex == -1) ToCheck.push_back(CCout);
						else	//room exsists
							ConnectOpening(NextRoomIndex, NewMap, CCout, true);
					}
				}
				if(y + 1 < (int)m_SizeY)
				{
					if(m_Modules[ModuleIndex].UpDownConnections[0] != "0_s")
					{
						CCout.To = {x, y + 1, z};
						CCout.R1OI = 4;
						CCout.R2OI = 5;
						int NextRoomIndex = NodeToRoomMap[z][y + 1][x];
						if(NextRoomIndex == -1) ToCheck.push_back(CCout);
						else	//room exsists
							ConnectOpening(NextRoomIndex, NewMap, CCout, true);
					}
				}
				if(y - 1 >= 0)
				{
					if(m_Modules[ModuleIndex].UpDownConnections[1] != "0_s")
					{
						CCout.To = {x, y - 1, z};
						CCout.R1OI = 5;
						CCout.R2OI = 4;
						int NextRoomIndex = NodeToRoomMap[z][y - 1][x];
						if(NextRoomIndex == -1) ToCheck.push_back(CCout);
						else	//room exsists
							ConnectOpening(NextRoomIndex, NewMap, CCout, true);
					}
				}
			}
			m_Nodes[z][y][x].ModulePossibilities.clear();
		}
	}

	Vec3f StartPos = Vec3f{(float)m_StartX, (float)m_StartY, (float)m_StartZ} * ROOM_SIZE;
	bool Done = false;
	ExitIndex = 0;
	for(int i = 0; i < NewMap.Rooms.size(); ++i)	//Create exit point
	{
		if(Done) break;

		auto &R = NewMap.Rooms[i];
		int Type = R.GetItemType();
		if(std::find(m_EntryExitRooms.begin(), m_EntryExitRooms.end(), Type) == m_EntryExitRooms.end()) continue;

		AxisAlignedBoundingBox RoomAABB = R.GetBoundingVolume();
		Vec3f RoomCenter = {(RoomAABB.V1.x + RoomAABB.V2.x) / 2, RoomAABB.V1.y + 0.1f, (RoomAABB.V1.z + RoomAABB.V2.z) / 2};
		float Dist = LEN(RoomCenter - StartPos);
		if(Dist > m_SizeX * ROOM_SIZE / 2)
		{
			ExitIndex = i;
			Done = GetRandomF(0, 5) > 2;
		}
	}
	m_Minimap.Clear();
	for(auto &R : NewMap.Rooms)	   //Generate Level mesh
	{
		const Mesh &M = R.GetMesh();
		uint32_t StartSize = (uint32_t)NewMap.LevelMesh.Vertices.size();
		for(const auto &V : M.Vertices)
		{
			NewMap.LevelMesh.Vertices.push_back(V);
		}
		for(const auto &I : M.Indices)
		{
			NewMap.LevelMesh.Indices.push_back(I + StartSize);
		}

		m_Minimap.AddRoom(R);
	}
	NewMap.CreateOctree(NodeToRoomMap);
	NewMap.LevelMesh.Init();
	NewMap.LevelMesh.UpdateBuffers();
	NewMap.LevelMesh.SetTexture(TextureMan.Texture(126, 126, 126));
	return NewMap;
}

static bool Symmetric(const std::string &N0, const std::string &N1, const std::string &N2, const std::string &N3)
{
	return (N0 == N1 && N1 == N2 && N2 == N3);
}

static std::string RotateUpDown(const std::string &S, size_t i)
{
	size_t Pos = S.find('_');
	std::string First = S.substr(0, Pos);
	std::string Second = S.substr(Pos + 1);

	if(Second[0] == 's') return S;
	else
	{
		int Index = std::stoi(Second.substr(1));
		Index = (Index + i) % 4;
		return First + "_r" + std::to_string(Index);
	}
}

static std::vector<std::string> SplitText(std::string text, char delimiter)
{
	std::vector<std::string> result;

	size_t i = text.find(delimiter);
	if(i != std::string::npos)
	{
		while(i != std::string::npos)
		{
			std::string tmp = text.substr(0, i);
			result.push_back(tmp);
			text = text.substr(i + 1);	  //include delimiter
			i = text.find(delimiter);
		}
	}
	//add last part of text, or full text if no delimiters where found
	result.push_back(text);
	return result;
}

void WFCMap::GenerateWFCModules(MeshManager &meshMan, const Settings &set)
{
	XMLElement xmlRoot = ParseXML(set.WFCFile);

	m_topPossibilities = SplitText(xmlRoot.Get("Top").GetText(), ' ');
	m_bottomPossibilities = SplitText(xmlRoot.Get("Bottom").GetText(), ' ');

	XMLElement tiles = xmlRoot.Get("tiles");
	for(const auto &tile : tiles.Elements)
	{
		std::string meshName = tile.Get("mesh").GetText();
		std::string name = tile.Get("name").GetText();

		std::string neighbours[] = {tile.Get("N0").GetText(), tile.Get("N1").GetText(), tile.Get("N2").GetText(),
									tile.Get("N3").GetText()};

		std::string upDownConnections[] = {tile.Get("NUp").GetText(), tile.Get("NDown").GetText()};
		bool decoration = !tile.Get("Decoration").Tag.Name.empty();

		WFCModule module;
		if(name.empty()) name = meshName;

		module.Name = name;
		module.Decoration = decoration;
		module.NeightbourConnections[0] = neighbours[0];
		module.NeightbourConnections[1] = neighbours[1];
		module.NeightbourConnections[2] = neighbours[2];
		module.NeightbourConnections[3] = neighbours[3];

		module.UpDownConnections[0] = upDownConnections[0];
		module.UpDownConnections[1] = upDownConnections[1];

		if(meshName == "Empty")
		{
			m_Modules.push_back(module);
			m_Modules.back().Type = (int)m_Modules.size() - 1;
		}
		else
		{
			module.Mesh = *MeshMan.GetMesh(meshName);

			std::vector<Mesh> CM = MeshMan.CollectCollisionMeshes(meshName);
			if(Symmetric(neighbours[0], neighbours[1], neighbours[2], neighbours[3]))
			{
				m_Modules.push_back(module);
				m_Modules.back().Mesh.Init();
				m_Modules.back().Mesh.UpdateBuffers();
				m_Modules.back().Type = (int)m_Modules.size() - 1;

				MeshMan.AddMesh(module.Mesh, module.Name);	  //Add mesh with module name
				MeshMan.AddCollisionMeshes(CM, module.Name);
			}
			else
			{
				module.Name = name + "_r0";
				m_Modules.push_back(module);
				m_Modules.back().Mesh.Init();
				m_Modules.back().Mesh.UpdateBuffers();
				m_Modules.back().Type = (int)m_Modules.size() - 1;

				MeshMan.AddMesh(module.Mesh, module.Name);
				MeshMan.AddCollisionMeshes(CM, module.Name);
				for(size_t ii = 1; ii < 4; ++ii)
				{
					Mat3f Rot;
					Rot.SetRotation({0, PI / 2, 0});
					for(auto &V : module.Mesh.Vertices)
					{
						V.Position = Rot * V.Position;
						V.Normal = Rot * V.Normal;
					}
					module.Name = name + "_r" + std::to_string(ii);
					module.NeightbourConnections[0] = neighbours[(0 + ii) % 4];
					module.NeightbourConnections[1] = neighbours[(1 + ii) % 4];
					module.NeightbourConnections[2] = neighbours[(2 + ii) % 4];
					module.NeightbourConnections[3] = neighbours[(3 + ii) % 4];

					module.UpDownConnections[0] = RotateUpDown(upDownConnections[0], ii);
					module.UpDownConnections[1] = RotateUpDown(upDownConnections[1], ii);

					m_Modules.push_back(module);
					m_Modules.back().Mesh.Init();
					m_Modules.back().Mesh.UpdateBuffers();
					m_Modules.back().Type = (int)m_Modules.size() - 1;

					MeshMan.AddMesh(module.Mesh, module.Name);
					for(auto &m : CM)
					{
						for(auto &V : m.Vertices)
						{
							V.Position = Rot * V.Position;
							V.Normal = Rot * V.Normal;
						}
					}
					MeshMan.AddCollisionMeshes(CM, module.Name);
				}
			}
		}
	}
}

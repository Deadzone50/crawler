#pragma once

#include <map>
#include <vector>

#include "CharacterLevel.h"
#include "Item.h"
#include "Shader.h"
#include "vector/Vector.h"
#include "Weapon.h"

constexpr uint8_t INV_EMPTY = 255;
constexpr uint8_t EQUIPMENT_SLOTS = 9;

/*
1x1 Bomb, Ammo, Potion
1x2 Knife
2x2 Pistol
2x3 Smg
2x4 2h Rifle etc
2x5 2h Heavy

20
19    ###
18    ###
17  # ###  #
16  # ###  #
15  # ###  #
14
13    ####
12 ##      ##
11 ##      ##
10 ##      ##
9
1	20x8
0
*/

struct StorageBox
{
	Vec2i Pos;
	Vec2i Size;
	std::vector<bool> Occupied;
};

class InventoryItem
{
public:
	enum class Type
	{
		Weapon,
		Ammo
	};

	InventoryItem(uint8_t Index, ItemType Type);

	uint8_t GetIndex() const { return m_Index; }
	Type GetItemType() const { return m_InventoryType; }

	uint8_t GetInventoryBoxIndex() const { return m_InventoryBoxIndex; }
	Vec2i GetInventoryPos() const { return m_InventoryPos; }
	Vec2i GetInventorySize() const { return m_InventorySize; }
	const std::string &GetInventoryIcon() const { return m_InventoryIcon; }

	void SetInventoryBoxIndex(uint8_t i) { m_InventoryBoxIndex = i; }
	void SetInventoryPos(Vec2i P) { m_InventoryPos = P; }

private:
	Type m_InventoryType;
	uint8_t m_InventoryBoxIndex;
	uint8_t m_Index;
	Vec2i m_InventoryPos;
	Vec2i m_InventorySize;
	std::string m_InventoryIcon;
};

class Inventory
{
public:
	Inventory();
	void RemoveItem(uint8_t Index);

	void AddWeapon(const Weapon &Weapon, uint8_t InventoryBoxIndex);
	void EquipWeapon(uint8_t ItemIndex, uint8_t EquippedIndex);

	void AddAmmo(AmmoType Type, uint32_t Amount);
	void RemoveAmmo();
	uint32_t GetAmmo() { return GetAmmo(GetCurrentWeapon().GetAmmoType()); }
	uint32_t GetAmmo(AmmoType Type) { return m_Ammo[Type]; }

	Weapon &GetCurrentWeapon() { return m_Weapons[m_EquippedWeapons[m_CurrentWeaponIndex]]; }
	const Weapon &GetCurrentWeapon() const { return m_Weapons[m_EquippedWeapons[m_CurrentWeaponIndex]]; }
	uint8_t GetCurrentWeaponIndex() const { return m_CurrentWeaponIndex; }
	bool SwitchWeapon(uint8_t EquippedIndex);
	void SetModifiers(std::map<SkillTag, int> Modifiers);

	std::vector<InventoryItem> &GetItems() { return m_Items; }

	//VISUAL
	void Render(Shader &Shader) const;
	//returns if sucessfully picked up/placed an item
	bool Click(Vec2i MousePos, uint8_t &ItemIndex);
	uint8_t GetItemUnderMouse(Vec2i MousePos) const;
	void GetScaleAndStart(float &Scale, Vec2i &Start) const;

private:
	Vec2i FindFreePos(Vec2i Size) const;

	std::vector<InventoryItem> m_Items;
	std::vector<Weapon> m_Weapons;
	std::map<AmmoType, uint32_t> m_Ammo;

	uint8_t m_EquippedWeapons[EQUIPMENT_SLOTS] = {INV_EMPTY, INV_EMPTY, INV_EMPTY, INV_EMPTY, INV_EMPTY,
												  INV_EMPTY, INV_EMPTY, INV_EMPTY, INV_EMPTY};
	uint8_t m_CurrentWeaponIndex = 0;

	std::map<SkillTag, int> m_Modifiers;

	std::vector<StorageBox> m_Storage;
};

#pragma once

#include <map>
#include <string>

#include "MeshManager.h"

void LoadMeshes(MeshManager &MeshMan);

void LoadTextures(MeshManager &MeshMan);

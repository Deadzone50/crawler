#pragma once

#include "Colliders.h"
#include "Item.h"
#include "Structs.h"
#include "vector/Vector.h"

int RayTriangleIntersection(Vec3f Origin, Vec3f Direction, Vec3f Triangle[3], float &tmax);

Vec3f SphereTriangleIntersection(Vec3f Origin, float &Radius, Vec3f Triangle[3]);

bool RayAABBCollisionTest(Vec3f Origin, Vec3f Direction, AxisAlignedBoundingBox AABB, float &MinLen, float &MaxLen);

bool RayPlaneCollisionTest(Vec3f PlaneN, Vec3f PlaneP, Vec3f RayOrigin, Vec3f RayDir, float &MaxLen);

bool RaySphereCollisionTest(Vec3f Origin, Vec3f Direction, float &MaxLen, Vec3f SphereOrigin, float SphereRadius);

bool ColliderAABBCollisionTest(const SphereCollider &Col, const AxisAlignedBoundingBox AABB);

bool InAABB(Vec3f Pos, const AxisAlignedBoundingBox AABB);

bool AABBOverlapTest(const AxisAlignedBoundingBox &BB1, const AxisAlignedBoundingBox &BB2);

bool RayColliderIntersection(Vec3f Origin, Vec3f Direction, Collider *Col, float &tmax);
Vec3f SphereColliderIntersection(Vec3f Origin, float &Radius, Collider *Col);

class Level;

struct CollsionManifold
{
	std::vector<Vec3f> Points;
	Vec3f Normal = {};
	float Penetration = 0;
};

void HandleCollision(Item &O1, Item &O2);

void HandleCollisionWorld(const Level *Map, Item &O2);

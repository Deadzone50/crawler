#pragma once

#include <functional>

#include "Enemy.h"
#include "Item.h"
#include "Lights.h"
#include "Mesh.h"
#include "Navigation.h"
#include "Octree.h"
#include "Structs.h"
#include "vector/Vector.h"

class PlayerClass;

class LevelCollidable
{
public:
	bool RayAABBCollision(Vec3f Origin, Vec3f Direction, float &t) const;
	bool RayCollision(Vec3f Origin, Vec3f Direction, float &t) const;
	//Returns smallest Dir
	bool SphereCollision(Vec3f Origin, float &MaxRadius, Vec3f &Dir) const;

	void SetPos(Vec3f Pos);
	virtual void MovePos(Vec3f Pos);
	Vec3f GetPos() const;
	void Rotate(float Angle);

	void UpdateBoundingVolume();

	AxisAlignedBoundingBox GetBoundingVolume() const { return m_AABB; }
	const Mesh &GetMesh() const { return m_Mesh; }
	const std::vector<Collider *> &GetColliders() const { return m_Colliders; }

protected:
	AxisAlignedBoundingBox m_AABB = {0, 0};
	Mesh m_Mesh;
	std::vector<Collider *> m_Colliders;
};

class LevelObject : public LevelCollidable
{
public:
	LevelObject() {}
	LevelObject(Mesh *M, std::function<void()> OnUse = nullptr);
	void SetOnUse(std::function<void()> OnUse) { m_OnUse = OnUse; }
	void Use();
	void DrawHighlight() const;

	bool IsInteractable() const { return m_OnUse != nullptr; }

private:
	std::function<void()> m_OnUse = nullptr;
};

class LevelRoom : public LevelCollidable
{
public:
	LevelRoom();
	LevelRoom(const Mesh &M, const std::vector<const Mesh *> &CollisionMeshes, int Type, bool Decoration);

	void MovePos(Vec3f Pos) override;

	NavZone &GetNavZone() { return m_NavZone; }
	const NavZone &GetNavZone() const { return m_NavZone; }
	void UpdateNavZone(const LevelRoom &Other, int Index);

	int GetItemType() const { return m_Type; }
	bool IsDecoration() const { return m_Decoration; }

private:
	NavZone m_NavZone;
	int m_Type = 0;
	bool m_Decoration = false;
};

class MapObject
{
public:
	bool MapRayCollision(Vec3f Origin, Vec3f Direction, float &t, bool EarlyReturn = false) const;
	std::vector<bool> MapRayCollision(std::vector<Vec3f> Origins, std::vector<Vec3f> Directions, std::vector<float> &t) const;
	Vec3f MapCollisionTest(const SphereCollider &Col, float &Amount) const;

	//Collision with objects
	LevelObject *RayObjectCollision(Vec3f Origins, Vec3f Directions, float &t) const;

	//Collision with characters
	void HandleCharacterCollisions(const std::vector<PlayerClass> &Players);

	void Draw(Shader &Shader) const { LevelMesh.Draw(Shader); }
	void DrawOctree() const;
	void DrawColliders() const;

	void CreateOctree(const std::vector<std::vector<std::vector<int>>> &NodeToRoomMap);
	std::vector<int> GetPossibleRooms(const SphereCollider &Col) const { return m_Octree.GetRooms(Col, m_Octree.m_RootNode); }
	std::vector<int> GetPossibleRooms(const Vec3f Pos) const { return m_Octree.GetRooms(Pos, m_Octree.m_RootNode); }
	std::vector<int> GetPossibleRooms(const AxisAlignedBoundingBox &AABB) const
	{
		return m_Octree.GetRooms(AABB, m_Octree.m_RootNode);
	}

	std::vector<LevelRoom> Rooms;
	std::vector<LevelObject> Objects;
	std::vector<EnemyClass> Enemies;
	std::vector<Item> Items;
	std::vector<Light> Lights;
	Mesh LevelMesh;

private:
	Octree m_Octree;
};

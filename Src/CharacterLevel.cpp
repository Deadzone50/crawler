#include "CharacterLevel.h"

#include "Debug.h"
#include "Drawer.h"
#include "IO.h"
#include "Random.h"
#include "RenderText.h"
#include "States.h"
#include "XML.h"

extern Vec2i G_ScreenSize;

std::vector<SkillNode> ParseXMLSkills(const XMLElement &SkillsXML)
{
	std::vector<SkillNode> Result;
	for(const auto &E : SkillsXML.Elements)
	{
		SkillNode N;
		N.Name = E.GetAttribute("name");

		const auto Size = E.GetAttribute("size");
		if(Size == "Small")
			N.Size = NodeSize::Small;
		else if(Size == "Large")
			N.Size = NodeSize::Large;

		for(const auto &Effect : E.Elements)
		{
			N.Modifiers[StringToSkillTag(Effect.Tag.Name)] += std::stoi(Effect.Text);
		}
		Result.push_back(N);
	}
	return Result;
};

void Skillmanager::Initialize()
{
	XMLElement SkillsXML = ParseXML("Assets/Skills.xml");
	m_AllSkills = ParseXMLSkills(SkillsXML);
}

void Skilltree::Generate(std::vector<SkillNode> Skills)
{
	const uint32_t DIST = 100;
#if 1
	//Remove some possibilities, to make the tree more cohesive
	Skills.erase(Skills.begin() + GetRandomUI(0, Skills.size() - 1));
	Skills.erase(Skills.begin() + GetRandomUI(0, Skills.size() - 1));

	//Add some skills to the tree
	const int Num = 10;
	m_Skills.resize(Num);
	for(int i = 0; i < Num; ++i)
	{
		m_Skills[i] = Skills[GetRandomUI(0, Skills.size() - 1)];
	}

	m_Skills[0].Children = {1, 4};
	m_Skills[1].Children = {2, 3};
	m_Skills[4].Children = {5};
	m_Skills[1].Parents = {0};
	m_Skills[4].Parents = {0};
	m_Skills[2].Parents = {1};
	m_Skills[3].Parents = {1};
	m_Skills[5].Parents = {4};

	m_Skills[6].Children = {7, 8};
	m_Skills[7].Children = {9};
	m_Skills[8].Children = {9};
	m_Skills[7].Parents = {6};
	m_Skills[8].Parents = {6};
	m_Skills[9].Parents = {7, 8};

	m_Skills[0].TreePos = {DIST * 2, 0, 0};
	m_Skills[1].TreePos = {DIST * 1, 50, 0};
	m_Skills[2].TreePos = {DIST * 0, 100, 0};
	m_Skills[3].TreePos = {DIST * 2, 100, 0};
	m_Skills[4].TreePos = {DIST * 3, 50, 0};
	m_Skills[5].TreePos = {DIST * 3, 150, 0};

	m_Skills[6].TreePos = {DIST * 5, 0, 0};
	m_Skills[7].TreePos = {DIST * 4, 100, 0};
	m_Skills[8].TreePos = {DIST * 6, 100, 0};
	m_Skills[9].TreePos = {DIST * 5, 200, 0};
#else
	m_Skills.insert(m_Skills.begin(), Skills.begin(), Skills.end());
	m_Skills.insert(m_Skills.begin(), Skills.begin(), Skills.end());
	m_Skills.insert(m_Skills.begin(), Skills.begin(), Skills.end());
	int Num = m_Skills.size();

	for(int i = 0; i < Num / 3; ++i)
	{
		m_Skills[i].TreePos = {DIST * 0, 45.0f * i, 0};
	}
	for(int i = 0; i < Num / 3; ++i)
	{
		m_Skills[i + Num / 3].TreePos = {DIST * 1, 45.0f * i, 0};
	}
	for(int i = 0; i < Num / 3; ++i)
	{
		m_Skills[i + 2 * Num / 3].TreePos = {DIST * 2, 45.0f * i, 0};
	}
#endif
#if 0
	//bool Connected[Num] = {false};
	//Create paths
	for(uint32_t i = 0; i < Num-1; ++i)
	{
		//Connected[i] = true;
		uint32_t To = GetRandomUI(i+1, 9);
		m_Skills[i].Child = To;
		//Connected[To];
	}

	SkillNode *Current = &m_Skills[0];
	Vec3f CurrentPos = {0,0,0};
	Vec3f CurrentStartPos = {200,0,0};
	bool Visited[Num] = {true};
	while(true)
	{
		Visited[Current->Child] = true;
		Current = &m_Skills[Current->Child];
		CurrentPos.y += 40;

		if(Current->Child == -1)
		{
			if(LEN2(Current->TreePos) == 0)
			{
				Current->TreePos = CurrentPos;
			}
			static int i = 0;
			for(; i < Num; ++i)
			{
				if(!Visited[i])
				{
					Visited[i] = true;
					Current = &m_Skills[i];
					break;
				}
			}
			if(i == Num)
			{
				break;
			}
			if(LEN2(m_Skills[Current->Child].TreePos) > 0)	//If child has a position already
			{
				CurrentPos = m_Skills[Current->Child].TreePos + Vec3f{200,0,0};
			}
			else
			{
				CurrentPos = CurrentStartPos;
				CurrentStartPos += Vec3f{200,0,0};
			}
		}
		Current->TreePos = CurrentPos;
	}
#endif
}

void Skilltree::Save() {}

void Skilltree::Load() {}

void Skilltree::Reset()
{
	for(auto &S : m_Skills)
	{
		if(S.Allocated)
		{
			S.Allocated = false;
			++m_Skillpoints;
		}
	}
}

const float NODE_RADIUS = 20;

static void DrawSkillNode(Vec3f ScreenPos, const SkillNode &Node, Shader &Shader, bool Highlight)
{

	const Vec3f Col = (Node.Allocated) ? Vec3f{0, 1, 0} : Vec3f{0.7f, 0.7f, 0.7f};
	const Vec3f Rx = {NODE_RADIUS, 0, 0};
	const Vec3f Ry = {0, NODE_RADIUS, 0};
	DrawLine(ScreenPos - Rx - Ry, ScreenPos + Rx - Ry, Col);
	DrawLine(ScreenPos - Rx + Ry, ScreenPos + Rx + Ry, Col);
	DrawLine(ScreenPos - Rx - Ry, ScreenPos - Rx + Ry, Col);
	DrawLine(ScreenPos + Rx - Ry, ScreenPos + Rx + Ry, Col);

	//TODO: draw icon

	const Vec3f TextCol = (Highlight) ? Vec3f{0, 0.9f, 0} : Vec3f{0.8f, 0.8f, 0.8f};

	static RenderText Text("", 12, {1, 1, 1});
	Text.SetText(Node.Name);
	Text.SetColor(TextCol);

	Mat4f ModelMatrix;
	ModelMatrix.SetCol(3, {ScreenPos.x - Text.GetTotalLen() / 2, ScreenPos.y, ScreenPos.z - 1.0f * !Highlight, 1});
	Shader.SetMat4("Model", ModelMatrix);

	Text.Draw(Shader);
};

bool MouseOnNode(const SkillNode &Node, Vec2i MousePos)
{
	return (Node.TreePos.x - NODE_RADIUS < MousePos.x && Node.TreePos.x + NODE_RADIUS > MousePos.x) &&
		   (Node.TreePos.y - NODE_RADIUS < MousePos.y && Node.TreePos.y + NODE_RADIUS > MousePos.y);
}

void Skilltree::HandleAndDraw(Shader &Shader, const IOStates &IO)
{
	const Vec3f C = {G_ScreenSize.x * 0.1f, G_ScreenSize.y * 0.1f, 0};
	const Vec2i MouseP = IO.MousePos - Vec2i{(int)(G_ScreenSize.x * 0.1f), (int)(G_ScreenSize.y * 0.1f)};
	for(auto &S : m_Skills)
	{
		bool Highlight = MouseOnNode(S, MouseP);
		if(Highlight && !S.Allocated && IO.MouseButton[MOUSE_LEFT] && m_Skillpoints > 0)
		{
			bool CanAllocate = false;
			if(S.Parents.empty())
			{
				CanAllocate = true;
			}
			else
			{
				for(auto &P : S.Parents)	//Check atlease one parent is allocated
				{
					if(m_Skills[P].Allocated)
					{
						CanAllocate = true;
						break;
					}
				}
			}
			if(CanAllocate)
			{
				--m_Skillpoints;
				S.Allocated = true;
			}
		}
		DrawSkillNode(S.TreePos + C, S, Shader, Highlight);
		for(auto &Child : S.Children)
		{
			SkillNode &To = m_Skills[Child];
			DrawLine(S.TreePos + C, To.TreePos + C, {1, 0, 0});
		}
	}
}

std::map<SkillTag, int> Skilltree::GetAllocatedEffects() const
{
	std::map<SkillTag, int> Result;
	for(const auto &S : m_Skills)
	{
		if(S.Allocated)
		{
			for(const auto &E : S.Modifiers)
			{
				Result[E.first] += E.second;
			}
		}
	}
	return Result;
}

std::vector<Candidate> GenerateCandidates(uint8_t Num, const std::vector<SkillNode> &Skills)
{
	std::vector<Candidate> Result(Num);

	for(auto &C : Result)
	{
		C.Name = "Leif" + std::to_string(GetRandomUI(0, 999));
		C.Skilltree.Generate(Skills);
		C.Stats.Strenght = GetRandomUI(5, 15);
		C.Stats.Dexterity = GetRandomUI(5, 15);
		C.Stats.Intelligence = GetRandomUI(5, 15);

		C.StatGrowth.Strenght = GetRandomUI(1, 5);
		C.StatGrowth.Dexterity = GetRandomUI(1, 5);
		C.StatGrowth.Intelligence = GetRandomUI(1, 5);
	}

	return Result;
}

std::map<SkillTag, int> Candidate::GetModifiers() const
{
	std::map<SkillTag, int> Result = Skilltree.GetAllocatedEffects();
	Result[SkillTag::Strenght] += Stats.Strenght;
	Result[SkillTag::Dexterity] += Stats.Dexterity;
	Result[SkillTag::Intelligence] += Stats.Intelligence;
	return Result;
}

void Candidate::AddXP(uint32_t Xp)
{
	Log("Got xp: " + std::to_string(Xp));
	m_Xp += Xp;
	while(m_Xp > GetNextLevelXp())
	{
		++m_Level;
		Skilltree.AddSkillpoint();
		Log("Level up! level: " + std::to_string(m_Level));
	}
}

#pragma once

#include <vector>

#include "CharacterLevel.h"
#include "Scene.h"
#include "Shader.h"

class EnemyClass;
class PlayerClass;
class Level;
class Socket;
class Address;
struct Event;
struct GLFWwindow;
class NetworkManager;

struct IOStates
{
	bool KeyStates[512] = {false};
	bool KeyChanged[512] = {false};

	bool InitMouse = false;
	Vec2i MousePos;
	Vec3f MousePos3d, MouseDir;
	bool MouseButton[5] = {false};
	bool MouseChanged[5] = {false};
};

struct NetworkState
{
	NetworkManager *NM;
	NetworkManager *DNM;
	bool DebugMulti = false;
	bool Multiplayer = false;
};

struct GameState
{
	SceneType CurrentScene = SceneType::Base;
	std::vector<PlayerClass> Players;
	std::vector<std::shared_ptr<Event>> EventsOut, EventsIn, EventsReceived;
	Level *Map = nullptr;
	PlayerClass *Player = nullptr;	  //The controlled player
	uint8_t PlayerIndex = 0;
	Menu *CurrentMenu = nullptr;
	bool InMap = false;
	Skillmanager PlayerLevel;
	std::vector<Candidate> CharacterCandidates;
	uint8_t CurrentCandidate = 0;
};

struct RenderState
{
	GLFWwindow *Window;
	Shader MegaShader;
	Mat4f ProjectionMatrixPerspective, ProjectionMatrixOrtho;
	uint32_t OcclusionFBO, OcclusionTextureId;
	uint32_t RenderbufferId;
	uint32_t ScreenFBO, ScreenVAO, ScreenVBO, ScreenTextureId;
};

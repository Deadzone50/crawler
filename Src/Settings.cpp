#include "Settings.h"

#include <fstream>
#include <sstream>

#include "Console.h"

void ParseSettingsFile(Settings &Set)
{
	std::ifstream file;
	file.open("Settings.txt");
	if(!file.is_open())
	{
		Log("Settings.txt not found");
		return;
	}

	std::string Line, Word;
	while(std::getline(file, Line))
	{
		std::istringstream iss(Line);
		iss >> Word;
		if(Word == "Resolution")
		{
			iss >> Set.Resolution;
		}
		else if(Word == "Volume")
		{
			iss >> Set.Volume;
		}
		else if(Word == "WFCFile")
		{
			iss >> Set.WFCFile;
		}
		else if(Word == "Size")
		{
			iss >> Set.Size;
		}
		else if(Word == "Seed")
		{
			iss >> Set.Seed;

			//Always start at next seed
			if(Set.Seed > 0)
			{
				Set.Seed = Set.Seed - 1;
			}
		}
	}
}

void SaveSettingsFile(Settings &Set)
{
	std::ofstream file;
	file.open("Settings.txt", std::ofstream::trunc);
	if(!file.is_open())
	{
		Log("Settings.txt not found");
		return;
	}

	file << "Resolution " << Set.Resolution << "\n";
	file << "Volume " << Set.Volume << "\n";
	file << "WFCFile " << Set.WFCFile << "\n";
	file << "Size " << Set.Size << "\n";
	file << "Seed " << Set.Seed << "\n";
}

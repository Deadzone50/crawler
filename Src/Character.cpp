#include "Character.h"
#include "Debug.h"
#include "Event.h"
#include "Level.h"
#include "Quaternions.h"
#include "Random.h"
#include "SoundManager.h"
#include "States.h"

extern GameState Game;

bool operator!(const CharacterState &rhs)
{
	return rhs == CharacterState::None;
}

CharacterState operator&(CharacterState lhs, CharacterState rhs)
{
	return static_cast<CharacterState>(static_cast<std::underlying_type<CharacterState>::type>(lhs) &
									   static_cast<std::underlying_type<CharacterState>::type>(rhs));
}

CharacterState operator|(CharacterState lhs, CharacterState rhs)
{
	return static_cast<CharacterState>(static_cast<std::underlying_type<CharacterState>::type>(lhs) |
									   static_cast<std::underlying_type<CharacterState>::type>(rhs));
}

CharacterState operator~(CharacterState rhs)
{
	return static_cast<CharacterState>(~static_cast<std::underlying_type<CharacterState>::type>(rhs));
}

Character::Character()
{
	m_State = CharacterState::Idle;

	m_Pos = {0, 0, 0};
	m_Up = {0, 1, 0};
	m_Right = {1, 0, 0};
	m_CurrentSpeed = {0, 0, 0};
	m_LookingDirection2D = {0, 1};
	m_LookingDirection = {0, 0, 1};
	m_Health = 20;
	m_Speed = 3;
	m_DamageTaken = 0;

	m_Falling = true;
}

bool Character::IsAlive() const
{
	return !(m_State & CharacterState::Dead);
}

void Character::SetState(CharacterState NewState)
{
	if(m_State != CharacterState::Dead && NewState == CharacterState::Dead)
	{
		OnDeath();
	}
	m_AnimationTimer = 0;
	m_State = NewState;
}

void Character::OnDeath()
{
	m_Health = 0;

	float Pitch = GetRandomF(0.7f, 1.2f);
	SoundMan.Play("Death-01.wav", m_Pos, Game.Player->GetPosition(), Game.Player->GetLookingDir(), 1, Pitch);

	float r = GetRandomF(0, 10);
	if(r < 4)
	{
		float R = GetRandomF(0, 6.28f);
		const Vec3f &P = m_Pos + Vec3f{cos(R), 0, sin(R)};
		if(r < 2)
		{
			SpawnItem(ItemType::Ammo_Shotgun, P, {0, 0, 0, 1}, Game);
		}
		else
		{
			SpawnItem(ItemType::Health, P, {0, 0, 0, 1}, Game);
		}
	}
	std::shared_ptr<DeathEvent> DE = std::make_shared<DeathEvent>();
	DE->XP = 100;
	AddEvent(DE, Game);

	if(!m_Rig.Joints.empty())
	{
		Mat4f ModelMatrix;
		const Vec3f Up = {0, 1, 0};
		const Vec3f Forward = {m_LookingDirection2D.x, 0, m_LookingDirection2D.y};
		const Vec3f Right = CROSS(Forward, Up);
		ModelMatrix.SetRow(0, {-Right.x, Up.x, Forward.x, m_Pos.x});
		ModelMatrix.SetRow(1, {-Right.y, Up.y, Forward.y, m_Pos.y});
		ModelMatrix.SetRow(2, {-Right.z, Up.z, Forward.z, m_Pos.z});
		ModelMatrix.SetRow(3, {0, 0, 0, 1});
		m_Rig.CreateRagdoll(ModelMatrix);

		//Add random movement to a random joint
		const uint32_t Num = m_Rig.Joints.size();
		for(int i = 0; i < 3; ++i)
		{
			const uint32_t Index = GetRandomUI(0, Num - 1);
			m_Rig.Joints[Index].WorldPos1 += GetRandomV3(-0.1f, 0.1f);
		}
	}
	DropWeapon(Game);
}

void Character::SetMesh(const std::string &Name)
{
	m_Mesh = MeshMan.GetMesh(Name);
	auto t = MeshMan.GetRig(Name);
	if(t != nullptr) m_Rig = *t;
}

void Character::AddWeapon(const std::string &Name, uint8_t InventoryBoxIndex)
{
	std::string WeaponPrefixC, WeaponPrefixW;
	std::string WeaponAttackType;

	const auto &Weapon = WeaponMan.GetWeapon(Name);
	ItemType Type = Weapon.GetItemType();
	if(Type == ItemType::Pitchfork)
	{
		WeaponPrefixC = "2H_";
		WeaponPrefixW = "W2_";
		WeaponAttackType = "Thrust";
	}
	else if(Type == ItemType::Polearm)
	{
		WeaponPrefixC = "2H_";
		WeaponPrefixW = "W2_";
		WeaponAttackType = "Swing";
	}
	else if(Type == ItemType::Machete)
	{
		WeaponPrefixC = "1H_";
		WeaponPrefixW = "W1_";
		WeaponAttackType = "Swing";
	}

	bool FoundWalkC = false;
	bool FoundAttackC = false;
	bool FoundWalkW = false;
	bool FoundAttackW = false;
	for(int i = 0; i < m_Rig.Animations.size(); ++i)
	{
		const auto &A = m_Rig.Animations[i];
		const std::string &Prefix = A.Name.substr(0, 3);
		const std::string &Name = A.Name.substr(3);
		if(Prefix == WeaponPrefixC)
		{
			if(Name == "Walk")
			{
				m_WalkAnimationCI = i;
				FoundWalkC = true;
			}
			else if(Name == WeaponAttackType)
			{
				m_AttackAnimationCI = i;
				FoundAttackC = true;
			}
		}
		else if(Prefix == WeaponPrefixW)
		{
			if(Name == "Walk")
			{
				m_WalkAnimationWI = i;
				FoundWalkW = true;
			}
			else if(Name == WeaponAttackType)
			{
				m_AttackAnimationWI = i;
				FoundAttackW = true;
			}
		}
		if(FoundWalkC && FoundAttackC && FoundWalkW && FoundAttackW) break;
	}
	m_Inventory.AddWeapon(Weapon, InventoryBoxIndex);
}

Mat4f Character::GetModelMatrix() const
{
	const Vec3f Up = {0, 1, 0};

	Vec3f Forward = {m_LookingDirection2D.x, 0, m_LookingDirection2D.y};
	Vec3f Right = CROSS(Forward, Up);

	Mat4f ModelMatrix;
	ModelMatrix.SetRow(0, {-Right.x, Up.x, Forward.x, m_Pos.x});
	ModelMatrix.SetRow(1, {-Right.y, Up.y, Forward.y, m_Pos.y});
	ModelMatrix.SetRow(2, {-Right.z, Up.z, Forward.z, m_Pos.z});
	ModelMatrix.SetRow(3, {0, 0, 0, 1});

	return ModelMatrix;
}

void Character::DrawCharacter(Shader &Shader)
{
	Mat4f ModelMatrix = GetModelMatrix();
	Shader.SetMat4("Model", ModelMatrix);
	Shader.SetBool("Skinned", true);

	size_t NumJoints = m_Rig.Joints.size();
	for(int i = 0; i < NumJoints; ++i)
	{
		Mat4f ModelJointTransform;
		ModelJointTransform.Translate(m_Rig.Joints[i].Translation);
		Mat4f Rot;
		Rot.Rotate(m_Rig.Joints[i].Rotation);
		ModelJointTransform = ModelJointTransform * Rot;
		if(m_Rig.Joints[i].Parent > -1)
		{
			ModelJointTransform = m_Rig.Joints[m_Rig.Joints[i].Parent].ModelJointTransform * ModelJointTransform;
		}
		m_Rig.Joints[i].ModelJointTransform = ModelJointTransform;

		Mat4f Mat = ModelJointTransform * m_Rig.InverseBindMatrices[i];
		Shader.SetMat4("JointMatrices[" + std::to_string(i) + "]", Mat);
	}
	m_Mesh->Draw(Shader);
	Shader.SetBool("Skinned", false);
	if(IsAlive())
	{
		Mat4f ModelJointTransform;
		ModelJointTransform.Translate(m_WeaponJoint.Translation);
		Mat4f Rot;
		Rot.Rotate(m_WeaponJoint.Rotation);
		ModelJointTransform = ModelMatrix * ModelJointTransform * Rot;

		Shader.SetMat4("Model", ModelJointTransform);
		m_Inventory.GetCurrentWeapon().DrawModel(Shader);
	}
}

SphereCollider Character::GetCollider() const
{
	SphereCollider Col{Collider::Type::Sphere, {0, 0, 0}, m_Pos + Vec3f{0, 0.5f, 0}, 0.5f};
	return Col;
}

Vec3f Character::CheckLevelCollision(Level &Map)
{
	Vec3f CollisionNormal = {0, 0, 0};
	const float Height = 1.0f;
#if 0
	const float HeightUp = 1.0f;
	const float Width = 0.5f;
	float tDown = Height;
	float tUp = HeightUp;
	float tLeft = Width;
	float tRight = Width;
	float tForward = Width;
	float tBack = Width;

	std::vector<float> tvec = {tDown, tUp, tLeft, tRight, tForward, tBack};
	std::vector<Vec3f> posvec(6, m_Pos+Vec3f{0,Height,0});
	std::vector<Vec3f> dirvec = {{0,-1,0}, {0,1,0}, {-1,0,0}, {1,0,0}, {0,0,-1}, {0,0,1}};
	std::vector<bool> Results = Map.RayCollision(posvec, dirvec, tvec);
	if(Results[0])
	{
		m_Falling = false;
		Change.y += Height - tvec[0];
		m_CurrentSpeed.y = 0;
	}
	if(Results[1])
	{
		Change.y -= HeightUp - tvec[1];
		m_CurrentSpeed.y = 0;
	}
	for(int i = 2; i < 6; ++i)
	{
		if(Results[i])
			Change -= dirvec[i]*(Width - tvec[i]);
	}
#else
	float Amount = 0;
	CollisionNormal = Map.MapCollisionTest(GetCollider(), Amount);
#endif
	return CollisionNormal * Amount;
}

bool Character::RayCollision(Vec3f Pos, Vec3f Dir, float &t) const
{
	if(!IsAlive()) return false;
	return RaySphereCollisionTest(Pos, Dir, t, m_Pos + Vec3f{0, 1, 0}, 0.5f);
}

Vec3f Character::HandleCollision(Level &Map)
{
	Vec3f CollisionNormal = CheckLevelCollision(Map);
	if(CollisionNormal.y != 0)	  //Hits floor or roof
	{
		m_CurrentSpeed.y = 0;
		if(CollisionNormal.y > 0)	 //If colliding with something from above
		{
			m_Falling = false;
		}
	}
	if(LEN2(CollisionNormal))
	{
		m_Pos += CollisionNormal;
	}
	return CollisionNormal;
}

void Character::Animate(float dt)
{
	m_AnimationTimer += dt;
	if(m_State == CharacterState::Dead)
	{
		if(m_AnimationTimer < 5)
		{
			const Vec3f Up = {0, 1, 0};
			const Vec3f Forward = {m_LookingDirection2D.x, 0, m_LookingDirection2D.y};
			const Vec3f Right = CROSS(Forward, Up);
			Mat4f ModelMatrix;
			ModelMatrix.SetRow(0, {-Right.x, Up.x, Forward.x, m_Pos.x});
			ModelMatrix.SetRow(1, {-Right.y, Up.y, Forward.y, m_Pos.y});
			ModelMatrix.SetRow(2, {-Right.z, Up.z, Forward.z, m_Pos.z});
			ModelMatrix.SetRow(3, {0, 0, 0, 1});
			m_Rig.StepRagdoll(dt, Game, ModelMatrix);
		}
		else
		{
			m_ToRemove = true;
		}
		return;
	}
	MeshRig &MR = m_Rig;
	size_t NumJoints = MR.Joints.size();
	const Animation *CurrentAnimation;
	const Animation *WeaponAnimation;
	if(!!(m_State & CharacterState::Attacking))
	{
		CurrentAnimation = &m_Rig.Animations[m_AttackAnimationCI];
		WeaponAnimation = &m_Rig.Animations[m_AttackAnimationWI];
	}
	else
	{
		CurrentAnimation = &m_Rig.Animations[m_WalkAnimationCI];
		WeaponAnimation = &m_Rig.Animations[m_WalkAnimationWI];
	}

	for(int i = 0; i < NumJoints; ++i)
	{
		size_t NumStepsT = CurrentAnimation->TranslationSteps[i].size();
		if(NumStepsT)	 //Skip if there is no animation
		{
			float StepLenT = CurrentAnimation->TranslationSteps[i][1].Time -
							 CurrentAnimation->TranslationSteps[i][0].Time;	   //Assume equal len
			int TotalStepsT = m_AnimationTimer / StepLenT;
			int CurrentStepIndexT = TotalStepsT % NumStepsT;
			int NextStepIndexT = (CurrentStepIndexT + 1) % NumStepsT;
			float tt = (m_AnimationTimer - TotalStepsT * StepLenT) / StepLenT;
			MR.Joints[i].Translation = (1 - tt) * CurrentAnimation->TranslationSteps[i][CurrentStepIndexT].Translation +
									   tt * CurrentAnimation->TranslationSteps[i][NextStepIndexT].Translation;
		}
		size_t NumStepsR = CurrentAnimation->RotationSteps[i].size();
		if(NumStepsR)
		{
			float StepLenR =
				CurrentAnimation->RotationSteps[i][1].Time - CurrentAnimation->RotationSteps[i][0].Time;	//Assume equal len
			int TotalStepsR = m_AnimationTimer / StepLenR;
			int CurrentStepIndexR = TotalStepsR % NumStepsR;
			int NextStepIndexR = (CurrentStepIndexR + 1) % NumStepsR;
			float tr = (m_AnimationTimer - TotalStepsR * StepLenR) / StepLenR;
			MR.Joints[i].Rotation = QSlerp(CurrentAnimation->RotationSteps[i][CurrentStepIndexR].Rotation,
										   CurrentAnimation->RotationSteps[i][NextStepIndexR].Rotation, tr);
		}
	}

	const int i = 0;
	size_t NumStepsT = WeaponAnimation->TranslationSteps[i].size();
	if(NumStepsT)	 //Skip if there is no animation
	{
		float StepLenT =
			WeaponAnimation->TranslationSteps[i][1].Time - WeaponAnimation->TranslationSteps[i][0].Time;	//Assume equal len
		int TotalStepsT = m_AnimationTimer / StepLenT;
		int CurrentStepIndexT = TotalStepsT % NumStepsT;
		int NextStepIndexT = (CurrentStepIndexT + 1) % NumStepsT;
		float tt = (m_AnimationTimer - TotalStepsT * StepLenT) / StepLenT;
		m_WeaponJoint.Translation = (1 - tt) * WeaponAnimation->TranslationSteps[i][CurrentStepIndexT].Translation +
									tt * WeaponAnimation->TranslationSteps[i][NextStepIndexT].Translation;
	}
	size_t NumStepsR = WeaponAnimation->RotationSteps[i].size();
	if(NumStepsR)
	{
		float StepLenR =
			WeaponAnimation->RotationSteps[i][1].Time - WeaponAnimation->RotationSteps[i][0].Time;	  //Assume equal len
		int TotalStepsR = m_AnimationTimer / StepLenR;
		int CurrentStepIndexR = TotalStepsR % NumStepsR;
		int NextStepIndexR = (CurrentStepIndexR + 1) % NumStepsR;
		float tr = (m_AnimationTimer - TotalStepsR * StepLenR) / StepLenR;
		m_WeaponJoint.Rotation = QSlerp(WeaponAnimation->RotationSteps[i][CurrentStepIndexR].Rotation,
										WeaponAnimation->RotationSteps[i][NextStepIndexR].Rotation, tr);
	}
}

void Character::DrawSkeleton()
{
	const Vec3f Up = {0, 1, 0};
	Vec3f Forward = {m_LookingDirection2D.x, 0, m_LookingDirection2D.y};
	Vec3f Right = CROSS(Forward, Up);
	Mat4f ModelMatrix;
	ModelMatrix.SetRow(0, {-Right.x, Up.x, Forward.x, m_Pos.x});
	ModelMatrix.SetRow(1, {-Right.y, Up.y, Forward.y, m_Pos.y});
	ModelMatrix.SetRow(2, {-Right.z, Up.z, Forward.z, m_Pos.z});
	ModelMatrix.SetRow(3, {0, 0, 0, 1});
	m_Rig.Draw(ModelMatrix);
}

void Character::StepWeapon(float dt)
{
	Weapon &Current = m_Inventory.GetCurrentWeapon();
	if(Current.OnCooldown())
	{
		Current.ReduceTimers(dt);
		if(Current.ReadyToFire())
		{
			Current.Fire(Game, m_Pos + Vec3f{0, WEAPONHEIGHT, 0} + m_LookingDirection, m_LookingDirection);
		}
		if(!Current.OnCooldown())
		{
			SetState(m_State & ~CharacterState::Attacking);
		}
	}
}

void Character::DropWeapon(GameState &Game)
{
	const Weapon &W = m_Inventory.GetCurrentWeapon();
	Mat4f ModelMatrix = GetModelMatrix();

	Mat4f WorldTransform;
	WorldTransform.Translate(m_WeaponJoint.Translation);
	Mat4f Rot;
	Rot.Rotate(m_WeaponJoint.Rotation);
	WorldTransform = ModelMatrix * WorldTransform * Rot;

	Vec3f WPos = ToVec3(WorldTransform.GetCol(3));
	Vec4f WRot = MatToQuaternion(WorldTransform.GetRotation());

	SpawnItem(W.GetItemType(), WPos, WRot, Game);
}

#pragma once

#include <vector>

#include "Item.h"
#include "vector/Vector.h"
#include "Weapon.h"

class GameState;

enum class EventType : uint8_t
{
	Attack,
	Damage,
	Spawn,
	DeSpawn,
	EnemyDeath
};

struct Event
{
	EventType Type;
};

struct AttackEvent : public Event
{
	AttackEvent() { Type = EventType::Attack; }
	Vec3f Pos = {}, Dir = {};
	AttackStats Stats = {};
};

struct DamageEvent : public Event
{
	DamageEvent() { Type = EventType::Damage; }
	uint8_t PlayerIndex = 0;
	uint8_t Damage = 0;
};

struct SpawnEvent : public Event
{
	SpawnEvent() { Type = EventType::Spawn; }
	ItemType ItemType = ItemType::Ammo_Shotgun;
	Vec3f Pos = {};
	Vec4f Rot = {};
};

struct DeSpawnEvent : public Event
{
	DeSpawnEvent() { Type = EventType::DeSpawn; }
	uint32_t ItemIndex = 0;
};

struct DeathEvent : public Event
{
	DeathEvent() { Type = EventType::EnemyDeath; }
	uint32_t XP = 0;
};

void AddEvent(std::shared_ptr<Event> E, GameState &Game);

void HandleEvent(std::shared_ptr<Event> E, GameState &Game);

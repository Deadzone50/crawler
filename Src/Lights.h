#pragma once

#include "Mesh.h"
#include "vector/Vector.h"

class Shader;

class Light
{
public:
	Light() {}
	Light(Vec3f Pos, Vec3f Col)
	{
		m_Position = Pos;
		m_Color = Col;
	}
	void SetPos(Vec3f Pos) { m_Position = Pos; }
	Vec3f GetPos() const { return m_Position; }
	void SetCol(Vec3f Col) { m_Color = Col; }
	Vec3f GetCol() const { return m_Color; }
	void Draw(Shader &Shader) const { m_Mesh->Draw(Shader); }
	void SetMesh(Mesh *Mesh) { m_Mesh = Mesh; }
	void AddToRender(Shader &S, int Index);

private:
	Vec3f m_Position = {};
	Vec3f m_Color = {};
	Mesh *m_Mesh = nullptr;
};

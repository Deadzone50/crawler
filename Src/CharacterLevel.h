#pragma once

#include <cstdint>
#include <map>
#include <string>
#include <vector>

#include "Item.h"

const uint32_t BASE_XP_PER_LEVEL = 200;
//200 * level ^ 2

struct IOStates;

enum class SkillTag
{
	BladeDamage,
	BladeAttackspeed,
	BluntDamage,
	BluntAttackspeed,
	PierceDamage,
	PierceAttackspeed,
	PistolDamage,
	PistolAttackspeed,
	PistolSpread,
	PistolShots,
	ShotgunDamage,
	ShotgunAttackspeed,
	ShotgunSpread,
	ShotgunShots,
	MachinegunDamage,
	MachinegunAttackspeed,
	MachinegunSpread,
	MachinegunShots,
	RifleDamage,
	RifleAttackspeed,
	RifleSpread,
	RifleShots,
	RocketlauncherDamage,
	RocketlauncherAttackspeed,
	RocketlauncherSpread,
	RocketlauncherShots,

	MeleeDamage,
	ProjectileDamage,
	ExplosiveDamage,
	MeleeAttackspeed,
	ProjectileAttackspeed,
	ExplosiveAttackspeed,
	ProjectileSpread,
	ExplosiveSpread,

	Health,
	Strenght,
	Dexterity,
	Intelligence
};

inline SkillTag StringToSkillTag(const std::string &S)
{
	if(S == "ShotgunDamage")
		return SkillTag::ShotgunDamage;
	if(S == "ShotgunAttackspeed")
		return SkillTag::ShotgunAttackspeed;
	if(S == "ShotgunSpread")
		return SkillTag::ShotgunSpread;
	if(S == "ShotgunShots")
		return SkillTag::ShotgunShots;
	if(S == "Health")
		return SkillTag::Health;
	if(S == "Strenght")
		return SkillTag::Strenght;
	if(S == "Dexterity")
		return SkillTag::Dexterity;
	if(S == "Intelligence")
		return SkillTag::Intelligence;
	DEBUG_ABORT("String does not match tag");
	return SkillTag::ShotgunDamage;
};

enum class NodeSize
{
	Small,
	Large
};

struct SkillNode
{
	bool Allocated = false;
	NodeSize Size = NodeSize::Small;
	std::string Name;
	std::map<SkillTag, int> Modifiers;

	Vec3f TreePos = {0, 0, 0};
	std::vector<int> Children;
	std::vector<int> Parents;
};

class Skilltree
{
public:
	void Generate(std::vector<SkillNode> Skills);
	void Save();
	void Load();

	void Reset();

	void HandleAndDraw(Shader &Shader, const IOStates &IO);

	std::map<SkillTag, int> GetAllocatedEffects() const;

	void AddSkillpoint() { ++m_Skillpoints; }
	uint32_t GetSkillpoints() const { return m_Skillpoints; }

private:
	//std::map<uint32_t, uint32_t> m_Mapping;
	std::vector<SkillNode> m_Skills;
	uint32_t m_Skillpoints = 30;
};

struct CharacterStats
{
	uint32_t Strenght = 1;
	uint32_t Dexterity = 1;
	uint32_t Intelligence = 1;
};

class Skillmanager
{
public:
	void Initialize();
	const std::vector<SkillNode> &GetPossibleSkills() const { return m_AllSkills; };

private:
	std::vector<SkillNode> m_AllSkills;
};

class Candidate
{
public:
	std::string Name;
	Skilltree Skilltree;
	CharacterStats Stats;
	CharacterStats StatGrowth;

	std::map<SkillTag, int> GetModifiers() const;

	void AddXP(uint32_t Xp);

	uint32_t GetNextLevelXp() const { return m_Level * m_Level * BASE_XP_PER_LEVEL; }

private:
	uint32_t m_Level = 1;
	uint32_t m_Xp = 0;
};

std::vector<Candidate> GenerateCandidates(uint8_t Num, const std::vector<SkillNode> &Skills);

#pragma once

#include "Menu.h"

enum class SceneType
{
	Game,
	Base,
	Menu
};

void ExitLevel();

void SwitchToGame();

void SwitchToBaseScreen();

void SwitchToMenu(MenuType Menu);

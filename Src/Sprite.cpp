#include "Sprite.h"
#include "TextureManager.h"

Sprite::Sprite(const std::string &FileName, Vec2i Size)
{
	const float hx = Size.x * 0.5f;
	const float hy = Size.y * 0.5f;

	Vertex V1, V2, V3, V4;
	V1.Position = {-hx, -hy, 0};
	V1.TexCoord = {0, 0};
	V2.Position = {hx, -hy, 0};
	V2.TexCoord = {1, 0};
	V3.Position = {hx, hy, 0};
	V3.TexCoord = {1, 1};
	V4.Position = {-hx, hy, 0};
	V4.TexCoord = {0, 1};

	Vertices.push_back(V1);
	Vertices.push_back(V2);
	Vertices.push_back(V3);
	Vertices.push_back(V4);

	Indices = {0, 1, 2, 2, 3, 0};

	SetTexture(TextureMan.Texture(FileName));
}

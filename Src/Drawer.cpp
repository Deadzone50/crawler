#include "Drawer.h"

#include <GL/glew.h>
#include <algorithm>

extern Vec2i G_ScreenSize;

void Drawer::Initialize()
{
	m_VerticesPoints.reserve(MAX_POINTS);
	m_VerticesLines.reserve(MAX_LINES);
	m_PersistentVerticesPoints.reserve(MAX_POINTS);
	m_PersistentVerticesLines.reserve(MAX_LINES);

	glGenVertexArrays(1, &m_VAOP);
	glGenBuffers(1, &m_VBOP);

	glBindVertexArray(m_VAOP);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOP);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vec3f) * MAX_POINTS, NULL, GL_DYNAMIC_DRAW);
	//position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 2 * sizeof(Vec3f), (void *)0);
	glEnableVertexAttribArray(0);
	//color
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 2 * sizeof(Vec3f), (void *)sizeof(Vec3f));
	glEnableVertexAttribArray(1);

	glGenVertexArrays(1, &m_VAOL);
	glGenBuffers(1, &m_VBOL);

	glBindVertexArray(m_VAOL);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOL);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vec3f) * MAX_LINES, NULL, GL_DYNAMIC_DRAW);
	//position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 2 * sizeof(Vec3f), (void *)0);
	glEnableVertexAttribArray(0);
	//color
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 2 * sizeof(Vec3f), (void *)sizeof(Vec3f));
	glEnableVertexAttribArray(1);

	//PERSISTENT
	glGenVertexArrays(1, &m_VAOPP);
	glGenBuffers(1, &m_VBOPP);

	glBindVertexArray(m_VAOPP);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOPP);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vec3f) * MAX_POINTS, NULL, GL_DYNAMIC_DRAW);
	//position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 2 * sizeof(Vec3f), (void *)0);
	glEnableVertexAttribArray(0);
	//color
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 2 * sizeof(Vec3f), (void *)sizeof(Vec3f));
	glEnableVertexAttribArray(1);

	glGenVertexArrays(1, &m_VAOPL);
	glGenBuffers(1, &m_VBOPL);

	glBindVertexArray(m_VAOPL);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOPL);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vec3f) * MAX_LINES, NULL, GL_DYNAMIC_DRAW);
	//position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 2 * sizeof(Vec3f), (void *)0);
	glEnableVertexAttribArray(0);
	//color
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 2 * sizeof(Vec3f), (void *)sizeof(Vec3f));
	glEnableVertexAttribArray(1);
}

void Drawer::UpdateBuffers()
{
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOP);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vec3f) * m_VerticesPoints.size(), m_VerticesPoints.data(), GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBOL);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vec3f) * m_VerticesLines.size(), m_VerticesLines.data(), GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBOPP);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vec3f) * m_PersistentVerticesPoints.size(), m_PersistentVerticesPoints.data(),
				 GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBOPL);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vec3f) * m_PersistentVerticesLines.size(), m_PersistentVerticesLines.data(),
				 GL_DYNAMIC_DRAW);
}

bool Drawer::DrawPoint(Vec3f P, Vec3f Color)
{
	if(m_VerticesPoints.size() + 2 > MAX_POINTS)
	{
		return false;
	}
	m_VerticesPoints.push_back(P);
	m_VerticesPoints.push_back(Color);
	m_Update = true;
	return true;
}

bool Drawer::DrawPersistentPoint(Vec3f P, Vec3f Color)
{
	if(m_PersistentVerticesPoints.size() + 2 > MAX_POINTS)
	{
		return false;
	}
	m_PersistentVerticesPoints.push_back(P);
	m_PersistentVerticesPoints.push_back(Color);
	m_Update = true;
	return true;
}

bool Drawer::DrawPersistentLine(Vec3f S, Vec3f E, Vec3f Color)
{
	if(m_PersistentVerticesLines.size() + 4 > MAX_LINES)
	{
		return false;
	}
	m_PersistentVerticesLines.push_back(S);
	m_PersistentVerticesLines.push_back(Color);
	m_PersistentVerticesLines.push_back(E);
	m_PersistentVerticesLines.push_back(Color);
	m_Update = true;
	return true;
}

bool Drawer::DrawLine(Vec3f S, Vec3f E, Vec3f Color)
{
	if(m_VerticesLines.size() + 4 > MAX_LINES)
	{
		return false;
	}
	m_VerticesLines.push_back(S);
	m_VerticesLines.push_back(Color);
	m_VerticesLines.push_back(E);
	m_VerticesLines.push_back(Color);
	m_Update = true;
	return true;
}

bool Drawer::DrawDirLine(Vec3f S, Vec3f E, Vec3f Color)
{
	if(m_VerticesLines.size() + 4 > MAX_LINES)
	{
		return false;
	}
	m_VerticesLines.push_back(S);
	m_VerticesLines.push_back({0, 0, 0});
	m_VerticesLines.push_back(E);
	m_VerticesLines.push_back(Color);
	m_Update = true;
	return true;
}

void Drawer::RenderGraphics(Shader &Shader)
{
	if(m_Enabled)
	{
		if(m_Update)
		{
			UpdateBuffers();
			m_Update = false;
		}

		glBindVertexArray(m_VAOP);
		glDrawArrays(GL_POINTS, 0, (int)m_VerticesPoints.size() / 2);

		glBindVertexArray(m_VAOL);
		glDrawArrays(GL_LINES, 0, (int)m_VerticesLines.size() / 2);

		glBindVertexArray(m_VAOPP);
		glDrawArrays(GL_POINTS, 0, (int)m_PersistentVerticesPoints.size() / 2);

		glBindVertexArray(m_VAOPL);
		glDrawArrays(GL_LINES, 0, (int)m_PersistentVerticesLines.size() / 2);
	}
}

void Drawer::RenderTextLines(Shader &Shader)
{
	if(m_Enabled)
	{
		Mat4f ModelMatrix;
		Vec3f Pos = {20, G_ScreenSize.y, 0};
		for(int Row = 0; Row < m_TextLines.size(); ++Row)
		{
			Pos.y -= m_TextLines[Row].GetCharSize() + 1;
			ModelMatrix.SetCol(3, {Pos.x, Pos.y, Pos.z, 1});
			Shader.SetMat4("Model", ModelMatrix);
			m_TextLines[Row].Draw(Shader);
		}
	}
}

void Drawer::Clear()
{
	m_VerticesPoints.clear();
	m_VerticesLines.clear();
	m_Update = true;
}

void Drawer::ClearPersistent()
{
	m_PersistentVerticesPoints.clear();
	m_PersistentVerticesLines.clear();
	m_Update = true;
}

void Drawer::ClearPoints()
{
	m_VerticesPoints.clear();
	m_Update = true;
}

void Drawer::ClearLines()
{
	m_VerticesLines.clear();
	m_Update = true;
}

void MegaDrawer::Initialize()
{
	m_Drawer.Initialize();
	m_DebugDrawer.Initialize();
}

void MegaDrawer::RenderGraphics(Shader &Shader, float dt)
{
	DrawParticles(dt);
	m_Drawer.RenderGraphics(Shader);
}

void MegaDrawer::DrawParticles(float dt)
{
	auto it = m_Particles.begin();
	while(it != m_Particles.end())
	{
		if(it->TimeLeft < 0)
			it = m_Particles.erase(it);
		else
		{
			Vec3f Col = std::clamp(it->TimeLeft, 0.5f, 1.0f) * it->Color;
			m_Drawer.DrawPoint(it->Pos, Col);
			it->TimeLeft -= dt;
			it->Speed.y -= 600 * dt * dt;
			it->Pos += it->Speed * dt;
			++it;
		}
	}
}

extern MegaDrawer DrawHandler;

void DrawPoint(Vec3f P, Vec3f Color)
{
	DrawHandler.DrawPoint(P, Color);
}

void DrawLine(Vec3f S, Vec3f E, Vec3f Color)
{
	DrawHandler.DrawLine(S, E, Color);
}

void DrawBoxP(const Vec3f *P, Vec3f Color)
{
	DrawHandler.DrawPoint(P[0], Color);
	DrawHandler.DrawPoint(P[1], Color);
	DrawHandler.DrawPoint(P[2], Color);
	DrawHandler.DrawPoint(P[3], Color);
	DrawHandler.DrawPoint(P[4], Color);
	DrawHandler.DrawPoint(P[5], Color);
	DrawHandler.DrawPoint(P[6], Color);
	DrawHandler.DrawPoint(P[7], Color);
}

void DrawBox(const Vec3f *P, const uint32_t *I, Vec3f Color)
{
	DrawHandler.DrawLine(P[I[0]], P[I[1]], Color);
	DrawHandler.DrawLine(P[I[1]], P[I[2]], Color);
	DrawHandler.DrawLine(P[I[2]], P[I[3]], Color);
	DrawHandler.DrawLine(P[I[3]], P[I[0]], Color);

	DrawHandler.DrawLine(P[I[4]], P[I[5]], Color);
	DrawHandler.DrawLine(P[I[5]], P[I[6]], Color);
	DrawHandler.DrawLine(P[I[6]], P[I[7]], Color);
	DrawHandler.DrawLine(P[I[7]], P[I[4]], Color);

	DrawHandler.DrawLine(P[I[0]], P[I[4]], Color);
	DrawHandler.DrawLine(P[I[1]], P[I[5]], Color);
	DrawHandler.DrawLine(P[I[2]], P[I[6]], Color);
	DrawHandler.DrawLine(P[I[3]], P[I[7]], Color);
}

void CreateParticle(Vec3f Pos, Vec3f Speed, Vec3f Color, float Lifetime)
{
	DrawHandler.CreateParticle(Pos, Speed, Color, Lifetime);
}

void RenderGraphics(Shader &Shader, float dt)
{
	DrawHandler.RenderGraphics(Shader, dt);
}

void ClearGraphics()
{
	DrawHandler.Clear();
}

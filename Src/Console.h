#pragma once

#include <Windows.h>
#include <string>

void Log(const std::string &Text, bool Print = true);
void EnableLogFile();
void ToggleGameConsole();
void EnableGameConsole();
void DisableGameConsole();

void DrawConsole(uint32_t Start, uint8_t Num);

size_t GetConsoleLogSize();

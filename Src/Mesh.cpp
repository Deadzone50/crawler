#include <GL/glew.h>

#include "Mesh.h"

bool operator==(const Vertex &lhs, const Vertex &rhs)
{
	return ((lhs.Position == rhs.Position) && (lhs.Color == rhs.Color) && (lhs.Normal == rhs.Normal) &&
			(lhs.TexCoord == rhs.TexCoord));
}

void Mesh::Init()
{
	glGenVertexArrays(1, &m_VAO);
	glGenBuffers(1, &m_VBO);
	glGenBuffers(1, &m_EBO);
}

void Mesh::UpdateBuffers()
{
	glBindVertexArray(m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * Vertices.size(), Vertices.data(), GL_STATIC_DRAW);

	//position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, Position));
	glEnableVertexAttribArray(0);
	//color
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, Color));
	glEnableVertexAttribArray(1);
	//normal
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, Normal));
	glEnableVertexAttribArray(2);
	//tex
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, TexCoord));
	glEnableVertexAttribArray(3);
	//joints
	glVertexAttribPointer(4, 4, GL_UNSIGNED_BYTE, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, Joints));
	glEnableVertexAttribArray(4);
	//weights
	glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, Weights));
	glEnableVertexAttribArray(5);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t) * Indices.size(), Indices.data(), GL_STATIC_DRAW);
	m_NumElements = (uint32_t)Indices.size();
}

void Mesh::Draw(Shader &Shader) const
{
	Shader.SetSampler("Texture", 3);
	glActiveTexture(GL_TEXTURE3);				  //activate texture unit 3
	glBindTexture(GL_TEXTURE_2D, m_TextureId);	  //bind texture to texture unit 3
	glBindVertexArray(m_VAO);
	glDrawElements(GL_TRIANGLES, m_NumElements, GL_UNSIGNED_INT, (void *)0);
}

#include "SoundManager.h"

#include <algorithm>
#include <combaseapi.h>
#include <fstream>
#include <thread>

#include "Console.h"
#include "vector/Matrix.h"

bool LoadWavFile(const std::string &Name, SoundData &Xaudio2Data)
{
	std::ifstream file(Name, std::ios::binary);
	if(!file.is_open())
	{
		Log("Cannot open " + Name);
		return false;
	}
	char Buffer[4] = {};
	file.read(Buffer, 4);	 //ChunkID
	if(std::strncmp(Buffer, "RIFF", 4) != 0)
	{
		Log("Not RIFF");
		return false;
	}
	file.ignore(4);			 //Chunksize
	file.read(Buffer, 4);	 //Format
	if(std::strncmp(Buffer, "WAVE", 4) != 0)
	{
		Log("Not WAVE");
		return false;
	}
	file.read(Buffer, 4);	 //SubchunkID
	if(std::strncmp(Buffer, "fmt ", 4) != 0)
	{
		Log("Not fmt");
		return false;
	}
	file.ignore(4);			  //SubchunkSize, fmt(16)
	char Buffer2[16] = {};	  //copy fmt chunk to WFX
	file.read(Buffer2, 16);
	std::memcpy(&Xaudio2Data.WFX, Buffer2, 16);

	file.read(Buffer, 4);	 //SubchunkID
	if(std::strncmp(Buffer, "data", 4) != 0)
	{
		Log("Not data");
		return false;
	}
	file.read(Buffer, 4);	 //SubchunkSize, data
	uint32_t DataLen = 0;
	std::memcpy(&DataLen, &Buffer[0], 4);

	BYTE *Data = new BYTE[DataLen];
	file.read((char *)Data, DataLen);	 //Data

	Xaudio2Data.Buffer.AudioBytes = DataLen;
	Xaudio2Data.Buffer.pAudioData = Data;
	Xaudio2Data.Buffer.Flags = XAUDIO2_END_OF_STREAM;	 //no more data after this buffer
	return true;
}

bool SoundManager::Initialize(int Volume)
{
	HRESULT res = CoInitializeEx(nullptr, COINIT_MULTITHREADED);
	if(res != S_OK)
	{
		Log("Failed to initialize com library");
		return false;
	}
	res = XAudio2Create(&m_XAudioPointer, 0, XAUDIO2_DEFAULT_PROCESSOR);
	if(res != S_OK)
	{
		Log("Failed to initialize xaudio2");
		return false;
	}
	res = m_XAudioPointer->CreateMasteringVoice(&m_MasterVoice);
	if(res != S_OK)
	{
		Log("Failed to create mastering voice");
		return false;
	}
	SetVolume(Volume);
	return true;
}

SoundManager::~SoundManager()
{
	CoUninitialize();
	m_XAudioPointer->Release();
}

bool SoundManager::LoadSoundFile(const std::string &Name)
{
	SoundData XaudioSoundData;
	if(!LoadWavFile("Assets/Sound/" + Name, XaudioSoundData))
		return false;

	Log("Loaded: " + Name);
	m_SoundMap[Name] = XaudioSoundData;
	return true;
}

class VoiceCallback : public IXAudio2VoiceCallback
{
public:
	HANDLE BufferEndEvent;
	VoiceCallback() : BufferEndEvent(CreateEvent(NULL, FALSE, FALSE, NULL)) {}
	~VoiceCallback() { CloseHandle(BufferEndEvent); }

	//Called when the voice has just finished playing a contiguous audio stream.
	void OnStreamEnd() { SetEvent(BufferEndEvent); }

	//Unused methods are stubs
	void OnVoiceProcessingPassEnd() {}
	void OnVoiceProcessingPassStart(UINT32 SamplesRequired) {}
	void OnBufferEnd(void *pBufferContext) {}
	void OnBufferStart(void *pBufferContext) {}
	void OnLoopEnd(void *pBufferContext) {}
	void OnVoiceError(void *pBufferContext, HRESULT Error) {}
};

void PlayVoice(IXAudio2 *XAudioPointer, const SoundData &S, const Vec3f &SoundPos, const Vec3f &PlayerPos,
			   const Vec3f &PlayerFacingDir, float Volume, float Pitch)
{
	//TODO: Voice pooling/reuse?
	//TODO: Do this with a grouped operation set?
	IXAudio2SourceVoice *SourceVoice = nullptr;
	VoiceCallback VoiceCallback;
	auto res = XAudioPointer->CreateSourceVoice(&SourceVoice, (WAVEFORMATEX *)&S.WFX, 0, 2.0f, &VoiceCallback);
	if(res != S_OK)
	{
		Log("Failed to create source voice");
		return;
	}
	res = SourceVoice->SubmitSourceBuffer(&S.Buffer);
	if(res != S_OK)
	{
		Log("Failed to submit source buffer");
		return;
	}
	const Vec3f &ToSound = SoundPos - PlayerPos;

	float Dist2 = LEN2(ToSound);
	if(Dist2 > 0)
	{
		float Falloff = std::clamp(1.0f / (Dist2 * 0.05f), 0.0f, 1.0f);

		const Vec3f R = CROSS(PlayerFacingDir, {0, 1, 0});
		float t = (DOT(NORMALIZE(ToSound), R) + 1.0f) * 0.5f;	 //0 - 1, L to R
		float OutputMatrix[8] = {0};
		OutputMatrix[0] = (1 - t) * Falloff * Volume;	 //L
		OutputMatrix[1] = t * Falloff * Volume;			 //R
		res = SourceVoice->SetOutputMatrix(NULL, 1, 2, &OutputMatrix[0]);
	}
	else
	{
		res = SourceVoice->SetVolume(Volume);
	}
	SourceVoice->SetFrequencyRatio(Pitch);
	SourceVoice->Start();
	WaitForSingleObjectEx(VoiceCallback.BufferEndEvent, INFINITE, TRUE);
	SourceVoice->Stop();
	SourceVoice->DestroyVoice();
};

void SoundManager::Play(const std::string &Name, Vec3f SoundPos, Vec3f PlayerPos, Vec3f PlayerFacingDir, float Volume,
						float Pitch)
{
	if(!m_Enabled)
		return;

	const SoundData &S = m_SoundMap[Name];
	std::thread t1(PlayVoice, m_XAudioPointer, S, SoundPos, PlayerPos, PlayerFacingDir, Volume, Pitch);
	t1.detach();
}

void SoundManager::SetVolume(int Vol)
{
	m_MasterVolume = Vol;
	m_MasterVoice->SetVolume(m_MasterVolume * 0.01f);
}

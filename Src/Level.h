#pragma once

#include <functional>
#include <map>

#include "Collision.h"
#include "LevelObjects.h"
#include "Mesh.h"
#include "PlayerClass.h"
#include "Settings.h"
#include "WFC.h"

struct GameState;

class Level
{
public:
	void Init(MeshManager &MeshMan, const Settings &Set);
	void Draw(Shader &Shader) const;

	void SetSeed(uint32_t Seed);
	uint32_t GetSeed() const { return m_Seed; }
	void SetSize(Vec3i Size);
	Vec3f GenerateLevel();
	Vec3f GenerateDebugLevel(GameState &Game);
	void StepGeneration();

	//Collision with level geometry
	bool MapRayCollision(Vec3f Origin, Vec3f Direction, float &t, bool EarlyReturn = false) const
	{
		return m_Map.MapRayCollision(Origin, Direction, t, EarlyReturn);
	}
	//Collision with level geometry
	std::vector<bool> MapRayCollision(std::vector<Vec3f> Origins, std::vector<Vec3f> Directions, std::vector<float> &t) const
	{
		return m_Map.MapRayCollision(Origins, Directions, t);
	}
	//Check if collider collides with level geometry, returns collision vector
	Vec3f MapCollisionTest(const SphereCollider &Col, float &Amount) const { return m_Map.MapCollisionTest(Col, Amount); }

	//Collision with objects
	LevelObject *RayObjectCollision(Vec3f Origins, Vec3f Directions, float &t);

	//Collision with characters
	void HandleCharacterCollisions(const std::vector<PlayerClass> &Players);

	std::vector<Item> GetCollisionItems(const AxisAlignedBoundingBox &AABB) const;

	void Highlight(Vec3f Origin, Vec3f Direction);
	bool Use(PlayerClass *Player);

	std::vector<EnemyClass> &GetEnemies() { return m_Map.Enemies; }
	std::vector<Light> &GetLights() { return m_Map.Lights; }
	void AddItem(Item &I) { m_Map.Items.push_back(I); }
	void RemoveItem(uint32_t Id);
	std::vector<Item> &GetItems() { return m_Map.Items; }

	void UpdateNavigation(const GameState &Game);
	void SpawnEnemy(const std::string &EnemyName, const std::string &WeaponName, const Vec3f &Pos);
	void SpawnEnemies(GameState &Game, float dt);
	std::vector<Vec3f> GetWaypointsToClosestPlayer(Vec3f Start);
	const Minimap &GetMinimap() { return m_WFC.GetMinimap(); }

	//DEBUG

	void DrawWFC(Shader &S);
	void DrawNavmesh() const;
	void DrawOctree() const { m_Map.DrawOctree(); }
	void DrawColliders() const { m_Map.DrawColliders(); }

private:
	void PlaceEnemies(uint32_t Num);
	void PlaceItems(uint32_t Num);
	void PlaceObjects(uint32_t EntranceIndex, uint32_t ExitIndex);
	void PlaceLights();

	WFCMap m_WFC;
	MapObject m_Map;

	std::map<std::string, EnemyClass> m_EnemyMap;
	uint32_t m_Seed;

	Vec3i m_Size = {10, 3, 10};
	float m_EnemySpawnTimer = 0;
};

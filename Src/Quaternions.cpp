#include "Quaternions.h"

Vec4f Quaternion(Vec3f Axis, float Angle)
{
	Vec4f Q;
	Q.x = Axis.x * sin(0.5f * Angle);
	Q.y = Axis.y * sin(0.5f * Angle);
	Q.z = Axis.z * sin(0.5f * Angle);
	Q.w = cos(0.5f * Angle);
	return Q;
}

Vec4f QMult(Vec4f V1, Vec4f V2)
{
	const float a = V1.w;
	const float b = V1.x;
	const float c = V1.y;
	const float d = V1.z;

	const float e = V2.w;
	const float f = V2.x;
	const float g = V2.y;
	const float h = V2.z;

	Vec4f Result;
	Result.w = a * e - b * f - c * g - d * h;
	Result.x = b * e + a * f + c * h - d * g;
	Result.y = a * g - b * h + c * e + d * f;
	Result.z = a * h + b * g - c * f + d * e;
	return Result;
}

Vec4f QSlerp(Vec4f Q1, Vec4f Q2, float t)
{
	//						sin((1-t)*theta)*Q1 + sin(t*theta)*Q1
	//Slerp(Q1, Q2, t) =	-------------------------------------
	//									sin(theta)
	// cos(theta) = V1 dot V2

	float CosTheta = DOT(Q1, Q2);
	if(CosTheta < 0)	//More than 180 deg rotation (Theta = 2*angle), we want the shortest distance
	{
		Q1 = -Q1;
		CosTheta = -CosTheta;
	}
	float Theta = acosf(CosTheta);
	if(Theta == 0 || isnan(Theta))	  //Q1 == Q2
		return Q1;
	Vec4f Q = (sinf((1 - t) * Theta) * Q1 + sinf(t * Theta) * Q2) / sinf(Theta);
	return Q;
}

Vec4f QNlerp(Vec4f Q1, Vec4f Q2, float t)
{
	return NORMALIZE((1 - t) * Q1 + t * Q2);
}

Vec4f MatToQuaternion(const Mat4f &M)
{
	Vec4f A = M.GetRow(0);
	Vec4f B = M.GetRow(1);
	Vec4f C = M.GetRow(2);

	Mat3f M3;
	M3.SetCol(0, {A.x, A.y, A.z});
	M3.SetCol(1, {B.x, B.y, B.z});
	M3.SetCol(2, {C.x, C.y, C.z});
	return MatToQuaternion(M3);
}

Vec4f MatToQuaternion(const Mat3f &M)
{
	Vec4f Q;
	float trace = M.m[0] + M.m[4] + M.m[8];
	if(trace > 0)
	{
		float s = 0.5f / sqrtf(trace + 1.0f);
		Q.w = 0.25f / s;
		Q.x = (M.m[7] - M.m[5]) * s;
		Q.y = (M.m[2] - M.m[6]) * s;
		Q.z = (M.m[3] - M.m[1]) * s;
	}
	else
	{
		if(M.m[0] > M.m[4] && M.m[0] > M.m[8])
		{
			float s = 2.0f * sqrtf(1.0f + M.m[0] - M.m[4] - M.m[8]);
			Q.w = (M.m[7] - M.m[5]) / s;
			Q.x = 0.25f * s;
			Q.y = (M.m[1] + M.m[3]) / s;
			Q.z = (M.m[2] + M.m[6]) / s;
		}
		else if(M.m[4] > M.m[8])
		{
			float s = 2.0f * sqrtf(1.0f + M.m[4] - M.m[0] - M.m[8]);
			Q.w = (M.m[2] - M.m[6]) / s;
			Q.x = (M.m[1] + M.m[3]) / s;
			Q.y = 0.25f * s;
			Q.z = (M.m[5] + M.m[7]) / s;
		}
		else
		{
			float s = 2.0f * sqrtf(1.0f + M.m[8] - M.m[0] - M.m[4]);
			Q.w = (M.m[3] - M.m[1]) / s;
			Q.x = (M.m[2] + M.m[6]) / s;
			Q.y = (M.m[5] + M.m[7]) / s;
			Q.z = 0.25f * s;
		}
	}
	return Q;
}

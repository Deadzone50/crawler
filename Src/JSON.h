#pragma once

#include <map>
#include <string>
#include <vector>

class JSONObject
{
public:
	bool HasBool(const std::string &Key) const { return KeysBools.count(Key); }
	bool HasText(const std::string &Key) const { return KeysValues.count(Key); }
	bool HasInt(const std::string &Key) const { return KeysInts.count(Key); }
	bool HasFloat(const std::string &Key) const { return KeysFloats.count(Key); }
	bool HasObj(const std::string &Key) const { return KeysObjects.count(Key); }

	bool HasBools(const std::string &Key) const { return KeysBoolArrays.count(Key); }
	bool HasTexts(const std::string &Key) const { return KeysValueArrays.count(Key); }
	bool HasInts(const std::string &Key) const { return KeysIntArrays.count(Key); }
	bool HasFloats(const std::string &Key) const { return KeysFloatArrays.count(Key); }
	bool HasObjs(const std::string &Key) const { return KeysObjectArrays.count(Key); }

	bool GetBool(const std::string &Key) const { return KeysBools.at(Key); }
	const std::string &GetText(const std::string &Key) const { return KeysValues.at(Key); }
	int GetInt(const std::string &Key) const { return KeysInts.at(Key); }
	float GetFloat(const std::string &Key) const { return KeysFloats.at(Key); }
	const JSONObject &GetObj(const std::string &Key) const { return KeysObjects.at(Key); }

	const std::vector<bool> &GetBools(const std::string &Key) const { return KeysBoolArrays.at(Key); }
	const std::vector<std::string> &GetTexts(const std::string &Key) const { return KeysValueArrays.at(Key); }
	const std::vector<int> &GetInts(const std::string &Key) const { return KeysIntArrays.at(Key); }
	const std::vector<float> &GetFloats(const std::string &Key) const { return KeysFloatArrays.at(Key); }
	const std::vector<JSONObject> &GetObjs(const std::string &Key) const { return KeysObjectArrays.at(Key); }

	std::map<std::string, bool> KeysBools;
	std::map<std::string, std::string> KeysValues;
	std::map<std::string, int> KeysInts;
	std::map<std::string, float> KeysFloats;
	std::map<std::string, JSONObject> KeysObjects;

	std::map<std::string, std::vector<bool>> KeysBoolArrays;
	std::map<std::string, std::vector<std::string>> KeysValueArrays;
	std::map<std::string, std::vector<int>> KeysIntArrays;
	std::map<std::string, std::vector<float>> KeysFloatArrays;
	std::map<std::string, std::vector<JSONObject>> KeysObjectArrays;
};

JSONObject ParseJSON(const std::string &Path);

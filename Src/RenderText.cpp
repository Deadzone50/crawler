#include <GL/glew.h>

#include "Mesh.h"
#include "RenderText.h"

void RenderText::SetColor(const Vec3f &Col)
{
	m_Color[0] = Col;
	m_Color[1] = Col;
	m_Color[2] = Col;
	m_Color[3] = Col;
	m_Changed = true;
}

void RenderText::SetColor(Vec3f Col[4])
{
	m_Color[0] = Col[0];
	m_Color[1] = Col[1];
	m_Color[2] = Col[2];
	m_Color[3] = Col[3];
	m_Changed = true;
}

RenderText::RenderText(const std::string &Text, uint32_t CharSize, Vec3f Col)
{
	m_Text = Text;
	m_Changed = true;
	m_CharSize = CharSize;
	m_Color[0] = Col;
	m_Color[1] = Col;
	m_Color[2] = Col;
	m_Color[3] = Col;
}

RenderText::~RenderText()
{
	if(m_Initialized)
	{
		glDeleteVertexArrays(1, &m_VAO);
		glDeleteBuffers(1, &m_VBO);
	}
}

void RenderText::Draw(Shader &Shader)
{
	if(!m_Initialized)
	{
		glGenVertexArrays(1, &m_VAO);
		glGenBuffers(1, &m_VBO);
		m_Initialized = true;

		glBindVertexArray(m_VAO);
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);

		//position
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, Position));
		glEnableVertexAttribArray(0);
		//color
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, Color));
		glEnableVertexAttribArray(1);
		//Texcoord
		glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, TexCoord));
		glEnableVertexAttribArray(3);
	}

	const uint32_t Len = (uint32_t)m_Text.size();

	glBindVertexArray(m_VAO);
	if(m_Changed)
	{
		Vertex Vertices[4];

		Vertices[0].Position = {0, 0};
		Vertices[1].Position = {(float)Len * m_CharSize, 0};
		Vertices[2].Position = {(float)Len * m_CharSize, (float)m_CharSize};
		Vertices[3].Position = {0, (float)m_CharSize};

		Vertices[0].TexCoord = {0, 0};
		Vertices[1].TexCoord = {1, 0};
		Vertices[2].TexCoord = {1, 1};
		Vertices[3].TexCoord = {0, 1};

		Vertices[0].Color = m_Color[0] * (1 + m_Highlight);
		Vertices[1].Color = m_Color[1] * (1 + m_Highlight);
		Vertices[2].Color = m_Color[2] * (1 + m_Highlight);
		Vertices[3].Color = m_Color[3] * (1 + m_Highlight);

		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * 4, &Vertices[0], GL_STATIC_DRAW);
		m_Changed = false;
	}

	for(uint32_t i = 0; i < Len; ++i)
	{
		Shader.SetInt("Characters[" + std::to_string(i) + "]", m_Text[i]);
	}

	Shader.SetInt("NumCharacters", Len);
	Shader.SetSampler("Texture", 0);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	Shader.SetInt("NumCharacters", 0);
}

#include "Event.h"

#include "Debug.h"
#include "Level.h"
#include "PlayerClass.h"
#include "States.h"

void AddEvent(std::shared_ptr<Event> E, GameState &Game)
{
	Game.EventsOut.push_back(E);
}

void HandleEvent(std::shared_ptr<Event> E, GameState &Game)
{
	if(E->Type == EventType::Attack)
	{
		std::shared_ptr<AttackEvent> tmp = std::static_pointer_cast<AttackEvent>(E);
		HandleAttack(tmp->Pos, tmp->Dir, Game, tmp->Stats);	   //creates damage events
	}
	else if(E->Type == EventType::Damage)	 //Damage current player
	{
		std::shared_ptr<DamageEvent> tmp = std::static_pointer_cast<DamageEvent>(E);
		if(tmp->PlayerIndex == Game.PlayerIndex)
		{
			Game.Players[tmp->PlayerIndex].Damage(tmp->Damage);
		}
	}
	else if(E->Type == EventType::Spawn)
	{
		std::shared_ptr<SpawnEvent> tmp = std::static_pointer_cast<SpawnEvent>(E);
		Item I(tmp->ItemType, tmp->Pos, tmp->Rot);
		Game.Map->AddItem(I);
	}
	else if(E->Type == EventType::DeSpawn)
	{
		std::shared_ptr<DeSpawnEvent> tmp = std::static_pointer_cast<DeSpawnEvent>(E);
		Game.Map->RemoveItem(tmp->ItemIndex);
	}
	else if(E->Type == EventType::EnemyDeath)
	{
		std::shared_ptr<DeathEvent> tmp = std::static_pointer_cast<DeathEvent>(E);
		Game.CharacterCandidates[Game.CurrentCandidate].AddXP(tmp->XP);
	}
	else
	{
		DEBUG_ABORT("NOT IMPLEMENTED");
	}
}

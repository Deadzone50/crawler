#pragma once

#include <vector>

#include "vector/Vector.h"

struct NeighbourDist
{
	int RoomIndex = -1;
	float Distance = -1;
};

struct PlayerDist
{
	int RoomIndex = -1;
	float Distance = FLT_MAX;
};

struct NavZone
{
	Vec3f Position = {};
	std::vector<NeighbourDist> Neighbours;
	std::vector<PlayerDist> PlayerDistances;
};

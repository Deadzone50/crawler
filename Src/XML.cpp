#include "XML.h"

#include <fstream>

#include "Debug.h"

std::string ReadUntil(std::ifstream &file, char end)
{
	std::string Text;
	char c;
	do
	{
		file.get(c);
		Text += c;
	}
	while(c != end);
	Text.pop_back();
	return Text;
}

void SkipComment(std::ifstream &file)
{
	char c;
	while(!file.eof())
	{
		file.get(c);
		if(c == '-')
		{
			file.get(c);
			if(c == '-')
			{
				file.get(c);
				if(c == '>')
				{
					break;
				}
			}
		}
	}
}

std::string ReadWhiteSpace(std::ifstream &file, char &c)
{
	std::string Text;
	do
	{
		file.get(c);
		Text += c;
	}
	while(c == ' ' || c == '\n' || c == '\t');
	Text.pop_back();
	return Text;
}

std::string ReadText(std::ifstream &file, char &c)
{
	std::string Text;
	do
	{
		file.get(c);
		Text += c;
	}
	while(c != '<' && c != '>' && c != ' ' && c != '=');
	Text.pop_back();
	return Text;
}

XMLTag ParseTag(std::ifstream &file)
{
	XMLTag Tag;

	char c;
	Tag.Name = ReadText(file, c);
	while(c == ' ')
	{
		XMLAttribute Attr;
		Attr.Name = ReadUntil(file, '=');
		file.ignore(1);	   //"
		Attr.Value = ReadUntil(file, '"');
		Tag.Attributes.push_back(Attr);

		file.get(c);
	}

	if(c == '/')
	{
		Tag.Name += '/';
		file.ignore(1);	   //>
	}

	return Tag;
}

XMLElement ParseElement(std::ifstream &file, XMLTag MainTag)
{
	XMLElement Result;
	Result.Tag = MainTag;	 //Main tag

	char c;
	std::string WS;
	WS = ReadWhiteSpace(file, c);
	while(true)
	{
		if(c == '<')
		{
			XMLTag Tag = ParseTag(file);
			if(Tag.Name[0] == '!' && Tag.Name[1] == '-' && Tag.Name[2] == '-')
			{
				SkipComment(file);
				ReadWhiteSpace(file, c);
			}
			else if(Tag.Name[0] == '/')
			{
				break;
			}
			else if(Tag.Name.back() == '/')
			{
				Tag.Name.pop_back();
				//Result.Tags.push_back(Tag);
				XMLElement Element;
				Element.Tag = Tag;
				Result.Elements.push_back(Element);
				WS = ReadWhiteSpace(file, c);
			}
			else
			{
				XMLElement Element = ParseElement(file, Tag);
				Result.Elements.push_back(Element);
				WS = ReadWhiteSpace(file, c);
			}
		}
		else if(std::isblank(c))
		{
			DEBUG_ABORT("Attribute");
		}
		else
		{
			Result.Text += WS;
			Result.Text += c;
			Result.Text.append(ReadText(file, c));
			while(c == ' ')
			{
				Result.Text += c;
				Result.Text.append(ReadText(file, c));
			}
		}
	}
	return Result;
}

XMLElement ParseXML(const std::string &Path)
{
	XMLElement Result;
	std::ifstream file;
	file.open(Path);
	if(!file.is_open())
	{
		Log(Path + " not found");
		return Result;
	}

	std::string line, elementString;
	getline(file, line);	//?xml
	if(line.find("<?xml") == std::string::npos)
	{
		Log(Path + " is not a xml file");
		return Result;
	}
	char c;
	ReadWhiteSpace(file, c);
	XMLTag Tag = ParseTag(file);
	Result = ParseElement(file, Tag);

	return Result;
}

static int IndentCount = 0;
static std::string AddIndent()
{
	std::string Result;
	for(int i = 0; i < IndentCount; ++i)
	{
		Result += "  ";
	}
	return Result;
}

static void PrintTagOpen(const XMLTag &Tag)
{
	std::string Result = AddIndent();
	Result += "<" + Tag.Name;
	for(auto &A : Tag.Attributes)
	{
		Result += " " + A.Name + "=" + A.Value;
	}
	Result += ">";
	Log(Result);
}

static void PrintTagClose(const XMLTag &Tag)
{
	std::string Result = AddIndent();
	Result += "</" + Tag.Name + ">";
	Log(Result);
}

static void PrintElement(const XMLElement &Element)
{
	if(!Element.Text.empty())
	{
		std::string Result = AddIndent();
		Result += "<" + Element.Tag.Name;
		for(auto &A : Element.Tag.Attributes)
		{
			Result += " " + A.Name + "=" + A.Value;
		}
		if(Element.Text.size() < 50) Result += ">" + Element.Text + "</" + Element.Tag.Name + ">";
		else Result += ">" + Element.Text.substr(0, 47) + "...</" + Element.Tag.Name + ">";
		Log(Result);
	}
	else
	{
		PrintTagOpen(Element.Tag);
		++IndentCount;
		for(auto &E : Element.Elements)
		{
			PrintElement(E);
		}
		--IndentCount;
		PrintTagClose(Element.Tag);
	}
}

void PrintXML(const XMLElement &XML)
{
	PrintElement(XML);
}

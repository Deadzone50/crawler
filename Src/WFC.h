#pragma once

#include <vector>

#include "LevelObjects.h"
#include "MeshManager.h"
#include "Minimap.h"
#include "Navigation.h"
#include "Settings.h"
#include "Shader.h"
#include "vector/Matrix.h"

struct WFCModule
{
	std::string Name;
	Mesh Mesh;
	std::string NeightbourConnections[4];
	std::string UpDownConnections[2];
	NavZone NavZone;
	int Type;
	bool Decoration;
};

struct WFCNode
{
	std::vector<int> ModulePossibilities;
};

class WFCMap
{
public:
	WFCMap() {}
	//Setup grid with initial possibilities, adds extra empty levels on bottom and top
	void Setup(Vec3i Size);
	void SetupDebug();
	void GenerateWFCModules(MeshManager &MeshMan, const Settings &Set);

	//Generate final WFC map node selection
	bool Generate(Vec3i Start);
	bool StepGeneration();
	bool SelectNext();

	void DrawMap(Shader &S);

	//Creates mesh out of the level, ignores rooms that are not connected to start room
	MapObject CreateMap(uint32_t &ExitIndex);

	const Minimap &GetMinimap() { return m_Minimap; }

	std::vector<int> &GetStartRooms() { return m_EntryExitRooms; }

private:
	bool UpdatePossibilities(int X, int Y, int Z);

	std::vector<std::vector<std::vector<WFCNode>>> m_InitialNodes;
	std::vector<std::vector<std::vector<WFCNode>>> m_Nodes;
	std::vector<WFCModule> m_Modules;
	uint32_t m_CurrentX = 0, m_CurrentY = 0, m_CurrentZ = 0;
	uint32_t m_SizeX = 0;
	uint32_t m_SizeY = 0;
	uint32_t m_SizeZ = 0;

	uint32_t m_StartX = 0;
	uint32_t m_StartY = 0;
	uint32_t m_StartZ = 0;

	std::vector<int> m_EntryExitRooms;	  //Used to find suitable level exits and entries
	std::vector<std::string> m_topPossibilities;
	std::vector<std::string> m_bottomPossibilities;
	bool m_Print = false;

	Minimap m_Minimap;
};

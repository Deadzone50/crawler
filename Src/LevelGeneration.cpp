#include "Level.h"

#include <chrono>

#include "Debug.h"
#include "Globals.h"
#include "Item.h"
#include "Quaternions.h"
#include "Random.h"
#include "Scene.h"
#include "States.h"
#include "TextureManager.h"

extern bool SPAWN_ENEMIES;

void Level::SetSeed(uint32_t Seed)
{
	m_Seed = Seed;
	SeedRandomGen(Seed);
}

void Level::SetSize(Vec3i Size)
{
	m_Size = Size;
}

Vec3f Level::GenerateLevel()
{
	bool Done = false;
	m_EnemySpawnTimer = 0;
	//Setup grid with initial possibilities
	m_WFC.Setup(m_Size);
	Vec3i Start = {0, 0, 0};
	while(!Done)
	{
		Start.x = GetRandomUI(1, m_Size.x - 1);
		Start.y = GetRandomUI(1, m_Size.y - 1);
		Start.z = GetRandomUI(1, m_Size.z - 1);
		auto t0 = std::chrono::high_resolution_clock::now();
		//Generate final wfc map node selection
		if(!m_WFC.Generate(Start))
		{
			Log("Generate failed, retrying with seed: " + std::to_string(m_Seed + 1));
			SetSeed(m_Seed + 1);
			continue;
		}
		auto t1 = std::chrono::high_resolution_clock::now();
		//Create map mesh
		uint32_t ExitIndex = 0;
		m_Map = m_WFC.CreateMap(ExitIndex);
		auto t2 = std::chrono::high_resolution_clock::now();
		Log("Generate: " + std::to_string(std::chrono::duration<float>(t1 - t0).count()));
		Log("Create: " + std::to_string(std::chrono::duration<float>(t2 - t1).count()));
		Log("----");
		if(!ExitIndex)
		{
			Log("CreateMap failed, retrying with seed: " + std::to_string(m_Seed + 1));
			SetSeed(m_Seed + 1);
			continue;
		}
		PlaceObjects(0, ExitIndex);
		//PlaceEnemies(5);
		//PlaceItems(m_Map.Rooms.size()/5);
		PlaceLights();
		//DrawNavmesh();

		Done = true;
	}
	return (Start + Vec3i{1, 1, 1}) * ROOM_SIZE;
}

Vec3f Level::GenerateDebugLevel(GameState &Game)
{
	const int X = 15;
	const int Z = 15;
	const Vec3i Start = {5, 0, 5};

	SPAWN_ENEMIES = false;

	SeedRandomGen(0);

	const Vec3f Center = {X * ROOM_SIZE / 2.0f, ROOM_SIZE + 2, Z * ROOM_SIZE / 2.0f};
	m_WFC.Setup({X, 1, Z});
	m_WFC.SetupDebug();
	m_WFC.Generate(Start);

	uint32_t ExitIndex = 0;
	m_Map = m_WFC.CreateMap(ExitIndex);

	m_Map.Lights.clear();
	Light L;
	L.SetMesh(MeshMan.GetMesh("Flask"));
	L.SetPos(Center + Vec3f{0, 20, 0});
	L.SetCol({30, 30, 30});
	m_Map.Lights.push_back(L);

	m_Map.Items.clear();
	Mesh *CubeM = MeshMan.GetMesh("Cube");
	Mesh *CylinderM = MeshMan.GetMesh("Cylinder");
	Mesh *SphereM = MeshMan.GetMesh("Sphere");

	const MeshCollider MeshCol = {Collider::Type::Mesh};
	const MeshCollider AABBCol = {Collider::Type::AABB};

	//SpawnItem(ItemType::Ammo_Shotgun, {30, 10, 20}, {0, 0, 0, 1}, Game);
	//for(int i = 0; i < 50; ++i)
	//	SpawnItem(ItemType::Machete, Vec3f{30, 10, 20} + GetRandomV3(-10, 10), Quaternion({0, 0, 1}, PI / 2), Game);
	//for(int i = 0; i < 50; ++i)
	//	SpawnItem(ItemType::Pitchfork, Vec3f{30, 10, 20} + GetRandomV3(-10, 10), Quaternion({0, 0, 1}, PI / 2), Game);
	//for(int i = 0; i < 50; ++i)
	//	SpawnItem(ItemType::Polearm, Vec3f{30, 10, 20} + GetRandomV3(-10, 10), Quaternion({0, 0, 1}, PI / 2), Game);

#if 0
	Item I2;
	I2.SetPos({10, 10, 10});
	I2.SetMesh(CubeM);
	I2.SetPhysicalProperties({5, 2, 5}, 1, MomentOfInertiaType::Box, &AABBCol);
	I2.UpdateColliders();
	m_Map.Items.push_back(I2);
#endif
#if 0
	for(int i = 0; i < 100; ++i)
	{
		//Game.PhysObjects.emplace_back(Center + GetRandomV3(-15, 15) * Vec3f{1, 0, 1} + Vec3f{0, GetRandomF(2, 15), 0},
		//							  GetRandomV3(0.1f, 7), GetRandomF(1, 15), PhysObject::Type::Box, CubeM, MeshColV);
		//Game.PhysObjects.back().SetRotation(NORMALIZE(GetRandomV3(-1, 1)), GetRandomF(0, 2 * PI));

		//Item I2;
		//I2.SetPos(Center + GetRandomV3(-15, 15) * Vec3f{1, 0, 1} + Vec3f{0, GetRandomF(2, 15), 0});
		//I2.SetMesh(CubeM);
		//I2.SetPhysicalProperties(GetRandomV3(0.1f, 7), GetRandomF(1, 15), MomentOfInertiaType::Box, &AABBCol);
		//I2.UpdateColliders();
		//m_Map.Items.push_back(I2);

		//Game.PhysObjects.emplace_back(Center+GetRandomV3(-15, 15)*Vec3f{1,0,1}+Vec3f{0,GetRandomF(2,15),0}, GetRandomF(0.1f,
		//1)*Vec3f{1,10,1}, GetRandomF(1, 5), PhysObject::Type::Cylinder, CylinderM, MeshColV);
		//Game.PhysObjects.back().SetRotation(NORMALIZE(GetRandomV3(-1, 1)), GetRandomF(0, 2*PI));

		SpawnItem(ItemType::Pitchfork, Center + GetRandomV3(-15, 15) * Vec3f{1, 0, 1} + Vec3f{0, GetRandomF(2, 15), 0},
				  Quaternion({1, 0, 0}, PI / 2.0f), Game);
	}
#endif
#if 0
	for(int i = 0; i < 100; ++i)
	{
		float D = GetRandomF(1, 4);
		SphereCollider SphereCol = {Collider::Type::Sphere};
		SphereCol.Radius = D*0.5f;
		std::vector<Collider *> SphereColV = {&SphereCol};
		Game.PhysObjects.emplace_back(Center+GetRandomV3(-15, 15)*Vec3f { 1, 0, 1 }+Vec3f{0,GetRandomF(10,15),0}, D*Vec3f{1,1,1}, GetRandomF(1, 5), PhysObject::Type::Sphere, SphereM, SphereColV);
	}
#endif
#if 0
	for(int i = 0; i < 10; ++i)
	{
		m_Map.Enemies.push_back(m_EnemyMap["Cultist"]);
		EnemyClass &Enemy = m_Map.Enemies.back();
		float r = GetRandomF(0, 1);
		if(r > 0.6f) Enemy.AddWeapon("Pitchfork", 6);
		else if(r > 0.3f) Enemy.AddWeapon("Polearm", 6);
		else Enemy.AddWeapon("Machete", 6);
		Enemy.GetInventory().EquipWeapon(0, 0);
		Vec3f Pos = Center + Vec3f{0, 0, -10} + GetRandomV3(-20, 20);
		Pos.y = GetRandomF(1, 10);
		Enemy.SetPosition(Pos);
	}

	//SpawnItem(ItemType::Ammo_Shotgun, Center - Vec3f{1, 0, 0}, {0, 0, 0, 1}, Game);
	//SpawnItem(ItemType::Health, Center + Vec3f{1, 0, 0}, {0, 0, 0, 1}, Game);

#endif
	return (Start + Vec3i{1, 2, 1}) * ROOM_SIZE;
}

void Level::StepGeneration()
{
	if(m_WFC.SelectNext())
	{
		m_WFC.StepGeneration();
	}
}

void Level::PlaceEnemies(uint32_t Num)
{
	m_Map.Enemies.clear();
	uint32_t NumRooms = (uint32_t)m_Map.Rooms.size();

	if(NumRooms < 2)
	{
		Log("No room for enemies");
		return;
	}

#if 1
	for(uint32_t i = 0; i < Num; ++i)
	{
		uint32_t RoomIndex = GetRandomUI(2, NumRooms - 1);
#else
	for(uint32_t i = 0; i < 5; ++i)
	{
		uint32_t RoomIndex = 6;
#endif
		m_Map.Enemies.push_back(m_EnemyMap["Cultist"]);
		EnemyClass &Enemy = m_Map.Enemies.back();
		float r = GetRandomF(0, 3);
		if(r < 1) Enemy.AddWeapon("Machete", 6);
		//else if(r < 2)
		//Enemy.AddWeapon("Shotgun");
		else Enemy.AddWeapon("Pitchfork", 6);
		Enemy.GetInventory().EquipWeapon(0, 0);

		AxisAlignedBoundingBox AABB = m_Map.Rooms[RoomIndex].GetBoundingVolume();
		Vec3f Pos = {};
		Pos.x = GetRandomF(AABB.V1.x, AABB.V2.x);
		Pos.y = (AABB.V1.y + AABB.V2.y) / 2;
		Pos.z = GetRandomF(AABB.V1.z, AABB.V2.z);
		Enemy.SetPosition(Pos);
		float t = 0;
		Vec3f CollisionDir = MapCollisionTest(Enemy.GetCollider(), t);
		if(t != 0)
		{
			Enemy.SetPosition(Pos + t * CollisionDir);
		}
	}
}

void Level::PlaceItems(uint32_t Num)
{
	m_Map.Items.clear();
	AxisAlignedBoundingBox AABB = m_Map.Rooms[0].GetBoundingVolume();
	Vec3f Pos = {};
	Pos.y = AABB.V1.y + 0.1f;
	Pos.z = -4;
	Pos.x = -1;
	Item H(ItemType::Health, Pos, {0, 0, 0, 1});
	Pos.x = 1;
	Item A(ItemType::Ammo_Shotgun, Pos, {0, 0, 0, 1});
	AddItem(H);
	AddItem(A);
}

void Level::PlaceObjects(uint32_t EntranceIndex, uint32_t ExitIndex)
{
	const auto &StartRooms = m_WFC.GetStartRooms();

	m_Map.Objects.clear();
	Mesh *DoorMesh = MeshMan.GetMesh("Door");
	//Entrance
	{
		auto &R = m_Map.Rooms[EntranceIndex];
		AxisAlignedBoundingBox RoomAABB = R.GetBoundingVolume();
		Vec3f RoomCenter = {(RoomAABB.V1.x + RoomAABB.V2.x) / 2, RoomAABB.V1.y + 0.1f, (RoomAABB.V1.z + RoomAABB.V2.z) / 2};
		int Type = R.GetItemType();
		DoorMesh->SetTexture(TextureMan.Texture(180, 0, 0));
		Vec3f ToBackWall;
		if(Type == StartRooms[0])
		{
			ToBackWall = {0, 0, -(RoomAABB.V2.z - RoomAABB.V1.z - 0.1f) / 2};
		}
		else if(Type == StartRooms[1])
		{
			ToBackWall = {-(RoomAABB.V2.x - RoomAABB.V1.x - 0.1f) / 2, 0, 0};
		}
		else if(Type == StartRooms[2])
		{
			ToBackWall = {0, 0, (RoomAABB.V2.z - RoomAABB.V1.z - 0.1f) / 2};
		}
		else if(Type == StartRooms[3])
		{
			ToBackWall = {(RoomAABB.V2.x - RoomAABB.V1.x - 0.1f) / 2, 0, 0};
		}
		Vec3f Pos = RoomCenter + ToBackWall;
		float Angle = (Type - 1) * PI * 0.5f;

		m_Map.Objects.emplace_back(DoorMesh);
		m_Map.Objects.back().Rotate(Angle);
		m_Map.Objects.back().SetPos(Pos);
		m_Map.Objects.back().UpdateBoundingVolume();
	}
	//Exit
	{
		auto &R = m_Map.Rooms[ExitIndex];
		AxisAlignedBoundingBox RoomAABB = R.GetBoundingVolume();
		Vec3f RoomCenter = {(RoomAABB.V1.x + RoomAABB.V2.x) / 2, RoomAABB.V1.y + 0.1f, (RoomAABB.V1.z + RoomAABB.V2.z) / 2};
		int Type = R.GetItemType();
		DoorMesh->SetTexture(TextureMan.Texture(0, 180, 0));
		Vec3f ToBackWall;
		if(Type == StartRooms[0])
		{
			ToBackWall = {0, 0, -(RoomAABB.V2.z - RoomAABB.V1.z - 0.1f) / 2};
		}
		else if(Type == StartRooms[1])
		{
			ToBackWall = {-(RoomAABB.V2.x - RoomAABB.V1.x - 0.1f) / 2, 0, 0};
		}
		else if(Type == StartRooms[2])
		{
			ToBackWall = {0, 0, (RoomAABB.V2.z - RoomAABB.V1.z - 0.1f) / 2};
		}
		else if(Type == StartRooms[3])
		{
			ToBackWall = {(RoomAABB.V2.x - RoomAABB.V1.x - 0.1f) / 2, 0, 0};
		}
		Vec3f Pos = RoomCenter + ToBackWall;
		float Angle = (Type - 1) * PI * 0.5f;

		m_Map.Objects.emplace_back(DoorMesh, ExitLevel);
		m_Map.Objects.back().Rotate(Angle);
		m_Map.Objects.back().SetPos(Pos);
		m_Map.Objects.back().UpdateBoundingVolume();
	}
}

void Level::PlaceLights()
{
	m_Map.Lights.clear();
	Light L;
	L.SetMesh(MeshMan.GetMesh("Flask"));
	for(const auto &R : m_Map.Rooms)
	{
		Vec3f Pos = (R.GetBoundingVolume().V1 + R.GetBoundingVolume().V2) / 2;
		Pos.y = R.GetBoundingVolume().V2.y - 0.3f;
		L.SetPos(Pos);
		L.SetCol(GetRandomV3(0, 1));
		m_Map.Lights.push_back(L);
	}
}

void Level::Init(MeshManager &MeshMan, const Settings &Set)
{
	m_EnemyMap["Cultist"].SetMesh("Cultist");
	m_WFC.GenerateWFCModules(MeshMan, Set);
}

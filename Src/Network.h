#pragma once

#include <vector>

#include "Event.h"
#include "Socket.h"

struct GameState;
struct NetworkState;

class PlayerClass;
class EnemyClass;

//"PING REQ"
//"PING RESP"
//"HS Init"
//"HS ACK"
//"HS done"

constexpr uint8_t READY_CHAR = 'r';
constexpr uint8_t EXIT_CHAR = 'o';
constexpr uint8_t PLAYERS_CHAR = 'p';
constexpr uint8_t EVENTS_CHAR = 'e';
constexpr uint8_t ENEMIES_CHAR = 'n';

struct Msg
{
	Address Addr;
	uint8_t Data[512] = {};
};

struct PendingAddress
{
	Address Addr;
	bool HSI = false;
	bool AHS = false;
};

class NetworkManager
{
public:
	NetworkManager(uint32_t Port);
	void AddConnection(const Address &Addr);
	void AddConnections(const std::vector<Address> &Addresses);
	void CloseConnections();

	uint8_t SetupNetworking(bool AmHost);

	void PingAll();
	void Ping(const Address &Addr);
	bool Send(const Msg &M);
	bool Receive(Msg &M);
	void Handle(Msg &M, GameState &Game);

	void SendMessages(GameState &Game);
	void ReceiveMessages(GameState &Game);

	void SetHost(bool H) { m_Host = H; }
	bool AmHost() const { return m_Host; }

	float GetPing(uint32_t i);

	void SendReady(uint8_t i);
	bool IsReady(uint8_t i);
	void ClearReady();

	void SendExitMap(uint8_t i);
	bool InMap(uint8_t i);
	bool AllExitedMap();
	void ClearExitMap();

private:
	void HostSend(GameState &Game);
	void ClientSend(GameState &Game);

	void SendHandshake(const PendingAddress &PA);
	void HandleHandshake(bool &HSI, bool &AHS, Msg &M, GameState &Game);

	void HandleReady(const Msg &M);
	void HandleExit(const Msg &M);

	void SendEventsMsgs(const Address &Addr, const std::vector<std::shared_ptr<Event>> &Events);
	void SendPlayersMsgs(const Address &Addr, const std::vector<PlayerClass> &Players);
	void SendEnemiesMsgs(const Address &Addr, const std::vector<EnemyClass> &Enemies);

	void HandleEventsMsg(const Msg &M, std::vector<std::shared_ptr<Event>> &Events);
	void HandlePlayersMsg(const Msg &M, std::vector<PlayerClass> &Players);
	void HandleEnemiesMsg(const Msg &M, std::vector<EnemyClass> &Enemies);

	Socket m_Socket;
	std::vector<Address> m_Addresses;
	std::vector<PendingAddress> m_PendingAddresses;
	PendingAddress *m_CurrentHandshake = {};

	uint32_t m_PingIndex = 0;
	bool m_PingHandled = true;
	bool m_Host = true;
	bool m_DebugMulti = false;
	std::vector<float> m_Ping;
	std::vector<bool> m_Ready;
	std::vector<bool> m_Exit;

	uint32_t m_Port;
};

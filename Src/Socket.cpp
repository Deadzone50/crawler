#include <winsock2.h>

#include "Socket.h"

#include "Console.h"

bool InitializeSockets()
{
	WSADATA WsaData;
	return WSAStartup(MAKEWORD(2, 2), &WsaData) == NO_ERROR;
}

void TerminateSockets()
{
	WSACleanup();
}

Address::Address(unsigned char a, unsigned char b, unsigned char c, unsigned char d, unsigned short port)
{
	m_address = (a << 24) | (b << 16) | (c << 8) | d;
	m_port = port;
}

Address::Address(unsigned int address, unsigned short port)
{
	m_address = address;
	m_port = port;
}

bool Address::operator==(const Address &A)
{
	return m_address == A.GetAddress() && m_port == A.GetPort();
}

unsigned char Address::GetA() const
{
	return (m_address & 0xff000000) >> 24;
}

unsigned char Address::GetB() const
{
	return (m_address & 0x00ff0000) >> 16;
}

unsigned char Address::GetC() const
{
	return (m_address & 0x0000ff00) >> 8;
}

unsigned char Address::GetD() const
{
	return (m_address & 0x000000ff);
}

std::string Address::ToString() const
{
	return std::to_string((int)GetA()) + ":" + std::to_string((int)GetB()) + ":" + std::to_string((int)GetC()) + ":" +
		   std::to_string((int)GetD()) + ":" + std::to_string(GetPort());
}

unsigned int Address::GetAddress() const
{
	return m_address;
}

unsigned short Address::GetPort() const
{
	return m_port;
}

Socket::Socket()
{
	m_handle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(m_handle <= 0)
		Log("Error creating socket");
	else
		Log("Socket created");

	DWORD nonblocking = 1;
	if(ioctlsocket(m_handle, FIONBIO, &nonblocking) != 0)
		Log("Error setting non-blocking");
	else
		Log("non-blocking set");
}

Socket::~Socket()
{
	Close();
}

bool Socket::Open(unsigned short port)
{
	sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(port);
	return (bind(m_handle, (const sockaddr *)&address, sizeof(sockaddr_in)) >= 0);
}

void Socket::Close()
{
	closesocket(m_handle);
}

bool Socket::Send(const Address &destination, const void *data, int size)
{
	sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = htonl(destination.GetAddress());
	address.sin_port = htons(destination.GetPort());
	int sent_bytes = sendto(m_handle, (const char *)data, size, 0, (sockaddr *)&address, sizeof(sockaddr_in));
	return (sent_bytes == size);
}

int Socket::Receive(Address &source, void *data, int size)
{
	sockaddr_in from = {};
	int from_len = sizeof(from);

	int bytes = recvfrom(m_handle, (char *)data, size, 0, (sockaddr *)&from, &from_len);
	if(bytes > 0)
	{
		unsigned int from_address = ntohl(from.sin_addr.s_addr);
		unsigned int from_port = ntohs(from.sin_port);
		source = Address(from_address, from_port);
	}
	return bytes;
}

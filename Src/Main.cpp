#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <WinSock2.h>

#include <algorithm>
#include <chrono>
#include <filesystem>

#include "AssetLoading.h"
#include "Console.h"
#include "Debug.h"
#include "Drawer.h"
#include "Enemy.h"
#include "Event.h"
#include "FileIO.h"
#include "IO.h"
#include "Level.h"
#include "MainLoop.h"
#include "Menu.h"
#include "Mesh.h"
#include "MeshManager.h"
#include "Network.h"
#include "PlayerClass.h"
#include "Shader.h"
#include "SoundManager.h"
#include "States.h"
#include "TextureManager.h"
#include "WFC.h"
#include "vector/Vector.h"

//#define GODRAYS

bool Wireframe = false;

IOStates IO;
GameState Game;
GameState Game2;
Vec2i G_ScreenSize;
RenderState RState;
NetworkState NState;

std::vector<Menu> Menus(3);

TextureManager TextureMan;
SoundManager SoundMan;
MeshManager MeshMan;
WeaponManager WeaponMan;

MegaDrawer DrawHandler;

void SetupGraphics()
{
	//Camera matrices
	RState.ProjectionMatrixPerspective = Perspective(90.0f, (float)G_ScreenSize.x / G_ScreenSize.y, 0.1f, 100.0f);
	RState.ProjectionMatrixOrtho = Ortho(0, G_ScreenSize.x, 0, G_ScreenSize.y, 0.1f, 10.0f);

	//Screen buffers
	{
		glActiveTexture(GL_TEXTURE2);	 //activate texture unit 2
		glBindTexture(GL_TEXTURE_2D, RState.ScreenTextureId);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16, G_ScreenSize.x, G_ScreenSize.y, 0, GL_RGBA, GL_UNSIGNED_SHORT, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glBindFramebuffer(GL_FRAMEBUFFER, RState.ScreenFBO);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, RState.ScreenTextureId, 0);
		{
			glBindRenderbuffer(GL_RENDERBUFFER, RState.RenderbufferId);
			glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, G_ScreenSize.x, G_ScreenSize.y);
			glBindRenderbuffer(GL_RENDERBUFFER, 0);

			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, RState.RenderbufferId);
		}
		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if(status != GL_FRAMEBUFFER_COMPLETE) Log("FBO NOT OK!!");
	}
	//Finel render screen VAO
	{
		Vertex Vertices[4];
		Vertices[0].Position = {0, 0, -1};
		Vertices[1].Position = {(float)G_ScreenSize.x, 0, -1};
		Vertices[2].Position = {(float)G_ScreenSize.x, (float)G_ScreenSize.y, -1};
		Vertices[3].Position = {0, (float)G_ScreenSize.y, -1};
		Vertices[0].TexCoord = {0, 0};
		Vertices[1].TexCoord = {1, 0};
		Vertices[2].TexCoord = {1, 1};
		Vertices[3].TexCoord = {0, 1};

		glBindVertexArray(RState.ScreenVAO);
		glBindBuffer(GL_ARRAY_BUFFER, RState.ScreenVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * 4, &Vertices[0], GL_STATIC_DRAW);
	}
	//// switch back to window-system-provided framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glfwSetWindowSize(RState.Window, G_ScreenSize.x, G_ScreenSize.y);
}

static void InitGL()
{
	LoadTextures(MeshMan);

	///Font
	uint32_t FontId = TextureMan.Texture("Assets/Font9x9.png");
	glActiveTexture(GL_TEXTURE0);			 //activate texture unit 0
	glBindTexture(GL_TEXTURE_2D, FontId);	 //bind font texture to texture unit 0

	glGenFramebuffers(1, &RState.ScreenFBO);
	glGenTextures(1, &RState.ScreenTextureId);	  //texture for sampling

	glGenVertexArrays(1, &RState.ScreenVAO);
	glGenBuffers(1, &RState.ScreenVBO);

	glGenRenderbuffers(1, &RState.RenderbufferId);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	{
#ifdef GODRAYS
		glGenFramebuffers(1, &RState.OcclusionFBO);
		glGenTextures(1, &RState.OcclusionTextureId);
		//Occlusion buffers
		{
			glActiveTexture(GL_TEXTURE1);	 //activate texture unit 1
			glBindFramebuffer(GL_FRAMEBUFFER, RState.OcclusionFBO);
			glBindTexture(GL_TEXTURE_2D, RState.OcclusionTextureId);

			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 512, 512, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

			glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, RState.OcclusionTextureId, 0);

			GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
			if(status != GL_FRAMEBUFFER_COMPLETE) Log("FBO NOT OK!!");
		}
#endif
		//Finel render screen VAO
		{
			glBindVertexArray(RState.ScreenVAO);
			glBindBuffer(GL_ARRAY_BUFFER, RState.ScreenVBO);
			//position
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, Position));
			glEnableVertexAttribArray(0);
			//color
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, Color));
			glEnableVertexAttribArray(1);
			//Texcoord
			glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, TexCoord));
			glEnableVertexAttribArray(3);
		}
		SetupGraphics();
	}
}

static void InitAssets(Settings &Set)
{
	DrawHandler.Initialize();
	DrawHandler.ToggleDebug();

	SoundMan.Initialize(Set.Volume);
	for(const auto &Entry : std::filesystem::directory_iterator("Assets/Sound"))
	{
		SoundMan.LoadSoundFile(Entry.path().filename().string());
	}

	LoadMeshes(MeshMan);

	WeaponMan.ParseWeapons();

	Game.Map = new Level();
	Game.Map->SetSeed(Set.Seed);
	Game.Map->SetSize(Set.Size);
	Game.Map->Init(MeshMan, Set);

	Game2.Map = new Level();
	Game2.Map->SetSeed(Set.Seed);
	Game2.Map->SetSize(Set.Size);
	Game2.Map->Init(MeshMan, Set);

	Game.PlayerLevel.Initialize();
}

int main(int argc, char **argv)
{
	std::ios_base::sync_with_stdio(false);
	//HWND hwnd = GetConsoleWindow();
	//ShowWindow(hwnd, 0);
	//FreeConsole();

	//EnableLogFile();

	G_ScreenSize = Vec2i{800, 600};
	if(!InitializeSockets())
	{
		Log("Error: socket init failed");
		glfwTerminate();
		return EXIT_FAILURE;
	}

	if(!glfwInit())
	{
		Log("Error glfwInit");
		glfwTerminate();
		return EXIT_FAILURE;
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	RState.Window = glfwCreateWindow(G_ScreenSize.x, G_ScreenSize.y, "Crawler", NULL, NULL);
	if(!RState.Window)
	{
		Log("Error glfwCreateWindow");
		glfwTerminate();
		return EXIT_FAILURE;
	}
	glfwMakeContextCurrent(RState.Window);

	glfwSetKeyCallback(RState.Window, KeyCallback);
	glfwSetCursorPosCallback(RState.Window, MousePosCallback);
	glfwSetMouseButtonCallback(RState.Window, MouseButtonCallback);
	glfwSetScrollCallback(RState.Window, MouseScrollCallback);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_FRAMEBUFFER_SRGB);	  //gamma correction
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glPointSize(5);
	//glDepthFunc(GL_LESS);

	GLenum GlewStatus = glewInit();
	if(GlewStatus != GLEW_OK)
	{
		Log("Error glewInit: " + std::string((char *)glewGetErrorString(GlewStatus)));
		glfwTerminate();
		return EXIT_FAILURE;
	}
	if(!GLEW_VERSION_3_3)
	{
		Log("Error: your graphic card does not support OpenGL 3.3");
		glfwTerminate();
		return EXIT_FAILURE;
	}

	Settings Set;
	ParseSettingsFile(Set);
	size_t x = Set.Resolution.find_first_of('X');
	int W = std::stoi(Set.Resolution.substr(0, x));
	int H = std::stoi(Set.Resolution.substr(x + 1));
	G_ScreenSize = {W, H};

	RState.MegaShader = Shader("Shader/shader.vert", "Shader/shader.frag");
	RState.MegaShader.Use();
	InitGL();
	InitMainMenu(Menus, NState, RState, Set);
	InitAssets(Set);
	NState.NM = new NetworkManager(30000);

	ResetTimers();
	MainLoop(Game, RState, NState, IO);
	glfwDestroyWindow(RState.Window);
	glfwTerminate();
	return EXIT_SUCCESS;
}

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "MainLoop.h"

#include <chrono>
#include <map>

#include "BaseScreen.h"
#include "Debug.h"
#include "Drawer.h"
#include "Enemy.h"
#include "IO.h"
#include "Level.h"
#include "Network.h"
#include "PlayerClass.h"
#include "Random.h"
#include "SoundManager.h"
#include "States.h"
#include "vector/Matrix.h"

bool ConsoleOpen = false;
BaseScreen BScreen;

extern bool Wireframe;
extern bool STOP_TIME;
extern bool SPAWN_ENEMIES;
extern bool DRAW_DEBUG;
extern Vec2i G_ScreenSize;
extern MegaDrawer DrawHandler;
extern GameState Game2;

std::map<std::string, float> G_TimeMeasurements;

static void PrintTimestamps()
{
	for(const auto &T : G_TimeMeasurements)
	{
		Log(T.first + ":" + std::to_string(T.second));
	}
	ClearTimestamps();
}

static void DrawDebug(const GameState &Game)
{
	Game.Map->DrawNavmesh();
	Game.Map->DrawOctree();
	Game.Map->DrawColliders();
};

//void DrawFanWorld(Shader &MegaShader)
//{
//	Mat4f ModelMatrix;
//	for(int x = 0; x < 10; ++x)
//	{
//		//Rander Fans Roof
//		for(int y = 0; y < 10; ++y)
//		{
//			ModelMatrix.Identity();
//			ModelMatrix.SetRotation({3.14f/2,0,0});
//			ModelMatrix.Translate({x*3.0f, 7.5f, y*3.0f});
//			MegaShader.SetMat4("Model", ModelMatrix);
//			MeshMap["Shaft"].Draw();
//			ModelMatrix.Identity();
//			ModelMatrix.SetRotation({3.14f/2,0,0});
//			ModelMatrix.Rotate({0,Time*0.4f+x*y,0});
//			ModelMatrix.Translate({x*3.0f, 7.5f, y*3.0f});
//			MegaShader.SetMat4("Model", ModelMatrix);
//			MeshMap["Fan"].Draw();
//		}
//		//Rander Fans Walls
//		for(int y = 0; y < 3; ++y)
//		{
//			ModelMatrix.Identity();
//			ModelMatrix.Translate({x*3.0f, y*3.0f, 0});
//			ModelMatrix.Rotate({0,3.14f/2,0});
//			MegaShader.SetMat4("Model", ModelMatrix);
//			MeshMap["Shaft"].Draw();
//
//			ModelMatrix.Identity();
//			ModelMatrix.Rotate({0,0,Time*0.4f+x*y});
//			ModelMatrix.Translate({x*3.0f, y*3.0f, 0});
//			ModelMatrix.Rotate({0,3.14f/2,0});
//			MegaShader.SetMat4("Model", ModelMatrix);
//			MeshMap["Fan"].Draw();
//
//			ModelMatrix.Identity();
//			ModelMatrix.Translate({x*3.0f, y*3.0f, 27.5f});
//			MegaShader.SetMat4("Model", ModelMatrix);
//			MeshMap["Shaft"].Draw();
//
//			ModelMatrix.Identity();
//			ModelMatrix.Rotate({0,0,Time*0.4f+x*y});
//			ModelMatrix.Translate({x*3.0f, y*3.0f, 27.5f});
//			MegaShader.SetMat4("Model", ModelMatrix);
//			MeshMap["Fan"].Draw();
//
//			ModelMatrix.Identity();
//			ModelMatrix.Translate({-x*3.0f, y*3.0f, 0});
//			ModelMatrix.Rotate({0,3.14f,0});
//			MegaShader.SetMat4("Model", ModelMatrix);
//			MeshMap["Shaft"].Draw();
//
//			ModelMatrix.Identity();
//			ModelMatrix.Rotate({0,0,Time*0.4f+x*y});
//			ModelMatrix.Translate({-x*3.0f, y*3.0f, 0});
//			ModelMatrix.Rotate({0,3.14f,0});
//			MegaShader.SetMat4("Model", ModelMatrix);
//			MeshMap["Fan"].Draw();
//
//			ModelMatrix.Identity();
//			ModelMatrix.Translate({-x*3.0f, y*3.0f, 27.5f});
//			ModelMatrix.Rotate({0,3*3.14f/2,0});
//			MegaShader.SetMat4("Model", ModelMatrix);
//			MeshMap["Shaft"].Draw();
//
//			ModelMatrix.Identity();
//			ModelMatrix.Rotate({0,0,Time*0.4f+x*y});
//			ModelMatrix.Translate({-x*3.0f, y*3.0f, 27.5f});
//			ModelMatrix.Rotate({0,3*3.14f/2,0});
//			MegaShader.SetMat4("Model", ModelMatrix);
//			MeshMap["Fan"].Draw();
//		}
//	}
//	//Floor
//	glBindVertexArray(ScreenVAO);
//	ModelMatrix.Identity();
//	ModelMatrix.SetRotation({3.14f/2,0,0});
//	ModelMatrix.Translate({-30.0f, 13.0f, 30.0f});
//	MegaShader.SetMat4("Model", ModelMatrix);
//	glDrawArrays(GL_QUADS, 0, 4);
//}

static void CleanDeadEnemies(std::vector<EnemyClass> &Enemies)
{
	//std::vector<EnemyClass> NewList;
	//NewList.reserve(Enemies.size());
	//for(const auto &E : Enemies)
	//{
	//	if(!E.ToRemove()) NewList.push_back(E);
	//}
	//Enemies = std::move(NewList);
}

static void CleanItems(std::vector<Item> &Items)
{
	//std::vector<Item> NewList;
	//NewList.reserve(Items.size());
	//for(const auto &I : Items)
	//{
	//	if(!I.ToRemove()) NewList.push_back(I);
	//}
	//Items = std::move(NewList);
}

static void StepGame(GameState &Game, NetworkState &NState, float Gamedt)
{
	//DebugTimestamp("StepGame", 0);
	const bool Host = NState.NM->AmHost();
	if(Host)
	{
		Game.Map->UpdateNavigation(Game);
		if(SPAWN_ENEMIES) Game.Map->SpawnEnemies(Game, Gamedt);
		auto &Enemies = Game.Map->GetEnemies();
		for(auto &E : Enemies)
		{
			E.Step(Gamedt, Game);
		}
		Game.Map->HandleCharacterCollisions(Game.Players);

		const float Physdt = Gamedt * !STOP_TIME * 1.0f;
		auto &Items = Game.Map->GetItems();
		const int Num = Items.size();
		int NumActive = 0;
		for(int i1 = 0; i1 < Num; ++i1)
		{
			Items[i1].Step(Physdt);
			if(Items[i1].IsActive()) ++NumActive;
		}
		Log("Active: " + std::to_string(NumActive));
		for(int i = 0; i < 5; ++i)
		{
			for(int i1 = 0; i1 < Num; ++i1)
			{
				auto &O1 = Items[i1];
				HandleCollisionWorld(Game.Map, O1);
				for(int i2 = i1 + 1; i2 < Num; ++i2)
				{
					auto &O2 = Items[i2];
					HandleCollision(O1, O2);
				}
			}
		}

		//static float CleanupTimer = 10;
		//CleanupTimer -= Gamedt;
		//if(CleanupTimer < 0)
		//{
		//	CleanItems(Items);
		//	CleanDeadEnemies(Enemies);
		//	CleanupTimer = 10;
		//}
	}
	//DebugTimestamp("StepGame", 1);
	if(!NState.Multiplayer)
	{
		Game.EventsIn = Game.EventsOut;
		Game.EventsOut.clear();
	}
	for(auto &E : Game.EventsIn)
	{
		HandleEvent(E, Game);
	}
	//DebugTimestamp("StepGame", 2);
	Game.EventsIn.clear();
	if(NState.DebugMulti)	 //Needed
	{
		SoundMan.Disable();
		for(auto &E : Game2.EventsIn)
		{
			HandleEvent(E, Game2);
		}
		SoundMan.Enable();
		Game2.EventsIn.clear();
	}
	//DebugTimestamp("StepGame", 3);
	Game.Player->Step(Gamedt, Game);
	Game.Map->Highlight(Game.Player->GetCameraPos(), Game.Player->GetLookingDir());
	Game.Player->DrawMinimap();
	if(DRAW_DEBUG) DrawDebug(Game);
	//DebugTimestamp("StepGame", 4);

	if(NState.DebugMulti)
	{
		if(Game2.Player->IsAlive())
		{
			if(GetRandomF(0, 1) > 0.99f)
			{
				Game2.Player->Attack();
			}
			if(GetRandomF(0, 1) > 0.99f)
			{
				Game2.Player->Jump();
			}
			Game2.Player->Rotate({0, Gamedt / 2});
			Game2.Player->SetMovement({0, 0, -1});
		}
		Game2.Player->Step(Gamedt, Game2);
	}
	//DebugTimestamp("StepGame", 5);
}

extern MeshManager MeshMan;

void Render(RenderState &RState, GameState &Game, float dt)
{
	Mat4f ModelMatrix;
	RState.MegaShader.SetMat4("Model", ModelMatrix);
	RState.MegaShader.SetMat4("View", Game.Player->GetViewMatrix());
	RState.MegaShader.SetMat4("Projection", RState.ProjectionMatrixPerspective);
	RState.MegaShader.SetVec3("CameraPos", Game.Player->GetCameraPos());
#ifdef GODRAYS
	{
		glActiveTexture(GL_TEXTURE1);	 //Occlusion
		MegaShader.SetBool("OcclusionPass", true);
		glBindFramebuffer(GL_FRAMEBUFFER, OcclusionFBO);
		glViewport(0, 0, 512, 512);

		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//DrawFanWorld();
		Map.Draw();

		glBindTexture(GL_TEXTURE_2D, 0);
		glBindTexture(GL_TEXTURE_2D, OcclusionTextureId);
		MegaShader.SetBool("OcclusionPass", false);
	}
#endif
	{
		glBindFramebuffer(GL_FRAMEBUFFER, RState.ScreenFBO);	//Render to texture
		glViewport(0, 0, G_ScreenSize.x, G_ScreenSize.y);

		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		if(Wireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		auto &LV = Game.Map->GetLights();
		int NumLights = 0;
		for(auto &L : LV)
		{
			if(NumLights >= 256) break;
			L.AddToRender(RState.MegaShader, NumLights++);

			ModelMatrix.SetPosition(L.GetPos());
			RState.MegaShader.SetMat4("Model", ModelMatrix);
			L.Draw(RState.MegaShader);
		}
		RState.MegaShader.SetInt("NumLights", NumLights);
		ModelMatrix.Identity();
		RState.MegaShader.SetMat4("Model", ModelMatrix);

		//DrawFanWorld();
		Game.Map->Draw(RState.MegaShader);
		//Game.Map->DrawWFC(RState.MegaShader);
		for(auto &E : Game.Map->GetEnemies())
		{
			E.DrawCharacter(RState.MegaShader);
		}
		for(const auto &I : Game.Map->GetItems())
		{
			I.Draw(RState.MegaShader);
		}
		for(uint8_t i = 0; i < Game.Players.size(); ++i)
		{
			if(i != Game.PlayerIndex) Game.Players[i].DrawCharacter(RState.MegaShader);
		}
		Game.Player->DrawWeapon(RState.MegaShader);

		if(Wireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		ModelMatrix.Identity();
		RState.MegaShader.SetMat4("Model", ModelMatrix);

		RState.MegaShader.SetBool("Basic", true);
		DrawHandler.RenderGraphics(RState.MegaShader, dt);
		DrawHandler.RenderDebugGraphics(RState.MegaShader);
		RState.MegaShader.SetBool("Basic", false);

		///SCREENSPACE
		{
			RState.MegaShader.SetMat4("Projection", RState.ProjectionMatrixOrtho);
			Mat4f ViewMatrix;
			ViewMatrix.SetRow(0, {1, 0, 0, 0});
			ViewMatrix.SetRow(1, {0, 1, 0, 0});
			ViewMatrix.SetRow(2, {0, 0, 1, -1});	//z is toward camera, translation is reversed
			ViewMatrix.SetRow(3, {0, 0, 0, 1});
			RState.MegaShader.SetMat4("View", ViewMatrix);
			RState.MegaShader.SetInt("NumLights", 0);
			glClear(GL_DEPTH_BUFFER_BIT);
			Game.Player->RenderUI(RState.MegaShader);
		}
	}
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);	 //Render to screen
		glClearColor(0.0f, 0.5f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		ModelMatrix.Identity();
		RState.MegaShader.SetMat4("Model", ModelMatrix);

		glActiveTexture(GL_TEXTURE2);	 //Bind Screentexture
		glBindTexture(GL_TEXTURE_2D, RState.ScreenTextureId);
#ifdef GODRAYS
		//Final

		Vec3f LightPos = {0, 30, 0};												  //Worldpos
		Vec4f Tmp = ProjectionMatrixPerspective * ViewMatrix * Vec4f{LightPos, 1};	  //Screenpos
		Vec2f SSPos;
		SSPos.x = (Tmp.x / Tmp.z + 1) / 2;	  //texture cordinates goes from 0 to 1, while screen cordinates
		SSPos.y = (Tmp.y / Tmp.z + 1) / 2;	  //goes from -1 to 1, TODO: negative z is behind the viewer
		MegaShader.SetVec2("ScreenLightPos", SSPos);

		MegaShader.SetBool("FinalPass", true);
		MegaShader.SetSampler("OcclusionTexture", 1);
		MegaShader.SetSampler("RenderTexture", 2);

		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		MegaShader.SetBool("FinalPass", false);
#else
		RState.MegaShader.SetBool("RenderToScreen", true);
		RState.MegaShader.SetSampler("RenderTexture", 2);
		glBindVertexArray(RState.ScreenVAO);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		RState.MegaShader.SetBool("RenderToScreen", false);
#endif
	}
}

void RenderConsole(RenderState &RState, uint32_t Start, uint8_t Num)
{
	DrawConsole(Start, Num);
}

void HandleConsole(uint32_t &S, uint8_t &N, IOStates &IO)
{
	static int Offset = 0;

	bool T = IO.KeyStates[Key::RUN];

	if(IO.KeyStates[Key::FORWARD]) Offset -= 1 + T * 10;
	else if(IO.KeyStates[Key::BACKWARD]) Offset += 1 + T * 10;
	else if(IO.KeyStates[Key::JUMP]) Offset = 0;

	int LogSize = (int)GetConsoleLogSize();
	int Num = 30;
	int Start = Offset + LogSize - Num;
	if(Num >= LogSize)
	{
		Start = 0;
		Num = LogSize;
	}
	else
	{
		if(Start < 0)
		{
			Offset = Num - LogSize;
			Start = 0;
		}
		else if(Num + Start >= LogSize)
		{
			Start = LogSize - Num;
			Offset = 0;
		}
	}
	S = Start;
	N = Num;
}

std::chrono::high_resolution_clock::time_point t0Render;
std::chrono::high_resolution_clock::time_point t0Game;
std::chrono::high_resolution_clock::time_point t0;
std::chrono::high_resolution_clock::time_point t0Network;

void ResetTimers()
{
	auto now = std::chrono::high_resolution_clock::now();
	t0Render = now;
	t0Game = now;
	t0Network = now;
	t0 = now;
}

void MainLoop(GameState &Game, RenderState &RState, NetworkState &NState, IOStates &IO)
{
	float GameTimeMs = 0;
	float NetworkTimeMs = 0;
	float RenderTimeMs = 0;
	float LoopTimeMs = 0;
	while(!glfwWindowShouldClose(RState.Window))
	{
		//DebugTimestamp("Main", 0);
		auto StartTimePoint = std::chrono::high_resolution_clock::now();
		float Renderdt = std::chrono::duration<float>(StartTimePoint - t0Render).count();
		float Gamedt = std::chrono::duration<float>(StartTimePoint - t0Game).count();
		float Networkdt = std::chrono::duration<float>(StartTimePoint - t0Network).count();
		if(IO.KeyStates[Key::CONSOLE] && IO.KeyChanged[Key::CONSOLE])
		{
			ToggleGameConsole();
			IO.KeyChanged[Key::CONSOLE] = false;
		}
		if(IO.KeyStates[Key::QUIT]) glfwSetWindowShouldClose(RState.Window, true);

		//DebugTimestamp("Main", 1);
		if(NState.Multiplayer)
		{
			auto t0Network2 = std::chrono::high_resolution_clock::now();
			Vec3f MyPos = Game.Player->GetPosition();	 //TODO: do this smarter
			Vec2f MyDir = Game.Player->GetFacingDir();
			CharacterState MyState = Game.Player->GetState();
			uint8_t WI = Game.Player->GetInventory().GetCurrentWeaponIndex();
			NState.NM->ReceiveMessages(Game);
			if(!NState.NM->AmHost())
			{
				Game.Player->SetPosition(MyPos);
				Game.Player->SetFacingDir(MyDir);
				Game.Player->SetState(MyState);
				Game.Player->GetInventory().SwitchWeapon(WI);
			}
			if(NState.DebugMulti)
			{
				Vec3f MyPos = Game2.Player->GetPosition();	  //TODO: do this smarter
				Vec2f MyDir = Game2.Player->GetFacingDir();
				CharacterState MyState = Game2.Player->GetState();
				uint8_t WI = Game2.Player->GetInventory().GetCurrentWeaponIndex();
				NState.DNM->ReceiveMessages(Game2);
				Game2.Player->SetPosition(MyPos);
				Game2.Player->SetFacingDir(MyDir);
				Game2.Player->SetState(MyState);
				Game2.Player->GetInventory().SwitchWeapon(WI);
			}
			if(NState.Multiplayer && Networkdt * 1000 > 16)
			{
				t0Network = std::chrono::high_resolution_clock::now();
				NState.NM->SendMessages(Game);
				if(NState.DebugMulti)
				{
					NState.DNM->SendMessages(Game2);
				}
			}
			NetworkTimeMs = std::chrono::duration<float>(std::chrono::high_resolution_clock::now() - t0Network2).count() * 1000;
		}
		//DebugTimestamp("Main", 2);

		static uint32_t Start = 0;
		static uint8_t Num = 20;
		if(Gamedt * 1000 > 8)
		{
			t0Game = std::chrono::high_resolution_clock::now();
			DrawHandler.Clear();
			DrawHandler.ClearDebug();

			if(ConsoleOpen)
			{
				HandleConsole(Start, Num, IO);
			}
			else if(Game.CurrentScene == SceneType::Game)
			{
				if(Game.Player->m_ShowInventory)
				{
					HandleMouseInventory();
					HandleKeysInventory();
				}
				else
				{
					HandleMousePlayer();
					if(Game.Player->IsAlive()) HandleKeysPlayer();
				}
				HandleKeysDebug();
				HandleKeysCommon();

				ResetChangedKeys();
			}
			if(!NState.Multiplayer && Game.InMap && Game.CurrentScene != SceneType::Menu || !NState.NM->AllExitedMap())
				StepGame(Game, NState, Gamedt);

			GameTimeMs = std::chrono::duration<float>(std::chrono::high_resolution_clock::now() - t0Game).count() * 1000;
		}
		//DebugTimestamp("Main", 3);
		if(Renderdt * 1000 > 4)	   //slow down reneding 4ms minimum
		{
			t0Render = std::chrono::high_resolution_clock::now();

			static uint8_t Frame = 0;
			if(!(++Frame % 60))	   //every 60th frame
			{
				Frame = 0;

				int y = 0;
				std::string Tmp;
				DrawHandler.ClearText();
				DrawHandler.ClearDebugText();

				Tmp = "Map seed " + std::to_string(Game.Map->GetSeed());
				DrawHandler.DrawDebugTextLine(Tmp, 10);
				if(Game.Player)
				{
					Tmp = "POS " + std::to_string(Game.Player->GetPosition().x) + ", " +
						  std::to_string(Game.Player->GetPosition().y) + ", " + std::to_string(Game.Player->GetPosition().z);
					DrawHandler.DrawDebugTextLine(Tmp, 10);
				}
				Tmp = "N: " + std::to_string(NetworkTimeMs) + "ms G: " + std::to_string(GameTimeMs) +
					  "ms R: " + std::to_string(RenderTimeMs) + "ms";
				DrawHandler.DrawTextLine(Tmp, 10);
				if(NState.Multiplayer)
				{
					Tmp = std::to_string(NState.NM->GetPing(0)) + " ms";
					DrawHandler.DrawDebugTextLine(Tmp, 10);
				}
			}

			if(Game.CurrentScene == SceneType::Game) Render(RState, Game, Renderdt);
			else
			{
				glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				glViewport(0, 0, G_ScreenSize.x, G_ScreenSize.y);

				RState.MegaShader.SetMat4("Projection", RState.ProjectionMatrixOrtho);
				Mat4f ViewMatrix, ModelMatrix;
				ViewMatrix.SetRow(0, {1, 0, 0, 0});
				ViewMatrix.SetRow(1, {0, 1, 0, 0});
				ViewMatrix.SetRow(2, {0, 0, 1, -1});	//z is reversed, translation is reversed
				ViewMatrix.SetRow(3, {0, 0, 0, 1});
				RState.MegaShader.SetMat4("View", ViewMatrix);
				RState.MegaShader.SetMat4("Model", ModelMatrix);

				if(ConsoleOpen) RenderConsole(RState, Start, Num);
				else if(Game.CurrentScene == SceneType::Base)
				{
					DrawHandler.Clear();
					DrawHandler.ClearDebug();
					BScreen.HandleAndDraw(RState.MegaShader, IO, Game);
				}
				else
				{
					Game.CurrentMenu->Handle(IO);
					Game.CurrentMenu->Draw(RState.MegaShader);
				}
				RState.MegaShader.SetMat4("Model", ModelMatrix);
				RState.MegaShader.SetBool("Basic", true);
				DrawHandler.RenderDebugGraphics(RState.MegaShader);
				DrawHandler.RenderGraphics(RState.MegaShader, Renderdt);
				RState.MegaShader.SetBool("Basic", false);
			}

			glClear(GL_DEPTH_BUFFER_BIT);
			DrawHandler.RenderTextLines(RState.MegaShader);
			DrawHandler.RenderDebugTextLines(RState.MegaShader);
			glfwSwapBuffers(RState.Window);
			RenderTimeMs = std::chrono::duration<float>(std::chrono::high_resolution_clock::now() - t0Render).count() * 1000;
		}
		glfwPollEvents();
		//DebugTimestamp("Main", 4);
		//PrintTimestamps();
	}
}

#pragma once

#include "vector/Vector.h"

union AxisAlignedBoundingBox
{
	struct
	{
		Vec3f Min, Max;
	};
	struct
	{
		Vec3f V1, V2;
	};
	Vec3f V[2];
};

void DrawAABB(const AxisAlignedBoundingBox &AABB, Vec3f Col);

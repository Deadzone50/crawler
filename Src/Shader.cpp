#include <GL/glew.h>

#include <fstream>
#include <sstream>

#include "Console.h"
#include "Shader.h"

Shader::Shader(const char *VertexPath, const char *FragmentPath, const char *GeometryPath)
{
	std::string VertexCode, FragmentCode, GeometryCode;
	std::ifstream VShaderFile, FShaderFile, GShaderFile;

	try
	{
		VShaderFile.open(VertexPath);
		FShaderFile.open(FragmentPath);
		std::stringstream VShaderStream, FShaderStream;

		VShaderStream << VShaderFile.rdbuf();
		FShaderStream << FShaderFile.rdbuf();

		VShaderFile.close();
		FShaderFile.close();

		VertexCode = VShaderStream.str();
		FragmentCode = FShaderStream.str();

		if(GeometryPath)
		{
			GShaderFile.open(GeometryPath);
			std::stringstream GShaderStream;

			GShaderStream << GShaderFile.rdbuf();
			GShaderFile.close();
			GeometryCode = GShaderStream.str();
		}
	}
	catch(std::ifstream::failure e)
	{
		Log("ERROR shader file read error");
	}

	const char *VCode = VertexCode.c_str();
	const char *FCode = FragmentCode.c_str();
	const char *GCode = GeometryCode.c_str();

	unsigned int VertexShader, FragmentShader, GeometryShader;
	int Success;
	char InfoLog[512];

	VertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(VertexShader, 1, &VCode, NULL);
	glCompileShader(VertexShader);

	glGetShaderiv(VertexShader, GL_COMPILE_STATUS, &Success);
	if(!Success)
	{
		glGetShaderInfoLog(VertexShader, 512, NULL, InfoLog);
		Log("ERROR, VertexShader compile failed for: " + std::string((char *)VertexPath) + ", error: " + InfoLog);
	}

	FragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(FragmentShader, 1, &FCode, NULL);
	glCompileShader(FragmentShader);
	glGetShaderiv(FragmentShader, GL_COMPILE_STATUS, &Success);
	if(!Success)
	{
		glGetShaderInfoLog(FragmentShader, 512, NULL, InfoLog);
		Log("ERROR, FragmentShader compile failed for: " + std::string((char *)FragmentPath) + ", error: " + InfoLog);
	}

	if(GeometryPath)
	{
		GeometryShader = glCreateShader(GL_GEOMETRY_SHADER);
		glShaderSource(GeometryShader, 1, &GCode, NULL);
		glCompileShader(GeometryShader);
		glGetShaderiv(GeometryShader, GL_COMPILE_STATUS, &Success);
		if(!Success)
		{
			glGetShaderInfoLog(GeometryShader, 512, NULL, InfoLog);
			Log("ERROR, GeometryShader compile failed: " + std::string(InfoLog));
		}
	}
	ID = glCreateProgram();
	glAttachShader(ID, VertexShader);
	glAttachShader(ID, FragmentShader);
	if(GeometryPath)
	{
		glAttachShader(ID, GeometryShader);
	}
	glLinkProgram(ID);
	glGetProgramiv(ID, GL_LINK_STATUS, &Success);
	if(!Success)
	{
		glGetProgramInfoLog(ID, 512, NULL, InfoLog);
		Log("ERROR, ShaderProgram link failed: " + std::string(InfoLog));
		exit(1);
	}
	glDeleteShader(VertexShader);
	glDeleteShader(FragmentShader);
	if(GeometryPath)
	{
		glDeleteShader(GeometryShader);
	}
}

void Shader::Use()
{
	glUseProgram(ID);
}

void Shader::SetBool(const std::string &Name, bool Value) const
{
	glUniform1i(glGetUniformLocation(ID, Name.c_str()), Value);
}

void Shader::SetInt(const std::string &Name, int Value) const
{
	glUniform1i(glGetUniformLocation(ID, Name.c_str()), Value);
}

void Shader::SetSampler(const std::string &Name, int Value) const
{
	SetInt(Name, Value);
}

void Shader::SetFloat(const std::string &Name, float Value) const
{
	glUniform1f(glGetUniformLocation(ID, Name.c_str()), Value);
}

void Shader::SetVec2(const std::string &Name, const Vec2f &Vec) const
{
	glUniform2f(glGetUniformLocation(ID, Name.c_str()), Vec.x, Vec.y);
}

void Shader::SetVec3(const std::string &Name, const Vec3f &Vec) const
{
	glUniform3f(glGetUniformLocation(ID, Name.c_str()), Vec.x, Vec.y, Vec.z);
}

void Shader::SetMat4(const std::string &Name, const Mat4f &Mat) const
{
	glUniformMatrix4fv(glGetUniformLocation(ID, Name.c_str()), 1, GL_TRUE, Mat.m);
}

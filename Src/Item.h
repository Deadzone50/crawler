#pragma once

#include "Colliders.h"
#include "Debug.h"
#include "Identifiable.h"
#include "Mesh.h"
#include "Shader.h"
#include "Structs.h"
#include "vector/Vector.h"

constexpr float MACHETE_RANGE = 1.0f;
constexpr float PITCHFORK_RANGE = 2.0f;
constexpr float POLEARM_RANGE = 1.5f;
constexpr float SHOTGUN_RANGE = 20.0f;

struct GameState;

enum class ItemType : uint8_t
{
	None,
	Ammo_44,
	Ammo_Shotgun,
	Health,
	Pitchfork,
	Polearm,
	Machete,
	Revolver,
	Shotgun,
	Machinegun
};

enum class MomentOfInertiaType
{
	None,
	Box,
	Cylinder,
	Sphere
};

inline ItemType StringToItemType(const std::string &S)
{
	if(S == "Ammo_44") return ItemType::Ammo_44;
	if(S == "Ammo_Shotgun") return ItemType::Ammo_Shotgun;
	if(S == "Health") return ItemType::Health;
	if(S == "Pitchfork") return ItemType::Pitchfork;
	if(S == "Polearm") return ItemType::Polearm;
	if(S == "Machete") return ItemType::Machete;
	if(S == "Revolver") return ItemType::Revolver;
	if(S == "Shotgun") return ItemType::Shotgun;
	if(S == "Machinegun") return ItemType::Machinegun;
	DEBUG_ABORT("String does not match tag");
	return ItemType::None;
}

struct ItemProperties
{
	ItemType Type = ItemType::None;
	const Mesh *Mesh = nullptr;
	std::string Name;
};

class Item : public Identifiable
{
public:
	Item();
	Item(ItemType Type, Vec3f Pos, Vec4f RotationQuat);
	void DrawModel(Shader &Shader) const { m_Properties.Mesh->Draw(Shader); }	 //Draw Model
	void Draw(Shader &Shader) const;
	void SetMesh(const Mesh *Mesh) { m_Properties.Mesh = Mesh; }
	const Mesh *GetMesh() const { return m_Properties.Mesh; }
	Vec3f GetPos() const { return m_Pos; }
	AxisAlignedBoundingBox GetAABB() const;
	ItemType GetItemType() const { return m_Properties.Type; }

	void SetProperties(const ItemProperties &Properties);

	//PHYSICS
	void Step(float dt);

	//Parameters in worldspace
	void AddImpulse(Vec3f Impulse, Vec3f ToPoint);
	void SetPos(Vec3f P) { m_Pos = P; }
	void Move(Vec3f P) { m_Pos += P; }

	bool IsActive() const { return m_Active; }
	Vec3f GetVelocity() const { return m_Velocity; }
	Vec3f GetAngularVelocity() const { return m_AngularVelocity; }

	float GetInvMass() const { return m_InverseMass; }
	Mat3f GetInvMomentOfInertiaWorld() const { return m_InverseMomentOfInertiaWorld; }
	float GetRestitution() const { return m_Restitution; }

	float GetMaxDistSqr() const { return m_MaxCornerDistSqr; };

	void SetColliders(std::vector<Collider *> Colliders) { m_Colliders = Colliders; }
	const std::vector<Collider *> &GetColliders() const { return m_Colliders; }

	void SetPhysicalProperties(Vec3f Scale, float InverseMass, MomentOfInertiaType MOIType);

	void SetRotation(Vec4f Quaternion);

	void UpdateColliders();

private:
	ItemProperties m_Properties;

	//Physics
	Vec3f m_Pos, m_Velocity, m_Force;
	Vec3f m_Torque, m_AngularVelocity;

	Mat3f m_RotationMatrix, m_InvRotationMatrix;
	Mat3f m_InverseMomentOfInertia, m_InverseMomentOfInertiaWorld;
	float m_InverseMass;
	float m_Restitution;

	//Recently Weighted Average Energy
	float m_RWASleep;
	float m_RWAEnergy;
	bool m_Active;

	Vec3f m_Scale;
	float m_MaxCornerDistSqr;

	std::vector<Collider *> m_Colliders;	//TODO: handle this better
};

void SpawnItem(ItemType Type, Vec3f Pos, Vec4f Rot, GameState &Game);

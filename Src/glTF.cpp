#include "glTF.h"

#include <map>
#include <set>

#include "JSON.h"
#include "vector/Matrix.h"

//componentType: 5126 = float, 5123 = unsigned short

uint8_t Byte = 0;
uint8_t ByteIndex = 8;
static void Write6Bits(uint8_t b, std::vector<uint8_t> &Result)
{
	for(int i = 5; i >= 0; --i)
	{
		Byte |= bool(b & (1U << i)) << --ByteIndex;
		if(ByteIndex == 0)
		{
			Result.push_back(Byte);
			ByteIndex = 8;
			Byte = 0;
		}
	}
}

static std::vector<uint8_t> Base64Decode(const std::string &Data)
{
	Byte = 0;
	ByteIndex = 8;
	std::vector<uint8_t> Result;
	Result.reserve(Data.size() * 6);
	for(auto &c : Data)
	{
		if(c == '+') Write6Bits(62, Result);
		else if(c == '/') Write6Bits(63, Result);
		else if(c == '=')
		{
		}
		else
		{
			uint8_t i = c - '0';
			if(i <= 9)	  //0-9
			{
				Write6Bits(i + 52, Result);
			}
			else
			{
				i = c - 'A';
				if(i < 26)	  //A-Z
				{
					Write6Bits(i, Result);
				}
				else
				{
					i = c - 'a' + 26;
					Write6Bits(i, Result);
				}
			}
		}
	}
	return Result;
}

struct BufferInfo
{
	int BufferIndex;
	int Offset;
	int Count;
	//Type ?
	//ComponentType
};

static std::vector<float> ReadFloats(const BufferInfo &BI, const std::vector<std::vector<uint8_t>> &Buffers)
{
	auto &Data = Buffers[BI.BufferIndex];
	std::vector<float> Result;
	Result.reserve(BI.Count);
	float F;
	uint32_t fb;
	for(int i = 0; i < BI.Count; ++i)
	{
		fb = Data[BI.Offset + i * 4 + 3] << 24 | Data[BI.Offset + i * 4 + 2] << 16 | Data[BI.Offset + i * 4 + 1] << 8 |
			 Data[BI.Offset + i * 4 + 0];
		std::memcpy(&F, &fb, 4);
		Result.push_back(F);
	}
	return Result;
}

static std::vector<Mat4f> ReadMat4fs(const BufferInfo &BI, const std::vector<std::vector<uint8_t>> &Buffers)
{
	auto &Data = Buffers[BI.BufferIndex];
	std::vector<Mat4f> Result;
	Result.reserve(BI.Count);
	Mat4f M;
	Vec4f V;
	uint32_t fb[4];
	for(int i1 = 0; i1 < BI.Count; ++i1)
	{
		for(int i2 = 0; i2 < 4; ++i2)
		{
			fb[0] = Data[BI.Offset + i1 * 64 + i2 * 16 + 3] << 24 | Data[BI.Offset + i1 * 64 + i2 * 16 + 2] << 16 |
					Data[BI.Offset + i1 * 64 + i2 * 16 + 1] << 8 | Data[BI.Offset + i1 * 64 + i2 * 16 + 0];
			fb[1] = Data[BI.Offset + i1 * 64 + i2 * 16 + 7] << 24 | Data[BI.Offset + i1 * 64 + i2 * 16 + 6] << 16 |
					Data[BI.Offset + i1 * 64 + i2 * 16 + 5] << 8 | Data[BI.Offset + i1 * 64 + i2 * 16 + 4];
			fb[2] = Data[BI.Offset + i1 * 64 + i2 * 16 + 11] << 24 | Data[BI.Offset + i1 * 64 + i2 * 16 + 10] << 16 |
					Data[BI.Offset + i1 * 64 + i2 * 16 + 9] << 8 | Data[BI.Offset + i1 * 64 + i2 * 16 + 8];
			fb[3] = Data[BI.Offset + i1 * 64 + i2 * 16 + 15] << 24 | Data[BI.Offset + i1 * 64 + i2 * 16 + 14] << 16 |
					Data[BI.Offset + i1 * 64 + i2 * 16 + 13] << 8 | Data[BI.Offset + i1 * 64 + i2 * 16 + 12];
			std::memcpy(&V.V[0], &fb[0], 16);
			M.SetCol(i2, V);
		}
		Result.push_back(M);
	}
	return Result;
}

static std::vector<Vec2f> ReadVec2s(const BufferInfo &BI, const std::vector<std::vector<uint8_t>> &Buffers)
{
	auto &Data = Buffers[BI.BufferIndex];
	std::vector<Vec2f> Result;
	Result.reserve(BI.Count);
	Vec2f V;
	uint32_t fb[2];
	for(int i = 0; i < BI.Count; ++i)
	{
		fb[0] = Data[BI.Offset + i * 8 + 3] << 24 | Data[BI.Offset + i * 8 + 2] << 16 | Data[BI.Offset + i * 8 + 1] << 8 |
				Data[BI.Offset + i * 8 + 0];
		fb[1] = Data[BI.Offset + i * 8 + 7] << 24 | Data[BI.Offset + i * 8 + 6] << 16 | Data[BI.Offset + i * 8 + 5] << 8 |
				Data[BI.Offset + i * 8 + 4];
		std::memcpy(&V.x, &fb[0], 8);
		Result.push_back(V);
	}
	return Result;
}

static std::vector<Vec3f> ReadVec3fs(const BufferInfo &BI, const std::vector<std::vector<uint8_t>> &Buffers)
{
	auto &Data = Buffers[BI.BufferIndex];
	std::vector<Vec3f> Result;
	Result.reserve(BI.Count);
	Vec3f V;
	uint32_t fb[3];
	for(int i = 0; i < BI.Count; ++i)
	{
		fb[0] = Data[BI.Offset + i * 12 + 3] << 24 | Data[BI.Offset + i * 12 + 2] << 16 | Data[BI.Offset + i * 12 + 1] << 8 |
				Data[BI.Offset + i * 12 + 0];
		fb[1] = Data[BI.Offset + i * 12 + 7] << 24 | Data[BI.Offset + i * 12 + 6] << 16 | Data[BI.Offset + i * 12 + 5] << 8 |
				Data[BI.Offset + i * 12 + 4];
		fb[2] = Data[BI.Offset + i * 12 + 11] << 24 | Data[BI.Offset + i * 12 + 10] << 16 | Data[BI.Offset + i * 12 + 9] << 8 |
				Data[BI.Offset + i * 12 + 8];
		std::memcpy(&V.V[0], &fb[0], 12);
		Result.push_back(V);
	}
	return Result;
}

static std::vector<Vec4b> ReadVec4bs(const BufferInfo &BI, const std::vector<std::vector<uint8_t>> &Buffers)
{
	auto &Data = Buffers[BI.BufferIndex];
	std::vector<Vec4b> Result;
	Result.reserve(BI.Count);
	Vec4b V;
	for(int i = 0; i < BI.Count; ++i)
	{
		V.V[0] = Data[BI.Offset + i * 4 + 0];
		V.V[1] = Data[BI.Offset + i * 4 + 1];
		V.V[2] = Data[BI.Offset + i * 4 + 2];
		V.V[3] = Data[BI.Offset + i * 4 + 3];
		Result.push_back(V);
	}
	return Result;
}

//static std::vector<Vec4f> ReadVec4fs(const BufferInfo &BI, const std::vector<std::vector<uint8_t>> &Buffers)
//{
//	auto &Data = Buffers[BI.BufferIndex];
//	std::vector<Vec4f> Result;
//	Result.reserve(BI.Count);
//	Vec4f V;
//	uint16_t fb[4];
//	for(int i = 0; i < BI.Count; ++i)
//	{
//		fb[0] = Data[BI.Offset+i*8+1] << 8 | Data[BI.Offset+i*8+0];
//		fb[1] = Data[BI.Offset+i*8+3] << 8 | Data[BI.Offset+i*8+2];
//		fb[2] = Data[BI.Offset+i*8+5] << 8 | Data[BI.Offset+i*8+4];
//		fb[3] = Data[BI.Offset+i*8+7] << 8 | Data[BI.Offset+i*8+6];
//		V.V[0] = fb[0] / 65535.0f;
//		V.V[1] = fb[1] / 65535.0f;
//		V.V[2] = fb[2] / 65535.0f;
//		V.V[3] = fb[3] / 65535.0f;
//		Result.push_back(V);
//	}
//	return Result;
//}

static std::vector<Vec4f> ReadVec4fs(const BufferInfo &BI, const std::vector<std::vector<uint8_t>> &Buffers)
{
	auto &Data = Buffers[BI.BufferIndex];
	std::vector<Vec4f> Result;
	Result.reserve(BI.Count);
	Vec4f V;
	uint32_t fb[4];
	for(int i = 0; i < BI.Count; ++i)
	{
		fb[0] = Data[BI.Offset + i * 16 + 3] << 24 | Data[BI.Offset + i * 16 + 2] << 16 | Data[BI.Offset + i * 16 + 1] << 8 |
				Data[BI.Offset + i * 16 + 0];
		fb[1] = Data[BI.Offset + i * 16 + 7] << 24 | Data[BI.Offset + i * 16 + 6] << 16 | Data[BI.Offset + i * 16 + 5] << 8 |
				Data[BI.Offset + i * 16 + 4];
		fb[2] = Data[BI.Offset + i * 16 + 11] << 24 | Data[BI.Offset + i * 16 + 10] << 16 | Data[BI.Offset + i * 16 + 9] << 8 |
				Data[BI.Offset + i * 16 + 8];
		fb[3] = Data[BI.Offset + i * 16 + 15] << 24 | Data[BI.Offset + i * 16 + 14] << 16 | Data[BI.Offset + i * 16 + 13] << 8 |
				Data[BI.Offset + i * 16 + 12];
		std::memcpy(&V.V[0], &fb[0], 16);
		Result.push_back(V);
	}
	return Result;
}

static std::vector<uint32_t> ReadIndices(const BufferInfo &BI, const std::vector<std::vector<uint8_t>> &Buffers)
{
	auto &Data = Buffers[BI.BufferIndex];
	std::vector<uint32_t> Result;
	Result.reserve(BI.Count);
	uint16_t b;
	for(int i = 0; i < BI.Count; ++i)
	{
		b = Data[BI.Offset + i * 2 + 1] << 8 | Data[BI.Offset + i * 2];
		Result.push_back(b);
	}
	return Result;
}

BufferInfo GetBufferInfo(int AccessorIndex, const std::vector<JSONObject> &Accessors, const std::vector<JSONObject> &BufferViews)
{
	BufferInfo Result;
	int BufferViewIndex = Accessors[AccessorIndex].GetInt("bufferView");
	Result.Count = Accessors[AccessorIndex].GetInt("count");
	Result.BufferIndex = BufferViews[BufferViewIndex].GetInt("buffer");
	Result.Offset = BufferViews[BufferViewIndex].GetInt("byteOffset");
	return Result;
}

void ParseGlTFFile(const std::string &Path, std::vector<std::string> &MeshNames, std::vector<Mesh> &MeshesOut,
				   std::vector<MeshRig> &MeshRigs)
{
	JSONObject Json = ParseJSON(Path);

	auto &Buffers = Json.GetObjs("buffers");
	size_t NumBuffers = Buffers.size();
	std::vector<std::vector<uint8_t>> DecodedData(NumBuffers);
	for(int bi = 0; bi < NumBuffers; ++bi)
	{
		std::string BufferData = Buffers[bi].GetText("uri");
		size_t i = BufferData.find_first_of(',');
		DecodedData[bi] = Base64Decode(BufferData.substr(i + 1));
	}

	auto &Nodes = Json.GetObjs("nodes");
	auto &Accessors = Json.GetObjs("accessors");
	auto &BufferViews = Json.GetObjs("bufferViews");

	//Mesh
	auto &Meshes = Json.GetObjs("meshes");
	for(auto &N : Nodes)
	{
		std::string Name = N.GetText("name");
		if(!N.HasInt("mesh")) continue;
		int MeshIndex = N.GetInt("mesh");
		auto &M = Meshes[MeshIndex];
		auto &Primitives = M.GetObjs("primitives");

		std::vector<uint32_t> Indices;
		{
			//Indices
			int AccessorIndex = Primitives[0].GetInt("indices");
			auto BI = GetBufferInfo(AccessorIndex, Accessors, BufferViews);
			Indices = ReadIndices(BI, DecodedData);
		}
		auto &Atrib0 = Primitives[0].GetObj("attributes");
		std::vector<Vec3f> PosVector;
		{
			//Positions
			int AccessorIndex = Atrib0.GetInt("POSITION");
			auto BI = GetBufferInfo(AccessorIndex, Accessors, BufferViews);
			PosVector = ReadVec3fs(BI, DecodedData);
		}
		std::vector<Vec3f> NormVector;
		{
			//Normals
			int AccessorIndex = Atrib0.GetInt("NORMAL");
			auto BI = GetBufferInfo(AccessorIndex, Accessors, BufferViews);
			NormVector = ReadVec3fs(BI, DecodedData);
		}
		std::vector<Vec2f> TexVector;
		{
			//Tex coords
			try
			{
				int AccessorIndex = Atrib0.GetInt("TEXCOORD_0");
				auto BI = GetBufferInfo(AccessorIndex, Accessors, BufferViews);
				TexVector = ReadVec2s(BI, DecodedData);
			}
			catch(...)
			{
				TexVector.resize(NormVector.size(), {0, 0});
			}
		}
		//std::vector<OpeningIndices> OpeningsTmp(6, {-1,-1,-1,-1, false});
		//if(Primitives.size() > 1)
		//{
		//	auto &Materials = Json.GetObjs("materials");
		//	for(auto &P : Primitives)
		//	{
		//		// Openings
		//		int MaterialIndex = P.GetInt("material");
		//		auto &MatName = Materials[MaterialIndex].GetText("name");
		//		if(MatName.find("Opening") == 0)
		//		{
		//			int OpeningIndex = MatName[7] - '0';

		//			int AccessorIndex = P.GetInt("indices");
		//			auto BI = GetBufferInfo(AccessorIndex, Accessors, BufferViews);
		//			auto IV = ReadIndices(BI, DecodedData);
		//			int ii[4] = {IV[0], IV[1], IV[2], IV[5]};

		//			int AccessorIndex2 = P.GetObj("attributes").GetInt("POSITION");
		//			auto BI2 = GetBufferInfo(AccessorIndex2, Accessors, BufferViews);
		//			auto PV = ReadVec3fs(BI2, DecodedData);
		//			for(int OpI = 0; OpI < 4; ++OpI)
		//			{
		//				int i3 = ii[OpI];
		//				for(int PosI = 0; PosI < PosVector.size(); ++PosI)
		//				{
		//					if(PV[i3].x - 0.01f < PosVector[PosI].x && PosVector[PosI].x < PV[i3].x + 0.01f &&
		//					   PV[i3].y - 0.01f < PosVector[PosI].y && PosVector[PosI].y < PV[i3].y + 0.01f &&
		//					   PV[i3].z - 0.01f < PosVector[PosI].z && PosVector[PosI].z < PV[i3].z + 0.01f)
		//					{
		//						OpeningsTmp[OpeningIndex].I[OpI] = PosI;
		//					}
		//				}
		//			}
		//		}
		//	}
		//}
		MeshRig Rig;
		std::vector<Vec4b> JointVector;
		std::vector<Vec4f> WeightVector;
		if(N.HasInt("skin"))
		{
			int SkinIndex = N.GetInt("skin");
			auto &Skin = Json.GetObjs("skins")[SkinIndex];
			int InverseBindMatricesAccessorIndex = Skin.GetInt("inverseBindMatrices");

			auto BI = GetBufferInfo(InverseBindMatricesAccessorIndex, Accessors, BufferViews);
			Rig.InverseBindMatrices = ReadMat4fs(BI, DecodedData);

			auto &JointIndices = Skin.GetInts("joints");
			size_t NumJoints = JointIndices.size();
			std::map<int, int> NodeToJoint;
			int JointIndex = 0;
			for(int i : JointIndices)
			{
				NodeToJoint[i] = JointIndex++;

				Joint NewJoint;
				auto &J = Nodes[i];
				NewJoint.Name = J.GetText("name");
				if(J.HasFloats("translation"))
				{
					auto &F = J.GetFloats("translation");
					NewJoint.Translation = {F[0], F[1], F[2]};
				}
				else if(J.HasInts("translation"))
				{
					auto &I = J.GetInts("translation");
					NewJoint.Translation = {(float)I[0], (float)I[1], (float)I[2]};
				}
				if(J.HasFloats("rotation"))
				{
					auto &F = J.GetFloats("rotation");
					NewJoint.Rotation = {F[0], F[1], F[2], F[3]};
				}
				else if(J.HasInts("rotation"))
				{
					auto &I = J.GetInts("rotation");
					NewJoint.Rotation = {(float)I[0], (float)I[1], (float)I[2], (float)I[3]};
				}
				if(J.HasInts("children"))
				{
					NewJoint.Children = J.GetInts("children");
				}
				Rig.Joints.push_back(NewJoint);
			}
			int JointsAccessorIndex = Atrib0.GetInt("JOINTS_0");
			int WeightsAccessorIndex = Atrib0.GetInt("WEIGHTS_0");

			auto BI1 = GetBufferInfo(JointsAccessorIndex, Accessors, BufferViews);
			auto BI2 = GetBufferInfo(WeightsAccessorIndex, Accessors, BufferViews);
			JointVector = ReadVec4bs(BI1, DecodedData);
			WeightVector = ReadVec4fs(BI2, DecodedData);

			//Animations
			if(Json.HasObjs("animations"))
			{
				auto &Animations = Json.GetObjs("animations");
				for(auto &A : Animations)
				{
					Animation NewAni;
					NewAni.Name = A.GetText("name");
					auto &Channels = A.GetObjs("channels");
					auto &Samplers = A.GetObjs("samplers");

					std::vector<int> TranslationJointToSamplerIndex(NumJoints, -1);
					std::vector<int> RotationJointToSamplerIndex(NumJoints, -1);
					std::vector<int> ScaleJointToSamplerIndex(NumJoints, -1);

					for(auto &C : Channels)
					{
						int SamplerIndex = C.GetInt("sampler");
						auto &T = C.GetObj("target");
						int TargetNodeIndex = T.GetInt("node");
						int JIndex = NodeToJoint[TargetNodeIndex];
						auto &Path = T.GetText("path");

						if(Path == "translation") TranslationJointToSamplerIndex[JIndex] = SamplerIndex;
						else if(Path == "rotation") RotationJointToSamplerIndex[JIndex] = SamplerIndex;
						else if(Path == "scale") ScaleJointToSamplerIndex[JIndex] = SamplerIndex;
					}
					NewAni.TranslationSteps.resize(NumJoints);
					NewAni.RotationSteps.resize(NumJoints);
					for(int i = 0; i < NumJoints; ++i)
					{
						if(TranslationJointToSamplerIndex[i] > -1)
						{
							auto &TSampler = Samplers[TranslationJointToSamplerIndex[i]];

							int TInput = TSampler.GetInt("input");
							auto TBII = GetBufferInfo(TInput, Accessors, BufferViews);

							int TOutput = TSampler.GetInt("output");
							auto TBIO = GetBufferInfo(TOutput, Accessors, BufferViews);

							auto TTimes = ReadFloats(TBII, DecodedData);
							auto Translations = ReadVec3fs(TBIO, DecodedData);

							size_t NumSteps = TTimes.size();
							for(int s = 0; s < NumSteps; ++s)
							{
								TranslationStep S;
								S.Time = TTimes[s];
								S.Translation = Translations[s];
								NewAni.TranslationSteps[i].push_back(S);
							}
						}
						if(RotationJointToSamplerIndex[i] > -1)
						{
							auto &RSampler = Samplers[RotationJointToSamplerIndex[i]];

							int RInput = RSampler.GetInt("input");
							auto RBII = GetBufferInfo(RInput, Accessors, BufferViews);

							int ROutput = RSampler.GetInt("output");
							auto RBIO = GetBufferInfo(ROutput, Accessors, BufferViews);

							auto RTimes = ReadFloats(RBII, DecodedData);
							auto Rotations = ReadVec4fs(RBIO, DecodedData);

							size_t NumSteps = RTimes.size();
							for(int s = 0; s < NumSteps; ++s)
							{
								RotationStep S;
								S.Time = RTimes[s];
								S.Rotation = Rotations[s];
								NewAni.RotationSteps[i].push_back(S);
							}
						}
						//if(ScaleJointToSamplerIndex[i] > -1)
						//{
						//	auto &SSampler = Samplers[ScaleJointToSamplerIndex[i]];

						//	int SInput = SSampler.GetInt("input");
						//	auto SBII = GetBufferInfo(SInput, Accessors, BufferViews);

						//	int SOutput = SSampler.GetInt("output");
						//	auto SBIO = GetBufferInfo(SOutput, Accessors, BufferViews);

						//	auto STimes = ReadFloats(SBII, DecodedData);
						//	auto Scales = ReadVec3fs(SBIO, DecodedData);
						//}
					}
					Rig.Animations.push_back(NewAni);
				}
			}
			for(int i = 0; i < Rig.Joints.size(); ++i)
			{
				auto &J = Rig.Joints[i];
				for(int j = 0; j < J.Children.size(); ++j)
				{
					int CI = NodeToJoint[J.Children[j]];
					J.Children[j] = CI;
					Rig.Joints[CI].Parent = i;
				}
			}
		}
		if(JointVector.empty())
		{
			JointVector.resize(PosVector.size());
			WeightVector.resize(PosVector.size());
		}

		Mesh NewMesh;
		size_t Size = PosVector.size();
		NewMesh.Vertices.reserve(Size);
		NewMesh.Indices = Indices;
		for(int i = 0; i < Size; ++i)
		{
			Vertex V;
			V.Position = PosVector[i];
			V.Normal = NormVector[i];
			V.TexCoord = TexVector[i];
			V.Joints = JointVector[i];
			V.Weights = WeightVector[i];
			NewMesh.Vertices.push_back(V);
		}

		MeshNames.push_back(Name);
		MeshesOut.push_back(NewMesh);
		MeshRigs.push_back(Rig);
	}
}

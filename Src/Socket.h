#pragma once

#include <string>

bool InitializeSockets();

void TerminateSockets();

class Address
{
public:
	Address()
	{
		m_address = 0;
		m_port = 0;
	}
	Address(unsigned char a, unsigned char b, unsigned char c, unsigned char d, unsigned short port);
	Address(unsigned int address, unsigned short port);

	bool operator==(const Address &A);

	unsigned char GetA() const;
	unsigned char GetB() const;
	unsigned char GetC() const;
	unsigned char GetD() const;
	unsigned int GetAddress() const;
	unsigned short GetPort() const;
	std::string ToString() const;

private:
	unsigned int m_address;
	unsigned short m_port;
};

class Socket
{
public:
	Socket();
	~Socket();

	bool Open(unsigned short port);
	void Close();
	bool Send(const Address &destination, const void *data, int size);
	int Receive(Address &source, void *data, int BufferSize);

private:
	unsigned long long m_handle;
};

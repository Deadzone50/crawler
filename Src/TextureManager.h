#pragma once

#include <map>
#include <string>

class TextureManager
{
public:
	TextureManager();
	uint32_t Texture(const std::string &FileName);
	uint32_t Texture(uint8_t R, uint8_t G, uint8_t B);

private:
	std::map<std::string, uint32_t> m_Textures;
};

extern TextureManager TextureMan;

#pragma once

#include <vector>

#include "RenderText.h"
#include "Shader.h"
#include "vector/Vector.h"

const size_t MAX_POINTS = 1000000 * 2;
const size_t MAX_LINES = 1000000 * 4;

class Drawer
{
public:
	void Initialize();

	bool DrawPoint(Vec3f P, Vec3f Color);
	bool DrawLine(Vec3f S, Vec3f E, Vec3f Color);
	bool DrawDirLine(Vec3f S, Vec3f E, Vec3f Color);
	bool DrawTriangle(Vec3f V1, Vec3f V2, Vec3f V3, Vec3f Color);
	void DrawTextLine(const std::string &Text, uint32_t CharSize, Vec3f Color = {1, 1, 1})
	{
		m_TextLines.emplace_back(Text, CharSize, Color);
	}

	bool DrawPersistentPoint(Vec3f P, Vec3f Color);
	bool DrawPersistentLine(Vec3f S, Vec3f E, Vec3f Color);

	void UpdateBuffers();
	void Clear();
	void ClearPersistent();
	void ClearPoints();
	void ClearLines();
	void ClearText() { m_TextLines.clear(); }
	void Enable() { m_Enabled = !m_Enabled; }

	void RenderGraphics(Shader &Shader);
	void RenderTextLines(Shader &Shader);

private:
	std::vector<Vec3f> m_VerticesPoints;
	std::vector<Vec3f> m_VerticesLines;
	std::vector<RenderText> m_TextLines;
	std::vector<Vec3f> m_PersistentVerticesPoints;
	std::vector<Vec3f> m_PersistentVerticesLines;

	uint32_t m_VAOP = 0, m_VBOP = 0;
	uint32_t m_VAOL = 0, m_VBOL = 0;
	uint32_t m_VAOPP = 0, m_VBOPP = 0;
	uint32_t m_VAOPL = 0, m_VBOPL = 0;

	bool m_Enabled = true;
	bool m_Update = false;
};

struct Particle
{
	Vec3f Pos, Speed, Color;
	float TimeLeft;
};

class MegaDrawer
{
public:
	void Initialize();

	void DrawPoint(Vec3f P, Vec3f Color) { m_Drawer.DrawPoint(P, Color); }
	void DrawLine(Vec3f S, Vec3f E, Vec3f Color) { m_Drawer.DrawLine(S, E, Color); }
	void DrawTextLine(const std::string &Text, uint32_t Size, Vec3f Color = {1, 1, 1})
	{
		m_Drawer.DrawTextLine(Text, Size, Color);
	}

	void DrawDebugPoint(Vec3f P, Vec3f Color) { m_DebugDrawer.DrawPoint(P, Color); }
	void DrawDebugLine(Vec3f S, Vec3f E, Vec3f Color) { m_DebugDrawer.DrawLine(S, E, Color); }
	void DrawPersistentDebugPoint(Vec3f P, Vec3f Color) { m_DebugDrawer.DrawPersistentPoint(P, Color); }
	void DrawPersistentDebugLine(Vec3f S, Vec3f E, Vec3f Color) { m_DebugDrawer.DrawPersistentLine(S, E, Color); }
	void DrawDebugDirLine(Vec3f S, Vec3f E, Vec3f Color) { m_DebugDrawer.DrawDirLine(S, E, Color); }
	void DrawDebugTextLine(const std::string &Text, uint32_t Size, Vec3f Color = {1, 1, 1})
	{
		m_DebugDrawer.DrawTextLine(Text, Size, Color);
	}

	void CreateParticle(Vec3f Pos, Vec3f Speed, Vec3f Color, float Lifetime)
	{
		m_Particles.push_back({Pos, Speed, Color, Lifetime});
	}

	void Clear() { m_Drawer.Clear(); }
	void ClearText() { m_Drawer.ClearText(); }

	void ClearDebug() { m_DebugDrawer.Clear(); }
	void ClearPersistentDebug() { m_DebugDrawer.ClearPersistent(); }
	void ClearDebugText() { m_DebugDrawer.ClearText(); }

	//Rendering

	void RenderGraphics(Shader &Shader, float dt);
	void RenderTextLines(Shader &Shader) { m_Drawer.RenderTextLines(Shader); }

	void RenderDebugGraphics(Shader &Shader) { m_DebugDrawer.RenderGraphics(Shader); }
	void RenderDebugTextLines(Shader &Shader) { m_DebugDrawer.RenderTextLines(Shader); }
	void ToggleDebug() { m_DebugDrawer.Enable(); }

private:
	void DrawParticles(float dt);

	Drawer m_Drawer, m_DebugDrawer;
	std::vector<Particle> m_Particles;
};

void DrawPoint(Vec3f P, Vec3f Color);
void DrawLine(Vec3f S, Vec3f E, Vec3f Color);
void DrawBox(const Vec3f *P, const uint32_t *I, Vec3f Color);
void DrawBoxP(const Vec3f *P, Vec3f Color);

void CreateParticle(Vec3f Pos, Vec3f Speed, Vec3f Color, float Lifetime);

#pragma once

#include <vector>

#include "Menu.h"
#include "RenderText.h"
#include "vector/Matrix.h"

struct GameState;
class Candidate;

struct ScreenText
{
	RenderText Text;
	Mat4f Transform;
};

class BaseScreen
{
public:
	BaseScreen();
	void HandleAndDraw(Shader &Shader, const IOStates &IO, GameState &Game);

private:
	void HandleMainscreen(const IOStates &IO, GameState &Game);
	void DrawCharacterStats(const Candidate &Character);

	enum class CurrentScreen
	{
		Base,
		Roster
	};

	CurrentScreen m_Currentscreen = CurrentScreen::Base;

	Menu m_Menus[2];
	Menu *m_CurrentMenu = &m_Menus[static_cast<uint8_t>(CurrentScreen::Base)];
	std::vector<ScreenText> m_Text;
};

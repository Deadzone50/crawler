#include "Scene.h"

#include "Console.h"
#include "IO.h"
#include "MainLoop.h"
#include "Network.h"
#include "PlayerClass.h"
#include "States.h"

extern GameState Game;
extern RenderState RState;
extern NetworkState NState;
extern std::vector<Menu> Menus;

void ExitLevel()
{
	if(NState.Multiplayer)
		NState.NM->SendExitMap(Game.PlayerIndex);
	else
		Game.InMap = false;
	Game.Player->SetPosition({0, 10000, 0});
	SwitchToBaseScreen();
}

void SwitchToGame()
{
	HideMouse();
	ResetMouse();
	ResetTimers();
	RState.MegaShader.SetMat4("Projection", RState.ProjectionMatrixPerspective);
	Game.CurrentScene = SceneType::Game;
}

void SwitchToBaseScreen()
{
	ShowMouse();
	RState.MegaShader.SetMat4("Projection", RState.ProjectionMatrixOrtho);
	Mat4f ViewMatrix;
	ViewMatrix.SetRow(0, {1, 0, 0, 0});
	ViewMatrix.SetRow(1, {0, 1, 0, 0});
	ViewMatrix.SetRow(2, {0, 0, -1, -1});
	ViewMatrix.SetRow(3, {0, 0, 0, 1});
	RState.MegaShader.SetMat4("View", ViewMatrix);
	Game.CurrentScene = SceneType::Base;
}

void SwitchToMenu(MenuType Menu)
{
	ShowMouse();
	RState.MegaShader.SetMat4("Projection", RState.ProjectionMatrixOrtho);
	Mat4f ViewMatrix;
	ViewMatrix.SetRow(0, {1, 0, 0, 0});
	ViewMatrix.SetRow(1, {0, 1, 0, 0});
	ViewMatrix.SetRow(2, {0, 0, -1, -1});
	ViewMatrix.SetRow(3, {0, 0, 0, 1});
	RState.MegaShader.SetMat4("View", ViewMatrix);
	Game.CurrentScene = SceneType::Menu;
	Game.CurrentMenu = &Menus[Menu];
}

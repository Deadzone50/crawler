#include "Level.h"

#include <list>

#include "Collision.h"
#include "Console.h"
#include "Debug.h"
#include "Enemy.h"
#include "FileIO.h"
#include "Globals.h"
#include "MeshManager.h"
#include "PlayerClass.h"
#include "Random.h"
#include "Scene.h"
#include "SoundManager.h"
#include "States.h"
#include "vector/Matrix.h"

LevelObject *Level::RayObjectCollision(Vec3f Origins, Vec3f Directions, float &t)
{
	LevelObject *Hit = nullptr;
	for(auto &LO : m_Map.Objects)
	{
		AxisAlignedBoundingBox AABB = LO.GetBoundingVolume();

		float tMin = 0;
		if(RayAABBCollisionTest(Origins, Directions, AABB, tMin, t))
		{
			Hit = &LO;
		}
	}
	return Hit;
}

void Level::Draw(Shader &Shader) const
{
	m_Map.Draw(Shader);
	for(auto &o : m_Map.Objects)
	{
		o.GetMesh().Draw(Shader);
	}
}

void Level::DrawWFC(Shader &S)
{
	m_WFC.DrawMap(S);
}

void Level::HandleCharacterCollisions(const std::vector<PlayerClass> &Players)
{
	size_t NumEnemies = m_Map.Enemies.size();
	for(uint32_t i1 = 0; i1 < NumEnemies; ++i1)
	{
		if(!m_Map.Enemies[i1].IsAlive()) continue;
		SphereCollider C1 = m_Map.Enemies[i1].GetCollider();
		C1.Position.y = 0;
		for(uint32_t i2 = i1 + 1; i2 < NumEnemies; ++i2)
		{
			if(!m_Map.Enemies[i2].IsAlive()) continue;
			SphereCollider C2 = m_Map.Enemies[i2].GetCollider();
			C2.Position.y = 0;

			float MinDist = C1.Radius + C2.Radius;
			Vec3f Dir = C1.Position - C2.Position;
			float Dist = LEN(Dir);
			float Diff = Dist - MinDist;
			if(Diff < 0)
			{
				Vec3f Tmp = (Dir / Dist) * Diff / 2;
				m_Map.Enemies[i1].AddPosition(-Tmp);
				m_Map.Enemies[i2].AddPosition(Tmp);
			}
		}
		for(auto &P : Players)
		{
			if(!P.IsAlive()) continue;
			SphereCollider C2 = P.GetCollider();
			C2.Position.y = 0;
			float MinDist = C1.Radius + C2.Radius;
			Vec3f Dir = C1.Position - C2.Position;
			float Dist = LEN(Dir);
			float Diff = Dist - MinDist;
			if(Diff < 0)
			{
				Vec3f Tmp = (Dir / Dist) * Diff;
				m_Map.Enemies[i1].AddPosition(-Tmp);
			}
		}
	}
}

std::vector<Item> Level::GetCollisionItems(const AxisAlignedBoundingBox &AABB) const
{
	std::vector<Item> Result;
	const auto &Rooms = m_Map.GetPossibleRooms(AABB);
	for(int i : Rooms)
	{
		const auto &R = m_Map.Rooms[i];
		auto &Colliders = R.GetColliders();

		Item I;
		I.SetPos(R.GetPos());
		I.SetColliders(Colliders);
		I.UpdateColliders();
		Result.push_back(I);
	}
	return Result;
}

void Level::Highlight(Vec3f Origin, Vec3f Direction)
{
	float t = 1.5f;
	LevelObject *Hit = RayObjectCollision(Origin, Direction, t);
	if(Hit != nullptr)
	{
		if(Hit->IsInteractable()) Hit->DrawHighlight();
	}
}

bool Level::Use(PlayerClass *Player)
{
	float t = 1.5f;
	LevelObject *Hit = RayObjectCollision(Player->GetCameraPos(), Player->GetLookingDir(), t);
	if(Hit != nullptr && Hit->IsInteractable())
	{
		Hit->Use();
	}
	return Hit != nullptr;
}

void Level::RemoveItem(uint32_t Id)
{
	for(auto it = m_Map.Items.begin(); it != m_Map.Items.end(); ++it)
	{
		if(it->GetId() == Id)
		{
			m_Map.Items.erase(it);
			return;
		}
	}
	Log("Item not found");
}

void Level::DrawNavmesh() const
{
	for(auto &R : m_Map.Rooms)
	{
		const NavZone &NZ1 = R.GetNavZone();
		DrawDebugPoint(NZ1.Position, {10, 0, 10});
		for(const auto &N : NZ1.Neighbours)
		{
			const NavZone &NZ2 = m_Map.Rooms[N.RoomIndex].GetNavZone();
			DrawDebugLine(NZ1.Position, NZ2.Position, {10, 0, 10});
		}
		for(const auto &N : NZ1.PlayerDistances)
		{
			if(N.RoomIndex == -1) continue;
			const NavZone &NZ2 = m_Map.Rooms[N.RoomIndex].GetNavZone();
			DrawDebugDirLine(NZ1.Position + Vec3f{0.1f, 0.1f, 0.1f}, NZ2.Position + Vec3f{0.1f, 0.1f, 0.1f}, {0, 1, 0});
		}
	}
}

void Level::UpdateNavigation(const GameState &Game)
{
	struct DistanceCheck
	{
		int FromIndex;
		int ToIndex;
		float CurrentDistance;
	};

	size_t NumPlayers = Game.Players.size();
	for(auto &R : m_Map.Rooms)
	{
		R.GetNavZone().PlayerDistances.clear();
		R.GetNavZone().PlayerDistances.resize(NumPlayers);
	}

	for(int PlayerIndex = 0; PlayerIndex < NumPlayers; ++PlayerIndex)
	{
		int StartingRoomIndex = -1;
		Vec3f Pos = Game.Players[PlayerIndex].GetPosition() + Vec3f{0, 1, 0};
		auto V = m_Map.GetPossibleRooms(Pos);
		for(auto I : V)
		{
			const AxisAlignedBoundingBox AABB = m_Map.Rooms[I].GetBoundingVolume();
			if(InAABB(Pos, AABB))
			{
				StartingRoomIndex = I;
				break;
			}
		}
		if(StartingRoomIndex < 0)	 //TODO: player is outside map
		{
			StartingRoomIndex = 0;
			Log("Nav: Player outside map");
		}
		for(int i = 0; i < 1000; ++i)
		{
			std::list<DistanceCheck> ToCheck = {{-1, StartingRoomIndex, 0}};
			while(!ToCheck.empty())
			{
				DistanceCheck DC = ToCheck.front();
				ToCheck.pop_front();
				NavZone &NZ = m_Map.Rooms[DC.ToIndex].GetNavZone();
				//Update if distance is shorter than previously found
				if(NZ.PlayerDistances[PlayerIndex].Distance > DC.CurrentDistance)
				{
					NZ.PlayerDistances[PlayerIndex].RoomIndex = DC.FromIndex;
					NZ.PlayerDistances[PlayerIndex].Distance = DC.CurrentDistance;
					for(const auto &Ni : NZ.Neighbours)
					{
						const float NewDistance = DC.CurrentDistance + Ni.Distance;
						ToCheck.push_back({DC.ToIndex, Ni.RoomIndex, NewDistance});
					}
				}
			}
		}
		//if(i == 1000) Log("Nav: 1000 iterations hit");
	}
}

void Level::SpawnEnemy(const std::string &EnemyName, const std::string &WeaponName, const Vec3f &Pos)
{
	m_Map.Enemies.push_back(m_EnemyMap[EnemyName]);
	EnemyClass &Enemy = m_Map.Enemies.back();
	Enemy.AddWeapon(WeaponName, 6);
	Enemy.GetInventory().EquipWeapon(0, 0);
	Enemy.SetPosition(Pos);
}

void Level::SpawnEnemies(GameState &Game, float dt)
{
	m_EnemySpawnTimer -= dt;
	if(m_EnemySpawnTimer < 0)
	{
		m_EnemySpawnTimer = GetRandomF(10, 40);
		SoundMan.Play("Spawn-01.wav");

		const uint32_t NumRooms = m_Map.Rooms.size();
		const uint32_t Num = (m_Map.Enemies.size() > 10) ? GetRandomUI(1, 10) : GetRandomUI(8, 15);
		for(uint32_t i = 0; i < Num; ++i)
		{
			uint32_t RoomIndex = GetRandomUI(0, NumRooms - 1);
			auto &Room = m_Map.Rooms[RoomIndex];
			while(Room.IsDecoration())
			{
				RoomIndex = GetRandomUI(0, NumRooms - 1);
				Room = m_Map.Rooms[RoomIndex];
			}

			std::string WeaponName;
			float r = GetRandomF(0, 3);
			if(r < 1) WeaponName = "Machete";
			//else if(r < 2)
			//Enemy.AddWeapon("Shotgun");
			else WeaponName = "Pitchfork";

			AxisAlignedBoundingBox AABB = Room.GetBoundingVolume();
			Vec3f Pos = {};
			Pos.x = (AABB.V1.x + AABB.V2.x) / 2;
			Pos.y = (AABB.V1.y + AABB.V2.y) / 2 + 1;
			Pos.z = (AABB.V1.z + AABB.V2.z) / 2;
			SpawnEnemy("Cultist", WeaponName, Pos);
			//float t = 0;
			//Vec3f CollisionDir = MapCollisionTest(Enemy.GetCollider(), t);
			//if(t != 0)
			//{
			//	Enemy.SetPosition(Pos +t*CollisionDir);
			//}
		}
	}
}

std::vector<Vec3f> Level::GetWaypointsToClosestPlayer(Vec3f Start)
{
	int StartingRoomIndex = -1;
	auto V = m_Map.GetPossibleRooms(Start);
	for(auto I : V)
	{
		const AxisAlignedBoundingBox AABB = m_Map.Rooms[I].GetBoundingVolume();
		if(InAABB(Start, AABB))
		{
			StartingRoomIndex = I;
			break;
		}
	}
	if(StartingRoomIndex == -1)	   //outside map
		return {};
	NavZone &NZ = m_Map.Rooms[StartingRoomIndex].GetNavZone();

	int PlayerIndex = -1;
	float Closest = FLT_MAX;
	for(int i = 0; i < NZ.PlayerDistances.size(); ++i)
	{
		if(NZ.PlayerDistances[i].Distance < Closest)
		{
			Closest = NZ.PlayerDistances[i].Distance;
			PlayerIndex = i;
		}
	}

	std::vector<Vec3f> Result;
	if(PlayerIndex != -1)
	{
		int NextIndex = StartingRoomIndex;
		while(true)
		{
			NavZone &NZ1 = m_Map.Rooms[NextIndex].GetNavZone();
			Result.push_back(NZ1.Position);
			if(NZ1.PlayerDistances[PlayerIndex].Distance > 0) NextIndex = NZ1.PlayerDistances[PlayerIndex].RoomIndex;
			else break;
		}
	}
	return Result;
}

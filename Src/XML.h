#pragma once

#include <string>
#include <vector>

struct XMLAttribute
{
	std::string Name, Value;
};

struct XMLTag
{
	std::string Name;
	std::vector<XMLAttribute> Attributes;
};

class XMLElement
{
public:
	//Get first matching element
	XMLElement Get(const std::string &Name, const std::string &AttributeName = "", const std::string &AttributeValue = "") const
	{
		for(const auto &E : Elements)
		{
			if(E.Tag.Name == Name)
			{
				if(AttributeName.empty()) return E;
				else
				{
					for(const auto &A : E.Tag.Attributes)
					{
						if(A.Name == AttributeName && A.Value == AttributeValue)
						{
							return E;
						}
					}
				}
			}
		}
		return XMLElement();
	}
	//Get all matching elements
	std::vector<XMLElement> GetAll(const std::string &Name) const
	{
		std::vector<XMLElement> Result;
		for(auto &E : Elements)
		{
			if(E.Tag.Name == Name) Result.push_back(E);
		}
		return Result;
	}

	//Get first matching Attribute
	std::string GetAttribute(const std::string &Name) const
	{
		for(auto &A : Tag.Attributes)
		{
			if(A.Name == Name)
			{
				return A.Value;
			}
		}
		return "";
	}

	//Get text of element
	std::string GetText() const { return Text; }

	XMLTag Tag;
	std::string Text;
	std::vector<XMLElement> Elements;
};

XMLElement ParseXML(const std::string &Path);

void PrintXML(const XMLElement &XML);

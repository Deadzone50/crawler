#pragma once

#include <vector>

#include "vector/Vector.h"

class LevelRoom;

struct MapWall
{
	Vec2f S, E;
	float Depth = 0;
	bool Found = false;
};

class Minimap
{
public:
	void Clear() { m_Walls.clear(); }
	void AddRoom(const LevelRoom &Room);
	void Draw(const Vec3f &CameraPos, const Vec3f &LookingDir) const;

private:
	void AddWall(Vec3f S, Vec3f E);
	std::vector<MapWall> m_Walls;
};

#include "Enemy.h"

#include "Debug.h"
#include "Drawer.h"
#include "Event.h"
#include "Level.h"
#include "PlayerClass.h"
#include "Random.h"
#include "States.h"

void EnemyClass::Move(float dt)
{
	Vec3f Dir = m_Goal - m_Pos;
	Dir.y = 0;
	if(LEN2(Dir))
	{
		Dir = NORMALIZE(Dir);

		m_LookingDirection2D.x = Dir.x;
		m_LookingDirection2D.y = Dir.z;
		m_LookingDirection2D = NORMALIZE(m_LookingDirection2D);
		m_LookingDirection = {m_LookingDirection2D.x, 0, m_LookingDirection2D.y};
		m_Pos += dt * Dir * m_Speed;
	}
}

static Vec3f ClosestPlayerPos(const std::vector<PlayerClass> &Players, const Vec3f &Pos)
{
	Vec3f ClosestPlayer = {0, 0, 0};
	float MinDist = FLT_MAX;
	for(const auto &P : Players)
	{
		if(P.IsAlive())
		{
			float Dist = LEN(Pos - P.GetPosition());
			if(Dist < MinDist)
			{
				ClosestPlayer = P.GetPosition();
				MinDist = Dist;
			}
		}
	}
	return ClosestPlayer;
}

static void HandleWaypoints(Vec3f Pos, std::vector<Vec3f> &Waypoints, const Level &Map)
{
	if(Waypoints.size() > 1)
	{
		const Vec3f P = Pos + Vec3f{0, EYEHEIGHT, 0};
		auto it = Waypoints.begin() + 1;
		while(it != Waypoints.end())
		{
			const Vec3f ToNext = *it - P;
			float t = LEN(ToNext);
			bool LineOfSight = !Map.MapRayCollision(P, ToNext / t, t, true);
			if(LineOfSight)
			{
				it = Waypoints.erase(it - 1) + 1;
			}
			else
			{
				break;
			}
		}
	}
}

void EnemyClass::Wander(float dt, GameState &Game, bool LineOfSight, float LenToPlayer, Vec3f ClosestPlayer)
{
	if(LineOfSight && LenToPlayer < m_Inventory.GetCurrentWeapon().GetAttackDistance() + 1)
	{
		m_Wandering = false;
		m_Goal = m_Pos;
		m_WanderTimer = 0;
		Attack(ClosestPlayer);
	}
	else
	{
		if(LEN2((m_Pos - m_Goal) * Vec3f{1, 0, 1}) < 1)	   //reached waypoint
		{
			if(!m_Waypoints.empty())
				m_Waypoints.erase(m_Waypoints.begin());
			else
				Log("ERROR: Waypoints empty");
		}
		if(m_Waypoints.empty())
		{
			m_Waypoints = Game.Map->GetWaypointsToClosestPlayer(m_Pos + Vec3f{0, 1, 0});
			if(m_Waypoints.empty())	   //player outside map
				return;
		}
		m_Goal = m_Waypoints.front();
		Move(dt);

		m_WanderTimer += dt;
		if((m_WanderTime < m_WanderTimer) && (LenToPlayer < m_WanderDistanceToPlayerAtCollision))
		{
			m_Wandering = false;
			m_Goal = m_Pos;
			m_WanderTimer = 0;
		}
	}
}

void EnemyClass::Step(float dt, GameState &Game)
{
	Character::Animate(dt);
	if(m_State == CharacterState::Dead)
		return;

	if(m_Pos.y < -50)
	{
		SetState(CharacterState::Dead);
	}

	Character::StepWeapon(dt);

	m_CurrentSpeed.y -= dt * 9.81f * 5;
	m_Pos += dt * m_CurrentSpeed;

	Vec3f CollisionNormal = HandleCollision(*Game.Map);
	if(!(m_State & CharacterState::Attacking))
	{
		Vec3f ClosestPlayer = ClosestPlayerPos(Game.Players, m_Pos);

		Vec3f ToClosestPlayer = ClosestPlayer - m_Pos;
		float LenToPlayer = LEN(ToClosestPlayer);
		float t = LenToPlayer;
		bool LineOfSight = !Game.Map->MapRayCollision(m_Pos + Vec3f{0, EYEHEIGHT, 0}, ToClosestPlayer / LenToPlayer, t, true);
		if(LineOfSight)
		{
			m_Aware = true;
		}
		if(m_Aware)
		{
			if(m_Wandering)
			{
				Wander(dt, Game, LineOfSight, LenToPlayer, ClosestPlayer);
			}
			else
			{
				if(LineOfSight)
				{
					if(fabsf(CollisionNormal.x) > 0.001f || fabsf(CollisionNormal.z) > 0.001f)	  //if colliding with wall
					{
						m_WanderTime = GetRandomF(1.0f, 5.0f);
						m_Wandering = true;
						m_Waypoints.clear();
						m_WanderDistanceToPlayerAtCollision = LenToPlayer;
						m_Goal = ClosestPlayer;
					}
					else
					{
						m_Waypoints.clear();
						m_Goal = ClosestPlayer;
					}
				}
				else if(m_Waypoints.empty())
				{
					m_Waypoints = Game.Map->GetWaypointsToClosestPlayer(m_Pos + Vec3f{0, 1, 0});
					if(m_Waypoints.empty())	   //outside map
						return;
					HandleWaypoints(m_Pos, m_Waypoints, *Game.Map);
					m_Goal = m_Waypoints.front();
				}
				else if(LEN2((m_Pos - m_Goal) * Vec3f{1, 0, 1}) < 1)	//reached last waypoint
				{
					m_Waypoints.erase(m_Waypoints.begin());
					if(m_Waypoints.empty())
					{
						m_Waypoints = Game.Map->GetWaypointsToClosestPlayer(m_Pos + Vec3f{0, 1, 0});
						if(m_Waypoints.empty())	   //outside map
							return;
						m_Wandering = false;
						m_WanderTimer = 0;
					}
					HandleWaypoints(m_Pos, m_Waypoints, *Game.Map);
					m_Goal = m_Waypoints.front();
				}
				DrawDebugLine(m_Pos, m_Goal, {0, 0, 10});
				float Reach = m_Inventory.GetCurrentWeapon().GetAttackDistance() + 1;
				if(LineOfSight && LenToPlayer < Reach)
				{
					Attack(ClosestPlayer);
				}
				else
				{
					Move(dt);
				}
			}
		}
	}
	if(m_DamageTaken > 0)
	{
		m_Health -= m_DamageTaken;
		m_DamageTaken = 0;
		if(m_Health <= 0)
		{
			SetState(CharacterState::Dead);
		}
	}
}

void EnemyClass::Attack(Vec3f TargetPos)
{
	Vec3f tmp = NORMALIZE(TargetPos - m_Pos);
	m_LookingDirection2D = NORMALIZE(Vec2f{tmp.x, tmp.z});
	m_LookingDirection = {m_LookingDirection2D.x, 0, m_LookingDirection2D.y};
	if(!(m_State & CharacterState::Attacking) && !m_Inventory.GetCurrentWeapon().OnCooldown())
	{
		SetState(m_State | CharacterState::Attacking);
		m_Inventory.GetCurrentWeapon().StartAttack();
	}
}

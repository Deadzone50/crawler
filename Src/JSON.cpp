#include "JSON.h"

#include <fstream>
#include <sstream>

#include "Console.h"

static void SkipWhitespace(std::istringstream &iss)
{
	while(iss.peek() == ' ' || iss.peek() == '\t') iss.ignore();
}

std::string ReadString(std::istringstream &iss)
{
	std::string Result;
	char c;
	SkipWhitespace(iss);
	iss.ignore();	 //'"'
	do
	{
		iss.get(c);
		Result += c;
	}
	while(c != '"');
	Result.pop_back();	  //remove '"'
	return Result;
}

JSONObject ParseObject(std::ifstream &file)
{
	JSONObject Result;
	std::string Line;
	char c;
	do
	{
		getline(file, Line);
		std::istringstream iss(Line);

		std::string Key = ReadString(iss);
		SkipWhitespace(iss);
		iss.ignore(1);	  //remove ':'
		SkipWhitespace(iss);
		c = iss.peek();
		if(c == '[')
		{
			bool Float = false;
			do
			{
				getline(file, Line);
				iss = std::istringstream(Line);
				SkipWhitespace(iss);
				c = iss.peek();
				if(c == '{')
				{
					Result.KeysObjectArrays[Key].emplace_back(ParseObject(file));
					getline(file, Line);
					iss = std::istringstream(Line);
					iss.ignore(256, '}');
				}
				else if(c == '"')
				{
					Result.KeysValueArrays[Key].emplace_back(ReadString(iss));
				}
				else if(c == 't' || c == 'f')	 //boolean
				{
					if(c == 't')
					{
						iss.ignore(4);
						Result.KeysBoolArrays[Key].push_back(true);
					}
					else
					{
						iss.ignore(5);
						Result.KeysBoolArrays[Key].push_back(false);
					}
				}
				else if(c == 'n')
				{
					DebugBreak();
				}	 //null
				else
				{
					auto CurPos = iss.tellg();
					int i;
					iss >> i;
					c = iss.peek();
					if(!Float && (c == -1 || c == ','))
					{
						Result.KeysIntArrays[Key].push_back(i);
					}
					else
					{
						if(!Float)
						{
							Float = true;
							for(auto I : Result.KeysIntArrays[Key])
							{
								Result.KeysFloatArrays[Key].push_back(I);
							}
							Result.KeysIntArrays.erase(Key);
						}
						iss.clear();
						iss.seekg(CurPos);
						float f;
						iss >> f;
						Result.KeysFloatArrays[Key].push_back(f);
					}
				}
				c = iss.get();
			}
			while(c == ',');
			getline(file, Line);
			iss = std::istringstream(Line);
			iss.ignore(256, ']');
		}
		else if(c == '{')
		{
			Result.KeysObjects[Key] = ParseObject(file);
			getline(file, Line);
			iss = std::istringstream(Line);
			iss.ignore(256, '}');
		}
		else if(c == '"')
		{
			Result.KeysValues[Key] = ReadString(iss);
		}
		else if(c == 't' || c == 'f')	 //boolean
		{
			if(c == 't')
			{
				iss.ignore(4);
				Result.KeysBools[Key] = true;
			}
			else
			{
				iss.ignore(5);
				Result.KeysBools[Key] = false;
			}
		}
		else if(c == 'n')
		{
			DebugBreak();
		}	 //null
		else
		{
			auto CurPos = iss.tellg();
			int i;
			iss >> i;
			c = iss.peek();
			if(c == -1 || c == ',')
			{
				Result.KeysInts[Key] = i;
			}
			else
			{
				iss.seekg(CurPos);
				float f;
				iss >> f;
				Result.KeysFloats[Key] = f;
			}
		}
		c = iss.get();
	}
	while(c == ',');

	return Result;
}

JSONObject ParseJSON(const std::string &path)
{
	std::ifstream file;
	file.open(path);
	if(!file.is_open())
	{
		Log(path + " not found");
		return {};
	}

	std::string line;
	getline(file, line);	//{
	if(line[0] != '{')
	{
		Log(path + " is not an json file");
		return {};
	}

	JSONObject result = ParseObject(file);
	return result;
}

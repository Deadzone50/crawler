#include "Debug.h"

#include <chrono>
#include <map>

#include "Drawer.h"
#include "winuser.h"

bool D_NUMPAD[10] = {false};
bool D_NUMPAD_CHANGED[10] = {false};

bool D_Numpad(int i)
{
	return D_NUMPAD[i];
}
bool D_NumpadDown(int i)
{
	return D_NUMPAD[i] && D_NUMPAD_CHANGED[i];
}

extern MegaDrawer DrawHandler;
extern std::map<std::string, float> G_TimeMeasurements;

static std::map<std::string, std::chrono::high_resolution_clock::time_point> last;
void ClearTimestamps()
{
	last.clear();
	G_TimeMeasurements.clear();
}

void DebugTimestamp(std::string Name, uint8_t Number)
{
	std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
	if(Number)
	{
		G_TimeMeasurements[Name + std::to_string(Number)] += std::chrono::duration<float>(now - last[Name]).count();
	}
	last[Name] = now;
}

void DisplayErrorBox(const std::string &Text)
{
	Log(Text);
	std::wstring Tmp = std::wstring(Text.begin(), Text.end());
	int msgboxID = MessageBox(NULL, (LPCWSTR)Tmp.c_str(), (LPCWSTR)L"Error", MB_OK | MB_ICONWARNING);
	abort();
}

void DrawDebugPoint(Vec3f P, Vec3f Color)
{
	DrawHandler.DrawDebugPoint(P, Color);
}

void DrawDebugLine(Vec3f S, Vec3f E, Vec3f Color)
{
	DrawHandler.DrawDebugLine(S, E, Color);
}

void DrawDebugPointPersistent(Vec3f P, Vec3f Color)
{
	DrawHandler.DrawPersistentDebugPoint(P, Color);
}

void DrawDebugLinePersistent(Vec3f S, Vec3f E, Vec3f Color)
{
	DrawHandler.DrawPersistentDebugLine(S, E, Color);
}

void DrawDebugDirLine(Vec3f S, Vec3f E, Vec3f Color)
{
	DrawHandler.DrawDebugDirLine(S, E, Color);
}

void DrawDebugCube(Vec3f Min, Vec3f Max, Vec3f Color)
{
	Vec3f P0 = {Min.x, Min.y, Min.z};
	Vec3f P1 = {Max.x, Min.y, Min.z};
	Vec3f P2 = {Max.x, Max.y, Min.z};
	Vec3f P3 = {Min.x, Max.y, Min.z};

	Vec3f P4 = {Min.x, Min.y, Max.z};
	Vec3f P5 = {Max.x, Min.y, Max.z};
	Vec3f P6 = {Max.x, Max.y, Max.z};
	Vec3f P7 = {Min.x, Max.y, Max.z};

	DrawDebugLine(P0, P1, Color);
	DrawDebugLine(P1, P2, Color);
	DrawDebugLine(P2, P3, Color);
	DrawDebugLine(P3, P0, Color);

	DrawDebugLine(P4, P5, Color);
	DrawDebugLine(P5, P6, Color);
	DrawDebugLine(P6, P7, Color);
	DrawDebugLine(P7, P4, Color);

	DrawDebugLine(P0, P4, Color);
	DrawDebugLine(P1, P5, Color);
	DrawDebugLine(P2, P6, Color);
	DrawDebugLine(P3, P7, Color);
}

void ClearPersistentDebugDraws()
{
	DrawHandler.ClearPersistentDebug();
}

void ToggleDebugDraws()
{
	DrawHandler.ToggleDebug();
}

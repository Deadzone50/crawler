#include "Random.h"

#include <random>

static std::mt19937 gen(0);

void SeedRandomGen(uint32_t Seed)
{
	gen.seed(Seed);
}

uint32_t GetRandomUI(uint32_t Min, uint32_t Max)
{
	return (gen() % (Max - Min + 1) + Min);
}

float GetRandomF(float Min, float Max)
{
	return ((float)gen() / gen.max()) * (Max - Min) + Min;
}

Vec3f GetRandomV3(float Min, float Max)
{
	Vec3f V;
	V.x = ((float)gen() / gen.max()) * (Max - Min) + Min;
	V.y = ((float)gen() / gen.max()) * (Max - Min) + Min;
	V.z = ((float)gen() / gen.max()) * (Max - Min) + Min;
	return V;
}

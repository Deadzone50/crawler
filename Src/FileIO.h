#pragma once

#include <string>
#include <vector>

#include "Mesh.h"

void ParseObjFile(const std::string &Path, const std::string &Name, std::vector<std::string> &ObjectNames,
				  std::vector<Mesh> &Meshes);

void ParseDaeFile(const std::string &Path, std::vector<std::string> &ObjectNames, std::vector<Mesh> &Meshes);

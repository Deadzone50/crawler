#include "LevelObjects.h"

#include "Collision.h"
#include "Debug.h"
#include "Drawer.h"
#include "vector/Matrix.h"

bool LevelCollidable::RayAABBCollision(Vec3f Origin, Vec3f Dir, float &t) const
{
	bool Result = false;
	float Min = 0;
	return RayAABBCollisionTest(Origin, Dir, m_AABB, Min, t);
}

bool LevelCollidable::RayCollision(Vec3f Origin, Vec3f Dir, float &t) const
{
	bool Result = false;
	for(auto &C : m_Colliders)
	{
		if(RayColliderIntersection(Origin, Dir, C, t))
		{
			Result = true;
		}
	}
	return Result;
}

bool LevelCollidable::SphereCollision(Vec3f Origin, float &Radius, Vec3f &Dir) const
{
	bool Result = false;
	float Min = FLT_MAX;
	for(auto &C : m_Colliders)
	{
		Vec3f Normal = SphereColliderIntersection(Origin, Radius, C);
		float Len = LEN2(Normal);
		if(Len && Radius < FLT_MAX)
		{
			Min = Radius;
			Result = true;
			Dir = Normal;
		}
	}
	return Result;
}

void LevelCollidable::UpdateBoundingVolume()
{
	if(m_Mesh.Vertices.empty()) return;
	m_AABB.V1 = {FLT_MAX, FLT_MAX, FLT_MAX};
	m_AABB.V2 = {-FLT_MAX, -FLT_MAX, -FLT_MAX};
	for(auto &V : m_Mesh.Vertices)
	{
		if(V.Position.x < m_AABB.V1.x) m_AABB.V1.x = V.Position.x;
		if(V.Position.y < m_AABB.V1.y) m_AABB.V1.y = V.Position.y;
		if(V.Position.z < m_AABB.V1.z) m_AABB.V1.z = V.Position.z;

		if(V.Position.x > m_AABB.V2.x) m_AABB.V2.x = V.Position.x;
		if(V.Position.y > m_AABB.V2.y) m_AABB.V2.y = V.Position.y;
		if(V.Position.z > m_AABB.V2.z) m_AABB.V2.z = V.Position.z;
	}
	m_Mesh.UpdateBuffers();
}

void LevelCollidable::SetPos(Vec3f Pos)
{
	Vec3f Center = {};
	uint32_t Num = 0;
	float Lowest = FLT_MAX;
	for(auto &V : m_Mesh.Vertices)
	{
		Center += V.Position;
		++Num;
		if(V.Position.y < Lowest)
		{
			Lowest = V.Position.y;
		}
	}
	Center = Center / (float)Num;
	Center.y = Lowest;

	for(auto &V : m_Mesh.Vertices)
	{
		V.Position = V.Position - Center + Pos;
	}
	for(auto &C : m_Colliders)
	{
		auto *C1 = static_cast<MeshCollider *>(C);
		for(auto &P : C1->Points)
		{
			P = P - Center + Pos;
		}
	}
}

void LevelCollidable::MovePos(Vec3f Pos)
{
	for(auto &V : m_Mesh.Vertices)
	{
		V.Position += Pos;
	}
	for(auto &C : m_Colliders)
	{
		auto *C1 = static_cast<MeshCollider *>(C);
		for(auto &P : C1->Points)
		{
			P += Pos;
		}
	}
	m_AABB.Min += Pos;
	m_AABB.Max += Pos;
}

//Return center of room floor level
Vec3f LevelCollidable::GetPos() const
{
	Vec3f Center = {};
	uint32_t Num = 0;
	float Lowest = FLT_MAX;
	for(auto &V : m_Mesh.Vertices)
	{
		Center += V.Position;
		++Num;
		if(V.Position.y < Lowest)
		{
			Lowest = V.Position.y;
		}
	}
	Center = Center / (float)Num;
	Center.y = Lowest;
	return Center;
}

void LevelCollidable::Rotate(float Angle)
{
	Mat3f Rot;
	Rot.SetRotation({0, Angle, 0});
	for(auto &V : m_Mesh.Vertices)
	{
		V.Position = Rot * V.Position;
	}
}

LevelObject::LevelObject(Mesh *M, std::function<void()> OnUse)
{
	m_Mesh = *M;
	m_Mesh.Init();
	m_OnUse = OnUse;
	UpdateBoundingVolume();
}

void LevelObject::Use()
{
	m_OnUse();
}

void LevelObject::DrawHighlight() const
{
	Vec3f A[2] = {m_AABB.V1, m_AABB.V2};
	Vec3f P[8] = {};
	for(uint8_t z = 0; z < 2; ++z)
	{
		for(uint8_t y = 0; y < 2; ++y)
		{
			for(uint8_t x = 0; x < 2; ++x)
			{
				float X = A[x].x;
				float Y = A[y].y;
				float Z = A[z].z;
				P[z * 4 + y * 2 + x] = {X, Y, Z};
			}
		}
	}
	DrawLine(P[0], P[1], {1, 0, 0});
	DrawLine(P[2], P[3], {1, 0, 0});
	DrawLine(P[0], P[2], {1, 0, 0});
	DrawLine(P[1], P[3], {1, 0, 0});

	DrawLine(P[4], P[5], {1, 0, 0});
	DrawLine(P[6], P[7], {1, 0, 0});
	DrawLine(P[4], P[6], {1, 0, 0});
	DrawLine(P[5], P[7], {1, 0, 0});

	DrawLine(P[1], P[5], {1, 0, 0});
	DrawLine(P[0], P[4], {1, 0, 0});
	DrawLine(P[2], P[6], {1, 0, 0});
	DrawLine(P[3], P[7], {1, 0, 0});
}

static std::vector<Collider *> CreateColliders(const std::vector<const Mesh *> &CollisionMeshes)
{
	std::vector<Collider *> Result;
	for(auto C : CollisionMeshes)
	{
		Result.push_back(CreateCollider(C, {1, 1, 1}, Collider::Type::Mesh));
	}
	return Result;
}

LevelRoom::LevelRoom()
{
	m_AABB.Min = {-2.5f, 0, -2.5f};
	m_AABB.Max = {2.5f, 5.0f, 2.5f};
}

LevelRoom::LevelRoom(const Mesh &M, const std::vector<const Mesh *> &CollisionMeshes, int Type, bool Decoration) : LevelRoom()
{
	m_Mesh = M;
	m_Colliders = CreateColliders(CollisionMeshes);
	m_Type = Type;
	m_Decoration = Decoration;

	if(m_Colliders.empty())
	{
		m_NavZone.Position = (m_AABB.V1 + m_AABB.V2) / 2;
	}
	else
	{
		uint32_t NumPoints = 0;
		for(const auto &C : m_Colliders)
		{
			const auto *C1 = static_cast<MeshCollider *>(C);
			for(const auto &P : C1->Points)
			{
				m_NavZone.Position += P;
				++NumPoints;
			}
		}
		m_NavZone.Position = m_NavZone.Position / NumPoints + Vec3f{0, 1, 0};
	}
}

void LevelRoom::MovePos(Vec3f Pos)
{
	LevelCollidable::MovePos(Pos);
	m_NavZone.Position += Pos;
}

void LevelRoom::UpdateNavZone(const LevelRoom &Other, int Index)
{
	for(auto &N : m_NavZone.Neighbours)
	{
		if(N.RoomIndex == Index) return;
	}
	const NavZone &ONav = Other.GetNavZone();
	float Dist = LEN(ONav.Position - m_NavZone.Position);
	m_NavZone.Neighbours.push_back({Index, Dist});
}

bool MapObject::MapRayCollision(Vec3f Origin, Vec3f Direction, float &t, bool EarlyReturn) const
{
	bool HitWall = false;
	for(const auto &LP : Rooms)
	{
		AxisAlignedBoundingBox AABB = LP.GetBoundingVolume();
		float tMin = 0;
		float tMax = t;
		if(RayAABBCollisionTest(Origin, Direction, AABB, tMin, tMax))
		{
			if(LP.RayCollision(Origin, Direction, t))
			{
				HitWall = true;
				if(EarlyReturn) return HitWall;
			}
		}
	}
	//bool HitObject = RayObjectCollision(Origins, Directions, t) != nullptr;
	return HitWall;
}

std::vector<bool> MapObject::MapRayCollision(std::vector<Vec3f> Origins, std::vector<Vec3f> Directions,
											 std::vector<float> &t) const
{
	uint32_t NumTests = (uint32_t)Origins.size();
	std::vector<bool> Result(NumTests, false);
	for(auto &LP : Rooms)
	{
		AxisAlignedBoundingBox AABB = LP.GetBoundingVolume();
		for(uint32_t i = 0; i < NumTests; ++i)
		{
			float tMin = 0;
			float tMax = t[i];
			if(RayAABBCollisionTest(Origins[i], Directions[i], AABB, tMin, tMax))
			{
				if(LP.RayCollision(Origins[i], Directions[i], t[i]))
				{
					Result[i] = true;
				}
			}
		}
	}
	return Result;
}

//Check if collider collides with level colliders, returns collision vector
Vec3f MapObject::MapCollisionTest(const SphereCollider &Col, float &Amount) const
{
	Vec3f Result = {0, 0, 0};
	auto V = GetPossibleRooms(Col);
	float Radius = Col.Radius;
	for(auto I : V)
	{
		const auto &LP = Rooms[I];
		//for(const auto &C : LP.GetColliders()) DrawCollider(C, {100, 0, 0});
		Vec3f Dir = {};
		if(LP.SphereCollision(Col.Position, Radius, Dir))
		{
			Result = Dir;
			Amount = (Col.Radius - Radius);
		}
	}
	return Result;
}

static void DrawNode(const OctreeNode &Node, const std::vector<LevelRoom> &Rooms)
{
	DrawDebugCube(Node.AABB.Min, Node.AABB.Max, Node.Color);
	if(Node.Children.empty())
	{
		for(int I : Node.RoomIndices)
		{
			const AxisAlignedBoundingBox AABB = Rooms[I].GetBoundingVolume();
			DrawDebugCube(AABB.V1 + Vec3f{0.01f, 0.01f, 0.01f}, AABB.V2 + Vec3f{0.01f, 0.01f, 0.01f}, Node.Color);
		}
	}
	else
	{
		for(const auto &C : Node.Children)
		{
			DrawNode(C, Rooms);
		}
	}
}

void MapObject::DrawOctree() const
{
	DrawNode(m_Octree.m_RootNode, Rooms);
}

void MapObject::DrawColliders() const
{
	for(const auto &R : Rooms)
	{
		const auto &Colliders = R.GetColliders();
		for(const auto &C : Colliders)
		{
			DrawCollider(C, {10, 0, 10});
		}
	}
}

void MapObject::CreateOctree(const std::vector<std::vector<std::vector<int>>> &NodeToRoomMap)
{
	//TODO Resize to actual map geometry

	const uint32_t d = NodeToRoomMap.size();
	const uint32_t h = NodeToRoomMap[0].size();
	const uint32_t w = NodeToRoomMap[0][0].size();

	uint32_t d2 = 1;
	uint32_t h2 = 1;
	uint32_t w2 = 1;
	//make boundraries line up, if level is not a power of 2
	while(d2 < d) d2 *= 2;
	while(h2 < h) h2 *= 2;
	while(w2 < w) w2 *= 2;
	const Vec3f Center = Vec3f{w2 * ROOM_SIZE, h2 * ROOM_SIZE, d2 * ROOM_SIZE} / 2 - Vec3f{ROOM_SIZE / 2, 0, ROOM_SIZE / 2};

	uint32_t Size = std::max(std::max(d2, h2), w2);	   //Take largest size

	AxisAlignedBoundingBox RootAABB = {};
	RootAABB.Min = Center - Size / 2 * Vec3f{1, 1, 1} * ROOM_SIZE;
	RootAABB.Max = Center + Size / 2 * Vec3f{1, 1, 1} * ROOM_SIZE;
	m_Octree.m_RootNode = CreateOctreeAABBs(RootAABB);

	for(uint32_t z = 0; z < d; ++z)
	{
		for(uint32_t y = 0; y < h; ++y)
		{
			for(uint32_t x = 0; x < w; ++x)
			{
				int i = NodeToRoomMap[z][y][x];
				if(i != -1)
				{
					const Vec3f CenterOfRoom = {x * ROOM_SIZE, y * ROOM_SIZE + ROOM_SIZE / 2, z * ROOM_SIZE};
					m_Octree.AddRoom(i, CenterOfRoom);
				}
			}
		}
	}
	m_Octree.Clean();
}

#include "Lights.h"

#include <string>

#include "Shader.h"

void Light::AddToRender(Shader &S, int Index)
{
	S.SetVec3("Lights[" + std::to_string(Index) + "].Pos", m_Position);
	S.SetVec3("Lights[" + std::to_string(Index) + "].Color", m_Color);
}

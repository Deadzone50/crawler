#pragma once

#include "Character.h"
#include "CharacterLevel.h"
#include "Globals.h"
#include "Minimap.h"
#include "RenderText.h"
#include "vector/Matrix.h"
#include "vector/Vector.h"

class Level;

class PlayerClass : public Character
{
public:
	PlayerClass();

	void SetRotation(Vec2f Angles);
	void Rotate(Vec2f Angles);
	Mat4f GetViewMatrix();

	void SetMovement(const Vec3f &Amount);

	void Jump();
	void Interact(GameState &Game);
	void Attack();
	void SwitchWeapon(uint8_t Number);

	void Step(float dt, GameState &Game);
	void Reset();

	void UpdateUI();
	void RenderUI(Shader &Shader);
	void UpdateMinimap(const Minimap &MM) { m_Minimap = MM; }
	void DrawMinimap() const;
	void DrawWeapon(Shader &Shader);

	bool m_Debug = false;
	bool m_ShowMinimap = true;
	bool m_ShowInventory = false;

	Vec3f GetCameraPos() const { return m_Pos + Vec3f{0, EYEHEIGHT, 0}; }
	Vec3f GetLookingDir() const { return m_LookingDirection; }

	void SetModifiers(const std::map<SkillTag, int> &Modifiers);

private:
	Vec2f m_Rotation;

	RenderText m_HudAmmo, m_HudHealth;

	Minimap m_Minimap;
};

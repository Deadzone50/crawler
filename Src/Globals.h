#pragma once

constexpr float PI = 3.14159265359f;

constexpr float ROOM_SIZE = 5.0f;

constexpr float EYEHEIGHT = 1.7f;
constexpr float WEAPONHEIGHT = 1.3f;

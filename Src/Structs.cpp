#include "Structs.h"

#include "Drawer.h"

void DrawAABB(const AxisAlignedBoundingBox &AABB, Vec3f Col)
{
	const Vec3f P0 = {AABB.Min.x, AABB.Min.y, AABB.Min.z};
	const Vec3f P1 = {AABB.Max.x, AABB.Min.y, AABB.Min.z};
	const Vec3f P2 = {AABB.Max.x, AABB.Max.y, AABB.Min.z};
	const Vec3f P3 = {AABB.Min.x, AABB.Max.y, AABB.Min.z};

	const Vec3f P4 = {AABB.Min.x, AABB.Min.y, AABB.Max.z};
	const Vec3f P5 = {AABB.Max.x, AABB.Min.y, AABB.Max.z};
	const Vec3f P6 = {AABB.Max.x, AABB.Max.y, AABB.Max.z};
	const Vec3f P7 = {AABB.Min.x, AABB.Max.y, AABB.Max.z};

	DrawLine(P0, P1, Col);
	DrawLine(P1, P2, Col);
	DrawLine(P2, P3, Col);
	DrawLine(P3, P0, Col);

	DrawLine(P4, P5, Col);
	DrawLine(P5, P6, Col);
	DrawLine(P6, P7, Col);
	DrawLine(P7, P4, Col);

	DrawLine(P0, P4, Col);
	DrawLine(P1, P5, Col);
	DrawLine(P2, P6, Col);
	DrawLine(P3, P7, Col);
}

#pragma once
#include <string>
#include <vector>

#include "Mesh.h"
#include "Rigging.h"

void ParseGlTFFile(const std::string &Path, std::vector<std::string> &MeshNames, std::vector<Mesh> &Meshes,
				   std::vector<MeshRig> &MeshRigs);

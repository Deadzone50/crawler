#pragma once

#include <string>

#include "vector/Vector.h"

struct Settings
{
	std::string Resolution = "800X600";
	std::string WFCFile = "WFC.txt";
	Vec3i Size = {10, 5, 10};
	uint32_t Seed = 0;
	int Volume = 20;
};

void ParseSettingsFile(Settings &Set);

void SaveSettingsFile(Settings &Set);

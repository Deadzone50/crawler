#pragma once
#include "vector/Vector.h"

struct GLFWwindow;

#define MOUSE_LEFT 0
#define MOUSE_MIDDLE 1
#define MOUSE_RIGHT 2
#define MOUSE_WHEEL_UP 3
#define MOUSE_WHEEL_DOWN 4

enum Key : uint16_t
{
	LEFT = 350,
	RIGHT,
	UP,
	DOWN,
	STRAFE_LEFT,
	STRAFE_RIGHT,
	FORWARD,
	BACKWARD,
	JUMP,
	USE,
	RUN,
	QUIT,
	CONSOLE,
	MAP,
	INVENTORY,
	BACKSPACE,
	ENTER,
	BACK
};

void KeyCallback(GLFWwindow *Window, int Key, int Scancode, int Action, int Mods);

void MousePosCallback(GLFWwindow *Window, double Xpos, double Ypos);
void MouseButtonCallback(GLFWwindow *Window, int Button, int Action, int Mods);
void MouseScrollCallback(GLFWwindow *Window, double Xoffset, double Yoffset);

void HandleKeysDebug();
void HandleKeysCommon();

void HandleMousePlayer();
void HandleKeysPlayer();

void HandleMouseInventory();
void HandleKeysInventory();

void ResetMouse();
void ResetChangedKeys();
void HideMouse();
void ShowMouse();

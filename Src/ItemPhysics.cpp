#include "Item.h"

#include <algorithm>
#include <unordered_set>

#include "Colliders.h"
#include "Collision.h"
#include "Console.h"

AxisAlignedBoundingBox Item::GetAABB() const
{
	//TODO: fix this, make level rooms meshes use transforms and colliders
	//auto AABB = CalculateAABB(m_Scale, m_Properties.Mesh, m_RotationMatrix);
	auto AABB = CalculateAABB(m_Colliders, Mat3f());
	//const Vec3f Offset = -(AABB.Min + AABB.Max) / 2.0f;
	//AABB.Min += Offset + m_Pos;
	//AABB.Max += Offset + m_Pos;
	return AABB;
}

void Item::SetPhysicalProperties(Vec3f Scale, float InverseMass, MomentOfInertiaType MOIType)
{
	m_InverseMass = InverseMass;
	m_Scale = Scale;
	m_Active = (InverseMass != 0);

	const AxisAlignedBoundingBox AABB = CalculateAABB(Scale, m_Properties.Mesh, Mat3f());
	const Vec3f Offset = -(AABB.Min + AABB.Max) / 2.0f;

	//Sides of box
	const Vec3f Size = AABB.Max - AABB.Min;
	const float W2 = Size.x * Size.x;
	const float H2 = Size.y * Size.y;
	const float D2 = Size.z * Size.z;
	//Radius of cylinder/sphere
	const float R2 = Size.x * Size.x / 4;

	//inverse of diagonal matrix is the elements inverted
	switch(MOIType)
	{
		case MomentOfInertiaType::None: m_MaxCornerDistSqr = (W2 + H2 + D2) / 4; break;
		case MomentOfInertiaType::Box:
			m_InverseMomentOfInertia.m[0] = 12.0f * InverseMass / (H2 + D2);
			m_InverseMomentOfInertia.m[4] = 12.0f * InverseMass / (W2 + D2);
			m_InverseMomentOfInertia.m[8] = 12.0f * InverseMass / (W2 + H2);
			m_MaxCornerDistSqr = (W2 + H2 + D2) / 4;
			break;
		case MomentOfInertiaType::Cylinder:
			m_InverseMomentOfInertia.m[0] = 12.0f * InverseMass / (3 * R2 + H2);
			m_InverseMomentOfInertia.m[4] = 2.0f * InverseMass / R2;
			m_InverseMomentOfInertia.m[8] = 12.0f * InverseMass / (3 * R2 + H2);
			m_MaxCornerDistSqr = R2 + H2 / 4;
			break;
		case MomentOfInertiaType::Sphere:
			m_InverseMomentOfInertia.m[0] = 2.5f * InverseMass / R2;
			m_InverseMomentOfInertia.m[4] = 2.5f * InverseMass / R2;
			m_InverseMomentOfInertia.m[8] = 2.5f * InverseMass / R2;
			m_MaxCornerDistSqr = R2;
			break;
	}
	m_InverseMomentOfInertiaWorld = m_InverseMomentOfInertia;
}

void Item::Step(float dt)
{
	if(m_InverseMass == 0) return;
	if(m_Pos.y < -50)
	{
		m_Active = false;
		return;
	}
	//Kinetic energy = 0.5*(mv^2 + Iw^2) ~ v^2 + w^2
	float Energy = DOT(m_Velocity, m_Velocity) + DOT(m_AngularVelocity, m_AngularVelocity);
	float Bias = 0.95f;
	//lerp between old and new;
	m_RWAEnergy = Bias * m_RWAEnergy + (1 - Bias) * Energy;

	if(m_RWAEnergy < m_RWASleep)
	{
		m_Active = false;
		m_Velocity = {0, 0, 0};
		m_AngularVelocity = {0, 0, 0};
	}
	else if(m_RWAEnergy > 5 * m_RWASleep)
	{
		m_RWAEnergy = 5 * m_RWASleep;
		m_Active = true;
	}
	if(m_Active)
	{
		//F = m*a
		//V = V + a*dt
		//x = x + V*dt
		const Vec3f F = Vec3f{0, -9.81f, 0} / m_InverseMass;
		m_Velocity += F * m_InverseMass * dt;
		m_Pos += m_Velocity * dt;

		//T = I*a
		//w = w + a*dt
		//o = o + w*dt
		m_AngularVelocity += m_InverseMomentOfInertiaWorld * m_Torque * dt;
		if(LEN2(m_AngularVelocity))	   //rotate current rotation by amount this step
		{
			const Vec3f Axis = NORMALIZE(m_AngularVelocity);
			const float Angle = LEN(m_AngularVelocity) * dt;
			m_RotationMatrix.Rotate(Axis, Angle);
			m_InvRotationMatrix = TRANSPOSE(m_RotationMatrix);
			//I_w = R_o * I_o * R_o^T
			//I_w^-1 = R_o^T^-1 * I_o^-1 * R_o^-1
			//I_w^-1 = R_o * I_o^-1 * R_o^T
			m_InverseMomentOfInertiaWorld = m_RotationMatrix * m_InverseMomentOfInertia * m_InvRotationMatrix;
		}
		UpdateColliders();
	}
}

void Item::AddImpulse(Vec3f Impulse, Vec3f ToPoint)
{
	//J = F*dt = m*dv
	//dv = J/m
	m_Velocity += m_InverseMass * Impulse;

	//T = r x F
	//T*dt = I*dw = r x J
	//dw = I^-1 * r x J
	m_AngularVelocity += m_InverseMomentOfInertiaWorld * CROSS(ToPoint, Impulse);
}

void Item::UpdateColliders()
{
	for(auto C : m_Colliders)
	{
		if(C->ColliderType == Collider::Type::Mesh)
		{
			MeshCollider *MC = static_cast<MeshCollider *>(C);
			MC->Points.clear();
			for(auto &P : MC->OriginalPoints)
			{
				MC->Points.push_back(m_Pos + m_RotationMatrix * P);
			};
		}
		else if(C->ColliderType == Collider::Type::Sphere)
		{
			SphereCollider *MC = static_cast<SphereCollider *>(C);
			MC->Position = m_Pos + MC->Offset;
		}
	}
}

void Item::SetRotation(Vec4f Quaternion)
{
	Mat4f Rot;
	Rot.Rotate(Quaternion);

	m_RotationMatrix = Rot.GetRotation();
	m_InvRotationMatrix = TRANSPOSE(m_RotationMatrix);
	m_InverseMomentOfInertiaWorld = m_RotationMatrix * m_InverseMomentOfInertia * m_InvRotationMatrix;
}

#pragma once

#include "vector/Matrix.h"
#include "vector/Vector.h"

Vec4f Quaternion(Vec3f Axis, float Angle);

Vec4f QMult(Vec4f V1, Vec4f V2);

Vec4f QSlerp(Vec4f Q1, Vec4f Q2, float t);

Vec4f QNlerp(Vec4f Q1, Vec4f Q2, float t);

Vec4f MatToQuaternion(const Mat4f &M);

Vec4f MatToQuaternion(const Mat3f &M);

#pragma once

#include <string>

#include "vector/Matrix.h"

class Shader
{
public:
	Shader() { ID = 0; };
	Shader(const char *VertexPath, const char *FragmentPath, const char *GeometryPath = NULL);

	void Use();

	void SetBool(const std::string &Name, bool Value) const;
	void SetInt(const std::string &Name, int Value) const;
	void SetSampler(const std::string &Name, int Value) const;
	void SetFloat(const std::string &Name, float Value) const;
	void SetVec2(const std::string &Name, const Vec2f &Vec) const;
	void SetVec3(const std::string &Name, const Vec3f &Vec) const;
	void SetMat4(const std::string &Name, const Mat4f &Mat) const;

	unsigned int ID;
};

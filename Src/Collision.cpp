#include "Collision.h"

#include <algorithm>

#include "Drawer.h"
#include "Level.h"

int RayTriangleIntersection(Vec3f Origin, Vec3f Direction, Vec3f Triangle[3], float &tmax)
{
	Vec3f N = NORMALIZE(CROSS(Triangle[1] - Triangle[0], Triangle[2] - Triangle[0]));
	float Denominator = DOT(N, Direction);
	if(fabsf(Denominator) < 0.0001f)	//parallel
		return 0;

	float D = DOT(N, Triangle[0]);
	float t = -(DOT(N, Origin) - D) / Denominator;

	if(t < 0 || t >= tmax)	  //behind or too far away
		return 0;

	Vec3f Cross;

	Vec3f P = Origin + t * Direction;
	Vec3f Edge0 = Triangle[1] - Triangle[0];	//check if point is inside the triangle
	Vec3f ToPoint0 = P - Triangle[0];
	Cross = CROSS(Edge0, ToPoint0);
	if(DOT(N, Cross) < 0) return 0;

	Vec3f Edge1 = Triangle[2] - Triangle[1];	//check if point is inside the triangle
	Vec3f ToPoint1 = P - Triangle[1];
	Cross = CROSS(Edge1, ToPoint1);
	if(DOT(N, Cross) < 0) return 0;

	Vec3f Edge2 = Triangle[0] - Triangle[2];	//check if point is inside the triangle
	Vec3f ToPoint2 = P - Triangle[2];
	Cross = CROSS(Edge2, ToPoint2);
	if(DOT(N, Cross) < 0) return 0;

	tmax = t;
	if(Denominator > 0) return -1;	  //backside of triangle hit
	else return 1;
}

Vec3f SphereTriangleIntersection(Vec3f Origin, float &Radius, Vec3f Triangle[3])
{
	Vec3f TriangleN = NORMALIZE(CROSS(Triangle[1] - Triangle[0], Triangle[2] - Triangle[0]));

	float TDistanceFromOrigin = DOT(TriangleN, Triangle[0]);
	float SDistanceFromOrigin = DOT(TriangleN, Origin);

	float Dist = TDistanceFromOrigin - SDistanceFromOrigin;	   //distance from sphere center to triangle-plane
	if(abs(Dist) > Radius) return {0, 0, 0};

	Vec3f P = Origin + Dist * TriangleN;

	Vec3f Edge0 = Triangle[1] - Triangle[0];	//check if point is inside the triangle
	Vec3f Edge1 = Triangle[2] - Triangle[1];	//check if point is inside the triangle
	Vec3f Edge2 = Triangle[0] - Triangle[2];	//check if point is inside the triangle

	Vec3f ToPoint0 = P - Triangle[0];
	Vec3f ToPoint1 = P - Triangle[1];
	Vec3f ToPoint2 = P - Triangle[2];

	Vec3f Cross0 = CROSS(Edge0, ToPoint0);
	Vec3f Cross1 = CROSS(Edge1, ToPoint1);
	Vec3f Cross2 = CROSS(Edge2, ToPoint2);

	bool Outside = (DOT(TriangleN, Cross0) < 0) || (DOT(TriangleN, Cross1) < 0) || (DOT(TriangleN, Cross2) < 0);
	if(!Outside)
	{
		Radius = abs(Dist);	   //TODO: should this be able to be negative?
		return TriangleN;
	}
	else return {0, 0, 0};	  //TODO: handle sphere overlap with edges
}

bool RayAABBCollisionTest(Vec3f RayOrigin, Vec3f RayDir, AxisAlignedBoundingBox AABB, float &MinLen, float &MaxLen)
{
	//RO.x + t*RD.x == Min.x -> collision with zy-plane
	//t = Min.x - RO.x / RD.x

	Vec3f InvDir;				   //precompute inverse of direction
	InvDir.x = 1.0f / RayDir.x;	   //floating point division by zero defined: +Inf
	InvDir.y = 1.0f / RayDir.y;
	InvDir.z = 1.0f / RayDir.z;

	Vec3i Swap;	   //precalculate if the bounds have to be swapped
	Swap.x = (RayDir.x < 0);
	Swap.y = (RayDir.y < 0);
	Swap.z = (RayDir.z < 0);

	float T1X = (AABB.V[Swap.x].x - RayOrigin.x) * InvDir.x;	//intersection x	//Ro + Rd*t ,t[0,Tmax]
	float T2X = (AABB.V[1 - Swap.x].x - RayOrigin.x) * InvDir.x;

	float T1Y = (AABB.V[Swap.y].y - RayOrigin.y) * InvDir.y;	//intersection y
	float T2Y = (AABB.V[1 - Swap.y].y - RayOrigin.y) * InvDir.y;

	if(T1X < T1Y)	 //max of mins
		T1X = T1Y;
	if(T2X > T2Y)	 //min of maxs
		T2X = T2Y;

	float T1Z = (AABB.V[Swap.z].z - RayOrigin.z) * InvDir.z;	//intersection z
	float T2Z = (AABB.V[1 - Swap.z].z - RayOrigin.z) * InvDir.z;

	if(T1X < T1Z)	 //max of mins
		T1X = T1Z;
	if(T2X > T2Z)	 //min of maxs
		T2X = T2Z;

	if(T1X > MaxLen)	//check that it isn't too far
		return false;
	if(T1X > T2X)	 //miss
	{
		return false;
	}
	else if(T2X < 0)	//behind, miss
	{
		return false;
	}
	else if(T1X > 0)	//hit
	{
		MinLen = T1X;
		MaxLen = T2X;
		return true;
	}
	else
	{	 //origin inside box
		MinLen = 0;
		MaxLen = T2X;
		return true;
	}
}

bool RayPlaneCollisionTest(Vec3f PlaneN, Vec3f PlaneP, Vec3f RayOrigin, Vec3f RayDir, float &MaxLen)
{
	float d = -DOT(PlaneN, PlaneP);
	float t = (-d - DOT(PlaneN, RayOrigin)) / (DOT(PlaneN, RayDir));
	if(t >= 0 && t < MaxLen)
	{
		MaxLen = t;
		return true;
	}
	return false;
}

bool RaySphereCollisionTest(Vec3f Origin, Vec3f Dir, float &MaxLen, Vec3f SphereOrigin, float SphereRadius)
{
	Vec3f FromSphereToRay = Origin - SphereOrigin;
	float b = DOT(FromSphereToRay, Dir);
	float Distance2 = DOT(FromSphereToRay, FromSphereToRay) - SphereRadius * SphereRadius;

	//Exit if r�s origin outside s (Distance2 > 0) and r pointing away from s (b > 0)
	if(Distance2 > 0.0f && b > 0.0f)
	{
		return false;
	}
	float discr = b * b - Distance2;

	//A negative discriminant corresponds to ray missing sphere
	if(discr < 0.0f)
	{
		return false;
	}

	//Ray now found to intersect sphere, compute smallest t value of intersection
	float t = -b - sqrtf(discr);

	//If t is negative, ray started inside sphere so clamp t to zero
	if(t < 0.0f)
	{
		t = 0.0f;
	}

	if(t < MaxLen)
	{
		MaxLen = t;
		return true;
	}
	else
	{
		return false;
	}
}

bool ColliderAABBCollisionTest(const SphereCollider &Col, const AxisAlignedBoundingBox AABB)
{
	if(Col.Position.x + Col.Radius < AABB.Min.x || Col.Position.x - Col.Radius > AABB.Max.x ||
	   Col.Position.y + Col.Radius < AABB.Min.y || Col.Position.y - Col.Radius > AABB.Max.y ||
	   Col.Position.z + Col.Radius < AABB.Min.z || Col.Position.z - Col.Radius > AABB.Max.z)
		return false;
	else return true;
}

bool InAABB(Vec3f Pos, const AxisAlignedBoundingBox AABB)
{
	return (AABB.Min.x < Pos.x && Pos.x < AABB.Max.x) && (AABB.Min.y < Pos.y && Pos.y < AABB.Max.y) &&
		   (AABB.Min.z < Pos.z && Pos.z < AABB.Max.z);
}

bool AABBOverlapTest(const AxisAlignedBoundingBox &BB1, const AxisAlignedBoundingBox &BB2)
{
	return (BB1.Max.x > BB2.Min.x && BB1.Min.x < BB2.Max.x) && (BB1.Max.y > BB2.Min.y && BB1.Min.y < BB2.Max.y) &&
		   (BB1.Max.z > BB2.Min.z && BB1.Min.z < BB2.Max.z);
}

static void CalcImpulse(Item &O1, Item &O2, Vec3f Point, Vec3f Normal, float Penetration)
{
	//			    ((V_a + (w_a x r_a) - (V_b + (w_b x r_b)).n
	//J = -(1+e) * -----------------------------------------------------------------------
	//			    1/m_a + 1/m_b + [(I_a^-1 * r_a x n) x r_a + (I_b^-1 * r_b x n) x r_b].n

	//relative velocity
	const Vec3f R1 = Point - O1.GetPos();
	const Vec3f R2 = Point - O2.GetPos();
	const Vec3f N = Normal;

	Vec3f V1 = O1.GetVelocity() + CROSS(O1.GetAngularVelocity(), R1);
	Vec3f V2 = O2.GetVelocity() + CROSS(O2.GetAngularVelocity(), R2);
	Vec3f V = V1 - V2;

	//along collision normal
	float VN = DOT(V, N);

	if(VN > 0)	  //moving apart
	{
		return;
	}

	const float InvMass1 = O1.GetInvMass();
	const float InvMass2 = O2.GetInvMass();
	const Mat3f InvMoI1 = O1.GetInvMomentOfInertiaWorld();
	const Mat3f InvMoI2 = O2.GetInvMomentOfInertiaWorld();

	//restutution
	const float e = O1.GetRestitution();

	//impulse scalar
	const Vec3f a = CROSS(InvMoI1 * CROSS(R1, N), R1);
	const Vec3f b = CROSS(InvMoI2 * CROSS(R2, N), R2);
	float J = -(1 + e) * VN / ((InvMass1 + InvMass2) + DOT(a + b, N));

	if(InvMass1) O1.AddImpulse(J * N, R1);
	if(InvMass2) O2.AddImpulse(-J * N, R2);
	//friction
	//re-calculate relative velocity
	V1 = O1.GetVelocity() + CROSS(O1.GetAngularVelocity(), R1);
	V2 = O2.GetVelocity() + CROSS(O2.GetAngularVelocity(), R2);
	V = V1 - V2;
	Vec3f t = V - DOT(V, N) * N;	//remove component going in normal direction
	if(LEN2(t))						//check that there is movement in other than normal
	{
		const Vec3f T = NORMALIZE(t);

		//along tanget
		float VT = -DOT(V, T);
		const Vec3f at = CROSS(InvMoI1 * CROSS(R1, T), R1);
		const Vec3f bt = CROSS(InvMoI2 * CROSS(R2, T), R2);
		float JT = VT / ((InvMass1 + InvMass2) + DOT(at + bt, T));

		const float StaticFriction = 0.4f;
		const float DynamicFriction = 0.2f;

		//Coulumb's law, F_f <= uF_n
		float FrictionImpulse;
		if(fabsf(JT) < StaticFriction * J)
		{
			FrictionImpulse = JT;
		}
		else
		{
			FrictionImpulse = -J * DynamicFriction;
		}
		if(InvMass1) O1.AddImpulse(FrictionImpulse * T, R1);
		if(InvMass2) O2.AddImpulse(-FrictionImpulse * T, R2);
	}
	//stop sinking
	float Slop = 0.01f;
	Vec3f C = std::max(Penetration - Slop, 0.0f) * 0.2f * N;
	if(InvMass1) O1.Move(C);
	if(InvMass2) O2.Move(-C);
}

//returns most negative point
static void ProjectPointsToAxis(const std::vector<Vec3f> &P, const Vec3f &Normal, float &Min, float &Max)
{
	for(const auto &p : P)
	{
		//Negative behind plane
		float t1 = DOT(Normal, p);
		if(t1 < Min)
		{
			Min = t1;
		}
		if(t1 > Max)
		{
			Max = t1;
		}
	}
}

struct CollisionFace
{
	int FaceIndex;
	float Penetration;
};

struct FaceCollision
{
	Vec3f Normal;
	int FaceIndex;
	float Penetration = FLT_MAX;
};

struct EdgeCollision
{
	Vec3f Normal;
	int Edge1Index, Edge2Index;
	float Penetration = FLT_MAX;
};

static FaceCollision GetFaceCollision(const std::vector<Vec3f> &P1, const std::vector<Vec3f> &P2, const std::vector<Face> &Faces)
{
	FaceCollision Result;
	for(int i = 0; i < Faces.size(); ++i)
	{
		const auto &F = Faces[i];
		const Vec3f Normal = GetFaceNormal(P1, F);
		float Min1 = FLT_MAX;
		float Max1 = -FLT_MAX;
		float Min2 = FLT_MAX;
		float Max2 = -FLT_MAX;
		ProjectPointsToAxis(P1, Normal, Min1, Max1);
		ProjectPointsToAxis(P2, Normal, Min2, Max2);

		float Penetration = Max1 - Min2;
		if(Penetration > 0 && Min1 <= Max2)	   //check for overlap
		{
			if(Penetration < Result.Penetration)
			{
				Result.Penetration = Penetration;
				Result.FaceIndex = i;
				Result.Normal = Normal;
			}
		}
		else
		{
			return {};
		}
	}
	return Result;
}

static EdgeCollision GetEdgeCollision(Vec3f Pos1, const std::vector<Vec3f> &P1, const std::vector<Vec3f> &P2,
									  const std::vector<Edge> &Edges1, const std::vector<Edge> &Edges2)
{
	EdgeCollision Result;
	size_t NumEdges1 = Edges1.size();
	size_t NumEdges2 = Edges2.size();
	for(int i1 = 0; i1 < NumEdges1; ++i1)
	{
		const auto &E1 = Edges1[i1];
		const Vec3f ToE1P = P1[E1.Indices[0]] - Pos1;
		const Vec3f E1D = GetEdgeDirection(P1, E1);
		for(int i2 = 0; i2 < NumEdges2; ++i2)
		{
			const auto &E2 = Edges2[i2];
			const Vec3f E2D = GetEdgeDirection(P2, E2);

			Vec3f Normal = CROSS(E1D, E2D);
			if(LEN2(Normal) == 0) continue;
			Normal = NORMALIZE(Normal);
			if(DOT(ToE1P, Normal) < 0) Normal = -Normal;

			float Min1 = FLT_MAX;
			float Max1 = -FLT_MAX;
			float Min2 = FLT_MAX;
			float Max2 = -FLT_MAX;
			ProjectPointsToAxis(P1, Normal, Min1, Max1);
			ProjectPointsToAxis(P2, Normal, Min2, Max2);

			float Penetration = Max1 - Min2;
			if(Penetration > 0 && Min1 <= Max2)	   //check for overlap
			{
				if(DOT(Normal, P1[E1.Indices[0]]) < Max1)	 //check that edge is the most forward one
					continue;
				if(DOT(Normal, P2[E2.Indices[0]]) > Min2)	 //check that edge is the most backward one
					continue;
				if(Penetration < Result.Penetration)
				{
					Result.Penetration = Penetration;
					Result.Normal = Normal;
					Result.Edge1Index = i1;
					Result.Edge2Index = i2;
				}
			}
			else
			{
				return {};
			}
		}
	}
	return Result;
}

static std::vector<Vec3f> CreateFaceCollisionManifoldPoints(const std::vector<Vec3f> &P1, const std::vector<Vec3f> &P2,
															Face CollisionFace, const std::vector<Face> &Faces2,
															Vec3f CollisionFaceNormal)
{
	size_t NumFaceI = CollisionFace.Indices.size();

	struct Plane
	{
		Vec3f Normal, Point;
	};
	//create cutting planes
	std::vector<Plane> FaceEdgePlanes1;
	FaceEdgePlanes1.reserve(NumFaceI);
	for(int i = 0; i < NumFaceI; ++i)
	{
		const Vec3f &Start = P1[CollisionFace.Indices[i]];
		const Vec3f &End = P1[CollisionFace.Indices[(i + 1) % NumFaceI]];
		Vec3f Outward = NORMALIZE(CROSS(End - Start, CollisionFaceNormal));
		FaceEdgePlanes1.push_back(Plane{Outward, Start});
	}

	//find most antiparallel face of other object
	Face SupportFace;
	float MostNegative = FLT_MAX;
	for(const auto &F : Faces2)
	{
		Vec3f N = GetFaceNormal(P2, F);
		float t = DOT(N, CollisionFaceNormal);
		if(t < MostNegative)
		{
			SupportFace = F;
			MostNegative = t;
		}
	}

	std::vector<Vec3f> Support;
	for(const auto &PointI : SupportFace.Indices)
	{
		Support.push_back(P2[PointI]);
	}

	//Sutherland�Hodgman
	for(const auto &Plane : FaceEdgePlanes1)
	{
		std::vector<Vec3f> NewSupport;
		size_t NumPoints = Support.size();
		Vec3f PrevP = Support[0];
		for(int i = 1; i < NumPoints + 1; ++i)
		{
			Vec3f &CurrentP = Support[i % NumPoints];
			if(DOT(CurrentP - Plane.Point, Plane.Normal) < 0)	 //Behind plane
			{
				if(DOT(PrevP - Plane.Point, Plane.Normal) > 0)	  //if other point is outside
				{
					//cut intersections
					const Vec3f D = PrevP - CurrentP;
					float t = DOT(Plane.Point - CurrentP, Plane.Normal) / DOT(D, Plane.Normal);
					NewSupport.push_back(CurrentP + t * D);
				}
				NewSupport.push_back(CurrentP);
			}
			else
			{
				if(DOT(PrevP - Plane.Point, Plane.Normal) < 0)	  //if other point is inside
				{
					//cut intersections
					const Vec3f D = PrevP - CurrentP;
					float t = DOT(Plane.Point - CurrentP, Plane.Normal) / DOT(D, Plane.Normal);
					NewSupport.push_back(CurrentP + t * D);
				}
			}
			PrevP = CurrentP;
		}
		Support = NewSupport;
		if(Support.empty())
		{
			Log("ERROR: support face empty");
			break;
		}
	}

	//Keep points below collsion face
	std::vector<Vec3f> NewSupport;
	size_t NumPoints = Support.size();
	const Vec3f &PlanePoint = P1[CollisionFace.Indices[0]];
	for(int i = 0; i < NumPoints; ++i)
	{
		Vec3f &CurrentP = Support[i];
		if(DOT(CurrentP - PlanePoint, CollisionFaceNormal) < 0)
		{
			NewSupport.push_back(CurrentP);
		}
	}
	return NewSupport;
}

auto ProjectToLine = [](Vec3f Start, Vec3f End, Vec3f P) {
	Vec3f Line = End - Start;
	float t = DOT(P - Start, Line) / DOT(Line, Line);
	return Start + Line * t;
};

auto ProjectToPlane = [](Vec3f PlanePoint, Vec3f Normal, Vec3f P) {
	Vec3f ToPoint = P - PlanePoint;
	return P - DOT(Normal, ToPoint) * Normal;
};

static Vec3f CreateEdgeCollisionManifoldPoint(const std::vector<Vec3f> &P1, const std::vector<Vec3f> &P2, Edge Edge1, Edge Edge2)
{
	const Vec3f &Edge1P0 = P1[Edge1.Indices[0]];
	const Vec3f &Edge1P1 = P1[Edge1.Indices[1]];
	const Vec3f &Edge2P0 = P2[Edge2.Indices[0]];
	const Vec3f &Edge2P1 = P2[Edge2.Indices[1]];

	//Project edge1 to a plane with edge2 as normal
	const Vec3f Edge2Dir = NORMALIZE(Edge2P1 - Edge2P0);
	const Vec3f OnPlaneP0 = ProjectToPlane(Edge2P0, Edge2Dir, Edge1P0);
	const Vec3f OnPlaneP1 = ProjectToPlane(Edge2P0, Edge2Dir, Edge1P1);

	//Project edge2(plane point) on edge1(projected)
	const Vec3f LineOnPlane = OnPlaneP1 - OnPlaneP0;
	const float Denominator = DOT(LineOnPlane, LineOnPlane);
	if(Denominator == 0)	//if parallel
	{
		return Edge2P0;
	}
	else
	{
		float t = DOT(Edge2P0 - OnPlaneP0, LineOnPlane) / Denominator;

		//Point on edge1
		const Vec3f Edge1Point = Edge1P0 + t * (Edge1P1 - Edge1P0);

		//Project Edge1Point on edge2 =>  Point on edge2
		const Vec3f Edge2Point = ProjectToLine(Edge2P0, Edge2P1, Edge1Point);

		//Support point center of line
		return (Edge1Point + Edge2Point) / 2;
	}
}

CollsionManifold SATCollisionDumb(const MeshCollider *O1, const MeshCollider *O2, const Vec3f &Pos1)
{
	CollsionManifold Res;
	//SAT

	//Face A, B colliding
	//	Pick points from B, clipped to A face
	//Face B, A colliding
	//	Pick points from A, clipped to B face
	//Edges
	//	Calculate closest point between
	const std::vector<Vec3f> &P1 = O1->Points;
	const std::vector<Vec3f> &P2 = O2->Points;
	const auto &Faces1 = O1->Faces;
	const auto &Faces2 = O2->Faces;

	FaceCollision Face1 = GetFaceCollision(P1, P2, Faces1);
	if(Face1.Penetration == FLT_MAX) return {};
	FaceCollision Face2 = GetFaceCollision(P2, P1, Faces2);
	if(Face2.Penetration == FLT_MAX) return {};
	const auto &Edges1 = O1->Edges;
	const auto &Edges2 = O2->Edges;
	EdgeCollision Edge = GetEdgeCollision(Pos1, P1, P2, Edges1, Edges2);
	if(Edge.Penetration == FLT_MAX) return {};

	if(Edge.Penetration < Face1.Penetration && Edge.Penetration < Face2.Penetration)
	{
		Res.Penetration = Edge.Penetration;
		Res.Normal = Edge.Normal;
		Res.Points = {CreateEdgeCollisionManifoldPoint(P1, P2, Edges1[Edge.Edge1Index], Edges2[Edge.Edge2Index])};
	}
	else
	{
		if(Face1.Penetration <= Face2.Penetration)
		{
			Res.Penetration = Face1.Penetration;
			Res.Normal = Face1.Normal;
			Res.Points = CreateFaceCollisionManifoldPoints(P1, P2, Faces1[Face1.FaceIndex], Faces2, Face1.Normal);
		}
		else
		{
			Res.Penetration = Face2.Penetration;
			Res.Normal = -Face2.Normal;	   //Keep facing away from O1
			Res.Points = CreateFaceCollisionManifoldPoints(P2, P1, Faces2[Face2.FaceIndex], Faces1, Face2.Normal);
		}
	}
	return Res;
}

CollsionManifold SphereSphereCollision(const SphereCollider *O1, const SphereCollider *O2)
{
	Vec3f O1ToO2 = O2->Position - O1->Position;
	float Dist = LEN(O1ToO2);
	float Pen = (O1->Radius + O2->Radius) - Dist;
	if(Pen > 0)
	{
		CollsionManifold Res;
		Vec3f N1 = O1ToO2 / Dist;

		Res.Points = {O1->Position + N1 * O1->Radius};
		Res.Normal = N1;
		Res.Penetration = Pen;
		return Res;
	}
	else
	{
		return {};
	}
}

static float GetDistanceToEdge(Vec3f P, Vec3f S, Vec3f E)
{
	//Project point to a plane with edge as normal
	Vec3f OnPlaneP = ProjectToPlane(S, NORMALIZE(E - S), P);
	return LEN(P - S);
}

static CollsionManifold SphereMeshCollision(const SphereCollider *O1, const MeshCollider *O2)
{
	CollsionManifold Result;
	Result.Penetration = FLT_MAX;
	//distance to faces
	for(const auto &F : O2->Faces)
	{
		const Vec3f Normal = GetFaceNormal(O2->Points, F);
		float Min1 = DOT(Normal, O1->Position) - O1->Radius;
		float Max1 = DOT(Normal, O1->Position) + O1->Radius;
		float Min2 = FLT_MAX;
		float Max2 = -FLT_MAX;
		ProjectPointsToAxis(O2->Points, Normal, Min2, Max2);

		float Penetration = Max1 - Min2;
		if(Penetration > 0 && Min1 <= Max2)	   //check for overlap
		{
			if(Penetration < Result.Penetration)
			{
				Result.Penetration = Penetration;
				Result.Normal = -Normal;
			}
		}
		else
		{
			return {};
		}
	}
	//distance to edges
	for(const auto &E : O2->Edges)
	{
		const Vec3f &EdgeS = O2->Points[E.Indices[0]];
		const Vec3f &EdgeE = O2->Points[E.Indices[1]];
		const Vec3f EdgeDir = EdgeE - EdgeS;
		const Vec3f R = CROSS(O1->Position - EdgeS, EdgeDir);
		const Vec3f Normal = NORMALIZE(CROSS(EdgeDir, R));

		float Min1 = DOT(Normal, O1->Position) - O1->Radius;
		float Max1 = DOT(Normal, O1->Position) + O1->Radius;
		float Min2 = FLT_MAX;
		float Max2 = -FLT_MAX;
		ProjectPointsToAxis(O2->Points, Normal, Min2, Max2);
		float Penetration = Max1 - Min2;
		if(Penetration > 0 && Min1 <= Max2)	   //check for overlap
		{
			if(Penetration < Result.Penetration)
			{
				Result.Penetration = Penetration;
				Result.Normal = -Normal;
			}
		}
		else
		{
			return {};
		}
	}
	Result.Points = {O1->Position - Result.Normal * O1->Radius};
	return Result;
}

static CollsionManifold CollisionTest(const Item &O1, const Item &O2)
{
	CollsionManifold Res;
	Res.Penetration = FLT_MAX;

	const AxisAlignedBoundingBox &BB1 = O1.GetAABB();
	const AxisAlignedBoundingBox &BB2 = O2.GetAABB();
	if(AABBOverlapTest(BB1, BB2))
	{
		const auto Col1 = O1.GetColliders();
		const auto Col2 = O2.GetColliders();
		for(auto C1 : Col1)
		{
			for(auto C2 : Col2)
			{
				if(C1->ColliderType == Collider::Sphere)
				{
					if(C2->ColliderType == Collider::Sphere)
					{
						Res = SphereSphereCollision(static_cast<const SphereCollider *>(C1),
													static_cast<const SphereCollider *>(C2));
					}
				}
				if(C1->ColliderType == Collider::Mesh)
				{
					if(C2->ColliderType == Collider::Mesh)
					{
						const Vec3f &Pos1 = O1.GetPos();
						auto tmp =
							SATCollisionDumb(static_cast<const MeshCollider *>(C1), static_cast<const MeshCollider *>(C2), Pos1);

						if(tmp.Penetration > 0 && tmp.Penetration < Res.Penetration)
						{
							Res = tmp;
						}
					}
					else if(C2->ColliderType == Collider::Sphere)
					{
						auto tmp =
							SphereMeshCollision(static_cast<const SphereCollider *>(C2), static_cast<const MeshCollider *>(C1));
						if(tmp.Penetration > 0 && tmp.Penetration < Res.Penetration)
						{
							Res = tmp;
						}
					}
				}
			}
		}
	}
	return Res;
}

void HandleCollision(Item &O1, Item &O2)
{
	if(!O1.IsActive() && !O2.IsActive()) return;
	auto C = CollisionTest(O1, O2);
	for(auto &P : C.Points)
	{
		CalcImpulse(O1, O2, P, -C.Normal, C.Penetration);
		//const Vec3f Col = (C.Points.size() == 1) ? Vec3f{100, 0, 0} : Vec3f{100, 0, 100};
		//DrawPoint(P, Col);
		//DrawLine(P, P + C.Normal + Vec3f{0.01f, 0.01f, 0.01f}, Col);
	}
}

void HandleCollisionWorld(const Level *Map, Item &O2)
{
	if(!O2.IsActive()) return;
	auto Items = Map->GetCollisionItems(O2.GetAABB());
	for(auto &O1 : Items)
	{
		//for(const auto &C : O1.GetColliders()) DrawCollider(C, {100, 0, 0});
		auto C = CollisionTest(O1, O2);
		for(auto &P : C.Points)
		{
			CalcImpulse(O1, O2, P, -C.Normal, C.Penetration);
			//DrawPoint(P, {100, 0, 0});
			//DrawLine(P, P + C.Normal + Vec3f{0.01f, 0.01f, 0.01f}, {100, 0, 0});
		}
	}
}

static bool RayFaceIntersection(Vec3f Origin, Vec3f Direction, const Face &F, const std::vector<Vec3f> &P, float &t)
{
	const Vec3f N = GetFaceNormal(P, F);
	float tmax = t;
	if(RayPlaneCollisionTest(N, P[F.Indices[0]], Origin, Direction, tmax))
	{
		const Vec3f PlanePoint = Origin + Direction * tmax;
		const size_t NumIndices = F.Indices.size();
		for(int i = 1; i <= NumIndices; ++i)
		{
			const Vec3f ED = P[F.Indices[i % NumIndices]] - P[F.Indices[i - 1]];
			const Vec3f Inward = CROSS(N, ED);
			const Vec3f ToPlanePoint = PlanePoint - P[F.Indices[i - 1]];
			if(DOT(Inward, ToPlanePoint) < 0)
			{
				return false;
			}
		}
		t = tmax;
		return true;
	}
	else
	{
		return false;
	}
}

bool RayColliderIntersection(Vec3f Origin, Vec3f Direction, Collider *Col, float &tmax)
{
	bool Hit = false;
	auto *C1 = static_cast<MeshCollider *>(Col);
	for(auto F : C1->Faces)
	{
		if(RayFaceIntersection(Origin, Direction, F, C1->Points, tmax)) Hit = true;
	}
	return Hit;
}

Vec3f SphereColliderIntersection(Vec3f Origin, float &Radius, Collider *Col)
{
	Vec3f Result = {0, 0, 0};
	auto *C1 = new SphereCollider;
	C1->Position = Origin;
	C1->Radius = Radius;

	auto *C2 = static_cast<MeshCollider *>(Col);

	auto Collision = SphereMeshCollision(C1, C2);
	if(Collision.Penetration > 0)
	{
		Result = Collision.Normal;
		Radius = Radius - Collision.Penetration;
	}
	delete C1;
	return Result;
}

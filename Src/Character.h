#pragma once

#include "Collision.h"
#include "Identifiable.h"
#include "Inventory.h"
#include "Mesh.h"
#include "Rigging.h"
#include "Shader.h"
#include "vector/Matrix.h"
#include "vector/Vector.h"

class Level;

enum class CharacterState : uint8_t
{
	None = 0x00,
	Idle = 0x01,
	Walking = 0x02,
	Attacking = 0x04,
	Damaged = 0x08,
	Dead = 0x10,
};

bool operator!(const CharacterState &rhs);
CharacterState operator&(CharacterState lhs, CharacterState rhs);
CharacterState operator|(CharacterState lhs, CharacterState rhs);
CharacterState operator~(CharacterState rhs);

class Character : public Identifiable
{
public:
	Character();
	Vec3f GetPosition() const { return m_Pos; }
	Vec2f GetFacingDir() const { return m_LookingDirection2D; }
	CharacterState GetState() const { return m_State; }

	void SetPosition(Vec3f Pos) { m_Pos = Pos; }
	void AddPosition(Vec3f Pos) { m_Pos += Pos; }
	void SetFacingDir(Vec2f Dir) { m_LookingDirection2D = Dir; }
	void SetState(CharacterState State);

	Mat4f GetModelMatrix() const;

	void SetMesh(const std::string &Name);
	void DrawCharacter(Shader &Shader);

	SphereCollider GetCollider() const;

	bool IsAlive() const;
	bool ToRemove() const { return m_ToRemove; }

	void Animate(float dt);

	void Damage(uint8_t Amount) { m_DamageTaken += Amount; }
	void AddHealth(int16_t Amount) { m_Health += Amount; }
	int16_t GetHealth() const { return m_Health; }

	bool RayCollision(Vec3f Pos, Vec3f Dir, float &t) const;

	Vec3f CheckLevelCollision(Level &Map);
	Vec3f HandleCollision(Level &Map);

	//Handle cooldowns
	void StepWeapon(float dt);

	//INVENTORY

	//Add weapon and set animations
	void AddWeapon(const std::string &Name, uint8_t InventoryBoxIndex = 0);

	Inventory &GetInventory() { return m_Inventory; }
	const Inventory &GetInventory() const { return m_Inventory; }

	//DEBUG
	void DrawSkeleton();

protected:
	void DropWeapon(GameState &Game);
	void OnDeath();

	Mesh *m_Mesh = nullptr;
	MeshRig m_Rig;

	CharacterState m_State;

	Vec3f m_Pos, m_CurrentSpeed;
	Vec3f m_LookingDirection, m_Right, m_Up;
	Vec2f m_LookingDirection2D;

	Inventory m_Inventory;

	int16_t m_Health, m_DamageTaken;
	float m_Speed, m_AnimationTimer = 0;
	bool m_Falling;
	bool m_ToRemove = false;

	Joint m_WeaponJoint;

	uint32_t m_WalkAnimationCI = 0;
	uint32_t m_WalkAnimationWI = 0;
	uint32_t m_AttackAnimationCI = 0;
	uint32_t m_AttackAnimationWI = 0;
};

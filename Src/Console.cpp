#include "Console.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <list>

#include "Debug.h"
#include "Drawer.h"

extern MegaDrawer DrawHandler;

std::list<std::string> LogHistory;
std::fstream LogFile;
static bool UseLogFile = false;
static bool Odd1 = false;
size_t GetConsoleLogSize()
{
	return LogHistory.size();
}

void EnableLogFile()
{
	LogFile.open("log.txt", std::ofstream::app);
	LogFile << "************\n";
	if(!LogFile.is_open())
	{
		DEBUG_ABORT("COULD NOT OPEN FILE");
	}
	UseLogFile = true;
}

void Log(const std::string &Text, bool Print)
{
	LogHistory.push_back(Text);
	if(LogHistory.size() > 5000)
	{
		Odd1 = !Odd1;
		LogHistory.pop_front();
	}
	if(Print)
	{
		std::cout << Text << "\n";
	}
	if(UseLogFile)
	{
		LogFile << Text << '\n';
		LogFile.flush();
	}
}

extern bool ConsoleOpen;
void ToggleGameConsole()
{
	ConsoleOpen = !ConsoleOpen;
}
void EnableGameConsole()
{
	ConsoleOpen = true;
}
void DisableGameConsole()
{
	ConsoleOpen = false;
}

void DrawConsole(uint32_t Start, uint8_t Num)
{
	DrawHandler.ClearText();

	auto it = std::next(LogHistory.begin(), Start);

	bool Odd = (Start % 2) ^ Odd1;

	for(uint32_t i = 0; i + Start < LogHistory.size() && i < Num; ++i)
	{
		Odd = !Odd;
		Vec3f Col = Odd ? Vec3f{1, 1, 1} : Vec3f{0, 1, 0};
		DrawHandler.DrawTextLine(*it, 10, Col);
		++it;
	}
}

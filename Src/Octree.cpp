#include "Octree.h"

#include "LevelObjects.h"
#include "Random.h"

void OctreeNode::AddRoom(int i, Vec3f Pos)
{
	if(Children.empty())
	{
		RoomIndices.push_back(i);
	}
	else
	{
		for(auto &C : Children)
		{
			if(InAABB(Pos, C.AABB))
			{
				C.AddRoom(i, Pos);
				return;	   //TODO: make sure this is not in multiple
			}
		}
	}
}

OctreeNode CreateOctreeAABBs(const AxisAlignedBoundingBox &AABB)
{
	/*
			+-------+-------+
		   /  2    /   3   /|
		  +-------+-------+ |
		 /   6   /   7   /|3|
		+-------+-------+ | |
		|       |       |7|/|
		|   6   |   7   | + |
		|       |       |/|1|
		+-------+-------+ | +
		|       |       |5|/
		|   4   |   5   | /
		|       |       |/
		+-------+-------+
	*/
	const Vec3f Min = AABB.Min;
	const Vec3f Max = AABB.Max;
	const Vec3f Center = (Max + Min) / 2.0f;

	OctreeNode Node;

	Node.AABB = AABB;
	Node.Color = GetRandomV3(0.1f, 10.0f);
	if((AABB.Max.x - AABB.Min.x) > 7)
	{
		Node.Children.resize(8);
		Node.Children[0] = CreateOctreeAABBs({Vec3f{Min.x, Min.y, Min.z}, Vec3f{Center.x, Center.y, Center.z}});
		Node.Children[1] = CreateOctreeAABBs({Vec3f{Center.x, Min.y, Min.z}, Vec3f{Max.x, Center.y, Center.z}});
		Node.Children[2] = CreateOctreeAABBs({Vec3f{Min.x, Center.y, Min.z}, Vec3f{Center.x, Max.y, Center.z}});
		Node.Children[3] = CreateOctreeAABBs({Vec3f{Center.x, Center.y, Min.z}, Vec3f{Max.x, Max.y, Center.z}});
		Node.Children[4] = CreateOctreeAABBs({Vec3f{Min.x, Min.y, Center.z}, Vec3f{Center.x, Center.y, Max.z}});
		Node.Children[5] = CreateOctreeAABBs({Vec3f{Center.x, Min.y, Center.z}, Vec3f{Max.x, Center.y, Max.z}});
		Node.Children[6] = CreateOctreeAABBs({Vec3f{Min.x, Center.y, Center.z}, Vec3f{Center.x, Max.y, Max.z}});
		Node.Children[7] = CreateOctreeAABBs({Vec3f{Center.x, Center.y, Center.z}, Vec3f{Max.x, Max.y, Max.z}});
	}
	return Node;
}

void Octree::AddRoom(int Index, Vec3f Pos)
{
	m_RootNode.AddRoom(Index, Pos);
}

static void CleanNode(OctreeNode &Node)
{
	for(auto it2 = Node.Children.begin(); it2 != Node.Children.end();)
	{
		CleanNode(*it2);
		if(it2->Children.empty() && it2->RoomIndices.empty())
		{
			it2 = Node.Children.erase(it2);
		}
		else
		{
			++it2;
		}
	}
}

void Octree::Clean()
{
	//TODO: make this part of construction instead
	CleanNode(m_RootNode);
}

std::vector<int> Octree::GetRooms(const SphereCollider &Col, const OctreeNode &Node) const
{
	std::vector<int> Result;
	if(ColliderAABBCollisionTest(Col, Node.AABB))
	{
		if(Node.Children.empty())
		{
			return Node.RoomIndices;
		}
		else
		{
			for(const auto &C : Node.Children)
			{
				auto V = GetRooms(Col, C);
				Result.insert(Result.end(), V.begin(), V.end());
			}
		}
	}
	return Result;
}

std::vector<int> Octree::GetRooms(const Vec3f Pos, const OctreeNode &Node) const
{
	std::vector<int> Result;
	if(InAABB(Pos, Node.AABB))
	{
		if(Node.Children.empty())
		{
			return Node.RoomIndices;
		}
		else
		{
			for(const auto &C : Node.Children)
			{
				auto V = GetRooms(Pos, C);
				Result.insert(Result.end(), V.begin(), V.end());
			}
		}
	}
	return Result;
}

std::vector<int> Octree::GetRooms(const AxisAlignedBoundingBox &AABB, const OctreeNode &Node) const
{
	std::vector<int> Result;
	if(AABBOverlapTest(AABB, Node.AABB))
	{
		if(Node.Children.empty())
		{
			return Node.RoomIndices;
		}
		else
		{
			for(const auto &C : Node.Children)
			{
				auto V = GetRooms(AABB, C);
				Result.insert(Result.end(), V.begin(), V.end());
			}
		}
	}
	return Result;
}

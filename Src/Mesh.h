#pragma once

#include "vector/Vector.h"
#include <vector>

#include "Shader.h"

union Vec4b {
	struct
	{
		uint8_t x;
		uint8_t y;
		uint8_t z;
		uint8_t w;
	};
	uint8_t V[4];
};

struct Vertex
{
	Vec3f Position = {0};
	Vec3f Normal = {0};
	Vec3f Color = {0.5f, 0.5f, 0.5f};
	Vec2f TexCoord = {0};
	Vec4b Joints = {0};
	Vec4f Weights = {0};
};

bool operator==(const Vertex &lhs, const Vertex &rhs);

class Mesh
{
public:
	void Init();
	void UpdateBuffers();
	void SetTexture(uint32_t Texture) { m_TextureId = Texture; }
	void Draw(Shader &Shader) const;

	std::vector<Vertex> Vertices;
	std::vector<uint32_t> Indices;

private:
	uint32_t m_VAO = 0, m_VBO = 0, m_EBO = 0, m_NumElements = 0;
	uint32_t m_TextureId = 1;
};

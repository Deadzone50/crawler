#include "Colliders.h"

#include <algorithm>

#include "Debug.h"
#include "Drawer.h"
#include "Mesh.h"

Vec3f GetFaceNormal(const std::vector<Vec3f> &P, const Face &F)
{
	const Vec3f E1 = P[F.Indices[1]] - P[F.Indices[0]];
	const Vec3f E2 = P[F.Indices[2]] - P[F.Indices[1]];
	return NORMALIZE(CROSS(E1, E2));
}

Vec3f GetEdgeDirection(const std::vector<Vec3f> &P, const Edge &E)
{
	const Vec3f E1 = P[E.Indices[1]] - P[E.Indices[0]];
	return NORMALIZE(E1);
}

void DrawCollider(const Collider *C, Vec3f Color)
{
	if(C->ColliderType == Collider::Mesh || C->ColliderType == Collider::AABB)
	{
		const auto *C1 = static_cast<const MeshCollider *>(C);
		for(const auto &P : C1->Points)
		{
			DrawDebugPoint(P, Color);
		}
		for(const auto &F : C1->Faces)
		{
			int EndI = F.Indices.size() - 1;
			for(int i = 1; i <= EndI; ++i)
			{
				DrawDebugLine(C1->Points[F.Indices[i]], C1->Points[F.Indices[i - 1]], Color);
			}
			DrawDebugLine(C1->Points[F.Indices[EndI]], C1->Points[F.Indices[0]], Color);
		}
	}
}

AxisAlignedBoundingBox CalculateAABB(const Vec3f &Scale, const Mesh *M, const Mat3f &Rotation)
{
	AxisAlignedBoundingBox AABB = {Vec3f{FLT_MAX, FLT_MAX, FLT_MAX}, Vec3f{-FLT_MAX, -FLT_MAX, -FLT_MAX}};
	for(const auto &V : M->Vertices)
	{
		const Vec3f P = Rotation * V.Position * Scale;
		if(P.x > AABB.Max.x) AABB.Max.x = P.x;
		if(P.y > AABB.Max.y) AABB.Max.y = P.y;
		if(P.z > AABB.Max.z) AABB.Max.z = P.z;
		if(P.x < AABB.Min.x) AABB.Min.x = P.x;
		if(P.y < AABB.Min.y) AABB.Min.y = P.y;
		if(P.z < AABB.Min.z) AABB.Min.z = P.z;
	}
	return AABB;
}

AxisAlignedBoundingBox CalculateAABB(const std::vector<Collider *> &Colliders, const Mat3f &Rotation)
{
	AxisAlignedBoundingBox AABB = {Vec3f{FLT_MAX, FLT_MAX, FLT_MAX}, Vec3f{-FLT_MAX, -FLT_MAX, -FLT_MAX}};
	for(const auto &C : Colliders)
	{
		auto Col = static_cast<MeshCollider *>(C);
		for(const auto &V : Col->Points)
		{
			const Vec3f P = Rotation * V;
			if(P.x > AABB.Max.x) AABB.Max.x = P.x;
			if(P.y > AABB.Max.y) AABB.Max.y = P.y;
			if(P.z > AABB.Max.z) AABB.Max.z = P.z;
			if(P.x < AABB.Min.x) AABB.Min.x = P.x;
			if(P.y < AABB.Min.y) AABB.Min.y = P.y;
			if(P.z < AABB.Min.z) AABB.Min.z = P.z;
		}
	}
	return AABB;
}

static std::vector<Face> CreateFaces(const Mesh *M)
{
	const auto &V = M->Vertices;
	const auto &I = M->Indices;

	std::vector<Face> Faces;
	size_t NumIndices = I.size();
	for(size_t i = 0; i < NumIndices; i += 3)
	{
		//Triangle indices
		const int I0 = I[i + 0];
		const int I1 = I[i + 1];
		const int I2 = I[i + 2];

		Face F;
		F.Indices.push_back(I0);
		F.Indices.push_back(I1);
		F.Indices.push_back(I2);

		const Vec3f N = V[I1].Normal;	 //Use original normal, might not be current normal for rotated meshes
		const Vec3f &P = V[I0].Position;
		//Search for new vertices with same normal
		for(size_t ix = 0; ix < NumIndices; ++ix)
		{
			int Ix = I[ix];
			bool Found = false;
			for(auto Index : F.Indices)
			{
				if(Ix == Index)
				{
					Found = true;
					break;
				}
			}
			if(Found) continue;

			if(DOT(V[Ix].Normal, N) > 0.999f)
			{
				F.Indices.push_back(Ix);
			}
		}
		if(std::find(Faces.begin(), Faces.end(), F) == Faces.end()) Faces.push_back(F);
	}
	return Faces;
}

static std::vector<Vec3f> CreatePointsAndModifyIndices(const Mesh *M, const Vec3f &Scale, std::vector<Face> &Faces)
{
	std::vector<Vec3f> TrimmedPoints;
	for(auto &F : Faces)
	{
		for(auto &i : F.Indices)
		{
			const Vec3f P = M->Vertices[i].Position * Scale;
			bool Found = false;
			for(int Index = 0; Index < TrimmedPoints.size(); ++Index)
			{
				if(TrimmedPoints[Index] == P)
				{
					i = Index;
					Found = true;
					break;
				}
			}
			if(!Found)
			{
				TrimmedPoints.push_back(P);
				i = TrimmedPoints.size() - 1;
			}
		}
	}
	return TrimmedPoints;
}

static std::vector<Edge> CreateEdges(const std::vector<Face> &Faces)
{
	std::vector<Edge> Edges;
	for(const auto &F : Faces)
	{
		size_t NumPoints = F.Indices.size();
		for(int i = 0; i < NumPoints - 1; ++i)
		{
			const Edge E = {F.Indices[i], F.Indices[i + 1]};
			if(std::find(Edges.begin(), Edges.end(), E) == Edges.end()) Edges.push_back(E);
		}
		const Edge E = {F.Indices[NumPoints - 1], F.Indices[0]};
		if(std::find(Edges.begin(), Edges.end(), E) == Edges.end()) Edges.push_back(E);
	}
	return Edges;
}

static std::vector<Vec3f> CreateAABBCollider(const AxisAlignedBoundingBox &AABB, MeshCollider *MC)
{
	MC->Faces = {{{0, 1, 2, 3}}, {{7, 6, 5, 4}}, {{3, 2, 6, 7}}, {{4, 5, 1, 0}}, {{1, 5, 6, 2}}, {{3, 7, 4, 0}}};
	MC->Edges = CreateEdges(MC->Faces);
	const Vec3f &Min = AABB.Min;
	const Vec3f &Max = AABB.Max;
	return {{Min.x, Min.y, Max.z}, {Max.x, Min.y, Max.z}, {Max.x, Max.y, Max.z}, {Min.x, Max.y, Max.z},
			{Min.x, Min.y, Min.z}, {Max.x, Min.y, Min.z}, {Max.x, Max.y, Min.z}, {Min.x, Max.y, Min.z}};
}

Collider *CreateCollider(const Mesh *M, const Vec3f &Scale, Collider::Type Type)
{
	Collider *Result = nullptr;
	switch(Type)
	{
		case Collider::Type::Mesh: {
			MeshCollider *C = new MeshCollider;
			C->ColliderType = Collider::Type::Mesh;
			C->Faces = CreateFaces(M);
			C->OriginalPoints = CreatePointsAndModifyIndices(M, Scale, C->Faces);
			C->Points = C->OriginalPoints;
			C->Edges = CreateEdges(C->Faces);
			Result = C;
			break;
		}
		case Collider::Type::AABB: {
			MeshCollider *C = new MeshCollider;
			C->ColliderType = Collider::Type::Mesh;
			C->OriginalPoints = CreateAABBCollider(CalculateAABB(Scale, M, Mat3f()), C);
			C->Points = C->OriginalPoints;
			Result = C;
			break;
		}
		default: DEBUG_ABORT("Unknown collider type: " + std::to_string(Type));
	}
	return Result;
}

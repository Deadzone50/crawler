#include "Weapon.h"

#include <map>

#include "Debug.h"
#include "Drawer.h"
#include "Enemy.h"
#include "Event.h"
#include "Level.h"
#include "MeshManager.h"
#include "Network.h"
#include "PlayerClass.h"
#include "Random.h"
#include "SoundManager.h"
#include "States.h"
#include "XML.h"

extern NetworkState NState;

static bool TestCollision(Vec3f Pos, Vec3f Dir, float &t, GameState &Game, int &EnemyHit, int &PlayerHit)
{
	bool Result = false;
	if(Game.Map->MapRayCollision(Pos, Dir, t))
	{
		Result = true;
	}
	for(int i = 0; i < Game.Map->GetEnemies().size(); ++i)
	{
		if(Game.Map->GetEnemies()[i].RayCollision(Pos, Dir, t))
		{
			EnemyHit = i;
			Result = true;
		}
	}
	for(int i = 0; i < Game.Players.size(); ++i)
	{
		if(Game.Players[i].RayCollision(Pos, Dir, t))
		{
			PlayerHit = i;
			Result = true;
		}
	}
	return Result;
}

enum class HitType
{
	Wall,
	Character,
	None
};

struct WeaponHit
{
	HitType Hit = HitType::None;
	Vec3f HitPos = {};
};

static WeaponHit HandleWeaponHitRay(const Vec3f &Pos, const Vec3f &Dir, GameState &Game, std::map<uint32_t, uint8_t> &EnemyDamage,
									std::map<uint8_t, uint8_t> &PlayerDamage, float Range, uint8_t Damage)
{
	Vec3f Col = {0, 0, 1};
	int EnemyHit = -1;
	int PlayerHit = -1;
	bool WallHit = false;
	if(TestCollision(Pos, Dir, Range, Game, EnemyHit, PlayerHit))
	{
		if(PlayerHit >= 0)
		{
			Col = {1, 0, 0};
			PlayerDamage[PlayerHit] += Damage;
		}
		else if(EnemyHit >= 0)
		{
			Col = {1, 0, 0};
			EnemyDamage[EnemyHit] += Damage;
		}
		else
		{
			WallHit = true;
			Col = {0, 1, 0};
		}
	}
	DrawDebugLine(Pos + Vec3f{0.01f, 0.01f, 0.01f}, Pos + Range * Dir, Col);
	if(EnemyHit >= 0 || PlayerHit >= 0)
		return {HitType::Character, Pos + Range * Dir};
	else if(WallHit)
		return {HitType::Wall, Pos + Range * Dir};
	else
		return {HitType::None, Pos + Range * Dir};
}

static void CreateBloodSplatter(Vec3f Pos, Vec3f Dir)
{
	for(int i = 0; i < 7; ++i)
	{
		Vec3f R = GetRandomV3(-1, 1);
		float Life = GetRandomF(0.2f, 2);
		CreateParticle(Pos, 2 * Dir + R + Vec3f{0, 1, 0}, {1, 0, 0}, Life);
	}
}

static void CreateSparks(const Vec3f &Pos, const Vec3f &Dir, const Vec3f &Color, int Num, float Lifetime)
{
	for(int i = 0; i < Num; ++i)
	{
		Vec3f R = GetRandomV3(-1, 1);
		float Life = GetRandomF(0.05f, Lifetime);
		CreateParticle(Pos, Dir + R, Color, Life);
	}
}

void HandleAttack(const Vec3f &Pos, const Vec3f &Dir, GameState &Game, const AttackStats &Stats)
{
	std::map<uint32_t, uint8_t> EnemyDamage;
	std::map<uint8_t, uint8_t> PlayerDamage;
	std::vector<WeaponHit> WH(1);

	SoundMan.Play(Stats.AttackSound, Pos, Game.Player->GetCameraPos(), Game.Player->GetLookingDir(), 1.0f,
				  GetRandomF(0.9f, 1.1f));

	if(Stats.Projectile)
	{
		const Vec3f Col = {1, 0.7f, 0};
		CreateSparks(Pos + Vec3f{0.1f, 0, 0} + Game.Player->GetLookingDir(), 10 * Dir + Vec3f{0, 1, 0}, Col, 10, 0.15f);
		CreateSparks(Pos - Vec3f{0.1f, 0, 0} + Game.Player->GetLookingDir(), 10 * Dir + Vec3f{0, 1, 0}, Col, 10, 0.15f);
		CreateSparks(Pos + Vec3f{0, 0, 0.1f} + Game.Player->GetLookingDir(), 10 * Dir + Vec3f{0, 1, 0}, Col, 10, 0.15f);
		CreateSparks(Pos - Vec3f{0, 0, 0.1f} + Game.Player->GetLookingDir(), 10 * Dir + Vec3f{0, 1, 0}, Col, 10, 0.15f);
	}
	if(Stats.Shots > 1)
	{
		WH.reserve(20);
	}
	const Vec3f Right = NORMALIZE(CROSS(Dir, {0, 1, 0}));
	const Vec3f Up = NORMALIZE(CROSS(Right, Dir));
	for(int i = 0; i < Stats.Shots; ++i)
	{
		float x = GetRandomF(-Stats.Spread, Stats.Spread);
		float y = GetRandomF(-Stats.Spread, Stats.Spread);
		const Vec3f NewDir = NORMALIZE(Dir + Up * y + Right * x);
		WH.push_back(HandleWeaponHitRay(Pos, NewDir, Game, EnemyDamage, PlayerDamage, Stats.Range, Stats.Damage));
	}
	for(const auto &H : WH)
	{
		if(H.Hit == HitType::Character)
			CreateBloodSplatter(H.HitPos, -Dir);
		else if(H.Hit == HitType::Wall)
			CreateSparks(H.HitPos, -Dir, {1, 1, 0}, 5, 2);
	}

	if(NState.NM->AmHost())
	{
		for(auto &E : EnemyDamage)
		{
			Game.Map->GetEnemies()[E.first].Damage(E.second);
		}
		for(auto &P : PlayerDamage)
		{
			std::shared_ptr<DamageEvent> E = std::make_shared<DamageEvent>();
			E->PlayerIndex = P.first;
			E->Damage = P.second;

			AddEvent(E, Game);
		}
	}
}

void Weapon::ResetCooldown()
{
	m_Cooldown = m_Stats.Cooldown;
	m_Timer = m_Stats.AttackTimer;
}

void Weapon::DrawAnimated(Shader &Shader, Mat4f &ModelMatrix) const
{
	const Vec3f Up = ToVec3(ModelMatrix.GetCol(1));
	const Vec3f Forward = ToVec3(ModelMatrix.GetCol(2));

	float Amount = 0;
	float const Halftime = m_Stats.AttackTimer / 2;
	if(m_Timer > Halftime)
		Amount = m_Timer - Halftime;
	else
		Amount = Halftime - m_Timer;

	ModelMatrix.Move((0.8f + Amount) * Forward - 0.1f * Up);
	Shader.SetMat4("Model", ModelMatrix);
	Item::DrawModel(Shader);
}

void Weapon::ReduceTimers(float dt)
{
	m_Cooldown -= dt;
	if(m_Timer > 0)
		m_Timer -= dt;
	else
		m_Timer = 0;
}

void Weapon::Fire(GameState &Game, Vec3f Pos, Vec3f Dir)
{
	std::shared_ptr<AttackEvent> E = std::make_shared<AttackEvent>();
	E->Pos = Pos;
	E->Dir = Dir;
	E->Stats = m_Stats;
	AddEvent(E, Game);
	m_Attacked = true;
}

void Weapon::StartAttack()
{
	ResetCooldown();
	m_Attacked = false;
}

void WeaponManager::ParseWeapons()
{
	XMLElement WeaponsXML = ParseXML("Assets/Weapons.xml");
	for(const auto &E : WeaponsXML.Elements)
	{
		ItemProperties Properties;
		AttackStats Stats;
		std::vector<WeaponTag> Tags;

		Properties.Name = E.GetAttribute("name");
		Properties.Type = StringToItemType(E.Tag.Name);

		Weapon New;
		for(const auto &Element : E.Elements)
		{
			if(Element.Tag.Name == "Damage")
			{
				Stats.Damage = std::stoi(Element.Text);
			}
			else if(Element.Tag.Name == "Shots")
			{
				Stats.Shots = std::stoi(Element.Text);
			}
			else if(Element.Tag.Name == "Range")
			{
				Stats.Range = std::stof(Element.Text);
			}
			else if(Element.Tag.Name == "Spread")
			{
				Stats.Spread = std::stof(Element.Text);
			}
			else if(Element.Tag.Name == "AmmoType")
			{
				New.SetAmmoType(StringToAmmoType(Element.Text));
			}
			else if(Element.Tag.Name == "Cooldown")
			{
				Stats.Cooldown = std::stof(Element.Text);
			}
			else if(Element.Tag.Name == "Timer")
			{
				Stats.AttackTimer = std::stof(Element.Text);
			}
			else if(Element.Tag.Name == "Delay")
			{
				Stats.DelayTime = std::stof(Element.Text);
			}
			else if(Element.Tag.Name == "Tags")
			{
				for(const auto &Tag : Element.Elements)
				{
					Tags.push_back(StringToWeaponTag(Tag.Tag.Name));
					if(Tag.Tag.Name == "Projectile")
					{
						Stats.Projectile = true;
					}
				}
			}
			else if(Element.Tag.Name == "Mesh")
			{
				Properties.Mesh = MeshMan.GetMesh(Element.Text);
			}
			else if(Element.Tag.Name == "AttackSound")
			{
				Stats.AttackSound = Element.Text;
			}
		}
		New.SetProperties(Properties);
		New.SetStats(Stats);
		New.SetTags(Tags);
		m_WeaponMap[Properties.Name] = New;
	}
}

const Weapon &WeaponManager::GetWeapon(const std::string &Name)
{
	if(m_WeaponMap.find(Name) == m_WeaponMap.end())
	{
		DEBUG_ABORT(Name + " WEAPON NOT FOUND");
	}
	return m_WeaponMap[Name];
}

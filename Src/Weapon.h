#pragma once

#include <cstdint>
#include <map>
#include <string>
#include <vector>

#include "Debug.h"
#include "Item.h"
#include "Mesh.h"
#include "Shader.h"
#include "vector/Matrix.h"
#include "vector/Vector.h"

struct GameState;

enum class AmmoType : uint8_t
{
	None,
	Cartridge44,
	Shotgun
};

inline AmmoType StringToAmmoType(const std::string &S)
{
	if(S == "None")
		return AmmoType::None;
	if(S == "Cartridge44")
		return AmmoType::Cartridge44;
	if(S == "Shotgun")
		return AmmoType::Shotgun;
	DEBUG_ABORT("String does not match tag");
	return AmmoType::None;
}

enum class WeaponTag
{
	Melee,
	Projectile,
	Explosive,
	Blade,
	Blunt,
	Pierce,
	Pistol,
	Shotgun,
	Machinegun,
	Rifle,
	Rocketlauncher
};

inline WeaponTag StringToWeaponTag(const std::string &S)
{
	if(S == "Melee")
		return WeaponTag::Melee;
	if(S == "Projectile")
		return WeaponTag::Projectile;
	if(S == "Explosive")
		return WeaponTag::Explosive;
	if(S == "Blade")
		return WeaponTag::Blade;
	if(S == "Blunt")
		return WeaponTag::Blunt;
	if(S == "Pierce")
		return WeaponTag::Pierce;
	if(S == "Pistol")
		return WeaponTag::Pistol;
	if(S == "Shotgun")
		return WeaponTag::Shotgun;
	if(S == "Machinegun")
		return WeaponTag::Machinegun;
	if(S == "Rifle")
		return WeaponTag::Rifle;
	if(S == "Rocketlauncher")
		return WeaponTag::Rocketlauncher;
	DEBUG_ABORT("String does not match tag");
	return WeaponTag::Melee;
}

struct AttackStats
{

	uint8_t Damage = 1;
	uint8_t Shots = 1;
	float Range = 1;
	float Spread = 0;

	float Cooldown = 1;
	float AttackTimer = 0.5f;
	float DelayTime = 0;

	std::string AttackSound;
	bool Projectile = false;
};

class Weapon : public Item
{
public:
	void StartAttack();
	void Fire(GameState &Game, Vec3f Pos, Vec3f Dir);
	void ResetCooldown();
	void ReduceTimers(float dt);

	bool OnCooldown() const { return m_Cooldown > 0; }
	bool ReadyToFire() const { return (!m_Attacked && (m_Stats.AttackTimer - m_Timer) >= m_Stats.DelayTime); }
	bool Attacking() const { return m_Timer > 0; }

	void DrawAnimated(Shader &Shader, Mat4f &ModelMatrix) const;	//Draw with animation

	float GetAttackDistance() const { return m_Stats.Range / 2; }

	void ChangeDamage(float Amount) { m_Stats.Damage *= Amount; }
	void ChangeSpread(float Amount) { m_Stats.Spread *= Amount; }
	void ChangeShots(float Amount) { m_Stats.Shots *= Amount; }

	void SetStats(const AttackStats &Stats) { m_Stats = Stats; }
	void SetTags(const std::vector<WeaponTag> &Tags) { m_Tags = Tags; }

	bool DoesNotUseAmmo() const { return m_AmmoType == AmmoType::None; }

	void SetAmmoType(AmmoType AmmoType) { m_AmmoType = AmmoType; }
	AmmoType GetAmmoType() const { return m_AmmoType; }

private:
	AttackStats m_Stats;
	std::vector<WeaponTag> m_Tags;
	AmmoType m_AmmoType = AmmoType::None;

	float m_Cooldown = 0;
	float m_Timer = 0;
	bool m_Attacked = false;
};

void HandleAttack(const Vec3f &Pos, const Vec3f &Dir, GameState &Game, const AttackStats &Stats);

class WeaponManager
{
public:
	void ParseWeapons();
	const Weapon &GetWeapon(const std::string &Name);

private:
	std::map<std::string, Weapon> m_WeaponMap;
};

extern WeaponManager WeaponMan;

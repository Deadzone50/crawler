#include "Network.h"

#include <Windows.h>
#include <chrono>
#include <fstream>

#include "Console.h"
#include "Debug.h"
#include "Enemy.h"
#include "Level.h"
#include "PlayerClass.h"
#include "States.h"

template <class T> void ReadFromNetworkBuffer(T *Val, const uint8_t *Buffer, uint32_t &Offset)
{
	std::memcpy(Val, &Buffer[Offset], sizeof(T));
	Offset += sizeof(T);
}

template <class T> void ReadFromNetworkBuffer(std::shared_ptr<T> Val, const uint8_t *Buffer, uint32_t &Offset)
{
	std::memcpy(Val.get(), &Buffer[Offset], sizeof(T));
	Offset += sizeof(T);
}

template <class T> void WriteToNetworkBuffer(T Val, uint8_t *Buffer, uint32_t &Offset)
{
	std::memcpy(&Buffer[Offset], &Val, sizeof(T));
	Offset += sizeof(T);
}

void ReadFromNetworkBufferLen(uint8_t *Val, uint32_t Size, const uint8_t *Buffer, uint32_t &Offset)
{
	std::memcpy(Val, &Buffer[Offset], Size);
	Offset += Size;
}

void WriteToNetworkBufferLen(uint8_t *Val, uint32_t Size, uint8_t *Buffer, uint32_t &Offset)
{
	std::memcpy(&Buffer[Offset], Val, Size);
	Offset += Size;
}

static inline void ReadPlayer(std::vector<PlayerClass> &Players, const uint8_t *Buffer, uint32_t &CurrentOffset)
{
	Vec3f Pos;
	Vec2f Dir;
	CharacterState State;
	uint8_t Index;
	uint8_t WI;
	ReadFromNetworkBuffer(&Pos, Buffer, CurrentOffset);
	ReadFromNetworkBuffer(&Dir, Buffer, CurrentOffset);
	ReadFromNetworkBuffer(&State, Buffer, CurrentOffset);
	ReadFromNetworkBuffer(&Index, Buffer, CurrentOffset);
	ReadFromNetworkBuffer(&WI, Buffer, CurrentOffset);
	Players[Index].SetPosition(Pos);
	Players[Index].SetFacingDir(Dir);
	Players[Index].SetState(State);
	Players[Index].GetInventory().SwitchWeapon(WI);
}
static inline void WritePlayer(const PlayerClass &Player, uint8_t Index, uint8_t *Buffer, uint32_t &CurrentOffset)
{
	Vec3f Pos = Player.GetPosition();
	Vec2f Dir = Player.GetFacingDir();
	CharacterState State = Player.GetState();
	uint8_t WI = Player.GetInventory().GetCurrentWeaponIndex();
	WriteToNetworkBuffer(Pos, Buffer, CurrentOffset);
	WriteToNetworkBuffer(Dir, Buffer, CurrentOffset);
	WriteToNetworkBuffer(State, Buffer, CurrentOffset);
	WriteToNetworkBuffer(Index, Buffer, CurrentOffset);
	WriteToNetworkBuffer(WI, Buffer, CurrentOffset);
}
static inline void ReadEnemy(std::vector<EnemyClass> &Enemies, const uint8_t *Buffer, uint32_t &CurrentOffset)
{
	Vec3f Pos;
	Vec2f Dir;
	CharacterState State;
	uint16_t Index;
	ReadFromNetworkBuffer(&Pos, Buffer, CurrentOffset);
	ReadFromNetworkBuffer(&Dir, Buffer, CurrentOffset);
	ReadFromNetworkBuffer(&State, Buffer, CurrentOffset);
	ReadFromNetworkBuffer(&Index, Buffer, CurrentOffset);
	Enemies[Index].SetPosition(Pos);
	Enemies[Index].SetFacingDir(Dir);
	Enemies[Index].SetState(State);
}
static inline void WriteEnemy(const EnemyClass &Enemy, uint16_t Index, uint8_t *Buffer, uint32_t &CurrentOffset)
{
	Vec3f Pos = Enemy.GetPosition();
	Vec2f Dir = Enemy.GetFacingDir();
	CharacterState State = Enemy.GetState();
	WriteToNetworkBuffer(Pos, Buffer, CurrentOffset);
	WriteToNetworkBuffer(Dir, Buffer, CurrentOffset);
	WriteToNetworkBuffer(State, Buffer, CurrentOffset);
	WriteToNetworkBuffer(Index, Buffer, CurrentOffset);
}
static void ReadEvents(std::vector<std::shared_ptr<Event>> &Events, const uint8_t *Buffer, uint32_t &CurrentOffset)
{
	uint8_t NumEvents;
	ReadFromNetworkBuffer(&NumEvents, Buffer, CurrentOffset);
	EventType Type;
	for(uint8_t i = 0; i < NumEvents; ++i)
	{
		uint32_t tmp = CurrentOffset;
		ReadFromNetworkBuffer(&Type, Buffer, tmp);
		switch(Type)
		{
			case EventType::Attack: {
				std::shared_ptr<AttackEvent> New = std::make_shared<AttackEvent>();
				ReadFromNetworkBuffer(New, Buffer, CurrentOffset);
				Events.push_back(New);
				break;
			}
			case EventType::Damage: {
				std::shared_ptr<DamageEvent> New = std::make_shared<DamageEvent>();
				ReadFromNetworkBuffer(New, Buffer, CurrentOffset);
				Events.push_back(New);
				break;
			}
			case EventType::Spawn: {
				std::shared_ptr<SpawnEvent> New = std::make_shared<SpawnEvent>();
				ReadFromNetworkBuffer(New, Buffer, CurrentOffset);
				Events.push_back(New);
				break;
			}
			case EventType::DeSpawn: {
				std::shared_ptr<DeSpawnEvent> New = std::make_shared<DeSpawnEvent>();
				ReadFromNetworkBuffer(New, Buffer, CurrentOffset);
				Events.push_back(New);
				break;
			}
			default: {
				DEBUG_ABORT("EVENT NOT IMPLEMENTED: " + std::to_string((uint8_t)Type));
			}
		}
	}
}
static void WriteEvent(const std::shared_ptr<Event> &E, uint8_t *Buffer, uint32_t &CurrentOffset)
{
	switch(E->Type)
	{
		case EventType::Attack: {
			WriteToNetworkBufferLen((uint8_t *)E.get(), sizeof(AttackEvent), Buffer, CurrentOffset);
			break;
		}
		case EventType::Damage: {
			WriteToNetworkBufferLen((uint8_t *)E.get(), sizeof(DamageEvent), Buffer, CurrentOffset);
			break;
		}
		case EventType::Spawn: {
			WriteToNetworkBufferLen((uint8_t *)E.get(), sizeof(SpawnEvent), Buffer, CurrentOffset);
			break;
		}
		case EventType::DeSpawn: {
			WriteToNetworkBufferLen((uint8_t *)E.get(), sizeof(DeSpawnEvent), Buffer, CurrentOffset);
			break;
		}
		default: {
			DEBUG_ABORT("EVENT NOT IMPLEMENTED: " + std::to_string((uint8_t)E->Type));
		}
	}
}

std::vector<Address> ReadIPs(const std::string &Filename)
{
	std::vector<Address> Addresses;

	std::ifstream file;
	file.open(Filename);
	if(!file.is_open())
	{
		Log(Filename + " not found");
		return Addresses;
	}

	std::string Line;
	while(std::getline(file, Line))
	{
		int a, b, c, d, port;
		std::stringstream ss(Line);
		ss >> a >> b >> c >> d >> port;
		Addresses.emplace_back(a, b, c, d, port);
	}

	return Addresses;
}

NetworkManager::NetworkManager(uint32_t Port)
{
	m_Port = Port;
	m_Socket.Open(Port);
}

void NetworkManager::AddConnection(const Address &Addr)
{
	m_PendingAddresses.push_back({Addr, false, false});
	m_Ping.resize(m_PendingAddresses.size() + 1);
	m_Ready.resize(m_PendingAddresses.size() + 1);
	m_Exit.resize(m_PendingAddresses.size() + 1, true);
	Log("Added pending address: " + Addr.ToString());
}

void NetworkManager::AddConnections(const std::vector<Address> &Addresses)
{
	for(auto &A : Addresses)
	{
		AddConnection(A);
	}
}

void NetworkManager::CloseConnections()
{
	m_Socket.Close();
}

uint8_t NetworkManager::SetupNetworking(bool AmHost)
{
	m_Host = AmHost;
	auto Addresses = ReadIPs("IPs.txt");
	uint8_t NumPlayers = Addresses.size();
	if(Addresses.empty())
	{
		DEBUG_ABORT("No ips");
	}
	if(AmHost)
	{
		//host
		Addresses.erase(Addresses.begin());	   //remove host
		AddConnections(Addresses);
	}
	else
	{
		//client
		AddConnection(Addresses[0]);
	}
	return NumPlayers;
}

static auto t0 = std::chrono::system_clock::now();
static Address PingSentAddress;
static uint8_t PingAttempts = 0;

void NetworkManager::PingAll()
{
	if(m_PingHandled)
	{
		if(!m_Addresses.empty())
		{
			if(m_PingIndex >= m_Addresses.size())
				m_PingIndex = 0;

			Ping(m_Addresses[m_PingIndex]);
		}
	}
	else
	{
		++PingAttempts;
		Log("Ping not handled");
		if(PingAttempts > 5)
		{
			PingAttempts = 0;
			//	Log("Dropping: "+m_Addresses[m_PingIndex].ToString());
			//	m_Addresses.erase(m_Addresses.begin() + m_PingIndex);
			m_PingHandled = true;
		}
	}
}

void NetworkManager::Ping(const Address &Addr)
{
	m_PingHandled = false;
	PingSentAddress = Addr;
	t0 = std::chrono::system_clock::now();
	Msg M = {Addr, "PingReq"};
	Log("Sending PingReq to " + M.Addr.ToString());
	Send(M);
}

bool NetworkManager::Send(const Msg &M)
{
	//Log("Sending "+std::string((char*)M.Data)+" to "+M.Addr.ToString());
	return m_Socket.Send(M.Addr, M.Data, sizeof(M.Data));
}

bool NetworkManager::Receive(Msg &M)
{
	int r = m_Socket.Receive(M.Addr, M.Data, sizeof(M.Data));
	//if(r < 0)
	//{
	//	Log("Reopening socket");
	//	m_Socket.Close();
	//	m_Socket.Open(m_Port);
	//}
	//if(r > 0)
	//Log("Received "+std::string((char*)M.Data)+" from "+M.Addr.ToString());
	return r > 0;
}

static bool CmpData(const void *Buff1, const std::string &Text)
{
	return std::memcmp(Buff1, Text.c_str(), Text.size()) == 0;
}

void NetworkManager::Handle(Msg &M, GameState &Game)
{
	if(CmpData(M.Data, "PingReq"))
	{
		Log("Sending PingResp to " + M.Addr.ToString());
		Send({M.Addr, "PingResp"});
	}
	else if(CmpData(M.Data, "PingResp") && M.Addr == PingSentAddress)
	{
		auto t1 = std::chrono::system_clock::now();
		float dt = std::chrono::duration<float>(t1 - t0).count() * 1000;
		Log(M.Addr.ToString() + ", PING: " + std::to_string(dt) + " ms");
		m_PingHandled = true;
		m_Ping[m_PingIndex] = dt;
		++m_PingIndex;
	}
	else if(CmpData(M.Data, "HS"))
	{
		if(m_CurrentHandshake && M.Addr == m_CurrentHandshake->Addr)
		{
			HandleHandshake(m_CurrentHandshake->HSI, m_CurrentHandshake->AHS, M, Game);
		}
		else
			Log("Not current handshake: " + std::string((char *)M.Data));
	}
	else
	{
		Log("Unhandled message: " + std::string((char *)M.Data));
	}
}

void NetworkManager::HandleHandshake(bool &HSI, bool &AHS, Msg &M, GameState &Game)
{
	if(m_Host && CmpData(M.Data, "HS Init"))
	{
		HSI = true;
		Log("Sending HS ACK to " + M.Addr.ToString());
		Msg MR = {M.Addr, "HS ACK"};
		MR.Data[15] = m_Addresses.size() + 1;
		Send(MR);
	}
	else if(!m_Host && CmpData(M.Data, "HS ACK"))
	{
		HSI = true;
		Game.PlayerIndex = M.Data[15];
		Game.Player = &Game.Players[Game.PlayerIndex];
		Log("PlayerId = " + std::to_string(Game.PlayerIndex));

		Log("Sending HS done to " + M.Addr.ToString());
		Send({M.Addr, "HS done"});
	}
	else if(m_Host && CmpData(M.Data, "HS done"))
	{
		AHS = true;
	}
}

void NetworkManager::SendHandshake(const PendingAddress &PA)
{
	if(!PA.HSI)
	{
		Log("Sending HS Init to " + PA.Addr.ToString());
		Send({PA.Addr, "HS Init"});
	}
	else if(m_Host && !PA.AHS)
	{
		Log("Sending HS ACK to " + PA.Addr.ToString());
		Msg MR = {PA.Addr, "HS ACK"};
		MR.Data[15] = m_Addresses.size() + 1;
		Send(MR);
	}
}

void NetworkManager::SendMessages(GameState &Game)
{
	if(!m_PendingAddresses.empty())
	{
		m_CurrentHandshake = &m_PendingAddresses[0];
		if((m_Host && m_CurrentHandshake->HSI && m_CurrentHandshake->AHS) || (!m_Host && m_CurrentHandshake->HSI))
		{
			m_Addresses.push_back(m_CurrentHandshake->Addr);
			m_PendingAddresses.erase(m_PendingAddresses.begin());
			Log("Connection established");
		}
		else
		{
			SendHandshake(*m_CurrentHandshake);
		}
	}
	else	//if(!m_Addresses.empty())
	{
		static uint32_t Counter = 0;
		if(++Counter > 100)
		{
			PingAll();
			Counter = 0;
		}

		if(m_Host)
		{
			Game.EventsOut.insert(Game.EventsOut.end(), Game.EventsReceived.begin(), Game.EventsReceived.end());
			Game.EventsReceived.clear();
			HostSend(Game);
			Game.EventsIn = Game.EventsOut;
		}
		else
		{
			ClientSend(Game);
		}
		Game.EventsOut.clear();
	}
}

void NetworkManager::ReceiveMessages(GameState &Game)
{
	Msg M;
	while(Receive(M))
	{
		if(M.Data[0] == PLAYERS_CHAR)
			HandlePlayersMsg(M, Game.Players);
		else if(M.Data[0] == ENEMIES_CHAR)
			HandleEnemiesMsg(M, Game.Map->GetEnemies());
		else if(M.Data[0] == EVENTS_CHAR)
		{
			if(m_Host)
				HandleEventsMsg(M, Game.EventsReceived);
			else
				HandleEventsMsg(M, Game.EventsIn);
		}
		else if(M.Data[0] == READY_CHAR)
			HandleReady(M);
		else if(M.Data[0] == EXIT_CHAR)
			HandleExit(M);
		else
			Handle(M, Game);
	}
}

float NetworkManager::GetPing(uint32_t i)
{
	if(i < m_Ping.size())
		return m_Ping[i];
	else
		return 0;
}

void NetworkManager::SendReady(uint8_t i)
{
	m_Ready[i] = true;
	uint8_t Data[2] = {READY_CHAR, i};
	for(const auto &A : m_Addresses)
		m_Socket.Send(A, &Data, sizeof(Data));
}

bool NetworkManager::IsReady(uint8_t i)
{
	if(i < m_Ready.size())
		return m_Ready[i];
	else
		return false;
}

void NetworkManager::ClearReady()
{
	m_Ready = std::vector<bool>(m_Ready.size(), false);
}

void NetworkManager::SendExitMap(uint8_t i)
{
	m_Exit[i] = true;
	uint8_t Data[2] = {EXIT_CHAR, i};
	for(const auto &A : m_Addresses)
		m_Socket.Send(A, &Data, sizeof(Data));
}

bool NetworkManager::InMap(uint8_t i)
{
	if(i < m_Exit.size())
		return !m_Exit[i];
	else
		return false;
}

bool NetworkManager::AllExitedMap()
{
	bool Result = true;
	for(auto E : m_Exit)
	{
		if(!E)
			return false;
	}
	return Result;
}

void NetworkManager::ClearExitMap()
{
	m_Exit = std::vector<bool>(m_Exit.size(), false);
}

void NetworkManager::HostSend(GameState &Game)
{
	for(auto &Dest : m_Addresses)
	{
		SendPlayersMsgs(Dest, Game.Players);	//TODO: create messages only once before sending to all
		if(!Game.Map->GetEnemies().empty())
			SendEnemiesMsgs(Dest, Game.Map->GetEnemies());
		if(!Game.EventsOut.empty())
			SendEventsMsgs(Dest, Game.EventsOut);
	}
}

void NetworkManager::ClientSend(GameState &Game)
{
	if(!m_Addresses.empty())
	{
		//Player
		uint8_t Buffer[512] = {PLAYERS_CHAR, 1};
		uint32_t CurrentOffset = 2;
		WritePlayer(*Game.Player, Game.PlayerIndex, Buffer, CurrentOffset);
		m_Socket.Send(m_Addresses[0], Buffer, CurrentOffset);

		if(!Game.EventsOut.empty())
			SendEventsMsgs(m_Addresses[0], Game.EventsOut);
	}
	else
	{
		Log("No connections established");
	}
}

void NetworkManager::HandleReady(const Msg &M)
{
	uint8_t Index = M.Data[1];
	m_Ready[Index] = true;
}

void NetworkManager::HandleExit(const Msg &M)
{
	uint8_t Index = M.Data[1];
	m_Exit[Index] = true;
}

void NetworkManager::SendEventsMsgs(const Address &Addr, const std::vector<std::shared_ptr<Event>> &Events)
{
	uint8_t Buffer[512] = {EVENTS_CHAR};
	uint32_t CurrentOffset = 2;
	uint8_t Num = 0;
	size_t TotalWritten = 0;
	size_t TotalEvents = Events.size();
	for(auto &E : Events)
	{
		WriteEvent(E, Buffer, CurrentOffset);
		++Num;
		++TotalWritten;
		if(CurrentOffset + sizeof(AttackEvent) > 512 && TotalWritten < TotalEvents)
		{
			Buffer[1] = Num;
			m_Socket.Send(Addr, Buffer, CurrentOffset);

			std::memset(Buffer, 0, 512);
			Buffer[0] = EVENTS_CHAR;
			CurrentOffset = 2;
			Num = 0;
		}
	}
	Buffer[1] = Num;
	m_Socket.Send(Addr, Buffer, CurrentOffset);
}

void NetworkManager::SendPlayersMsgs(const Address &Addr, const std::vector<PlayerClass> &Players)
{
	uint8_t NumPlayers = Players.size();
	uint8_t Buffer[512] = {PLAYERS_CHAR, NumPlayers};
	uint32_t CurrentOffset = 2;
	for(uint8_t i = 0; i < NumPlayers; ++i)
	{
		WritePlayer(Players[i], i, Buffer, CurrentOffset);
	}
	m_Socket.Send(Addr, Buffer, CurrentOffset);
}

void NetworkManager::SendEnemiesMsgs(const Address &Addr, const std::vector<EnemyClass> &Enemies)
{
	uint8_t Buffer[512] = {ENEMIES_CHAR};
	uint32_t CurrentOffset = 2;
	uint8_t Num = 0;
	size_t TotalEnemies = Enemies.size();
	for(uint16_t i = 0; i < TotalEnemies; ++i)
	{
		WriteEnemy(Enemies[i], i, Buffer, CurrentOffset);
		++Num;
		if(CurrentOffset + 23 > 512 && i + 1 < TotalEnemies)
		{
			Buffer[1] = Num;
			m_Socket.Send(Addr, Buffer, CurrentOffset);

			std::memset(Buffer, 0, 512);
			Buffer[0] = ENEMIES_CHAR;
			CurrentOffset = 2;
			Num = 0;
		}
	}
	Buffer[1] = Num;
	m_Socket.Send(Addr, Buffer, CurrentOffset);
}

void NetworkManager::HandleEventsMsg(const Msg &M, std::vector<std::shared_ptr<Event>> &Events)
{
	uint32_t CurrentOffset = 1;
	ReadEvents(Events, M.Data, CurrentOffset);
}

void NetworkManager::HandlePlayersMsg(const Msg &M, std::vector<PlayerClass> &Players)
{
	uint8_t Num = M.Data[1];
	uint32_t CurrentOffset = 2;
	for(uint8_t i = 0; i < Num; ++i)
	{
		ReadPlayer(Players, M.Data, CurrentOffset);
	}
}

void NetworkManager::HandleEnemiesMsg(const Msg &M, std::vector<EnemyClass> &Enemies)
{
	uint8_t Num = M.Data[1];
	uint32_t CurrentOffset = 2;
	for(uint8_t i = 0; i < Num; ++i)
	{
		ReadEnemy(Enemies, M.Data, CurrentOffset);
	}
}

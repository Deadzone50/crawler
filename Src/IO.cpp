#include <GLFW/glfw3.h>

#include "IO.h"

#include <iostream>

#include "Debug.h"
#include "Drawer.h"
#include "Level.h"
#include "MainLoop.h"
#include "Menu.h"
#include "PlayerClass.h"
#include "Scene.h"
#include "States.h"

extern bool Wireframe;
extern bool TextInputActive;
extern IOStates IO;
extern GameState Game;
extern Vec2i G_ScreenSize;
extern RenderState RState;
uint32_t MouseSens = 800;
bool STOP_TIME = false;
bool SPAWN_ENEMIES = false;
bool DRAW_DEBUG = false;

void KeyCallback(GLFWwindow *Window, int Key, int Scancode, int Action, int Mods)
{
	uint16_t ActionKey = 0;
	switch(Key)
	{
		case GLFW_KEY_A: ActionKey = STRAFE_LEFT; break;
		case GLFW_KEY_D: ActionKey = STRAFE_RIGHT; break;
		case GLFW_KEY_W: ActionKey = FORWARD; break;
		case GLFW_KEY_S: ActionKey = BACKWARD; break;
		case GLFW_KEY_E: ActionKey = USE; break;
		case GLFW_KEY_SPACE: ActionKey = JUMP; break;
		case GLFW_KEY_ESCAPE: ActionKey = BACK; break;
		case GLFW_KEY_Q: ActionKey = QUIT; break;
		case GLFW_KEY_GRAVE_ACCENT:	   //�
			ActionKey = CONSOLE;
			break;
		case GLFW_KEY_BACKSPACE: ActionKey = BACKSPACE; break;
		case GLFW_KEY_ENTER:
		case GLFW_KEY_KP_ENTER: ActionKey = ENTER; break;
		case GLFW_KEY_LEFT_SHIFT: ActionKey = RUN; break;
		case GLFW_KEY_TAB: ActionKey = MAP; break;
		case GLFW_KEY_I: ActionKey = INVENTORY; break;
	}
	if(Action == GLFW_PRESS)
	{
		IO.KeyStates[Key] = true;
		IO.KeyChanged[Key] = true;
		IO.KeyChanged[ActionKey] = true;
		IO.KeyStates[ActionKey] = true;
		if(TextInputActive)
		{
			if(GLFW_KEY_SPACE <= Key && Key <= GLFW_KEY_GRAVE_ACCENT)
			{
				Game.CurrentMenu->Append(Key);
			}
			else if(GLFW_KEY_KP_0 <= Key && Key <= GLFW_KEY_KP_9)
			{
				Game.CurrentMenu->Append(Key - (GLFW_KEY_KP_0 - GLFW_KEY_0));
			}
		}
		if(GLFW_KEY_KP_0 <= Key && Key <= GLFW_KEY_KP_9)
		{
			D_NUMPAD[Key - GLFW_KEY_KP_0] = true;
			D_NUMPAD_CHANGED[Key - GLFW_KEY_KP_0] = true;
		}
	}
	else if(Action == GLFW_RELEASE)
	{
		IO.KeyChanged[ActionKey] = true;
		IO.KeyStates[ActionKey] = false;
		IO.KeyStates[Key] = false;
		IO.KeyChanged[Key] = true;
		if(GLFW_KEY_KP_0 <= Key && Key <= GLFW_KEY_KP_9)
		{
			D_NUMPAD[Key - GLFW_KEY_KP_0] = false;
			D_NUMPAD_CHANGED[Key - GLFW_KEY_KP_0] = true;
		}
	}
	else	//GLFW_REPEAT
	{
	}
}

void MousePosCallback(GLFWwindow *Window, double Xpos, double Ypos)
{
	IO.MousePos = {(int)Xpos, G_ScreenSize.y - (int)Ypos};
}

void MouseButtonCallback(GLFWwindow *Window, int Button, int Action, int Mods)
{
	uint16_t MouseButton = 0;
	switch(Button)
	{
		case GLFW_MOUSE_BUTTON_LEFT: MouseButton = MOUSE_LEFT; break;
		case GLFW_MOUSE_BUTTON_MIDDLE: MouseButton = MOUSE_MIDDLE; break;
		case GLFW_MOUSE_BUTTON_RIGHT: MouseButton = MOUSE_RIGHT; break;
		default: return;
	}
	IO.MouseChanged[MouseButton] = true;
	IO.MouseButton[MouseButton] = (Action == GLFW_PRESS);
}

void MouseScrollCallback(GLFWwindow *Window, double Xoffset, double Yoffset)
{
	uint16_t MouseButton = 0;
	if(Yoffset > 0) MouseButton = MOUSE_WHEEL_UP;
	else MouseButton = MOUSE_WHEEL_DOWN;
	IO.MouseChanged[MouseButton] = true;
	IO.MouseButton[MouseButton] = true;
}

void ResetMouse()
{
	IO.MouseButton[0] = false;
	IO.MouseChanged[0] = false;
	glfwSetCursorPos(RState.Window, G_ScreenSize.x / 2.0f, G_ScreenSize.y / 2.0f);
	IO.MousePos = G_ScreenSize / 2;
}

void ResetChangedKeys()
{
	std::memset(IO.KeyChanged, false, 512);
	std::memset(IO.MouseChanged, false, 5);
}

void HideMouse()
{
	glfwSetInputMode(RState.Window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	ResetMouse();
}

void ShowMouse()
{
	glfwSetInputMode(RState.Window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

void HandleMousePlayer()
{
	float MouseDistFromCenterX = G_ScreenSize.x / 2.0f - IO.MousePos.x;
	float MouseDistFromCenterY = IO.MousePos.y - G_ScreenSize.y / 2.0f;
	if(MouseDistFromCenterX || MouseDistFromCenterY)
	{
		float y = atanf(MouseDistFromCenterX / MouseSens);
		float x = atanf(MouseDistFromCenterY / MouseSens);
		Game.Player->Rotate({x, y});
		glfwSetCursorPos(RState.Window, G_ScreenSize.x / 2.0f, G_ScreenSize.y / 2.0f);
	}
	if(IO.MouseButton[MOUSE_LEFT] && Game.Player->IsAlive())
	{
		Game.Player->Attack();
	}
}

void HandleKeysDebug()
{
	if(IO.KeyChanged[GLFW_KEY_KP_ADD] && IO.KeyStates[GLFW_KEY_KP_ADD])
	{
		Game.Map->SetSeed(Game.Map->GetSeed() + 1);
		Game.Map->GenerateLevel();
		ResetTimers();
	}
	if(IO.KeyChanged[GLFW_KEY_KP_SUBTRACT] && IO.KeyStates[GLFW_KEY_KP_SUBTRACT])
	{
		Game.Map->SetSeed(Game.Map->GetSeed() - 1);
		Game.Map->GenerateLevel();
		ResetTimers();
	}
	if(IO.KeyChanged[GLFW_KEY_KP_MULTIPLY] && IO.KeyStates[GLFW_KEY_KP_MULTIPLY])
	{
		Game.Map->StepGeneration();
	}
	if(IO.KeyChanged[GLFW_KEY_Z] && IO.KeyStates[GLFW_KEY_Z])
	{
		Game.Player->m_Debug = !Game.Player->m_Debug;
	}
	if(IO.KeyChanged[GLFW_KEY_X] && IO.KeyStates[GLFW_KEY_X])
	{
		Wireframe = !Wireframe;
	}
	if(IO.KeyChanged[GLFW_KEY_C] && IO.KeyStates[GLFW_KEY_C])
	{
		if(IO.KeyStates[RUN]) ClearPersistentDebugDraws();
		else
		{
			DRAW_DEBUG = !DRAW_DEBUG;
			ToggleDebugDraws();
		}
	}
	if(IO.KeyChanged[GLFW_KEY_T] && IO.KeyStates[GLFW_KEY_T])
	{
		STOP_TIME = !STOP_TIME;
	}
	if(IO.KeyChanged[GLFW_KEY_INSERT] && IO.KeyStates[GLFW_KEY_INSERT])
	{
		if(IO.KeyStates[RUN])
		{
			SPAWN_ENEMIES = !SPAWN_ENEMIES;
			Log("Spawner: " + std::to_string(SPAWN_ENEMIES));
		}
		else if(IO.KeyStates[GLFW_KEY_LEFT_CONTROL]) Game.Map->SpawnEnemies(Game, 100);
		else
		{
			const Vec3f Origin = Game.Player->GetCameraPos();
			const Vec3f Dir = Game.Player->GetLookingDir();
			float t = 1000;
			bool hit = Game.Map->MapRayCollision(Origin, Dir, t);
			if(hit) Game.Map->SpawnEnemy("Cultist", "Machete", Origin + Dir * t);
		}
	}
}
uint8_t HeldItemIndex = INV_EMPTY;
Vec2i HeldItemPrevPos = {0, 0};

static void EnterInventory()
{
	Game.Player->m_ShowInventory = true;
	HeldItemIndex = INV_EMPTY;
	ShowMouse();
}

static void ExitInventory()
{
	Game.Player->m_ShowInventory = false;
	if(HeldItemIndex != INV_EMPTY)
	{
		Inventory &Inv = Game.Player->GetInventory();
		Inv.GetItems()[HeldItemIndex].SetInventoryPos(HeldItemPrevPos);
	}
	HideMouse();
}

void HandleKeysCommon()
{
	if(IO.KeyStates[QUIT])
	{
		exit(0);
	}
	if(IO.KeyChanged[INVENTORY] && IO.KeyStates[INVENTORY])
	{
		if(Game.Player->m_ShowInventory)
		{
			ExitInventory();
		}
		else
		{
			EnterInventory();
		}
	}
}

void HandleKeysPlayer()
{
	float MoveSpeed_ = 5.0f;

	float Factor = 3.0f / 2.0f;
	if(IO.KeyStates[RUN])
	{
		Factor *= 4;
	}
	Vec3f Movement = {0, 0, 0};
	if(IO.KeyStates[FORWARD])
	{
		Movement.z = -1;
	}
	else if(IO.KeyStates[BACKWARD])
	{
		Movement.z = +1;
	}
	if(IO.KeyStates[STRAFE_LEFT])
	{
		Movement.x = -1;
	}
	else if(IO.KeyStates[STRAFE_RIGHT])
	{
		Movement.x = 1;
	}
	if(LEN2(Movement))
	{
		Movement = NORMALIZE(Movement) * MoveSpeed_ * Factor;
		Game.Player->SetMovement(Movement);
	}
	if(IO.KeyStates[JUMP])
	{
		Game.Player->Jump();
	}
	if(IO.KeyChanged[USE] && IO.KeyStates[USE])
	{
		Game.Player->Interact(Game);
	}
	if(IO.KeyChanged[MAP] && IO.KeyStates[MAP])
	{
		Game.Player->m_ShowMinimap = !Game.Player->m_ShowMinimap;
	}
	if(IO.KeyChanged['1'] && IO.KeyStates['1'])
	{
		Game.Player->SwitchWeapon(0);
	}
	if(IO.KeyChanged['2'] && IO.KeyStates['2'])
	{
		Game.Player->SwitchWeapon(1);
	}
	if(IO.KeyChanged['3'] && IO.KeyStates['3'])
	{
		Game.Player->SwitchWeapon(2);
	}
	if(IO.KeyChanged['4'] && IO.KeyStates['4'])
	{
		Game.Player->SwitchWeapon(3);
	}
	if(IO.KeyChanged['5'] && IO.KeyStates['5'])
	{
		Game.Player->SwitchWeapon(4);
	}
	if(IO.KeyChanged['6'] && IO.KeyStates['6'])
	{
		Game.Player->SwitchWeapon(5);
	}
	if(IO.KeyChanged['7'] && IO.KeyStates['7'])
	{
		Game.Player->SwitchWeapon(6);
	}
	if(IO.KeyChanged['8'] && IO.KeyStates['8'])
	{
		Game.Player->SwitchWeapon(7);
	}
	if(IO.KeyChanged['9'] && IO.KeyStates['9'])
	{
		Game.Player->SwitchWeapon(8);
	}

	if(IO.KeyChanged[BACK] && IO.KeyStates[BACK])
	{
		SwitchToMenu(MenuType::Main);
	}
}

void HandleMouseInventory()
{
	if(IO.MouseButton[MOUSE_LEFT] && IO.MouseChanged[MOUSE_LEFT])
	{
		if(Game.Player->GetInventory().Click(IO.MousePos, HeldItemIndex))
		{
			if(HeldItemIndex != INV_EMPTY)
			{
				Inventory &Inv = Game.Player->GetInventory();
				HeldItemPrevPos = Inv.GetItems()[HeldItemIndex].GetInventoryPos();
			}
		}
	}
	if(HeldItemIndex != INV_EMPTY)	  //move held item with mouse
	{
		Inventory &Inv = Game.Player->GetInventory();
		float Scale;
		Vec2i Start;
		Inv.GetScaleAndStart(Scale, Start);
		const Vec2i MouseSquarePos = (IO.MousePos - Start) / Scale;
		Inv.GetItems()[HeldItemIndex].SetInventoryPos(MouseSquarePos);
	}
}

void HandleKeysInventory()
{
	if(IO.KeyChanged[BACK] && IO.KeyStates[BACK])
	{
		ExitInventory();
	}
	{
		uint8_t EquippedIndex = INV_EMPTY;
		if(IO.KeyChanged['1'] && IO.KeyStates['1'])
		{
			EquippedIndex = 0;
		}
		if(IO.KeyChanged['2'] && IO.KeyStates['2'])
		{
			EquippedIndex = 1;
		}
		if(IO.KeyChanged['3'] && IO.KeyStates['3'])
		{
			EquippedIndex = 2;
		}
		if(IO.KeyChanged['4'] && IO.KeyStates['4'])
		{
			EquippedIndex = 3;
		}
		if(IO.KeyChanged['5'] && IO.KeyStates['5'])
		{
			EquippedIndex = 4;
		}
		if(IO.KeyChanged['6'] && IO.KeyStates['6'])
		{
			EquippedIndex = 5;
		}
		if(IO.KeyChanged['7'] && IO.KeyStates['7'])
		{
			EquippedIndex = 6;
		}
		if(IO.KeyChanged['8'] && IO.KeyStates['8'])
		{
			EquippedIndex = 7;
		}
		if(IO.KeyChanged['9'] && IO.KeyStates['9'])
		{
			EquippedIndex = 8;
		}
		if(EquippedIndex != INV_EMPTY)
		{
			auto &Inv = Game.Player->GetInventory();
			uint8_t ItemIndex = Inv.GetItemUnderMouse(IO.MousePos);
			if(ItemIndex != INV_EMPTY && Inv.GetItems()[ItemIndex].GetItemType() == InventoryItem::Type::Weapon)
				Inv.EquipWeapon(ItemIndex, EquippedIndex);
		}
	}
}

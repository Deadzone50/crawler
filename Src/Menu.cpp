#include <thread>

#include "Console.h"
#include "IO.h"
#include "Level.h"
#include "Menu.h"
#include "Mesh.h"
#include "MeshManager.h"
#include "Network.h"
#include "Scene.h"
#include "Shader.h"
#include "SoundManager.h"
#include "States.h"

extern void SetupPlayers(uint8_t NumPlayers);
extern void SetupGraphics();
extern Vec2i G_ScreenSize;
extern GameState Game;
extern GameState Game2;
extern Vec2i G_ScreenSize;
extern MeshManager MeshMan;
extern uint32_t MouseSens;

bool TextInputActive = false;

static bool GameRunning = false;

MenuItem::MenuItem(const std::string &Text, std::function<void()> OnClick, uint8_t CharSize)
{
	m_OnClick = OnClick;
	m_Text.SetText(Text);
	m_Text.SetCharSize(CharSize);
	Vec3f Col[] = {{0, 0.1f}, {0, 0.1f}, {1}, {1}};
	m_Text.SetColor(Col);
	m_Type = MenuItemType::Button;
}

MenuItem::MenuItem(const std::string &Text, MenuItemType Type, uint8_t CharSize)
{
	m_Type = Type;
	m_Text.SetText(Text);
	m_Text.SetCharSize(CharSize);
	Vec3f Col[] = {{0, 0.1f}, {0, 0.1f}, {0.5f}, {0.5f}};
	m_Text.SetColor(Col);
}

MenuItem::MenuItem(std::string *Value, MenuItemType Type, uint8_t CharSize)
{
	m_Value = Value;
	m_Type = Type;
	m_Text.SetCharSize(CharSize);
	Vec3f Col[] = {{0, 0.1f}, {0, 0.1f}, {0.5f}, {0.5f}};
	m_Text.SetColor(Col);
}

void MenuItem::Click()
{
	if(m_OnClick) m_OnClick();
}

void MenuItem::ToggleHighlight()
{
	m_Text.ToggleHighlight();
	m_Highlighted = !m_Highlighted;
}

void MenuItem::PopBack()
{
	if(!m_Value->empty()) m_Value->pop_back();
}

void MenuItem::Draw(Shader &Shader, bool Selected)
{
	if(m_Value) m_Text.SetText(*m_Value);

	if(Selected)
	{
		std::string tmp = "*" + m_Text.GetText();
		m_Text.SetText(tmp);
	}
	m_Text.Draw(Shader);
	if(Selected)
	{
		std::string tmp = m_Text.GetText().substr(1);
		m_Text.SetText(tmp);
	}
}

void Menu::AddButton(uint32_t Row, uint32_t Col, const std::string &Text, std::function<void()> OnClick, uint8_t CharSize)
{
	ResizeMenu(Row, Col);
	GetItem(Row, Col) = MenuItem(Text, OnClick, CharSize);
}

void Menu::ChangeButtonText(uint32_t Row, uint32_t Col, const std::string &Text)
{
	GetItem(Row, Col).SetText(Text);
}

void Menu::SetHighlight(uint32_t Row, uint32_t Col, bool Set)
{
	GetItem(Row, Col).SetHighlight(Set);
}

void Menu::AddInput(uint32_t Row, uint32_t Col, std::string *Value, uint8_t CharSize)
{
	ResizeMenu(Row, Col);
	GetItem(Row, Col) = MenuItem(Value, MenuItemType::Input, CharSize);
}

void Menu::AddLabel(uint32_t Row, uint32_t Col, const std::string &Text, uint8_t CharSize)
{
	ResizeMenu(Row, Col);
	GetItem(Row, Col) = MenuItem(Text, MenuItemType::Label, CharSize);
	if(!Row && !Col)	//to not select the label if first item
		m_SelectedY = 1;
}

void Menu::AddLabel(uint32_t Row, uint32_t Col, std::string *Value, uint8_t CharSize)
{
	ResizeMenu(Row, Col);
	GetItem(Row, Col) = MenuItem(Value, MenuItemType::Label, CharSize);
	if(!Row && !Col)	//to not select the label if first item
		m_SelectedY = 1;
}

constexpr uint8_t V_SPACING = 20;
constexpr uint8_t H_SPACING = 40;

bool Menu::MouseSelect(Vec2i PosIn)
{
	Vec2f Pos = {(float)PosIn.x, (float)G_ScreenSize.y - PosIn.y};
	if(Pos.x > m_MarginsPercent.x * G_ScreenSize.x && Pos.y > m_MarginsPercent.y * G_ScreenSize.y)
	{
		float CurrentY = m_MarginsPercent.y * G_ScreenSize.y;
		uint32_t IndexY = 0;
		bool Found = false;
		while(IndexY < m_Rows)
		{
			if(Pos.y < CurrentY + V_SPACING)
			{
				Found = true;
				break;
			}
			CurrentY += V_SPACING;
			++IndexY;
		}
		if(Found)
		{
			float CurrentX = m_MarginsPercent.x * G_ScreenSize.x;
			for(uint32_t IndexX = 0; IndexX < m_Columns; ++IndexX)
			{
				MenuItem &MI = m_Items[IndexY][IndexX];
				uint32_t Len = MI.GetTotalLen() + H_SPACING;
				if((Pos.x < CurrentX + Len) && MI.GetItemType() != MenuItemType::Label)
				{
					m_SelectedX = IndexX;
					m_SelectedY = IndexY;
					return true;
				}
				CurrentX += Len;
			}
		}
	}
	return false;
}

uint32_t Menu::SelectUp()
{
	if(m_SelectedY == 0) return 0;

	uint32_t tmp1 = m_SelectedY;
	m_SelectedY -= 1;
	if(GetItem(m_SelectedY, m_SelectedX).GetItemType() == MenuItemType::Label)	  //If not selectable
	{
		uint32_t tmp2 = SelectUp();	   //can be same or the next possible
		if(tmp2 == tmp1 - 1)		   //the same
		{
			m_SelectedY = tmp1;
			return tmp1;
		}
		return tmp2;
	}
	else
	{
		return m_SelectedY;
	}
}

uint32_t Menu::SelectDown()
{
	if(m_SelectedY == m_Rows - 1) return m_Rows - 1;

	uint32_t tmp1 = m_SelectedY;
	m_SelectedY += 1;
	if(GetItem(m_SelectedY, m_SelectedX).GetItemType() == MenuItemType::Label)	  //If not selectable
	{
		uint32_t tmp2 = SelectDown();	 //can be same or the next possible
		if(tmp2 == tmp1 + 1)			 //the same
		{
			m_SelectedY = tmp1;
			return tmp1;
		}
		return tmp2;
	}
	else
	{
		return m_SelectedY;
	}
}

uint32_t Menu::SelectLeft()
{
	if(m_SelectedX == 0) return 0;

	uint32_t tmp1 = m_SelectedX;
	m_SelectedX -= 1;
	if(GetItem(m_SelectedY, m_SelectedX).GetItemType() == MenuItemType::Label)	  //If not selectable
	{
		uint32_t tmp2 = SelectLeft();	 //can be same or the next possible
		if(tmp2 == tmp1 - 1)			 //the same
		{
			m_SelectedX = tmp1;
			return tmp1;
		}
		return tmp2;
	}
	else
	{
		return m_SelectedX;
	}
}

uint32_t Menu::SelectRight()
{
	if(m_SelectedX == m_Columns - 1) return m_Columns - 1;

	uint32_t tmp1 = m_SelectedX;
	m_SelectedX += 1;
	if(GetItem(m_SelectedY, m_SelectedX).GetItemType() == MenuItemType::Label)	  //If not selectable
	{
		uint32_t tmp2 = SelectRight();	  //can be same or the next possible
		if(tmp2 == tmp1 + 1)			  //the same
		{
			m_SelectedX = tmp1;
			return tmp1;
		}
		return tmp2;
	}
	else
	{
		return m_SelectedX;
	}
}

void Menu::ClickSelected()
{
	if(GetItem(m_SelectedY, m_SelectedX).GetItemType() == MenuItemType::Button) GetItem(m_SelectedY, m_SelectedX).Click();
	else if(GetItem(m_SelectedY, m_SelectedX).GetItemType() == MenuItemType::Input)
	{
		GetItem(m_SelectedY, m_SelectedX).ToggleHighlight();
		m_ItemHighlighted = !m_ItemHighlighted;
		TextInputActive = m_ItemHighlighted;
	}
}

void Menu::Back()
{
	if(m_ParentMenu != m_CurrentMenu) SwitchToMenu(m_ParentMenu);
	else if(GameRunning) SwitchToGame();
}

void Menu::Draw(Shader &Shader)
{
	Mat4f ModelMatrix;
	Vec3f Pos = {m_MarginsPercent.x * G_ScreenSize.x, (1 - m_MarginsPercent.y) * G_ScreenSize.y, 0};
	for(uint32_t Row = 0; Row < m_Rows; ++Row)
	{
		Pos.y -= V_SPACING;
		for(uint32_t Col = 0; Col < m_Columns; ++Col)
		{
			ModelMatrix.SetCol(3, {Pos.x, Pos.y, Pos.z, 1});
			Shader.SetMat4("Model", ModelMatrix);
			MenuItem &MI = GetItem(Row, Col);
			MI.Draw(Shader, (Row == m_SelectedY && Col == m_SelectedX));
			Pos.x += MI.GetTotalLen() + H_SPACING;
		}
		Pos.x = m_MarginsPercent.x * G_ScreenSize.x;
	}
}

void Menu::Handle(const IOStates &IO)
{
	if(!ItemHighlighted())
	{
		if(IO.KeyStates[FORWARD] && IO.KeyChanged[FORWARD])
		{
			SelectUp();
		}
		if(IO.KeyStates[BACKWARD] && IO.KeyChanged[BACKWARD])
		{
			SelectDown();
		}
		if((IO.KeyStates[STRAFE_LEFT]) && IO.KeyChanged[STRAFE_LEFT])
		{
			SelectLeft();
		}
		if(IO.KeyStates[STRAFE_RIGHT] && IO.KeyChanged[STRAFE_RIGHT])
		{
			SelectRight();
		}
		if((IO.KeyStates[BACK]) && IO.KeyChanged[BACK])	   //ESC
		{
			Back();
		}
		if((IO.KeyStates[USE] && IO.KeyChanged[USE]))
		{
			ClickSelected();
		}
		if(IO.KeyStates[QUIT])
		{
			exit(0);
		}
		static Vec2i MousePrev = {0, 0};
		Vec2i Diff = MousePrev - IO.MousePos;
		if(Diff.x || Diff.y)	//If mouse moved
		{
			MouseSelect(IO.MousePos);
		}
		MousePrev = IO.MousePos;
		if(IO.MouseButton[MOUSE_LEFT] && IO.MouseChanged[MOUSE_LEFT])
		{
			if(MouseSelect(IO.MousePos))
			{
				ClickSelected();
			}
		}
	}
	else
	{
		if(((IO.KeyStates[BACK]) && IO.KeyChanged[BACK]) || (IO.KeyStates[ENTER] && IO.KeyChanged[ENTER]))
		{
			GetItem(m_SelectedY, m_SelectedX).ToggleHighlight();
			m_ItemHighlighted = !m_ItemHighlighted;
			TextInputActive = m_ItemHighlighted;
		}
		if((IO.KeyStates[BACKSPACE]) && IO.KeyChanged[BACKSPACE])
		{
			GetItem(m_SelectedY, m_SelectedX).PopBack();
		}
	}
	ResetChangedKeys();
}

void Menu::ResizeMenu(uint32_t Row, uint32_t Col)
{
	if(Row >= m_Rows)
	{
		m_Rows = Row + 1;
		m_Items.resize(m_Rows, std::vector<MenuItem>(m_Columns));
	}
	if(Col >= m_Columns)
	{
		m_Columns = Col + 1;
		for(auto &R : m_Items) R.resize(m_Columns);
	}
}

static void SetupPlayers(uint8_t NumPlayers)
{
	Game.Players.clear();
	Game.Players.resize(NumPlayers);
	for(int i = 0; i < NumPlayers; ++i)
	{
		Game.Players[i].SetPosition({-0.5f + i * 0.5f, 0.5f, 0});
		Game.Players[i].SetMesh("Player");
		Game.Players[i].AddWeapon("Machete");
		Game.Players[i].AddWeapon("Revolver");
		Game.Players[i].AddWeapon("Shotgun");
		Game.Players[i].AddWeapon("Gatling-gun", 6);
		Game.Players[i].GetInventory().EquipWeapon(3, 0);

		Game.Players[i].GetInventory().AddAmmo(AmmoType::Cartridge44, 200);
		Game.Players[i].GetInventory().AddAmmo(AmmoType::Shotgun, 20);
	}
}

void InitMainMenu(std::vector<Menu> &Menus, NetworkState &NS, RenderState &RS, Settings &Set)
{
	Menu &MainMenu = Menus[MenuType::Main];
	Menu &MultiplayerMenu = Menus[MenuType::Multi];
	Menu &OptionsMenu = Menus[MenuType::Option];

	//MAIN
	MainMenu.SetCurrent(MenuType::Main);
	MainMenu.AddButton(1, 0, "Start", [&] {
		if(!GameRunning)
		{
			EnableLogFile();
			SetupPlayers(1);
			Game.PlayerIndex = 0;
			Game.Player = &Game.Players[Game.PlayerIndex];
			GameRunning = true;
			MainMenu.ChangeButtonText(1, 0, "Continue");

			MainMenu.AddButton(0, 0, "Restart", [&MainMenu] {
				SetupPlayers(1);
				Game.PlayerIndex = 0;
				Game.Player = &Game.Players[Game.PlayerIndex];
				GameRunning = true;
				SwitchToBaseScreen();
			});

			SwitchToBaseScreen();
		}
		else
		{
			SwitchToGame();
		}
	});
	MainMenu.AddButton(2, 0, "DebugMulti", [&] {
		EnableLogFile();
		NS.Multiplayer = true;
		NS.DebugMulti = true;

		delete NS.NM;
		NS.NM = new NetworkManager(30000);
		NS.NM->SetHost(true);
		NS.DNM = new NetworkManager(30001);
		NS.DNM->SetHost(false);

		NS.NM->AddConnection({127, 0, 0, 1, 30001});
		NS.DNM->AddConnection({127, 0, 0, 1, 30000});

		SetupPlayers(2);
		Game.PlayerIndex = 0;
		Game.Player = &Game.Players[Game.PlayerIndex];
		Game.Player->UpdateUI();
		{
			Game2.Players.resize(2);
			for(int i = 0; i < 2; ++i)
			{
				Game2.Players[i].SetPosition({(float)i, 0.5f, -i * 10.0f});
				Game2.Players[i].SetMesh("Player");
				Game2.Players[i].AddWeapon("Machete");
				Game2.Players[i].AddWeapon("Shotgun", 1);
				Game.Players[i].GetInventory().EquipWeapon(1, 0);
			}
			Game2.PlayerIndex = 1;
			Game2.Player = &Game2.Players[Game2.PlayerIndex];
			Game2.Player->UpdateUI();
		}
		GameRunning = true;
		SwitchToBaseScreen();
	});
	MainMenu.AddButton(3, 0, "Multiplayer", [&] { SwitchToMenu(MenuType::Multi); });
	MainMenu.AddButton(4, 0, "Options", [&] { SwitchToMenu(MenuType::Option); });
	MainMenu.AddButton(5, 0, "Quit", [] { exit(0); });
	//MULTIPLAYER
	MultiplayerMenu.SetCurrent(MenuType::Multi);
	MultiplayerMenu.AddButton(0, 0, "Host", [&] {
		EnableLogFile();
		NS.Multiplayer = true;
		delete NS.NM;
		NS.NM = new NetworkManager(30000);
		NS.NM->SetHost(true);
		uint8_t NumPlayers = NS.NM->SetupNetworking(true);
		SetupPlayers(NumPlayers);
		Game.PlayerIndex = 0;
		Game.Player = &Game.Players[Game.PlayerIndex];
		Game.Player->UpdateUI();
		GameRunning = true;
		SwitchToBaseScreen();
	});
	MultiplayerMenu.AddButton(0, 1, "Client1", [&] {
		NS.Multiplayer = true;
		delete NS.NM;
		NS.NM = new NetworkManager(30001);
		NS.NM->SetHost(false);
		uint8_t NumPlayers = NS.NM->SetupNetworking(false);
		SetupPlayers(NumPlayers);
		Game.PlayerIndex = 1;
		Game.Player = &Game.Players[Game.PlayerIndex];
		Game.Player->UpdateUI();
		GameRunning = true;
		SwitchToBaseScreen();
	});
	MultiplayerMenu.AddButton(0, 2, "Client2", [&] {
		NS.Multiplayer = true;
		delete NS.NM;
		NS.NM = new NetworkManager(30002);
		NS.NM->SetHost(false);
		uint8_t NumPlayers = NS.NM->SetupNetworking(false);
		SetupPlayers(NumPlayers);
		Game.PlayerIndex = 1;
		Game.Player = &Game.Players[Game.PlayerIndex];
		Game.Player->UpdateUI();
		GameRunning = true;
		SwitchToBaseScreen();
	});
	MultiplayerMenu.AddButton(6, 0, "Back", [&] { SwitchToMenu(MenuType::Main); });
	//OPTIONS
	OptionsMenu.SetCurrent(MenuType::Option);
	static std::string VolumeText = "Volume " + std::to_string(SoundMan.GetVolume());
	static std::string ResolutionText = Set.Resolution;
	OptionsMenu.AddLabel(0, 0, &VolumeText, 15);
	OptionsMenu.AddButton(0, 1, "-", [&] {
		int CurrentVolume = SoundMan.GetVolume();
		CurrentVolume -= 10;
		if(CurrentVolume < 0) CurrentVolume = 0;
		VolumeText = "Volume " + std::to_string(CurrentVolume);
		SoundMan.SetVolume(CurrentVolume);
		SoundMan.Play("Pickup-Ammo-01.wav");
		Log("Volume: " + std::to_string(CurrentVolume));
	});
	OptionsMenu.AddButton(0, 2, "+", [&] {
		int CurrentVolume = SoundMan.GetVolume();
		CurrentVolume += 10;
		VolumeText = "Volume " + std::to_string(CurrentVolume);
		SoundMan.SetVolume(CurrentVolume);
		SoundMan.Play("Pickup-Ammo-01.wav");
		Log("Volume: " + std::to_string(CurrentVolume));
	});
	OptionsMenu.AddButton(1, 0, "800X600", [&] { ResolutionText = "800X600"; });
	OptionsMenu.AddButton(1, 1, "1280X720", [&] { ResolutionText = "1280X720"; });
	OptionsMenu.AddButton(2, 0, "1920X1080", [&] { ResolutionText = "1920X1080"; });
	OptionsMenu.AddButton(2, 1, "2560X1440", [&] { ResolutionText = "2560X1440"; });
	static std::string ResText = "Resolution: ";
	OptionsMenu.AddLabel(3, 0, &ResText, 15);

	OptionsMenu.AddInput(3, 1, &ResolutionText);
	OptionsMenu.AddButton(5, 1, "Apply", [&] {
		Set.Resolution = ResolutionText;
		size_t x = ResolutionText.find_first_of('X');
		int W = std::stoi(ResolutionText.substr(0, x));
		int H = std::stoi(ResolutionText.substr(x + 1));
		G_ScreenSize = {W, H};
		SetupGraphics();

		Set.Volume = SoundMan.GetVolume();
		SaveSettingsFile(Set);
		SwitchToMenu(MenuType::Option);
	});
	OptionsMenu.AddButton(5, 0, "Back", [&] { SwitchToMenu(MenuType::Main); });

	SwitchToMenu(MenuType::Main);
}

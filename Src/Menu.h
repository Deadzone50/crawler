#pragma once

#include <functional>
#include <string>
#include <vector>

#include "FileIO.h"
#include "RenderText.h"
#include "Settings.h"
#include "vector/Vector.h"

struct NetworkState;
struct RenderState;
struct IOStates;
class Shader;

enum MenuType : uint8_t
{
	Main = 0,
	Multi = 1,
	Option = 2
};

enum class MenuItemType
{
	Button,
	Input,
	Label,
};

class MenuItem
{
public:
	MenuItem() {}
	MenuItem(const std::string &Text, std::function<void()> OnClick, uint8_t CharSize = 20);
	MenuItem(const std::string &Text, MenuItemType Type, uint8_t CharSize = 20);
	MenuItem(std::string *Value, MenuItemType Type, uint8_t CharSize = 20);
	void Click();

	MenuItemType GetItemType() const { return m_Type; }
	std::string GetText() const { return m_Text.GetText(); }

	void ToggleHighlight();
	void SetHighlight(bool Set) { m_Text.SetHighlight(Set); }

	void SetText(const std::string &Text) { m_Text.SetText(Text); }
	void SetColor(Vec3f Color) { m_Text.SetColor(Color); }
	void Append(char c) { m_Value->push_back(c); }
	void PopBack();

	void Draw(Shader &Shader, bool Selected);

	size_t GetTotalLen() const { return m_Text.GetTotalLen(); }

private:
	RenderText m_Text;
	std::string *m_Value = nullptr;
	std::function<void()> m_OnClick = nullptr;
	MenuItemType m_Type = MenuItemType::Label;
	bool m_Highlighted = false;
};

class Menu
{
public:
	void AddButton(uint32_t Row, uint32_t Col, const std::string &Text, std::function<void()> OnClick, uint8_t CharSize = 20);
	void ChangeButtonText(uint32_t Row, uint32_t Col, const std::string &Text);
	void SetHighlight(uint32_t Row, uint32_t Col, bool Set);
	void AddInput(uint32_t Row, uint32_t Col, std::string *Value, uint8_t CharSize = 20);
	void AddLabel(uint32_t Row, uint32_t Col, const std::string &Text, uint8_t CharSize = 20);
	void AddLabel(uint32_t Row, uint32_t Col, std::string *Value, uint8_t CharSize = 20);
	void SetCurrent(MenuType P) { m_CurrentMenu = P; }
	void SetParent(MenuType P) { m_ParentMenu = P; }
	bool MouseSelect(Vec2i Pos);
	uint32_t SelectUp();
	uint32_t SelectDown();
	uint32_t SelectLeft();
	uint32_t SelectRight();
	void ClickSelected();
	void Append(char c) { GetItem(m_SelectedY, m_SelectedX).Append(c); }
	void PopBack() { GetItem(m_SelectedY, m_SelectedX).PopBack(); }
	bool ItemHighlighted() { return m_ItemHighlighted; }
	void Back();

	void Draw(Shader &Shader);
	void Handle(const IOStates &IO);

private:
	void ResizeMenu(uint32_t Row, uint32_t Col);
	MenuItem &GetItem(uint32_t Row, uint32_t Col) { return m_Items[Row][Col]; }
	std::vector<std::vector<MenuItem>> m_Items;
	uint32_t m_Columns = 0;
	uint32_t m_Rows = 0;
	uint32_t m_SelectedY = 0, m_SelectedX = 0;

	bool m_Changed = true;
	bool m_ItemHighlighted = false;

	MenuType m_ParentMenu = Main;
	MenuType m_CurrentMenu = Main;

	Vec2f m_MarginsPercent = {0.1f, 0.1f};
};

void InitMainMenu(std::vector<Menu> &Menus, NetworkState &NS, RenderState &RS, Settings &Set);

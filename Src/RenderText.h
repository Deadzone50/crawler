#pragma once

#include "Shader.h"
#include "vector/Vector.h"

class RenderText
{
public:
	RenderText() {}
	~RenderText();
	RenderText(const std::string &Text, uint32_t CharSize = 20, Vec3f Col = {1, 1, 1});
	void Draw(Shader &Shader);

	void SetText(const std::string &Text)
	{
		m_Text = Text;
		m_Changed = true;
	}
	void SetColor(const Vec3f &Col);
	void SetColor(Vec3f Col[4]);
	void SetCharSize(uint8_t Size)
	{
		m_CharSize = Size;
		m_Changed = true;
	}
	std::string GetText() const { return m_Text; }
	uint8_t GetCharSize() const { return m_CharSize; }
	size_t GetTotalLen() const { return m_CharSize * m_Text.size(); }
	void ToggleHighlight()
	{
		m_Highlight = !m_Highlight;
		m_Changed = true;
	}
	void SetHighlight(bool Set)
	{
		m_Highlight = Set;
		m_Changed = true;
	}

private:
	std::string m_Text;
	Vec3f m_Color[4] = {{1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}};
	bool m_Initialized = false, m_Changed = true, m_Highlight = false;
	uint32_t m_VAO = 0, m_VBO = 0;
	uint8_t m_CharSize = 20;
};

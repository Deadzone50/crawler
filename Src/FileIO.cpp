#include "FileIO.h"

#include <fstream>
#include <map>

#include "Console.h"
#include "Debug.h"
#include "vector/Vector.h"
#include "XML.h"

static std::vector<Vec3f> ConvertToVec3f(const std::string &float_array, int Num)
{
	std::istringstream iss(float_array);
	std::vector<Vec3f> result(Num);
	for(int i = 0; i < Num; ++i)
	{
		iss >> result[i].x;
		iss >> result[i].y;
		iss >> result[i].z;
	}
	return result;
}

static std::vector<Vec2f> ConvertToVec2f(const std::string &float_array, int Num)
{
	std::istringstream iss(float_array);
	std::vector<Vec2f> result(Num);
	for(int i = 0; i < Num; ++i)
	{
		iss >> result[i].x;
		iss >> result[i].y;
	}
	return result;
}

static std::vector<uint32_t> ConvertToUint(const std::string &uint_array, int Num)
{
	std::istringstream iss(uint_array);
	std::vector<uint32_t> result(Num);
	for(int i = 0; i < Num; ++i)
	{
		iss >> result[i];
	}
	return result;
}

static std::string GetFloatsFromSource(XMLElement &geometry, const std::string &SourceType, int &ArrayCount)
{
	std::string id = geometry.GetAttribute("id");
	const XMLElement &source = geometry.Get("mesh").Get("source", "id", id + "-" + SourceType);
	ArrayCount = std::stoi(source.Get("float_array").GetAttribute("count"));
	return source.Get("float_array").GetText();
}

static void GetTriangleData(XMLElement &XML, std::vector<std::string> &ObjectNames, std::vector<Mesh> &Meshes)
{
	std::vector<XMLElement> geometrires = XML.Get("library_geometries").GetAll("geometry");
	std::vector<XMLElement> visual_nodes = XML.Get("library_visual_scenes").Get("visual_scene").GetAll("node");

	for(auto &geometry : geometrires)
	{
		const std::string id = geometry.GetAttribute("id");
		for(auto &node : visual_nodes)
		{
			std::string url = node.Get("instance_geometry").GetAttribute("url");
			url = url.substr(1, std::string::npos);
			if(url == id)
			{
				ObjectNames.push_back(node.GetAttribute("name"));
				break;
			}
		}

		int PositionCount;
		std::string PositionFloats = GetFloatsFromSource(geometry, "positions", PositionCount);
		std::vector<Vec3f> Positions = ConvertToVec3f(PositionFloats, PositionCount / 3);

		int NormalCount;
		std::string NormalFloats = GetFloatsFromSource(geometry, "normals", NormalCount);
		std::vector<Vec3f> Normals = ConvertToVec3f(NormalFloats, NormalCount / 3);

		int TexCount;
		std::string TexFloats = GetFloatsFromSource(geometry, "map-0", TexCount);
		std::vector<Vec2f> TexCoords = ConvertToVec2f(TexFloats, TexCount / 2);

		std::vector<XMLElement> triangles_mats = geometry.Get("mesh").GetAll("triangles");

		std::vector<std::string> MatNames;
		std::vector<uint32_t> TI;
		//std::vector<OpeningIndices> OpeningsTmp(6, {-1,-1,-1,-1});
		for(auto &tm : triangles_mats)
		{
			std::string indices = tm.Get("p").GetText();
			int indices_count = std::stoi(tm.GetAttribute("count"));
			std::vector<uint32_t> TI1 = ConvertToUint(indices, indices_count * 3 * 3);

			//	std::string MatName = tm.GetAttribute("material");
			//	if(MatName.find("Opening") == 0)
			//	{
			//		int I[6] = {
			//			TI1[0],
			//			TI1[3],
			//			TI1[6],

			//			TI1[9],
			//			TI1[12],
			//			TI1[15]
			//		};
			//		int OpeningIndex = MatName[7] - '0';
			//		OpeningsTmp[OpeningIndex] = CreateOpening(I);
			//	}
			//	else
			{
				TI.insert(TI.end(), TI1.begin(), TI1.end());
			}
		}
		Mesh M;
		for(int i = 0; i < TI.size(); i += 3)
		{
			Vertex V;
			V.Position = Positions[TI[i]];
			V.Normal = Normals[TI[i + 1]];
			V.TexCoord = TexCoords[TI[i + 2]];
			M.Indices.push_back((uint32_t)M.Vertices.size());
			M.Vertices.push_back(V);

			//for(int jj = 0; jj < OpeningsTmp.size(); ++jj)
			//{
			//	for(int j = 0; j < 4; ++j)
			//	{
			//		if(OpeningsTmp[jj].I[j] == TI[i])
			//		{
			//			Openings.back()[jj].I[j] = M.Indices.back();
			//			break;
			//		}
			//	}
			//}
		}
		Meshes.push_back(M);
	}
}

void ParseDaeFile(const std::string &Path, std::vector<std::string> &ObjectNames, std::vector<Mesh> &Meshes)
{
	XMLElement XML = ParseXML(Path);
	//PrintXML(XML);
	GetTriangleData(XML, ObjectNames, Meshes);
}

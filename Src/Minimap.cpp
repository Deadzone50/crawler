#include "Minimap.h"

#include "Drawer.h"
#include "LevelObjects.h"

void Minimap::AddWall(Vec3f S, Vec3f E)
{
	MapWall W;
	W.S = {S.x, -S.z};
	W.E = {E.x, -E.z};
	W.Depth = (S.y + E.y) / 2;
	W.Found = true;
	m_Walls.push_back(W);
}

void Minimap::AddRoom(const LevelRoom &Room)
{
	//const Mesh &M = Room.GetCollisionMesh();

	//for(int i = 0; i < M.Indices.size(); i += 3)
	//{
	//	Vec3f Tri[3] = {M.Vertices[M.Indices[i]].Position, M.Vertices[M.Indices[i + 1]].Position,
	//					M.Vertices[M.Indices[i + 2]].Position};

	//	Vec3f N = NORMALIZE(CROSS(Tri[1] - Tri[0], Tri[2] - Tri[1]));

	//	if(fabs(DOT(N, {0, 1, 0})) < 0.3f)	  //Walls
	//	{
	//		if(fabs(Tri[0].y - Tri[1].y) > 0.2f || fabs(Tri[1].y - Tri[2].y) > 0.2f || fabs(Tri[0].y - Tri[2].y) > 0.2f)
	//		{
	//			const Vec2f P0 = {Tri[0].x, Tri[0].z};
	//			const Vec2f P1 = {Tri[1].x, Tri[1].z};
	//			const Vec2f P2 = {Tri[2].x, Tri[2].z};

	//			if(LEN2(P1 - P0) > LEN2(P2 - P1))
	//			{
	//				AddWall(Tri[0], Tri[1]);
	//			}
	//			else
	//			{
	//				AddWall(Tri[1], Tri[2]);
	//			}
	//		}
	//	}
	//}
}

void Minimap::Draw(const Vec3f &CameraPos, const Vec3f &LookingDir) const
{
	const Vec2f Offset = {0.5f, 0.5f};
	const float Scale = 0.015f;

	Vec3f R = NORMALIZE(CROSS(LookingDir, {0, 1, 0}));
	Vec3f Up = CROSS(R, LookingDir);
	Vec3f Center = CameraPos + LookingDir + Offset.x * R + Offset.y * Up;

	DrawPoint(Center - LookingDir * CameraPos.y * 0.001f, {10, 0, 0});	  //Player

	Vec2f PlayerPos2D = Vec2f{CameraPos.x, -CameraPos.z};
	for(const auto &W : m_Walls)
	{
		if(W.Found)
		{
			Vec2f S = W.S - PlayerPos2D;
			Vec2f E = W.E - PlayerPos2D;
			Vec3f Col = {1, 1, 1};
			if(CameraPos.y - W.Depth > 2)
			{
				Col = {0.5f, 0.5f, 0.5f};
			}
			else if(CameraPos.y - W.Depth < -2)
			{
				Col = {0.9f, 0.1f, 0.9f};
			}
			const Vec3f C = Center - LookingDir * W.Depth * 0.001f;
			DrawLine(C + S.x * R * Scale + S.y * Up * Scale, C + E.x * R * Scale + E.y * Up * Scale, Col);
		}
	}
}

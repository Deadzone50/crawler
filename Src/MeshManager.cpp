#include "MeshManager.h"
#include "Console.h"
#include "Debug.h"

void MeshManager::AddMesh(const Mesh &M, const std::string &Name)
{
	m_MeshMap[Name] = M;
	m_MeshMap[Name].Init();
	m_MeshMap[Name].UpdateBuffers();
	Log("Mesh added: " + Name);
}

void MeshManager::AddCollisionMeshes(const std::vector<Mesh> &Meshes, const std::string &Name)
{
	m_CollisionMeshMap[Name] = Meshes;
	for(auto &M : m_CollisionMeshMap[Name])
	{
		M.Init();
		M.UpdateBuffers();
	}
	Log("Collision meshes added: " + Name);
}

Mesh *MeshManager::GetMesh(const std::string &Name)
{
	if(m_MeshMap.find(Name) == m_MeshMap.end())
	{
		DEBUG_ABORT(Name + " MESH NOT FOUND");
	}
	return &m_MeshMap.at(Name);
}

std::vector<Mesh> MeshManager::CollectCollisionMeshes(const std::string &Name) const
{
	std::vector<Mesh> Result;
	for(const auto &M : m_MeshMap)
	{
		if(M.first.substr(0, (Name + "_col").size()) == Name + "_col")	  //find names beginning with xxx_col
		{
			Result.push_back(M.second);
		}
	}
	if(Result.empty())
	{
		Result.push_back(m_MeshMap.at(Name));
	}
	return Result;
}

std::vector<const Mesh *> MeshManager::GetCollisionMeshesVector(const std::string &Name) const
{
	std::vector<const Mesh *> Result;
	auto Map = m_CollisionMeshMap.find(Name);
	if(Map == m_CollisionMeshMap.end())
	{
		Log(Name + " COLLISION MESH NOT FOUND");
	}
	else
	{
		for(auto &M : Map->second)
		{
			Result.push_back(&M);
		}
	}
	return Result;
}

void MeshManager::AddRig(const MeshRig &R, const std::string &Name)
{
	m_MeshRigMap[Name] = R;
	Log("Mesh rig added: " + Name);
}

MeshRig *MeshManager::GetRig(const std::string &Name)
{
	if(m_MeshRigMap.find(Name) == m_MeshRigMap.end())
	{
		//DEBUG_ABORT(Name + " RIG NOT FOUND");
		Log(Name + " RIG NOT FOUND");
		return nullptr;
	}
	return &m_MeshRigMap[Name];
}

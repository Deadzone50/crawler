#pragma once

#include "Console.h"
#include "vector/Vector.h"

extern bool D_NUMPAD[10];
extern bool D_NUMPAD_CHANGED[10];

bool D_Numpad(int i);
bool D_NumpadDown(int i);

void ClearTimestamps();

//Calculates time from last entry, all Timestamps with same name and number get added together
//Entry 0 is used as the first of series
void DebugTimestamp(std::string Name, uint8_t Number);

void DisplayErrorBox(const std::string &Text);

//#define DEBUG_ABORT(T) Log("ABORT: " + std::string(__FILE__) + ":" +std::string(__func__) + ":" +
//std::to_string(__LINE__)  + ":" + T); abort();
#define DEBUG_ABORT(T)                                                                                                 \
	DisplayErrorBox("ABORT: " + std::string(__FILE__) + ":" + std::string(__func__) + ":" + std::to_string(__LINE__) + \
					":" + T);

void DrawDebugPoint(Vec3f P, Vec3f Color);

void DrawDebugLine(Vec3f S, Vec3f E, Vec3f Color);

void DrawDebugPointPersistent(Vec3f P, Vec3f Color);

void DrawDebugLinePersistent(Vec3f S, Vec3f E, Vec3f Color);

void DrawDebugDirLine(Vec3f S, Vec3f E, Vec3f Color);

void DrawDebugCube(Vec3f Min, Vec3f Max, Vec3f Color);

void ClearPersistentDebugDraws();

void ToggleDebugDraws();

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "BaseScreen.h"

#include "CharacterLevel.h"
#include "Drawer.h"
#include "Level.h"
#include "Network.h"
#include "PlayerClass.h"
#include "RenderText.h"
#include "Scene.h"
#include "States.h"

extern Vec2i G_ScreenSize;
extern GameState Game;
extern GameState Game2;
extern NetworkState NState;

BaseScreen::BaseScreen()
{
	Menu *M = &m_Menus[static_cast<uint8_t>(CurrentScreen::Base)];
	M->AddLabel(0, 0, "Base");
	M->AddButton(1, 0, "NextMap", [] {
		if(NState.Multiplayer)
			NState.NM->SendReady(Game.PlayerIndex);
		else
		{
			Game.Player->Reset();
			Game.Player->UpdateUI();
			Game.Map->SetSeed(Game.Map->GetSeed() + 1);
			Vec3f StartPos = Game.Map->GenerateLevel();
			Game.Player->SetPosition(StartPos);
			Game.Player->SetRotation({0, 0});
			Game.Player->UpdateMinimap(Game.Map->GetMinimap());
			Game.Player->SetModifiers(Game.CharacterCandidates[Game.CurrentCandidate].GetModifiers());
			Game.InMap = true;
			SwitchToGame();
		}
	});
	M->AddButton(1, 1, "DebugMap", [] {
		if(NState.Multiplayer)
			NState.NM->SendReady(Game.PlayerIndex);
		else
		{
			Game.Player->Reset();
			Game.Player->UpdateUI();
			Game.Map->SetSeed(Game.Map->GetSeed() + 1);
			Vec3f StartPos = Game.Map->GenerateDebugLevel(Game);
			Game.Player->SetPosition(StartPos);
			Game.Player->SetRotation({0, 0});
			Game.Player->UpdateMinimap(Game.Map->GetMinimap());
			Game.Player->SetModifiers(Game.CharacterCandidates[Game.CurrentCandidate].GetModifiers());
			Game.InMap = true;
			SwitchToGame();
		}
	});
	M->AddButton(2, 0, "Roster", [&] {
		m_Currentscreen = CurrentScreen::Roster;
		m_CurrentMenu = &m_Menus[static_cast<uint8_t>(CurrentScreen::Roster)];
	});
	M->AddButton(3, 0, "ExitToMenu", [] { SwitchToMenu(MenuType::Main); });

	M = &m_Menus[static_cast<uint8_t>(CurrentScreen::Roster)];
	M->AddLabel(0, 0, "Roster");
	M->AddButton(0, 1, "Char1", [&] {
		Game.CurrentCandidate = 0;
		m_Menus[static_cast<uint8_t>(CurrentScreen::Roster)].SetHighlight(0, 1, true);
		m_Menus[static_cast<uint8_t>(CurrentScreen::Roster)].SetHighlight(0, 2, false);
		m_Menus[static_cast<uint8_t>(CurrentScreen::Roster)].SetHighlight(0, 3, false);
	});
	M->AddButton(0, 2, "Char2", [&] {
		Game.CurrentCandidate = 1;
		m_Menus[static_cast<uint8_t>(CurrentScreen::Roster)].SetHighlight(0, 1, false);
		m_Menus[static_cast<uint8_t>(CurrentScreen::Roster)].SetHighlight(0, 2, true);
		m_Menus[static_cast<uint8_t>(CurrentScreen::Roster)].SetHighlight(0, 3, false);
	});
	M->AddButton(0, 3, "Char3", [&] {
		Game.CurrentCandidate = 2;
		m_Menus[static_cast<uint8_t>(CurrentScreen::Roster)].SetHighlight(0, 1, false);
		m_Menus[static_cast<uint8_t>(CurrentScreen::Roster)].SetHighlight(0, 2, false);
		m_Menus[static_cast<uint8_t>(CurrentScreen::Roster)].SetHighlight(0, 3, true);
	});
	M->AddButton(1, 0, "Reset", [&] { Game.CharacterCandidates[Game.CurrentCandidate].Skilltree.Reset(); });
	M->AddButton(2, 0, "Back", [&] {
		m_Currentscreen = CurrentScreen::Base;
		m_CurrentMenu = &m_Menus[static_cast<uint8_t>(CurrentScreen::Base)];
	});
	M->SetHighlight(0, 1, true);
}

void BaseScreen::HandleAndDraw(Shader &Shader, const IOStates &IO, GameState &Game)
{
	m_Text.clear();

	uint32_t NumCharacters = Game.CharacterCandidates.size();
	if(NumCharacters < 3)
	{
		auto NewCandidates = GenerateCandidates(3 - NumCharacters, Game.PlayerLevel.GetPossibleSkills());
		Game.CharacterCandidates.insert(Game.CharacterCandidates.end(), NewCandidates.begin(), NewCandidates.end());

		int i = 1;
		for(const auto &C : Game.CharacterCandidates)
		{
			m_Menus[static_cast<uint8_t>(CurrentScreen::Roster)].ChangeButtonText(0, i++, C.Name);
		}
	}

	if(m_Currentscreen == CurrentScreen::Roster)
	{
		Game.CharacterCandidates[Game.CurrentCandidate].Skilltree.HandleAndDraw(Shader, IO);
	}
	else
	{
		HandleMainscreen(IO, Game);
	}
	DrawCharacterStats(Game.CharacterCandidates[Game.CurrentCandidate]);

	m_CurrentMenu->Handle(IO);
	m_CurrentMenu->Draw(Shader);

	for(auto &T : m_Text)
	{
		Shader.SetMat4("Model", T.Transform);
		T.Text.Draw(Shader);
	}
}

void BaseScreen::DrawCharacterStats(const Candidate &Character)
{
	auto Modifiers = Character.GetModifiers();
	m_Text.push_back({RenderText("Str:" + std::to_string(Modifiers[SkillTag::Strenght]) + " +" +
									 std::to_string(Character.StatGrowth.Strenght),
								 12, Vec3f{0, 1, 1}),
					  Mat4f()});
	m_Text.push_back({RenderText("Dex:" + std::to_string(Modifiers[SkillTag::Dexterity]) + " +" +
									 std::to_string(Character.StatGrowth.Dexterity),
								 12, Vec3f{0, 1, 1}),
					  Mat4f()});
	m_Text.push_back({RenderText("Int:" + std::to_string(Modifiers[SkillTag::Intelligence]) + " +" +
									 std::to_string(Character.StatGrowth.Intelligence),
								 12, Vec3f{0, 1, 1}),
					  Mat4f()});

	uint32_t Skillpoints = Character.Skilltree.GetSkillpoints();
	m_Text.push_back({RenderText("Skillpoints:" + std::to_string(Skillpoints), 12, Vec3f{0, 1, 1}), Mat4f()});

	Mat4f ModelMatrix;
	Vec3f Pos = {(float)G_ScreenSize.x - 200, (float)G_ScreenSize.y - 20, 0};
	for(auto &T : m_Text)
	{
		ModelMatrix.SetCol(3, {Pos.x, Pos.y, Pos.z, 1});
		T.Transform = ModelMatrix;
		Pos.y -= 20.0f;
	}
}

//display info
void BaseScreen::HandleMainscreen(const IOStates &IO, GameState &Game)
{
	if(NState.Multiplayer)
	{
		Mat4f ModelMatrix;
		Vec3f Pos = {(float)G_ScreenSize.x - 200, (float)G_ScreenSize.y - 20 - 20 * 4, 0};
		bool AllReady = true;
		ModelMatrix.SetCol(3, {Pos.x, Pos.y, Pos.z, 1});
		m_Text.push_back({RenderText("Players:", 12, Vec3f{0, 1, 0}), ModelMatrix});
		for(int i = 0; i < Game.Players.size(); ++i)
		{
			Vec3f Col = {1, 1, 1};
			if(i == Game.PlayerIndex)
			{
				Col = {1, 0, 0};
			}
			Pos.y -= 20.0f;
			float Ping = NState.NM->GetPing(i);
			std::string tmp = "Player " + std::to_string(i) + " " + std::to_string(Ping) + " ms";
			if(NState.NM->IsReady(i))
				tmp += " R";
			else
				AllReady = false;
			if(NState.NM->InMap(i))
				tmp += " M";
			ModelMatrix.SetCol(3, {Pos.x, Pos.y, Pos.z, 1});
			m_Text.push_back({RenderText(tmp, 10, Col), ModelMatrix});
		}
		if(AllReady || (NState.DebugMulti && NState.NM->IsReady(0)))
		{
			Game.Player->Reset();
			Game.Map->SetSeed(Game.Map->GetSeed() + 1);
			Game.Map->GenerateLevel();
			NState.NM->ClearReady();
			NState.NM->ClearExitMap();
			if(NState.DebugMulti)
			{
				Game2.Player->Reset();
				Game2.Map->SetSeed(Game2.Map->GetSeed() + 1);
				Game2.Map->GenerateLevel();
			}
			SwitchToGame();
		}
	}
}

#pragma once

#include "Mesh.h"

class Sprite : public Mesh
{
public:
	Sprite(const std::string &FileName, Vec2i Size);
};

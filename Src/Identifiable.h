#pragma once

#include <cstdint>

class Identifiable
{
public:
	Identifiable();
	void SetId(uint32_t Id) { m_Id = Id; }
	uint32_t GetId() { return m_Id; }

protected:
	uint32_t m_Id = 0;
};

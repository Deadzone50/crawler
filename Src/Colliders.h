#pragma once

#include "vector/Matrix.h"
#include "vector/Vector.h"
#include <vector>

#include "Structs.h"

class Mesh;

struct Face
{
	std::vector<int> Indices;
	bool operator==(const Face &F2) const noexcept
	{
		bool Result = true;
		for(auto &p1 : Indices)
		{
			if(std::find(F2.Indices.begin(), F2.Indices.end(), p1) == F2.Indices.end())
			{
				Result = false;
				break;
			}
		}
		return Result;
	}
};

struct Edge
{
	int Indices[2];
	bool operator==(const Edge &E2) const noexcept
	{
		return Indices[0] == E2.Indices[0] && Indices[1] == E2.Indices[1] ||
			   Indices[1] == E2.Indices[0] && Indices[0] == E2.Indices[1];
	}
};

struct Collider
{
	enum Type
	{
		Sphere,
		AABB,
		Capsule,
		Mesh
	};
	Type ColliderType;
};

struct SphereCollider : public Collider
{
	Vec3f Offset = {0, 0, 0};
	Vec3f Position = {0, 0, 0};
	float Radius;
};

struct CapsuleCollider : public Collider
{
	Vec3f Position1;
	Vec3f Position2;
	float Radius;
};

struct MeshCollider : public Collider
{
	std::vector<Face> Faces;
	std::vector<Edge> Edges;
	std::vector<Vec3f> Points;
	std::vector<Vec3f> OriginalPoints;
};

Vec3f GetFaceNormal(const std::vector<Vec3f> &P, const Face &F);

Vec3f GetEdgeDirection(const std::vector<Vec3f> &P, const Edge &E);

void DrawCollider(const Collider *C, Vec3f Color);

AxisAlignedBoundingBox CalculateAABB(const Vec3f &Scale, const Mesh *M, const Mat3f &Rotation);

AxisAlignedBoundingBox CalculateAABB(const std::vector<Collider *> &Colliders, const Mat3f &Rotation);

Collider *CreateCollider(const Mesh *M, const Vec3f &Scale, Collider::Type Type);

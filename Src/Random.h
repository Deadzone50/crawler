#pragma once

#include <cstdint>

#include "vector/Vector.h"

void SeedRandomGen(uint32_t Seed);

//Inclusive range
uint32_t GetRandomUI(uint32_t Min, uint32_t Max);

float GetRandomF(float Min, float Max);
Vec3f GetRandomV3(float Min, float Max);

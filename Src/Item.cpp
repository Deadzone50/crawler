#include "Item.h"
#include "Event.h"
#include "MeshManager.h"
#include "States.h"

Item::Item()
{
	m_Pos = {0, 0, 0};
	m_Velocity = {};
	m_Force = {};
	m_Torque = {};
	m_AngularVelocity = {};

	m_InverseMass = 0;
	m_Restitution = 0.7f;

	//Recently Weighted Average Energy
	m_RWASleep = 0.08f;
	m_RWAEnergy = 1;
	m_Active = false;

	m_Scale = {1, 1, 1};
	m_MaxCornerDistSqr = 0;
}

void Item::SetProperties(const ItemProperties &Properties)
{
	m_Properties = Properties;
}

Item::Item(ItemType Type, Vec3f Pos, Vec4f RotationQuat) : Item()
{
	m_Properties.Type = Type;
	m_Pos = Pos;
	float InverseMass = 0.5f;
	MomentOfInertiaType MOIType = MomentOfInertiaType::Box;
	switch(Type)
	{
		case ItemType::None: break;

		case ItemType::Health: m_Properties.Mesh = MeshMan.GetMesh("Flask"); break;

		case ItemType::Ammo_44: m_Properties.Mesh = MeshMan.GetMesh("AmmoBox"); break;
		case ItemType::Ammo_Shotgun: m_Properties.Mesh = MeshMan.GetMesh("AmmoBox"); break;

		case ItemType::Machete: m_Properties.Mesh = MeshMan.GetMesh("Machete"); break;
		case ItemType::Pitchfork: m_Properties.Mesh = MeshMan.GetMesh("Pitchfork"); break;
		case ItemType::Polearm: m_Properties.Mesh = MeshMan.GetMesh("Polearm"); break;

		case ItemType::Revolver: m_Properties.Mesh = MeshMan.GetMesh("Revolver"); break;
		case ItemType::Shotgun: m_Properties.Mesh = MeshMan.GetMesh("Shotgun"); break;
		default: DEBUG_ABORT("Unhandled ItemType: " + std::to_string(static_cast<uint8_t>(Type))); break;
	}
	SetColliders({CreateCollider(m_Properties.Mesh, {1, 1, 1}, Collider::Type::AABB)});
	SetPhysicalProperties({1, 1, 1}, InverseMass, MOIType);
	SetRotation(RotationQuat);
	UpdateColliders();
}

void Item::Draw(Shader &Shader) const
{
	Mat4f ModelMatrix;
	ModelMatrix.Scale(m_Scale);
	ModelMatrix = Mat4f(m_RotationMatrix) * ModelMatrix;
	ModelMatrix.SetPosition(m_Pos);

	Shader.SetMat4("Model", ModelMatrix);
	m_Properties.Mesh->Draw(Shader);
}

void SpawnItem(ItemType Type, Vec3f Pos, Vec4f Rot, GameState &Game)
{
	std::shared_ptr<SpawnEvent> E = std::make_shared<SpawnEvent>();
	E->ItemType = Type;
	E->Pos = Pos;
	E->Rot = Rot;
	AddEvent(E, Game);
}

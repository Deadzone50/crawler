#pragma once

#include <map>
#include <string>

#include "Mesh.h"
#include "Rigging.h"

class MeshManager
{
public:
	void AddMesh(const Mesh &M, const std::string &Name);
	void AddCollisionMeshes(const std::vector<Mesh> &Meshes, const std::string &Name);
	//Get mesh by name, gives error if not found
	Mesh *GetMesh(const std::string &Name);
	//Get collision mesh that matches name, gives the normal mesh if not found
	std::vector<Mesh> CollectCollisionMeshes(const std::string &Name) const;
	std::vector<const Mesh *> GetCollisionMeshesVector(const std::string &Name) const;

	void AddRig(const MeshRig &R, const std::string &Name);
	MeshRig *GetRig(const std::string &Name);

private:
	std::map<std::string, Mesh> m_MeshMap;
	std::map<std::string, std::vector<Mesh>> m_CollisionMeshMap;
	std::map<std::string, MeshRig> m_MeshRigMap;
};

extern MeshManager MeshMan;

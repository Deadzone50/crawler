#pragma once

#include <vector>

#include "Collision.h"
#include "Globals.h"
#include "Structs.h"
#include "vector/Vector.h"

class LevelRoom;

struct OctreeNode
{
	AxisAlignedBoundingBox AABB;
	std::vector<OctreeNode> Children;
	std::vector<int> RoomIndices;
	Vec3f Color = {1, 1, 1};

	void AddRoom(int i, Vec3f Center);
};

class Octree
{
public:
	void AddRoom(int Index, Vec3f Pos);
	void Clean();
	std::vector<int> GetRooms(const SphereCollider &Col, const OctreeNode &Node) const;
	std::vector<int> GetRooms(const Vec3f Pos, const OctreeNode &Node) const;
	std::vector<int> GetRooms(const AxisAlignedBoundingBox &AABB, const OctreeNode &Node) const;
	OctreeNode m_RootNode;
};

OctreeNode CreateOctreeAABBs(const AxisAlignedBoundingBox &AABB);

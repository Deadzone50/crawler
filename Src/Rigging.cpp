#include "Rigging.h"

#include <algorithm>

#include "Debug.h"
#include "Level.h"
#include "Quaternions.h"
#include "States.h"

static Vec3f GetJointModelPosition(const Joint &J)
{
	Vec4f P4 = J.ModelJointTransform * Vec4f{0, 0, 0, 1};
	return {P4.x, P4.y, P4.z};
}

static void CreateBone(int i1, int i2, float Difflength, std::vector<Bone> &Bones, const std::vector<Joint> &Joints)
{
	Bone B;
	B.From = i1;
	B.To = i2;
	B.MinLenght = LEN(GetJointModelPosition(Joints[i1]) - GetJointModelPosition(Joints[i2])) - Difflength;
	B.MaxLenght = B.MinLenght + 2 * Difflength;
	Bones.push_back(B);
}

static void CreateBone(int i1, int i2, float MinDiff, float MaxDiff, std::vector<Bone> &Bones, const std::vector<Joint> &Joints)
{
	Bone B;
	B.From = i1;
	B.To = i2;
	B.MinLenght = LEN(GetJointModelPosition(Joints[i1]) - GetJointModelPosition(Joints[i2])) + MinDiff;
	B.MaxLenght = B.MinLenght - MinDiff + MaxDiff;
	Bones.push_back(B);
}

void MeshRig::CreateRagdoll(const Mat4f &ModelMatrix)
{
	for(int i = 0; i < Joints.size(); ++i)
	{
		Mat4f ModelJointTransform;
		ModelJointTransform.Translate(Joints[i].Translation);
		Mat4f Rot;
		Rot.Rotate(Joints[i].Rotation);
		ModelJointTransform = ModelJointTransform * Rot;
		if(Joints[i].Parent > -1)
		{
			ModelJointTransform = Joints[Joints[i].Parent].ModelJointTransform * ModelJointTransform;
		}
		Joints[i].ModelJointTransform = ModelJointTransform;
	}

	Bone B;
	for(int PI = 0; PI < Joints.size(); ++PI)
	{
		//Calculate world positions
		Vec3f P3 = GetJointModelPosition(Joints[PI]);
		Vec4f P4 = ModelMatrix * Vec4f{P3.x, P3.y, P3.z, 1};
		Joints[PI].WorldPos0 = {P4.x, P4.y, P4.z};
		Joints[PI].WorldPos1 = Joints[PI].WorldPos0;

		//Create bones with constraints
		for(int i1 = 0; i1 < Joints[PI].Children.size(); ++i1)
		{
			int CI1 = Joints[PI].Children[i1];
			//From parent to child
			CreateBone(PI, CI1, 0, Bones, Joints);
			//Between children
			for(int i2 = i1 + 1; i2 < Joints[PI].Children.size(); ++i2)
			{
				int CI2 = Joints[PI].Children[i2];
				CreateBone(CI1, CI2, 0, Bones, Joints);
			}
		}
	}
	/*
	[6]{Name = "Head.001"
	[5]{Name = "Head"

	[7]{Name = "Arm.L"
	[8]{Name = "Arm.L.001"
	[9]{Name = "Hand.L"
	[10]{Name = "Hand.L.001"

	[11]{Name = "Arm.R"
	[12]{Name = "Arm.R.001"
	[13]{Name = "Hand.R"
	[14]{Name = "Hand.R.001"

	[4]{Name = "Spine"
	[0]{Name = "Bone"

	[1]{Name = "Leg1.L"
	[2]{Name = "Leg2.L"
	[3]{Name = "Leg2.L.001"

	[15]{Name = "Leg1.R"
	[16]{Name = "Leg2.R"
	[17]{Name = "Leg2.R.001"
	*/

	//Neck
	CreateBone(6, 7, 0, Bones, Joints);
	CreateBone(6, 11, 0, Bones, Joints);

	//Body
	CreateBone(1, 7, 0, Bones, Joints);
	CreateBone(15, 11, 0, Bones, Joints);

	//Elbows
	CreateBone(7, 9, -0.5f, -0.03f, Bones, Joints);
	CreateBone(11, 13, -0.5f, -0.03f, Bones, Joints);

	//Knees
	CreateBone(15, 17, -0.5f, -0.03f, Bones, Joints);
	CreateBone(1, 3, -0.5f, -0.03f, Bones, Joints);

	CreateBone(2, 16, 0.5f, Bones, Joints);
}

void MeshRig::Draw(const Mat4f &ModelMatrix)
{
	DrawDebugPoint(ToVec3(ModelMatrix.GetCol(3)), {0, 0, 1});

	for(int i = 0; i < Joints.size(); ++i)
	{
		Mat4f GlobalJointTransform;
		GlobalJointTransform.Translate(Joints[i].Translation);
		Mat4f Rot;
		Rot.Rotate(Joints[i].Rotation);
		GlobalJointTransform = GlobalJointTransform * Rot;
		if(Joints[i].Parent > -1)
		{
			GlobalJointTransform = Joints[Joints[i].Parent].ModelJointTransform * GlobalJointTransform;
		}
		Joints[i].ModelJointTransform = GlobalJointTransform;

		//Mat4f Mat = GlobalJointTransform*InverseBindMatrices[i];

		Vec4f S = ModelMatrix * GlobalJointTransform * Vec4f{0, 0, 0, 1};
		DrawDebugPoint(ToVec3(S), {1, 0, 0});
	}
	for(const auto &B : Bones)
	{
		Joint &J1 = Joints[B.From];
		Joint &J2 = Joints[B.To];
		DrawDebugPoint(J1.WorldPos1, {0, 1, 0});
		DrawDebugPoint(J2.WorldPos1, {0, 1, 0});
		DrawDebugLine(J1.WorldPos1, J2.WorldPos1, {0, 1, 0});
	}
}

struct CordinateSystem
{
	Vec3f XAxis, YAxis, ZAxis;
};

static void DrawCS(Vec3f P, const CordinateSystem &CS)
{
	DrawDebugLine(P, P + 0.5f * CS.XAxis, {10, 0, 0});
	DrawDebugLine(P, P + 0.5f * CS.YAxis, {0, 10, 0});
	DrawDebugLine(P, P + 0.5f * CS.ZAxis, {0, 0, 10});
}

static void DrawMat(const Mat4f &M)
{
	Vec3f X = ToVec3(M.GetCol(0));
	Vec3f Y = ToVec3(M.GetCol(1));
	Vec3f Z = ToVec3(M.GetCol(2));
	Vec3f P = ToVec3(M.GetCol(3));
	DrawDebugLine(P, P + 0.5f * X, {10, 0, 0});
	DrawDebugLine(P, P + 0.5f * Y, {0, 10, 0});
	DrawDebugLine(P, P + 0.5f * Z, {0, 0, 10});
}

static void DrawMat(Vec3f P, const Mat3f &M)
{
	Vec3f X = M.GetCol(0);
	Vec3f Y = M.GetCol(1);
	Vec3f Z = M.GetCol(2);
	DrawDebugLine(P, P + 0.5f * X, {10, 0, 0});
	DrawDebugLine(P, P + 0.5f * Y, {0, 10, 0});
	DrawDebugLine(P, P + 0.5f * Z, {0, 0, 10});
}

//Returns World cordinate system
static CordinateSystem CalcRotation(const Bone &MainBone, const Vec3f &WorldXIsh, const CordinateSystem &ParentSystem,
									std::vector<Joint> &Joints)
{
	Joint &J1 = Joints[MainBone.From];
	Joint &J2 = Joints[MainBone.To];

	const Vec3f &P1 = J1.WorldPos1;
	const Vec3f &P2 = J2.WorldPos1;

	//World cordinate system
	const Vec3f WorldY = NORMALIZE(P2 - P1);
	const Vec3f WorldZ = NORMALIZE(CROSS(WorldXIsh, WorldY));
	const Vec3f WorldX = NORMALIZE(CROSS(WorldY, WorldZ));
	Mat3f Rot;
	Rot.SetCol(0, WorldX);
	Rot.SetCol(1, WorldY);
	Rot.SetCol(2, {WorldZ.x, WorldZ.y, WorldZ.z});

	//Parents cordinate system transposed
	Mat3f ParentRot_Trapsposed;
	ParentRot_Trapsposed.SetRow(0, ParentSystem.XAxis);
	ParentRot_Trapsposed.SetRow(1, ParentSystem.YAxis);
	ParentRot_Trapsposed.SetRow(2, ParentSystem.ZAxis);

	//Local cordinate system, to change from parent to this
	const Vec4f Q = MatToQuaternion(ParentRot_Trapsposed * Rot);
	J1.Rotation = Q;
	return {WorldX, WorldY, WorldZ};
}

void MeshRig::StepRagdoll(float dt, const GameState &Game, Mat4f &ModelMatrix)
{
	//simulate verlet nodes
	//Gravity and momentum
	for(auto &J : Joints)
	{
		Vec3f Velocity = (J.WorldPos1 - J.WorldPos0);
		//Update old position
		J.WorldPos0 = J.WorldPos1;

		const Vec3f Acc = {0, -9.81f, 0};
		J.WorldPos1 += Velocity + Acc * dt * dt;
	}
	//Lenght constraints
	for(const auto &B : Bones)
	{
		Joint &J1 = Joints[B.From];
		Joint &J2 = Joints[B.To];
		Vec3f V = J2.WorldPos1 - J1.WorldPos1;
		float Distance = LEN(V);

		if(B.MinLenght == B.MaxLenght)
		{
			float Diff = Distance - B.MinLenght;
			if(fabs(Diff) > 0.001f)
			{
				J1.WorldPos1 += 0.5f * Diff * V;
				J2.WorldPos1 -= 0.5f * Diff * V;
			}
		}
		else
		{
			float Diff = Distance - B.MinLenght;
			if(Diff < -0.001f)
			{
				J1.WorldPos1 += 0.5f * Diff * V;
				J2.WorldPos1 -= 0.5f * Diff * V;
			}
			else
			{
				Diff = Distance - B.MaxLenght;
				if(Diff > 0.001f)
				{
					J1.WorldPos1 += 0.5f * Diff * V;
					J2.WorldPos1 -= 0.5f * Diff * V;
				}
			}
		}
	}
	//Collision
	for(auto &J : Joints)
	{
		SphereCollider Col = {Collider::Type::Sphere, {0, 0, 0}, J.WorldPos1, 0.1f};
		float t = 0;
		Vec3f Dir = Game.Map->MapCollisionTest(Col, t);

		J.WorldPos1 += t * Dir;

		//Friction
		if(t > 0)
		{
			J.WorldPos1 -= (J.WorldPos1 - J.WorldPos0) * 0.01f;
			if(LEN2(J.WorldPos1 - J.WorldPos0) < 0.00001f)
			{
				J.WorldPos1 = J.WorldPos0;
			}
		}
	}
#if 0
	Vec3f Offset = {0,0,0};
	if(D_Numpad(8))
	{
		Offset.z -= 3*dt;
	}
	if(D_Numpad(2))
	{
		Offset.z += 3*dt;
	}
	if(D_Numpad(4))
	{
		Offset.x -= 3*dt;
	}
	if(D_Numpad(6))
	{
		Offset.x += 3*dt;
	}
	if(D_Numpad(7))
	{
		Offset.y -= 3*dt;
	}
	if(D_Numpad(9))
	{
		Offset.y += 3*dt;
	}
	Joints[6].WorldPos1 += Offset;

#endif
	Vec3f WorldRootPos = Joints[0].WorldPos1;
	Mat4f InvModel = INVERTaffine(ModelMatrix);
	Vec4f ModelRootPos = InvModel * Vec4f{WorldRootPos.x, WorldRootPos.y, WorldRootPos.z, 1};
	Joints[0].Translation = ToVec3(ModelRootPos);
	/*
	[0]{Name = "Bone"
	[1]{Name = "Leg1.L"
	[2]{Name = "Leg2.L"
	[3]{Name = "Leg2.L.001"
	[4]{Name = "Spine"
	[5]{Name = "Head"
	[6]{Name = "Head.001"
	[7]{Name = "Arm.L"
	[8]{Name = "Arm.L.001"
	[9]{Name = "Hand.L"
	[10]{Name = "Hand.L.001"
	[11]{Name = "Arm.R"
	[12]{Name = "Arm.R.001"
	[13]{Name = "Hand.R"
	[14]{Name = "Hand.R.001"
	[15]{Name = "Leg1.R"
	[16]{Name = "Leg2.R"
	[17]{Name = "Leg2.R.001"
	*/
	//Calculate rotations for Joint matrices to match bone angles
	CordinateSystem CS = {ToVec3(ModelMatrix.GetCol(0)), ToVec3(ModelMatrix.GetCol(1)), ToVec3(ModelMatrix.GetCol(2))};

	Vec3f RightIsh =
		NORMALIZE(Joints[Bones[2].From].WorldPos1 - Joints[Bones[2].To].WorldPos1);	   //Model is facing in Z direction
	auto CSRoot = CalcRotation(Bones[3], RightIsh, CS, Joints);						   //Lower torso

	RightIsh = NORMALIZE(Joints[Bones[12].From].WorldPos1 - Joints[Bones[12].To].WorldPos1);
	auto TorsoCS = CalcRotation(Bones[8], RightIsh, CSRoot, Joints);	//Upper torso

	CalcRotation(Bones[14], TorsoCS.XAxis, TorsoCS, Joints);	//Head

	CS = CalcRotation(Bones[15], -TorsoCS.XAxis, TorsoCS, Joints);	  //Left upper arm
	CalcRotation(Bones[16], CS.XAxis, CS, Joints);					  //Left lower arm

	CS = CalcRotation(Bones[18], -TorsoCS.XAxis, TorsoCS, Joints);	  //Right upper arm
	CalcRotation(Bones[19], CS.XAxis, CS, Joints);					  //Right lower arm

	CS = CalcRotation(Bones[6], CSRoot.XAxis, CSRoot, Joints);	  //Left upper leg
	CalcRotation(Bones[7], CS.XAxis, CS, Joints);				  //Left lower leg

	CS = CalcRotation(Bones[21], CSRoot.XAxis, CSRoot, Joints);	   //Right upper leg
	CalcRotation(Bones[22], CS.XAxis, CS, Joints);				   //Right lower leg
}

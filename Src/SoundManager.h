#pragma once

#include <map>
#include <string>
#include <vector>
#include <xaudio2.h>

#include "vector/Vector.h"

struct SoundData
{
	WAVEFORMATEXTENSIBLE WFX = {0};
	XAUDIO2_BUFFER Buffer = {0};
};

class SoundManager
{
public:
	~SoundManager();
	bool Initialize(int Volume);
	bool LoadSoundFile(const std::string &Name);
	void Play(const std::string &Name, Vec3f SoundPosition = {0, 0, 0}, Vec3f PlayerPosition = {0, 0, 0},
			  Vec3f PlayerFacingDir = {0, 0, -1}, float Volume = 1.0f, float Pitch = 1.0f);
	void SetVolume(int Vol);
	int GetVolume() const { return m_MasterVolume; }

	void Enable() { m_Enabled = true; }
	void Disable() { m_Enabled = false; }

private:
	std::map<std::string, SoundData> m_SoundMap;
	IXAudio2 *m_XAudioPointer = nullptr;
	IXAudio2MasteringVoice *m_MasterVoice = nullptr;

	int m_MasterVolume = 40;
	bool m_Enabled = true;
};

extern SoundManager SoundMan;

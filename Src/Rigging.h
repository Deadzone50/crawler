#pragma once

#include <string>
#include <vector>

#include "vector/Matrix.h"
#include "vector/Vector.h"

struct GameState;

struct TranslationStep
{
	float Time;
	Vec3f Translation;
};
struct RotationStep
{
	float Time;
	Vec4f Rotation;	   //quaternions
};

struct Animation
{
	std::string Name;
	//sequence of steps per joint
	std::vector<std::vector<TranslationStep>> TranslationSteps;
	//sequence of steps per joint
	std::vector<std::vector<RotationStep>> RotationSteps;
};

struct Joint
{
	std::string Name;
	Vec3f Translation = {0, 0, 0};
	Vec4f Rotation = {0, 0, 0, 0};	  //Quaternion
	//Vec3f Scale = {1,1,1};
	std::vector<int> Children;
	int Parent = -1;
	Mat4f ModelJointTransform;

	Vec3f WorldPos0 = {0, 0, 0};
	Vec3f WorldPos1 = {0, 0, 0};
};

struct Bone
{
	int From = -1;
	int To = -1;
	float MinLenght = 0;
	float MaxLenght = 0;
};

struct MeshRig
{
	int RootJoint = 0;
	std::vector<Joint> Joints;
	std::vector<Mat4f> InverseBindMatrices;
	std::vector<Animation> Animations;

	std::vector<Bone> Bones;

	void CreateRagdoll(const Mat4f &ModelMatrix);
	void Draw(const Mat4f &ModelMatrix);
	void StepRagdoll(float dt, const GameState &Game, Mat4f &ModelMatrix);
};

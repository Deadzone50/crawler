#pragma once
#include "Character.h"

class EnemyClass : public Character
{
public:
	void Move(float dt);
	void Step(float dt, GameState &Game);

	void Attack(Vec3f TargetPos);

private:
	void Wander(float dt, GameState &Game, bool LineOfSight, float LenToPlayer, Vec3f ClosestPlayer);

	Vec3f m_Goal = {0, 0, 0};
	std::vector<Vec3f> m_Waypoints;
	bool m_Wandering = false;
	bool m_Aware = false;
	float m_WanderTime = 0;
	float m_WanderTimer = 0;
	float m_WanderDistanceToPlayerAtCollision = 0;
};

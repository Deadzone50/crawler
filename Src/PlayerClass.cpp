#include "PlayerClass.h"

#include <iostream>

#include "Debug.h"
#include "Event.h"
#include "Globals.h"
#include "Item.h"
#include "Level.h"
#include "SoundManager.h"
#include "States.h"

extern Vec2i G_ScreenSize;

PlayerClass::PlayerClass()
{
	Reset();
}

void PlayerClass::Reset()
{
	SetRotation({0, 0});
	m_Pos = {0, 0, 0};
	m_CurrentSpeed = {0, 0, 0};

	m_Health = 100;
	m_Falling = true;
	m_HudHealth.SetColor(Vec3f{0.7f, 0, 0});
	m_HudAmmo.SetColor(Vec3f{0.7f, 0.7f, 0.7f});
}

void PlayerClass::SetRotation(Vec2f Angles)
{
	m_Rotation = Angles;

	//b = y rot, y = x rot
	//cos(b)		sin(b)*sin(y)	sin(b)*cos(y)	[0]
	//0				cos(y)			-sin(y)			[0]
	//-sin(b)		cos(b)*sin(y)	cos(b)*cos(y)	[-1]
	m_LookingDirection.x = -sin(m_Rotation.y) * cos(m_Rotation.x);
	m_LookingDirection.y = sin(m_Rotation.x);
	m_LookingDirection.z = -cos(m_Rotation.x) * cos(m_Rotation.y);
	m_LookingDirection = NORMALIZE(m_LookingDirection);

	m_Right = NORMALIZE(CROSS(m_LookingDirection, {0, 1, 0}));
	m_Up = NORMALIZE(CROSS(m_Right, m_LookingDirection));

	m_LookingDirection2D = NORMALIZE(Vec2f{m_LookingDirection.x, m_LookingDirection.z});
}

void PlayerClass::Rotate(Vec2f Angles)
{
	m_Rotation += Angles;

	if(m_Rotation.x > 1.55f)
		m_Rotation.x = 1.55f;
	if(m_Rotation.x < -1.55f)
		m_Rotation.x = -1.55f;

	SetRotation(m_Rotation);
}

Mat4f PlayerClass::GetViewMatrix()
{
	Vec3f Pos = m_Pos + Vec3f{0, EYEHEIGHT, 0};
	return LookAt(Pos, Pos + m_LookingDirection, m_Up);
}

void PlayerClass::SetMovement(const Vec3f &Amount)
{
	if(m_Debug)
	{
		m_CurrentSpeed = Amount.x * m_Right + Amount.y * m_Up - Amount.z * m_LookingDirection;
	}
	else
	{
		Vec3f PlaneForward = NORMALIZE(Vec3f{m_LookingDirection.x, 0, m_LookingDirection.z});
		float currenty = m_CurrentSpeed.y;
		m_CurrentSpeed = Amount.x * m_Right - Amount.z * PlaneForward;
		m_CurrentSpeed.y = currenty;
	}
}

void PlayerClass::Jump()
{
	if(!m_Falling)
	{
		m_CurrentSpeed.y = 10;
		m_Falling = true;
	}
}

void PlayerClass::Interact(GameState &Game)
{
	Game.Map->Use(this);
}

void PlayerClass::Attack()
{
	if(!(m_State & CharacterState::Attacking) && (m_Inventory.GetAmmo() || m_Inventory.GetCurrentWeapon().DoesNotUseAmmo()))
	{
		SetState(m_State | CharacterState::Attacking);
		m_Inventory.GetCurrentWeapon().StartAttack();
		if(!m_Inventory.GetCurrentWeapon().DoesNotUseAmmo())
			m_Inventory.RemoveAmmo();
		UpdateUI();
	}
}

void PlayerClass::SwitchWeapon(uint8_t Number)
{
	if(!(m_State & CharacterState::Attacking) && m_Inventory.SwitchWeapon(Number))
	{
		UpdateUI();
	}
}

void PlayerClass::Step(float dt, GameState &Game)
{
	//Character::Animate(dt);
	if(m_State == CharacterState::Dead)
		return;

	Character::StepWeapon(dt);

	if(!m_Debug)
	{
		m_CurrentSpeed.y -= dt * 9.81f * 5;
		m_Pos += dt * m_CurrentSpeed;

		HandleCollision(*Game.Map);

		if(m_CurrentSpeed.y < -0.1f)
			m_Falling = true;

		Vec3f EnemyCollisionNormal = {};
		auto &EV = Game.Map->GetEnemies();
		size_t NumEnemies = EV.size();
		SphereCollider C1 = GetCollider();
		for(uint32_t i1 = 0; i1 < NumEnemies; ++i1)
		{
			if(!EV[i1].IsAlive())
				continue;
			SphereCollider C2 = EV[i1].GetCollider();
			if(C1.Position.y > C2.Position.y + 1.0f || C1.Position.y < C2.Position.y - 1.0f)
				continue;
			float MinDist = C1.Radius + C2.Radius;
			Vec3f Dir = C1.Position - C2.Position;
			Dir.y = 0;
			float Dist = LEN(Dir);
			float Diff = Dist - MinDist;
			if(Diff < 0)
			{
				Vec3f Tmp = (Dir / Dist) * Diff;
				m_Pos -= Tmp;
			}
		}

		HandleCollision(*Game.Map);	   //Check extra time to net get showed through walls

		float Drag = (1 - dt * 20);
		if(Drag < 0)
			Drag = 0;
		m_CurrentSpeed.x *= Drag;
		m_CurrentSpeed.z *= Drag;
		if(abs(m_CurrentSpeed.x) < 0.1f)
			m_CurrentSpeed.x = 0;
		if(abs(m_CurrentSpeed.z) < 0.1f)
			m_CurrentSpeed.z = 0;
	}
	else
	{
		m_Pos += dt * m_CurrentSpeed;
		float Drag = (1 - dt * 20);
		if(Drag < 0)
			Drag = 0;
		m_CurrentSpeed.x *= Drag;
		m_CurrentSpeed.y *= Drag;
		m_CurrentSpeed.z *= Drag;
		if(LEN2(m_CurrentSpeed) < 0.1f)
			m_CurrentSpeed = {0, 0, 0};
	}
	auto it = Game.Map->GetItems().begin();
	while(it != Game.Map->GetItems().end())
	{
		if(LEN(m_Pos - it->GetPos()) < 2)
		{
			bool Pickup = false;
			if(it->GetItemType() == ItemType::Ammo_Shotgun)
			{
				SoundMan.Play("Pickup-Ammo-01.wav", m_Pos, m_Pos, m_LookingDirection);
				m_Inventory.AddAmmo(AmmoType::Shotgun, 5);
				Pickup = true;
			}
			if(it->GetItemType() == ItemType::Health)
			{
				if(m_Health < 100)
				{
					SoundMan.Play("Pickup-Health-01.wav", m_Pos, m_Pos, m_LookingDirection);
					m_Health += 20;
					Pickup = true;
				}
			}
			if(Pickup)
			{
				std::shared_ptr<DeSpawnEvent> E = std::make_shared<DeSpawnEvent>();
				E->ItemIndex = it->GetId();
				AddEvent(E, Game);
				UpdateUI();
				it = Game.Map->GetItems().erase(it);
			}
			else
			{
				++it;
			}
		}
		else
		{
			++it;
		}
	}

	if(m_DamageTaken > 0)
	{
		m_Health -= m_DamageTaken;
		m_DamageTaken = 0;
#if 0
		if(m_Health <= 0)
		{
			m_Health = 0;
			SetState(CharacterState::Dead);
		}
#endif
		UpdateUI();
	}
}

void PlayerClass::UpdateUI()
{
	m_HudHealth.SetText(std::to_string((int)m_Health));
	m_HudAmmo.SetText(std::to_string(m_Inventory.GetAmmo()));
}

void PlayerClass::RenderUI(Shader &Shader)
{
	Mat4f ModelMatrix;
	ModelMatrix.SetCol(3, {30, 30, 0, 1});
	Shader.SetMat4("Model", ModelMatrix);
	m_HudHealth.Draw(Shader);

	ModelMatrix.SetCol(3, {(float)G_ScreenSize.x - 100, 30, 0, 1});
	Shader.SetMat4("Model", ModelMatrix);
	m_HudAmmo.Draw(Shader);

	if(m_ShowInventory)
	{
		const Vec2i BL = {G_ScreenSize.x / 2, 20};
		const Vec2i TR = {G_ScreenSize.x - 20, G_ScreenSize.y - 20};
		m_Inventory.Render(Shader);
	}
}

void PlayerClass::DrawMinimap() const
{
	if(m_ShowMinimap)
		m_Minimap.Draw(GetCameraPos(), GetLookingDir());
}

void PlayerClass::DrawWeapon(Shader &Shader)
{
	Mat4f ModelMatrix;
	ModelMatrix.SetPosition(Vec3f{0, -0.2f, 0});
	ModelMatrix.Rotate(Vec3f{-m_Rotation.x, m_Rotation.y + PI, 0});

	ModelMatrix.Move(m_Pos + Vec3f{0, EYEHEIGHT, 0} - 0.7f * m_LookingDirection);
	m_Inventory.GetCurrentWeapon().DrawAnimated(Shader, ModelMatrix);
}

void PlayerClass::SetModifiers(const std::map<SkillTag, int> &Modifiers)
{
	for(const auto &M : Modifiers)
	{
		switch(M.first)
		{
			case SkillTag::Health: m_Health += M.second; break;
			case SkillTag::Strenght: m_Health += M.second; break;
		}
	}
	m_Inventory.SetModifiers(Modifiers);
}

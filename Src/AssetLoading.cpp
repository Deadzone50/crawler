#include "AssetLoading.h"

#include <vector>

#include "Console.h"
#include "Rigging.h"
#include "Sprite.h"
#include "TextureManager.h"
#include "glTF.h"

void LoadMeshes(MeshManager &MeshMan)
{
	std::vector<std::string> MeshNames;
	std::vector<Mesh> Meshes;
	std::vector<MeshRig> MeshRigs;
	ParseGlTFFile("Assets/Models/TestAnim.gltf", MeshNames, Meshes, MeshRigs);

	ParseGlTFFile("Assets/Models/DungeonRoomsWFC.gltf", MeshNames, Meshes, MeshRigs);
	ParseGlTFFile("Assets/Models/Player.gltf", MeshNames, Meshes, MeshRigs);
	ParseGlTFFile("Assets/Models/Cultist.gltf", MeshNames, Meshes, MeshRigs);
	ParseGlTFFile("Assets/Models/Items.gltf", MeshNames, Meshes, MeshRigs);
	ParseGlTFFile("Assets/Models/DungeonObjects.gltf", MeshNames, Meshes, MeshRigs);

	for(int i = 0; i < MeshNames.size(); ++i)
	{
		MeshMan.AddMesh(Meshes[i], MeshNames[i]);
		if(!MeshRigs[i].Joints.empty()) MeshMan.AddRig(MeshRigs[i], MeshNames[i]);
	}
	MeshMan.GetMesh("Machete")->SetTexture(TextureMan.Texture(40, 40, 40));
	MeshMan.GetMesh("Revolver")->SetTexture(TextureMan.Texture(40, 40, 40));
	MeshMan.GetMesh("Shotgun")->SetTexture(TextureMan.Texture(40, 40, 40));
	MeshMan.GetMesh("Gatling-gun")->SetTexture(TextureMan.Texture(40, 40, 40));

	MeshMan.GetMesh("Pitchfork")->SetTexture(TextureMan.Texture(210, 105, 30));
	MeshMan.GetMesh("AmmoBox")->SetTexture(TextureMan.Texture(30, 200, 30));
	MeshMan.GetMesh("Flask")->SetTexture(TextureMan.Texture(200, 30, 30));
	MeshMan.GetMesh("Cultist")->SetTexture(TextureMan.Texture(63, 49, 40));
}

void LoadTextures(MeshManager &MeshMan)
{
	TextureMan.Texture(255, 0, 255);	//load error texture first so it will be 1

	MeshMan.AddMesh(Sprite("Assets/Sprites/Error.png", {1, 1}), "ErrorIcon");
	MeshMan.AddMesh(Sprite("Assets/Sprites/Square.png", {1, 1}), "InventorySquare");

	MeshMan.AddMesh(Sprite("Assets/Sprites/44_Icon.png", {1, 1}), "44Icon");
	MeshMan.AddMesh(Sprite("Assets/Sprites/Shell_Icon.png", {1, 1}), "ShellIcon");

	MeshMan.AddMesh(Sprite("Assets/Sprites/Machete_Icon.png", {1, 2}), "MacheteIcon");
	MeshMan.AddMesh(Sprite("Assets/Sprites/Pitchfork_Icon.png", {2, 5}), "PitchforkIcon");
	MeshMan.AddMesh(Sprite("Assets/Sprites/Polearm_Icon.png", {2, 5}), "PolearmIcon");

	MeshMan.AddMesh(Sprite("Assets/Sprites/Revolver_Icon.png", {2, 2}), "RevolverIcon");
	MeshMan.AddMesh(Sprite("Assets/Sprites/Shotgun_Icon.png", {2, 4}), "ShotgunIcon");
	MeshMan.AddMesh(Sprite("Assets/Sprites/Gatling-gun_Icon.png", {2, 4}), "GatlingIcon");

	TextureMan.Texture("Assets/Font9x9.png");
}

#include "Inventory.h"

#include "Drawer.h"
#include "MeshManager.h"

extern Vec2i G_ScreenSize;

static bool CheckBoxAreaFree(const StorageBox &SB, Vec2i Pos, Vec2i Size)
{
	const Vec2i Offset = Pos - SB.Pos;
	if(Offset.x < 0 || Offset.y < 0 || Offset.x + Size.x > SB.Size.x || Offset.y + Size.y > SB.Size.y) return false;
	for(int y = Offset.y; y < Offset.y + Size.y; ++y)
	{
		for(int x = Offset.x; x < Offset.x + Size.x; ++x)
		{
			if(SB.Occupied[y * SB.Size.x + x]) return false;
		}
	}
	return true;
}

static void SetBoxArea(StorageBox &SB, Vec2i Pos, Vec2i Size, bool Occupied)
{
	const Vec2i Offset = Pos - SB.Pos;
	for(int y = Offset.y; y < Offset.y + Size.y; ++y)
	{
		for(int x = Offset.x; x < Offset.x + Size.x; ++x)
		{
			SB.Occupied[y * SB.Size.x + x] = Occupied;
		}
	}
}

static bool PlaceItem(StorageBox &SB, Vec2i Pos, Vec2i Size)
{
	//Check for room
	if(!CheckBoxAreaFree(SB, Pos, Size)) return false;
	//Place item
	SetBoxArea(SB, Pos, Size, true);
	return true;
}

InventoryItem::InventoryItem(uint8_t Index, ItemType Type)
{
	m_InventoryBoxIndex = 0;
	m_InventoryPos = {0, 0};
	m_Index = Index;

	switch(Type)
	{
		case ItemType::Ammo_44:
			m_InventorySize = {1, 1};
			m_InventoryIcon = "44Icon";
			m_InventoryType = Type::Ammo;
			break;
		case ItemType::Ammo_Shotgun:
			m_InventorySize = {1, 1};
			m_InventoryIcon = "ShellIcon";
			m_InventoryType = Type::Ammo;
			break;

		case ItemType::Machete:
			m_InventorySize = {1, 2};
			m_InventoryIcon = "MacheteIcon";
			m_InventoryType = Type::Weapon;
			break;
		case ItemType::Pitchfork:
			m_InventorySize = {2, 5};
			m_InventoryIcon = "PitchforkIcon";
			m_InventoryType = Type::Weapon;
			break;
		case ItemType::Polearm:
			m_InventorySize = {2, 5};
			m_InventoryIcon = "PolearmIcon";
			m_InventoryType = Type::Weapon;
			break;

		case ItemType::Revolver:
			m_InventorySize = {2, 2};
			m_InventoryIcon = "RevolverIcon";
			m_InventoryType = Type::Weapon;
			break;
		case ItemType::Shotgun:
			m_InventorySize = {2, 4};
			m_InventoryIcon = "ShotgunIcon";
			m_InventoryType = Type::Weapon;
			break;
		case ItemType::Machinegun:
			m_InventorySize = {3, 5};
			m_InventoryIcon = "GatlingIcon";
			m_InventoryType = Type::Weapon;
			break;

		default: m_InventorySize = {1, 1}; m_InventoryIcon = "ErrorIcon";
	}
}

Inventory::Inventory()
{
	auto CreateBox = [&](Vec2i Pos, Vec2i Size) {
		m_Storage.push_back({Pos, Size});
		m_Storage.back().Occupied = std::vector<bool>(Size.y * Size.x, false);
	};

	CreateBox({0, 1}, {20, 8});	   //Storage

	CreateBox({5, 10}, {2, 3});	   //Legpouch
	CreateBox({14, 10}, {2, 3});

	CreateBox({8, 13}, {4, 1});	   //Belt

	CreateBox({5, 15}, {1, 3});		//Armpouch
	CreateBox({14, 15}, {1, 3});	//Armpouch

	CreateBox({8, 15}, {3, 5});	   //Back
}

void Inventory::AddWeapon(const Weapon &Weapon, uint8_t InventoryBoxIndex)
{
	InventoryItem Item(m_Weapons.size(), Weapon.GetItemType());
	if(InventoryBoxIndex == 0)
	{
		Vec2i FreePos = FindFreePos(Item.GetInventorySize());
		if(FreePos == Vec2i{0, 0})	  //no free space found
			return;
		m_Items.push_back(std::move(Item));
		auto &I = m_Items.back();
		I.SetInventoryPos(FreePos);
		SetBoxArea(m_Storage[0], FreePos, I.GetInventorySize(), true);
	}
	else
	{	 //used for starting equip, one item per box
		m_Items.push_back(std::move(Item));
		auto &I = m_Items.back();
		I.SetInventoryBoxIndex(InventoryBoxIndex);
		I.SetInventoryPos(m_Storage[InventoryBoxIndex].Pos);
		//SetBoxArea(m_Storage[0], m_Storage[InventoryBoxIndex].Pos, I.GetInventorySize(), true);
	}
	m_Weapons.push_back(Weapon);
}

void Inventory::EquipWeapon(uint8_t ItemIndex, uint8_t EquippedIndex)
{
	if(EquippedIndex < EQUIPMENT_SLOTS && m_Items[ItemIndex].GetInventoryBoxIndex() != 0)
	{
		m_EquippedWeapons[EquippedIndex] = m_Items[ItemIndex].GetIndex();
	}
}

void Inventory::RemoveItem(uint8_t Index)
{
	const auto &Item = m_Items[Index];

	if(Item.GetItemType() == InventoryItem::Type::Weapon)
	{
		const uint8_t WeaponIndex = Item.GetIndex();

		m_Weapons.erase(m_Weapons.begin() + WeaponIndex);
		for(int i = 0; i < EQUIPMENT_SLOTS; ++i)
		{
			if(m_EquippedWeapons[i] > WeaponIndex)
			{
				m_EquippedWeapons[i] -= 1;
			}
			else if(m_EquippedWeapons[i] == WeaponIndex)
			{
				m_EquippedWeapons[i] = INV_EMPTY;
			}
		}
	}
	m_Items.erase(m_Items.begin() + Index);
}

void Inventory::AddAmmo(AmmoType Type, uint32_t Amount)
{
	//check if already exist in inventory
	const uint8_t Index = static_cast<uint8_t>(Type);
	bool Exists = std::find_if(m_Items.begin(), m_Items.end(), [Index](const InventoryItem &I1) {
					  return I1.GetIndex() == Index && I1.GetItemType() == InventoryItem::Type::Ammo;
				  }) != m_Items.end();
	if(!Exists)
	{
		ItemType ItemType = ItemType::Ammo_44;
		switch(Type)
		{
			case AmmoType::Cartridge44: ItemType = ItemType::Ammo_44; break;
			case AmmoType::Shotgun: ItemType = ItemType::Ammo_Shotgun; break;
			default: DEBUG_ABORT("UNHANDLED AMMO TYPE");
		}
		InventoryItem Item(Index, ItemType);
		Vec2i FreePos = FindFreePos(Item.GetInventorySize());
		if(FreePos == Vec2i{0, 0})	  //no free space found
			return;
		m_Items.push_back(std::move(Item));
		auto &I = m_Items.back();
		I.SetInventoryPos(FreePos);
		SetBoxArea(m_Storage[0], FreePos, I.GetInventorySize(), true);
	}
	m_Ammo[Type] += Amount;
}

void Inventory::RemoveAmmo()
{
	const AmmoType Type = GetCurrentWeapon().GetAmmoType();
	m_Ammo[Type] -= 1;
	if(m_Ammo[Type] == 0)
	{
		const uint8_t AmmoIndex = static_cast<uint8_t>(Type);
		uint8_t ItemIndex = std::find_if(m_Items.begin(), m_Items.end(),
										 [AmmoIndex](const InventoryItem &I1) {
											 return I1.GetIndex() == AmmoIndex && I1.GetItemType() == InventoryItem::Type::Ammo;
										 }) -
							m_Items.begin();

		RemoveItem(ItemIndex);
	}
}

bool Inventory::SwitchWeapon(uint8_t EquippedIndex)
{
	if(EquippedIndex < EQUIPMENT_SLOTS && m_EquippedWeapons[EquippedIndex] != INV_EMPTY)
	{
		m_CurrentWeaponIndex = EquippedIndex;
		return true;
	}
	else return false;
}

void Inventory::SetModifiers(std::map<SkillTag, int> Modifiers)
{
	for(auto &W : m_Weapons)
	{
		if(W.GetItemType() == ItemType::Shotgun)
		{
			W.ChangeDamage(1 + Modifiers[SkillTag::ShotgunDamage] * 0.01f);
			W.ChangeShots(1 + Modifiers[SkillTag::ShotgunShots] * 0.01f);
			W.ChangeSpread(1 + Modifiers[SkillTag::ShotgunSpread] * 0.01f);
		}
	}
	m_Modifiers = Modifiers;
}

static void DrawInventoryIcon(const std::string &Name, Vec2i Size, Mat4f &ModelMatrix, Shader &Shader, float Scale)
{
	const Mesh *M = MeshMan.GetMesh(Name);
	const Vec3f Offset = Vec3f{(Size.x - 1) / 2.0f, (Size.y - 1) / 2.0f} * Scale;
	ModelMatrix.Move(Offset);
	Shader.SetMat4("Model", ModelMatrix);
	ModelMatrix.Move(-Offset);
	M->Draw(Shader);
}

static void DrawStorageBox(const StorageBox &Box, Mat4f &ModelMatrix, Shader &Shader, float Scale)
{
	const Vec3f Pos = Vec3f{(float)Box.Pos.x, (float)Box.Pos.y, 0};
	ModelMatrix.Move(Pos * Scale);
	for(int y = 0; y < Box.Size.y; ++y)
	{
		for(int x = 0; x < Box.Size.x; ++x)
		{
			Shader.SetMat4("Model", ModelMatrix);
			MeshMan.GetMesh("InventorySquare")->Draw(Shader);
			ModelMatrix.Move(Vec3f{1.0f, 0, 0} * Scale);
		}
		ModelMatrix.Move(Vec3f{-(float)Box.Size.x, 1.0f, 0} * Scale);
	}
}

static void DrawItem(const InventoryItem &Item, Mat4f &ModelMatrix, Shader &Shader, float Scale)
{
	Vec2i IPos = Item.GetInventoryPos();
	const Vec3f Pos = Vec3f{(float)IPos.x, (float)IPos.y, 0};
	ModelMatrix.Move(Pos * Scale);
	DrawInventoryIcon(Item.GetInventoryIcon(), Item.GetInventorySize(), ModelMatrix, Shader, Scale);
}

void Inventory::GetScaleAndStart(float &Scale, Vec2i &Start) const
{
	Scale = G_ScreenSize.x / (2 * 21);
	Start = {G_ScreenSize.x / 2, 0};
}

void Inventory::Render(Shader &Shader) const
{
	float Scale;
	Vec2i Start;
	GetScaleAndStart(Scale, Start);

	Mat4f ModelMatrix;
	ModelMatrix.Scale({Scale, Scale, Scale});

	for(const auto &SB : m_Storage)
	{
		ModelMatrix.SetCol(3, {(float)Start.x + 0.5f * Scale, (float)Start.y + 0.5f * Scale, 0, 1});
		DrawStorageBox(SB, ModelMatrix, Shader, Scale);
	}
	//TODO: draw numbers for equipped slot
	for(const auto &I : m_Items)
	{
		ModelMatrix.SetCol(3, {(float)Start.x + 0.5f * Scale, (float)Start.y + 0.5f * Scale, 0.1f, 1});
		DrawItem(I, ModelMatrix, Shader, Scale);
	}
}

bool Inventory::Click(Vec2i MousePos, uint8_t &ItemIndex)
{
	static Vec2i PrevPos, PrevSize;
	if(ItemIndex == INV_EMPTY)	  //Select item
	{
		uint8_t i = GetItemUnderMouse(MousePos);
		if(i != INV_EMPTY)
		{
			const auto &I = m_Items[i];
			ItemIndex = i;
			PrevPos = I.GetInventoryPos();
			PrevSize = I.GetInventorySize();
			return true;
		}
	}
	else	//Place item
	{
		auto &Item = m_Items[ItemIndex];

		float Scale;
		Vec2i Start;
		GetScaleAndStart(Scale, Start);
		const Vec2i MouseSquarePos = (MousePos - Start) / Scale;

		const Vec2i ItemSize = Item.GetInventorySize();
		for(int i = 0; i < m_Storage.size(); ++i)
		{
			auto &SB = m_Storage[i];
			if(PlaceItem(SB, MouseSquarePos, ItemSize))
			{
				//remove from previous box
				SetBoxArea(m_Storage[Item.GetInventoryBoxIndex()], PrevPos, PrevSize, false);
				Item.SetInventoryBoxIndex(i);
				Item.SetInventoryPos(MouseSquarePos);
				ItemIndex = INV_EMPTY;
				return true;
			}
		}
	}
	return false;
}

uint8_t Inventory::GetItemUnderMouse(Vec2i MousePos) const
{
	float Scale;
	Vec2i Start;
	GetScaleAndStart(Scale, Start);
	const Vec2i MouseSquarePos = (MousePos - Start) / Scale;

	for(uint8_t i = 0; i < m_Items.size(); ++i)
	{
		const auto &I = m_Items[i];
		const Vec2i ItemPos = I.GetInventoryPos();
		const Vec2i ItemSize = I.GetInventorySize();

		if((ItemPos.x <= MouseSquarePos.x && MouseSquarePos.x < ItemPos.x + ItemSize.x) &&
		   (ItemPos.y <= MouseSquarePos.y && MouseSquarePos.y < ItemPos.y + ItemSize.y))
		{
			return i;
		}
	}
	return INV_EMPTY;
}

Vec2i Inventory::FindFreePos(Vec2i Size) const
{
	const auto &SB = m_Storage[0];
	for(int y = 0; y < SB.Size.y; ++y)
	{
		for(int x = 0; x < SB.Size.x; ++x)
		{
			const Vec2i Pos = Vec2i{x, y} + SB.Pos;
			if(CheckBoxAreaFree(SB, Pos, Size)) return Pos;
		}
	}
	return {0, 0};
}

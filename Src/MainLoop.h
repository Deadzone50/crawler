#pragma once

struct RenderState;
struct GameState;
struct NetworkState;
struct IOStates;

class Menu;

void ResetTimers();

void RenderMenu(RenderState &RState, Menu *CurrentMenu);

void Render(RenderState &RState, GameState &Game, float dt);

void MainLoop(GameState &Game, RenderState &RState, NetworkState &NState, IOStates &IO);

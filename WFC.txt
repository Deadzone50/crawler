{
	Mesh Empty
	N0 0
	N1 0
	N2 0
	N3 0
	NUp 0_s
	NDown 0_s
}
{
	Mesh C_Start
	N0 Doorway
	N1 0
	N2 0
	N3 0
	NUp 0_s
	NDown 0_s
}
{
	Mesh C_+
	N0 Doorway
	N1 Doorway
	N2 Doorway
	N3 Doorway
	NUp 0_s
	NDown 0_s
}
{
	Mesh C_Bend
	N0 Doorway
	N1 Doorway
	N2 0
	N3 0
	NUp 0_s
	NDown 0_s
}
{
	Mesh C_Corridor
	N0 Doorway
	N1 0
	N2 Doorway
	N3 0
	NUp 0_s
	NDown 0_s
}
{
	Mesh C_T
	N0 Doorway
	N1 Doorway
	N2 0
	N3 Doorway
	NUp 0_s
	NDown 0_s
}
{
	Mesh C_RampB1
	N0 Doorway
	N1 0
	N2 RampBottom
	N3 0
	NUp Ramp1_r0
	NDown 0_s
}
{
	Mesh C_RampB2
	N0 RampBottom
	N1 0
	N2 0
	N3 0
	NUp Ramp2_r0
	NDown 0_s
}
{
	Mesh C_RampT1
	N0 0
	N1 0
	N2 Doorway
	N3 0
	NUp 0_s
	NDown Ramp1_r0
}
{
	Mesh C_RampT2
	N0 Doorway
	N1 0
	N2 Doorway
	N3 0
	NUp 0_s
	NDown Ramp2_r0
}

{
	Mesh W_Bend
	N0 Walkway
	N1 Walkway
	N2 0
	N3 0
	NUp 0_s
	NDown 0_s
}
{
	Mesh W_Corridor
	N0 Walkway
	N1 0
	N2 Walkway
	N3 0
	NUp 0_s
	NDown 0_s
}
{
	Mesh W_Railway
	N0 Floor
	N1 Rail_A
	N2 Walkway
	N3 Rail_B
	NUp 0_s
	NDown 0_s
}
{
	Mesh W_Rail
	N0 Floor
	N1 Rail_A
	N2 0
	N3 Rail_B
	NUp 0_s
	NDown 0_s
}
{
	Mesh W_Railend
	N0 Rail_B
	N1 Rail_A
	N2 0
	N3 0
	NUp 0_s
	NDown 0_s
}
{
	Mesh W_Floor
	N0 Floor
	N1 Floor
	N2 Floor
	N3 Floor
	NUp 0_s
	NDown 0_s
}
{
	Mesh W_Doorway
	N0 Floor
	N1 Wall_A
	N2 Doorway
	N3 Wall_B
	NUp Wall_r0
	NDown 0_s
}
{
	Mesh W_FloorWall
	N0 Floor
	N1 Wall_A
	N2 0
	N3 Wall_B
	NUp Wall_r0
	NDown 0_s
}
{
	Mesh W_FloorWallCorner
	N0 Wall_B
	N1 Wall_A
	N2 0
	N3 0
	NUp Corner_r0
	NDown 0_s
}
{
	Mesh W_Wall
	N0 0
	N1 WallAir_A
	N2 0
	N3 WallAir_B
	NUp Wall_r0
	NDown Wall_r0
}
{
	Mesh W_WallCorner
	N0 WallAir_B
	N1 WallAir_A
	N2 0
	N3 0
	NUp Corner_r0
	NDown Corner_r0
}
{
	Name RoofCenter
	Mesh W_Floor
	N0 Roof
	N1 Roof
	N2 Roof
	N3 Roof
	NUp 0_s
	NDown 0_s
}
{
	Name RoofCorner
	Mesh W_Floor
	N0 Roof
	N1 Roof
	N2 0
	N3 0
	NUp 0_s
	NDown Corner_r0
}
{
	Name RoofSide
	Mesh W_Floor
	N0 Roof
	N1 Roof
	N2 0
	N3 Roof
	NUp 0_s
	NDown Wall_r0
}
{
	Mesh W_FloorWallend
	N0 Rail_B
	N1 Wall_A
	N2 0
	N3 0
	NUp Wallend_r0
	NDown 0_s
}
{
	Mesh W_FloorWallendM
	N0 Rail_A
	N1 0
	N2 0
	N3 Wall_B
	NUp WallendM_r0
	NDown 0_s
}
{
	Mesh W_FloorWallRail
	N0 Floor
	N1 Wall_A
	N2 0
	N3 Rail_B
	NUp Wallend_r0
	NDown 0_s
}
{
	Mesh W_FloorWallRailM
	N0 Floor
	N1 Rail_A
	N2 0
	N3 Wall_B
	NUp WallendM_r0
	NDown 0_s
}
{
	Mesh W_Wallend
	N0 0
	N1 WallAir_A
	N2 0
	N3 0
	NUp Wallend_r0
	NDown Wallend_r0
}
{
	Name W_WallendM
	Mesh W_Wallend
	N0 0
	N1 0
	N2 0
	N3 WallAir_B
	NUp WallendM_r0
	NDown WallendM_r0
}
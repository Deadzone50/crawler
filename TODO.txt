	Fix minimap
	Add rigidbody ragdolls
	Objects can fall through world at seams

	Imporve navmesh for large maps:
		- Calculate choke-points where beyond nothing changes?
		- Split into chunks and update only chunk with player?
		- Process only part of map each frame?


WEAPONS
	Knife, Sword, Axe, scythe									#blade
	Club, Hammer,												#blunt
	Spear, Pike, Rapier, Pick									#pierce

	javelin, throwing-star, throwing-knife, throwing-axe		#thrown?

	Glock, Revolver, Desert Eagle, MAC 11						#Pistol
	Mossberg 590, double barrel, SPAS-12						#Shotgun
	Winchester, K98k, Mosin-Nagant, SVT-40, Walther WA 2000		#Rifle
	AK-47, Mg42, BAR, Bren, Gatling gun 						#Machinegun
	Uzi, Suomi M31, MP40, MP5, P90								#Sub-Machinegun
	RPG-7, Milkor MGL, grenade, impact bomb, dynamite			#Explosives


	shields? chainsaw? cannon?

	How to handle player animations?

	Scale animation speed to fit attack

	Goblin/Kobold/Gremlin (~3ft): spear, knife/sword, club, Crossbow, bow

ITEMS
	Helm, Armor, Pants, Boots, Gloves, Amulet, Rings(2?)


LEVEL
	Generate path first, and then level around it?

	Subdivide outdoor floor and add noise offset?

	Ladders, Loot containers, Light objects

PHYSICS
	World multi levelitem collision

	Check speedup with different collision hierachies

	Improve edge-edge collision checks

	Add explosions

	convert ragdoll to impulse based?

	Spheres don't stop spinning
	Sphere - Mesh collsion not working
	Boxes get pushed throught ground

	Add weapon drop on death

RENDERING
	Add proper screenspace rendering for menus, maps, text etc

	Scale inventory to resolution

ENEMIES
	Priest/Mage - Fancier robes and tome/staff/wand?

	Goblin

	Lovecraft:
		Byakhee -  flying, lizard 10ft
		Night-Gaunt  -  flying, devilz 7ft
		Shantak - flying, Larger than elephants
		Mi-go - flying, insect, 5 ft

		Ghast
		Venusian - lizardmen, 7ft
		Hound of Tindalos - travels through time and space
		Dimensional Shambler - 7ft
		Man of Leng - satyr

#version 330 core
layout (location = 0) in vec3 VPos;
layout (location = 1) in vec3 VCol;
layout (location = 2) in vec3 VNorm;
layout (location = 3) in vec2 VTexCoord;
layout (location = 4) in vec4 Joints;
layout (location = 5) in vec4 Weights;

out vec3 FColor;
out vec3 FNormal;
out vec3 FWorldPos;
out vec2 FTexCoord;

uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;

uniform mat4 JointMatrices[18];
uniform bool Skinned;

void main()
{
	if(Skinned)
	{
		mat4 SkinMatrix =	Weights.x * JointMatrices[int(Joints.x)] + Weights.y * JointMatrices[int(Joints.y)] +
						Weights.z * JointMatrices[int(Joints.z)] + Weights.w * JointMatrices[int(Joints.w)];
		gl_Position = Projection*View*Model*SkinMatrix * vec4(VPos, 1.0);
		FWorldPos = vec3(Model*SkinMatrix * vec4(VPos, 1.0));
	}
	else
	{
		gl_Position = Projection*View*Model * vec4(VPos, 1.0);
		FWorldPos = vec3(Model * vec4(VPos, 1.0));
	}

	FColor = VCol;
	FNormal = VNorm;	//TODO: rotate normal
	FTexCoord = VTexCoord;

}

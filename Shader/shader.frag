#version 330 core
in vec3 FColor;
in vec3 FNormal;
in vec3 FWorldPos;
in vec2 FTexCoord;

out vec4 FragColor;

uniform vec3 CameraPos;

uniform sampler2D Texture;
uniform int Characters[256];
uniform int NumCharacters;

uniform sampler2D RenderTexture;
uniform bool RenderToScreen;

struct LightStruct
{
	vec3 Pos;
	vec3 Color;
};
uniform LightStruct Lights[256];
uniform int NumLights;

//Godrays
uniform sampler2D OcclusionTexture;
uniform bool OcclusionPass;
uniform bool FinalPass;
uniform bool Basic;
uniform vec2 ScreenLightPos;

vec3 CalculateLight(LightStruct Light, vec3 MaterialColor)
{
	float Dist = distance(FWorldPos, Light.Pos);
	return clamp(1.0/(Dist*Dist), 0, 1.0)*Light.Color*MaterialColor;
}

void main()
{
	if(RenderToScreen)
	{
		FragColor = vec4(texture(RenderTexture, FTexCoord).rgb, 1);
	}
	else if(FinalPass)	//Godrays
	{
		float decay = 0.968;
		float exposure = 0.6;
		float density = 0.926;
		float weight = 0.1;

		int NUM_SAMPLES = 100;

		vec2 tc = FTexCoord;
		float RayColor = 0;
		if(ScreenLightPos.y > 0.5)
		{
			vec2 deltaTC = tc - ScreenLightPos;
			deltaTC *= 1.0 / float(NUM_SAMPLES) * density;
			float illumdecay = 1.0;
			for(int i = 0; i < NUM_SAMPLES; ++i)
			{
				tc -= deltaTC;
				float col = texture(OcclusionTexture, tc).r*0.4;
				RayColor += col * illumdecay * weight;
				illumdecay *= decay;
			}
		}
		vec3 SceneColor = texture(RenderTexture, FTexCoord).rgb;

		if(distance(FTexCoord, ScreenLightPos) < 0.01)
		{
			FragColor = vec4(1,0,0,1);
		}
		else
		{
			FragColor = vec4(RayColor*vec3(0,0,1)*exposure+SceneColor*1.1, 1);
		}
	}
	else if(OcclusionPass)
	{
		FragColor = vec4(0.0, 0.0, 0.0, 1.0);
	}
	else if(NumCharacters > 0)	//Text rendering
	{
		//321 x 31
		//9x9

		const float ResX = 321.0;
		const float ResY = 31.0;
		const float PaddingX = 1.0/ResX;
		const float PaddingY = 1.0/ResY;

		const float StepX = 10.0/ResX;
		const float StepY = 10.0/ResY;

		const float SizeX = 9.0/ResX;
		const float SizeY = 9.0/ResY;

		int CIndex = int(FTexCoord.x*NumCharacters);
		int Character = Characters[CIndex];

		vec2 CharCoord;
		if(Character < 64)
		{
			CharCoord.x = (Character-32)*StepX + PaddingX;
			CharCoord.y = 2*StepY + PaddingY;
		}
		else if(Character < 96)
		{
			CharCoord.x = (Character-64)*StepX + PaddingX;
			CharCoord.y = 1*StepY + PaddingY;
		}
		else
		{
			CharCoord.x = (Character-96)*StepX + PaddingX;
			CharCoord.y = 0 + PaddingY;
		}

		vec2 Tmp = vec2(FTexCoord.x*NumCharacters - CIndex, FTexCoord.y);
		vec2 Coord = Tmp*vec2(SizeX,SizeY)+CharCoord;
		float Opacity =  texture(Texture, Coord).a;
		if(Opacity < 0.1)
		{
			discard;
		}
		FragColor = vec4(FColor*Opacity, 1);
	}
	else if(Basic)
	{
		FragColor = vec4(FColor, 1.0);
	}
	else	//World rendering
	{
		float Opacity =  texture(Texture, FTexCoord).a;
		if(Opacity < 0.1)
		{
			discard;
		}
		vec3 DiffuseColor = texture(Texture, FTexCoord).rgb;
		vec3 Color = DiffuseColor;
		if(NumLights != 0)
		{
			float DistCamera = distance(CameraPos, FWorldPos);
			Color = DiffuseColor*0.1 + clamp(0.7/(DistCamera*DistCamera), 0, 0.7)*DiffuseColor;
			for(int i = 0; i < NumLights; ++i)
			{
				Color += CalculateLight(Lights[i], DiffuseColor);
			}
		}
		FragColor = vec4(Color, 1.0);
	}
}
